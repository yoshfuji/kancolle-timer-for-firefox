// vim: set ts=8 sw=4 sts=4 ff=dos :

var ShipListView = null;
function SaveShipList(){
    if (!ShipListView)
	return;
    ShipListView.saveShipList();
}

/*
 * Tree
 */
var ShipInfoTree = {
    /* Columns*/
    columns: new Map([
	[ 'fleet',	{ label: '艦隊', flex: 1, }, ],
	[ 'id', 	{ label: 'ID', flex: 1, }, ],
    //  [ 'type', 	{ label: '艦種', flex: 2, }, ],
	[ 'stype', 	{ label: '艦種', flex: 1,
			  sortspecs: [
			      {
				  sortspec: '_stype',
				  label: '艦種',
				  skipdump: true,
			      },
			  ],
			}
	],
	[ 'name', 	{ label: '艦名', flex: 3, always: true,
			  sortspecs: [
			      {
				  sortspec: 'id',
				  label: 'ID',
				  skipdump: true,
			      },
			      {
				  sortspec: '_stype',
				  label: '艦種',
				  skipdump: true,
			      },
			      {
				  sortspec: '_yomi',
				  label: 'ヨミ',
			      },
			  ],
			},
	],
	[ 'lv', 	{ label: 'Lv', flex: 1,
			  subdump: true,
			  sortspecs: [
			      {
				  sortspec: '_lv',
				  label: 'Lv',
			      },
			      {
				  sortspec: '_lvupg',
				  label: '次改装Lv',
			      },
			      {
				  sortspec: '_lvupgremain',
				  label: '次改装Lv残',
			      },
			  ],
			},
	],
	[ 'exp', 	{ label: '経験値', flex: 2,
			  subdump: true,
			  sortspecs: [
			      {
				  sortspec: '_exp',
				  label: '経験値',
			      },
			      {
				  sortspec: '_expnext',
				  label: '次Lv経験値',
			      },
			      {
				  sortspec: '_expnextremain',
				  label: '次Lv経験値残',
			      },
			      {
				  sortspec: '_expupg',
				  label: '次改装Lv経験値',
			      },
			      {
				  sortspec: '_expupgremain',
				  label: '次改装Lv経験値残',
			      },
			  ],
			},
	],
	[ 'hp', 	{ label: 'HP', flex: 1,
			  subdump: true,
			  sortspecs: [
			      {
				  sortspec: '_hp',
				  label: 'HP',
			      },
			      //    {
			      //	sortspec: 'hpratio',
			      //	label: 'HP%',
			      //    },
			      {
				  sortspec: '_maxhp',
				  label: 'MaxHP',
			      },
			  ],
			},
	],
	[ 'karyoku',	{ label: '火力', flex: 1,
			  subdump: true,
			  sortspecs: [
				  {	sortspec: '_karyoku',	    label: '火力',	    },
				  {	sortspec: '_karyokumin',    label: '最小火力',	    },
				  {	sortspec: '_karyokumax',    label: '最大火力',	    },
				  {	sortspec: '_karyokuremain', label: '火力強化余地',  },
				  {	sortspec: '_karyokupowup',  label: '火力強化',	    },
			  ],
			},
	],
	[ 'raisou',	{ label: '雷装', flex: 1,
			  subdump: true,
			  sortspecs: [
			      {	sortspec: '_raisou',	    label: '雷装',	    },
			      {	sortspec: '_raisoumin',    label: '最小雷装',	    },
			      {	sortspec: '_raisoumax',    label: '最大雷装',	    },
			      {	sortspec: '_raisouremain',  label: '雷装強化余地',  },
			      {	sortspec: '_raisoupowup',   label: '雷装強化',	    },
			  ],
			},
	],
	[ 'taiku',	{ label: '対空', flex: 1,
			  subdump: true,
			  sortspecs: [
			      {	sortspec: '_taiku',	    label: '対空',	    },
			      {	sortspec: '_taikumin',	    label: '最小対空',	    },
			      {	sortspec: '_taikumax',	    label: '最大対空',	    },
			      {	sortspec: '_taikuremain',   label: '対空強化余地',  },
			      {	sortspec: '_taikupowup',    label: '対空強化',  },
			  ],
			},
	],
	[ 'soukou',	{ label: '装甲', flex: 1,
			  subdump: true,
			  sortspecs: [
			      {	sortspec: '_soukou',	    label: '装甲',	    },
			      {	sortspec: '_soukoumin',	    label: '最小装甲',	    },
			      {	sortspec: '_soukoumax',	    label: '最大装甲',	    },
			      {	sortspec: '_soukouremain',  label: '装甲強化余地',  },
			      {	sortspec: '_soukoupowup',   label: '装甲強化',  },
			  ],
			},
	],
	[ 'kaihi',	{ label: '回避', flex: 1, }, ],
	[ 'taisen',	{ label: '対潜', flex: 1, }, ],
	[ 'sakuteki',	{ label: '索敵', flex: 1, }, ],
	[ 'soku', 	{ label: '速力', flex: 1, }, ],
	[ 'length',	{ label: '射程', flex: 1, }, ],
	[ 'lucky', 	{ label: '運', flex: 1,
			  subdump: true,
			  sortspecs: [
			      {	sortspec: '_lucky',	    label: '運',	    },
			      {	sortspec: '_luckymax',	    label: '最大運',	    },
			      {	sortspec: '_luckyremain',   label: '運強化余地',    },
			  ],
			},
	],
	[ 'cond', 	{ label: '士気', flex: 1, }, ],
	[ 'ndock', 	{ label: '入渠', flex: 1,
			  subdump: true,
			  sortspecs: [
			      {
				  sortspec: '_ndock',
				  label: '入渠時間',
			      },
			  ],
			},
	],
	[ 'fuel', 	{ label: '燃料', flex: 1,
			  subdump: true,
			  sortspecs: [
			      {	sortspec: '_fuel',	    label: '燃料',	    },
			      {	sortspec: '_fuelmax',	    label: '最大燃料',	    },
			      {	sortspec: '_fuelremain',    label: '不足燃料',      },
			  ],
			},
	],
	[ 'bull', 	{ label: '弾薬', flex: 1,
			  subdump: true,
			  sortspecs: [
			      {	sortspec: '_bull',	    label: '弾薬',	    },
			      {	sortspec: '_bullmax',	    label: '最大弾薬',	    },
			      {	sortspec: '_bullremain',    label: '不足弾薬',      },
			  ],
			},
	],
	[ 'slotitem1', 	{ label: '装備1', flex: 1, }, ],
	[ 'slotitem2', 	{ label: '装備2', flex: 1, }, ],
	[ 'slotitem3', 	{ label: '装備3', flex: 1, }, ],
	[ 'slotitem4', 	{ label: '装備4', flex: 1, }, ],
	[ 'slotitem5', 	{ label: '装備5', flex: 1, }, ],
	[ 'slotitemx1',	{ label: '装備X1', flex: 1, }, ],
    ]),
    /* Sorting */
    sortkey: null,
    sortspec: null,
    sortorder: null,
    _sortold: {},
    sortold: {},
};

var __KanColleTimerShipFilterStypeGroup = [
    { label: '駆逐系',
      types: [
	{ id: 2, }	    //駆逐
      ],
    },
    { label: '軽巡系',
      types: [
	{ id: 3, },	    //軽巡
	{ id: 4, },	    //雷巡
      ],
    },
    { label: '重巡系',
      types: [
	{ id: 5, },	    //重巡
	{ id: 6, },	    //航巡
      ]
    },
    { label: '戦艦系',
      types: [
	{ id: 8, label: '高速戦艦' },
	{ id: 9, },	    //戦艦
	{ id: 10, },    //航空戦艦
      ]
    },
    { label: '空母系',
      types: [
	{ id: 7, },	    //水母
	{ id: 11, },    //軽水母
	{ id: 16, },    //空母
	{ id: 18, },    //装甲空母
      ],
    },
    { label: '潜水艦系',
      types: [
	{ id: 13, },    //潜水艦
	{ id: 14, },    //潜水空母
      ],
    },
    { label: '特務艦',
      types: [
	{ id: 17, },    //揚陸艦
	{ id: 19, },    //工作艦
	{ id: 20, },    //潜水母艦
	{ id: 21, },    //練習巡洋艦
	{ id: 22, },    //補給艦
      ],
    },
];

// Filter by Stype (Ship Class)
function __KanColleStypeFilterTemplate(type2spec){
    let stypes = KanColleDatabase.masterStype.list().reduce(function(p, c) {
	p[c] = KanColleDatabase.masterStype.get(c).api_name;
	return p;
    }, {});
    let shiptypes = KanColleDatabase.ship.list().map(function(e) {
	let ship = KanColleDatabase.ship.get(e);
	if (ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    if (shiptype)
		return shiptype.api_stype;
	}
	return -1;
    }).reduce(function(p, c) {
	p[c] = !p[c] || p[c] + 1;
	return p;
    }, {});
    let stypegroup = __KanColleTimerShipFilterStypeGroup.slice();
    let menu = [];

    if (Object.keys(stypes).length && Object.keys(shiptypes).length) {
	stypegroup.push({
	    label: '未分類',
	    types: Object.keys(stypes).filter(function(e,i,a) {
		    return stypegroup.every(function(f,j,b) {
			return f.types.every(function(g,k,c) {
			    return g.id != e;
			});
		    });
		   }).map(function(e) { return { id: e, log: true, }; }),
	});
	for( let i = 0; i < stypegroup.length; i++ ){
	    let count = 0;
	    let submenu = {
		label: stypegroup[i].label,
		menu: [],
	    };
	    for( let j = 0; j < stypegroup[i].types.length; j++ ){
		let label = stypegroup[i].types[j].label;
		let subsubmenu, _subsubmenu;
		if (!label)
		    label = stypes[stypegroup[i].types[j].id];
		if (!label)
		    label = 'UNKNOWN_' + stypegroup[i].types[j].id;
		// 未所持の艦種はスキップ
		if (!shiptypes[stypegroup[i].types[j].id])
		    continue;

		if (stypegroup[i].types[j].log) {
		    debugprint('未分類' + stypegroup[i].types[j].id +
			       ': ' + label);
		}

		_subsubmenu = type2spec([ stypegroup[i].types[j].id ]);
		subsubmenu = {
		    label: label,
		};
		if (typeof(_subsubmenu) == 'object')
		    subsubmenu.menu = _subsubmenu;
		else
		    subsubmenu.spec = _subsubmenu;
		submenu.menu.push(subsubmenu);
		count++;
	    }
	    if (count > 1) {
		let _subsubmenu = type2spec(stypegroup[i].types.map(function(e) { return e.id; }));
		let subsubmenu = {
		    label: stypegroup[i].label + 'すべて',
		};
		if (typeof(_subsubmenu) == 'object')
		    subsubmenu.menu = _subsubmenu;
		else
		    subsubmenu.spec = _subsubmenu;
		submenu.menu.unshift(subsubmenu);
	    }
	    menu.push(submenu);
	}
    }

    return {
	label: '艦種',
	menu: menu,
    };
}

// Filter by Slotitems
function __KanColleSlotitemFilterTemplate(itemok){
    let menu = [];
    let submenu = null;
    let slotitemowners = KanColleDatabase.slotitem.get(null,'owner') || {};
    let levelkeys = KanColleDatabase.slotitem._levelkeys;

    let itemlist = Object.keys(slotitemowners).sort(function(a,b){
	let type_a = slotitemowners[a].type[2];
	let type_b = slotitemowners[b].type[2];
	let id_a = slotitemowners[a].id;
	let id_b = slotitemowners[b].id;
	let diff = type_a - type_b;
	if (!diff)
	    diff = id_a - id_b;
	return diff;
    });

    for (let i = 0; i < itemlist.length; i++) {
	let k = itemlist[i];
	let itemname = slotitemowners[k].name;
	let itemtype = slotitemowners[k].type[2];
	let itemeqtype = KanColleDatabase.masterSlotitemEquiptype.get(itemtype);
	let itemtypename = itemeqtype.api_name;
	let itemnum = slotitemowners[k].num;
	let itemtotalnum = slotitemowners[k].totalnum;
	let itemmenutitle;
	let itemval = 'slotitem' + k;
	let menu_added = false;

	if (itemok && !itemok[itemtype])
	    continue;

	if (!itemtypename)
	    itemtypename = 'UNKNOWN_' + itemtype;

	/*
	if (!itemname)
	    itemname = KanColleDatabase.masterSlotitem.get(k).api_name;
	*/
	//debugprint(itemname + ': slotitem' + k);

	if (!submenu || lastitemtype != itemtypename) {
	    submenu = {
		label: itemtypename,
		menu: [],
	    };
	    menu.push(submenu);
	    lastitemtype = itemtypename;
	}

	for (let ki = 0; ki < levelkeys.length; ki++) {
	    let levelkey = levelkeys[ki].key;
	    let levellabel = levelkeys[ki].label;
	    let subsubmenu;
	    if (!slotitemowners[k].lv[levelkey] ||
		slotitemowners[k].lv[levelkey].length <= 1) {
		continue;
	    }
	    subsubmenu = [{
				label: itemname + '(' + itemnum + '/' + itemtotalnum + ')',
				spec: itemval,
			      }];
	    for (let j = 0; j < slotitemowners[k].lv[levelkey].length; j++) {
		let itemlvnum;
		let itemlvtotalnum;
		if (!slotitemowners[k].lv[levelkey][j])
		    continue;
		itemlvnum  = slotitemowners[k].lv[levelkey][j].num;
		itemlvtotalnum = slotitemowners[k].lv[levelkey][j].totalnum;
		subsubmenu.push({
				    label: itemname +
					   '[' + levellabel + j + ']' +
					   '(' + itemlvnum + '/' + itemlvtotalnum + ')',
				    spec: itemval + ':' + levelkey + '=' + j,
				});
	    }
	    submenu.menu.push({
				label: itemname + '(' + itemnum + '/' + itemtotalnum + ')',
				menu: subsubmenu,
			      });
	    menu_added = true;
	}
	if (!menu_added) {
	    submenu.menu.push({
				label: itemname + '(' + itemnum + '/' + itemtotalnum + ')',
				spec: itemval,
			      });
	}
    }

    return menu;
}

// The number of sallyarea
function KanColleTimerShipMaxSallyarea() {
    return Math.max.apply(null,
			    KanColleDatabase.ship.list()
				.map(function(e) {
					let ship = KanColleDatabase.ship.get(e);
					return parseInt(ship.api_sally_area || "0", 10);
				})
    );
}

var KanColleShipFilter = [
    {
	name: 'stype',
	get_menu_template: function() {
	    return __KanColleStypeFilterTemplate(function(types) {
						    return 'stype' + types.join('-');
						 });
	},
	regexp: /^stype((\d+-)*\d+)$/,
	get_filter: function(match) {
	    let stypesearch = '-' + match[1] + '-';
	    return function(e) {
		let shiptype;
		let ship = KanColleDatabase.ship.get(e);
		if (!ship)
		    return false;
		shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
		if (!shiptype)
		    return false;
		if (stypesearch.indexOf('-' + shiptype.api_stype + '-') != -1)
		    return true;
		return false;
	    };
	},
    },{
	name: 'slotitem',
	get_menu_template: function() {
	    let submenu = [];
	    submenu.push(__KanColleStypeFilterTemplate(function(types) {
		let hash = types.map(function(e) {
		    let stype = KanColleDatabase.masterStype.get(e);
		    return stype ? stype.api_equip_type : {};
		}).reduce(function(p,c) {
		    for (let k in c) {
			p[k] = p[k] || c[k];
		    }
		    return p;
		}, {});
		//debugprint(hash.toSource());
		return __KanColleSlotitemFilterTemplate(hash);
	    }));
	    submenu.unshift({
		label: '全て',
		menu: __KanColleSlotitemFilterTemplate(null),
	    });
	    return {
		label: '装備',
		menu: submenu,
	    };
	},
	regexp: /^slotitem(\d+)(\:([^=]+)=(\d+))?$/,
	get_filter: function(match) {
	    let slotitemid = match[1];
	    let slotitemkey = match[3];
	    let slotitemlv = match[4];
	    let owners = KanColleDatabase.slotitem.get(slotitemid,'owner');
	    let shiphash = Object.keys(owners ? ((slotitemlv === undefined || slotitemlv == '') ? owners.list : (owners.lv[slotitemkey][slotitemlv] ? owners.lv[slotitemkey][slotitemlv].list : {})) : {}).reduce(function(p,c) {
		p[c] = true;
		return p;
	    }, {});
	    return function(e) {
		return shiphash[e];
	    };
	},
    },{
	name: 'upgrade',
	_menu_template: {
	    label: '近代化',
	    menu: [
		{
		    label: '改修可能',
		    spec: 'upgrade0',
		},{
		    label: '最高改造段階で改修可能',
		    spec: 'upgrade1',
		},
		{
		    label: '運改修可能',
		    spec: 'upgrade2',
		},{
		    label: '最高改造段階で運改修可能',
		    spec: 'upgrade3',
		},
	    ],
	},
	get_menu_template: function() {
	    return this._menu_template;
	},
	regexp: /^upgrade(\d+)$/,
	get_filter: function(match) {
	    const proplists = [ [ 'karyoku', 'raisou', 'taiku', 'soukou' ],
				[ 'lucky' ] ];
	    let param = parseInt(match[1], 10);
	    let upgrade = (param & 1) ? 1 : 0;
	    let proplist = proplists[(param & 2) ? 1 : 0];
	    return function(e) {
		let ship = KanColleDatabase.ship.get(e);
		let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
		if (upgrade && shiptype.api_afterlv)
		    return false;
		for (j = 0; j < proplist.length; j++) {
		    k = proplist[j];
		    let p = getShipProperties(ship,k);
		    if (p && p.remain) {
			return true;
		    }
		}
		return false;
	    };
	},
    },{
	name: 'evolution',
	_menu_template: {
	    label: '改造',
	    menu: [
		{
		    label: '非最高改造段階',
		    spec: 'evolution0',
		},{
		    label: '保護下で非最高改造段階',
		    spec: 'evolution1',
		},
	    ],
	},
	get_menu_template: function() {
	    return this._menu_template;
	},
	regexp: /^evolution(\d+)$/,
	get_filter: function(match) {
	    let locked = parseInt(match[1], 10);
	    return function(e) {
		let ship = KanColleDatabase.ship.get(e);
		let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
		if (!shiptype.api_afterlv)
		    return false;
		if (locked && !ship.api_locked)
		    return false;
		return true;
	    };
	},
    },{
	name: 'misc',
	_menu_template: {
	    label: '状態',
	    menu: [
		{
		    label: '非保護艦',
		    spec: 'locked0',
		},{
		    label: '保護艦',
		    spec: 'locked1',
		},{
		    label: 'キラ無',
		    spec: 'cond0',
		},{
		    label: 'キラ有',
		    spec: 'cond1',
		},
	    ],
	},
	get_menu_template: function() {
	    return this._menu_template;
	},
	regexp: /^(locked|cond)(\d+)$/,
	get_filter: function(match) {
	    if (match[1] == 'locked') {
		let locked = parseInt(match[2], 10);
		//debugprint('locked:' + locked);
		return function(e) {
		    let ship = KanColleDatabase.ship.get(e);
		    //debugprint('ship.api_locked:' + ship.api_locked);
		    if (locked != ship.api_locked)
			return false;
		    return true;
		};
	    } else if (match[1] == 'cond') {
		let cond = parseInt(match[2], 10);
		//debugprint('cond:' + cond);
		return function(e) {
		    let ship = KanColleDatabase.ship.get(e);
		    if (cond == 1 && ship.api_cond < 50)
			return false;
		    if (cond == 0 && ship.api_cond >= 50)
			return false;
		    return true;
		};
	    }
	},
    },{
	name: 'sallyarea',
	get_menu_template: function() {
	    const num_area = KanColleTimerShipMaxSallyarea();
	    let submenu = [];

	    let menu = {
		label: '出撃',
		menu: []
	    };

	    for (let i = 4; i < (1 << (num_area + 1)) - 1; i++) {
		let b = i.toString(2);
		let label, spec;
		let bits = b.split("").map(function(val, idx) {
		    return {
			idx: idx,
			val: val,
		    }
		}).filter(function(val) {
		    return val.val != 0;
		}).map(function(val) {
		    return b.length - val.idx - 1;
		});

		let msb = bits[0];

		// ignore 100...0x entries
		if (i & ~((1 << msb) | 1)) {
		    if (!submenu[msb])
			submenu[msb] = [];
		    submenu[msb].push({
			label: '海域' + bits.join('+'),
			spec: 'sallyarea' + bits.join('-'),
		    });
		}
	    }

	    for (let i = 0; i < num_area + 1; i++) {
		menu.menu[i] = {
		    label: `海域${ i }..`,
		    menu: [],
		};
		// 100...0
		menu.menu[i].menu.push({
		    label: `海域${ i }`,
		    spec: `sallyarea${ i }`,
		});
		// 100...1
		if (i > 0) {
		    menu.menu[i].menu.push({
			label: `海域${ i }+0`,
			spec: `sallyarea${ i }-0`,
		    });
		}
		if (submenu[i] && submenu[i].length) {
		    menu.menu[i].menu.push({
			label: `海域${ i }+`,
			menu: submenu[i],
		    })
		}
	    }

	    return menu;
	},
	regexp: /^sallyarea((\d+-)*\d+)$/,
	get_filter: function(match) {
	    let sareasearch = '-' + match[1] + '-';
	    return function(e) {
		let sarea;
		let ship = KanColleDatabase.ship.get(e);
		if (!ship)
		    return false;
		sarea = ship.api_sally_area;
		if (!sarea)
		    sarea = 0;
		if (sareasearch.indexOf('-' + sarea + '-') != -1)
		    return true;
		return false;
	    };
	},
    },
];

function KanColleBuildFilterMenuList(id){
    let menulist;
    let menupopup;
    let menu;
    let menuitems = [];
    var defaultmenu = null;

    function buildmenuitem(label, value){
	let item = document.createElementNS(XUL_NS, 'menuitem');
	item.setAttribute('label', label);
	if (value)
	    item.setAttribute('value', value);
	item.addEventListener('command', function() { ShipListFilter(this); }, true);
	return item;
    }

    function createmenu(templ) {
	let popup;
	let menu;
	let mlist;

	if (!templ)
	    return;

	//debugprint(templ.toSource());

	if (templ.spec) {
	    let filtermenu = $('shipinfo-filtermenu');
	    if (filtermenu && filtermenu.getAttribute('value') == templ.spec)
		defaultmenu = buildmenuitem(templ.label, templ.spec);
	    return buildmenuitem(templ.label, templ.spec);
	}

	mlist = [];
	popup = document.createElementNS(XUL_NS, 'menupopup');
	menu = document.createElementNS(XUL_NS, 'menu');

	menu.setAttribute('label', templ.label);
	menu.appendChild(popup);

	if (templ.menu) {
	    for (let i = 0; i < templ.menu.length; i++)
		mlist.push(createmenu(templ.menu[i]));
	}
	for( let i = 0; i < mlist.length; i++ )
	    popup.appendChild(mlist[i]);

	if (!mlist.length)
	    menu.setAttribute('disabled', 'true');

	return menu;
    }

    menulist = document.createElementNS(XUL_NS, 'menulist');
    menulist.setAttribute('label', 'XXX');
    menulist.setAttribute('id', id);

    menupopup = document.createElementNS(XUL_NS, 'menupopup');
    menupopup.setAttribute('id', id + '-popup');

    // Default
    menu = buildmenuitem('すべて', null);
    menuitems.push(menu);

    // Build menus
    KanColleShipFilter.forEach(function(e) {
	menu = createmenu(e.get_menu_template());
	if (menu)
	    menuitems.push(menu);
    });

    // Finally build menu
    if (defaultmenu)
	menupopup.appendChild(defaultmenu);
    for (let i = 0; i < menuitems.length; i++)
	menupopup.appendChild(menuitems[i]);

    menulist.appendChild(menupopup);

    return menulist;
}

function ShipListFilter(item){
    debugprint('ShipListFilter(' + item.value + ')');

    $('shipinfo-filtermenu').setAttribute('label', item.getAttribute('label'));
    $('shipinfo-filtermenu').setAttribute('value', item.getAttribute('value'));

    KanColleShipInfoSetView();
}

function KanColleCreateFilterMenuList(box,id)
{
    let oldmenulist = $(id);
    let menulist = KanColleBuildFilterMenuList(id);
    let hbox;

    // Replace existing one or add new one.
    if (oldmenulist) {
	menulist.setAttribute('label', oldmenulist.getAttribute('label') || '');
	menulist.setAttribute('value', oldmenulist.getAttribute('value') || '');
	hbox = oldmenulist.parentNode;
	hbox.replaceChild(menulist, oldmenulist);
    }else {
	hbox = CreateElement('hbox');
	hbox.appendChild(menulist);
	box.appendChild(hbox);
    }
}

/*
 * Ship Filter
 */
// 艦種
var KanColleTimerShipFilterStype = {
    get_list: function(param) {
	let list = [];
	if (!param)
	    param = {};
	//debugprint('** KanColleTimerShipFilterStype(' + param.toSource() + ')');
	if (param._stype) {
	    // List stype
	    let stypemap = param._stype.reduce(function(p,c) {
			    p[c.id] = c;
			    return p;
			   }, {});
	    let shiptypes = KanColleDatabase.ship.list()
			    .map(function(e) {
				    let ship = KanColleDatabase.ship.get(e);
				    if (ship) {
					let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
					if (shiptype)
					    return shiptype.api_stype;
				    }
				    return null;
				 })
			    .reduce(function(p,c) {
				if (c != null && stypemap[c] && !p[c])
				    p[c] = stypemap[c];
				return p;
			    }, {});
	    let shiptype_keys = Object.keys(shiptypes);
	    let shiptype_list;

	    //debugprint('stypemap = ' + stypemap.toSource());
	    //debugprint('shiptypes = ' + shiptypes.toSource());

	    shiptype_keys.sort(function(a,b) { return a - b; });

	    //debugprint('shiptype_keys = ' + shiptype_keys.toSource());

	    shiptype_list = shiptype_keys.map(function(e) {
						let st = KanColleDatabase.masterStype.get(e);
						return {
						    id: [ e ],
						    label: shiptypes[e].label || (st ? st.api_name : ('UNKNOWN_' + e)),
						};
					      });
	    if (shiptype_list.length > 1)
		shiptype_list.unshift({ label: param._label + 'すべて', id: Object.keys(stypemap) });

	    //debugprint('shiptype_list = ' + shiptype_list.toSource());

	    for (let stype of shiptype_list) {
		let entry = {};

		entry.label = stype.label;

		if (param._stype_sublist)
		    entry.list = param._stype_sublist;
		else
		    entry.entry = null;

		entry.param = Object.create(param);
		entry.param.stype = stype.id;
		list.push(entry);
		//debugprint('entry.param = ' + entry.param.toSource());
	    }
	} else {
	    // List stype group
	    let group = __KanColleTimerShipFilterStypeGroup.slice();
	    // 未分類
	    let miscgroup = KanColleDatabase.masterStype.list()
			    .filter(function(e) {
					return group.every(function(f) {
					    return f.types.every(function(g) { return g.id != e; });
					});
				    })
			    .map(function(e) {
				    return { id: e };
			    });
	    if (miscgroup.length)
		group.push({ label: '未分類', types: miscgroup });
	    for (let grp of group) {
		let nparam = Object.create(param);
		nparam._stype = grp.types;
		nparam._label = grp.label;

		list.push({
		    label: grp.label,
		    list: null,
		    param: nparam,
		});
	    }
	}
	return list;
    },
    get_filter: function(param) {
	let stypemap = param.stype.reduce(function(p,c) {
	    p[c] = true;
	    return p;
	}, {});
	let shiptypemap = {};
	return function(e) {
	    let ship = KanColleDatabase.ship.get(e) || {};
	    if (ship && !shiptypemap[ship.api_ship_id]) {
		let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
		shiptypemap[ship.api_ship_id] = stypemap[shiptype.api_stype];
	    }
	    return shiptypemap[ship.api_ship_id];
	};
    },
};

// 装備
var KanColleTimerShipFilterSlotitem = {
    get_list: function(param) {
	if (!param)
	    param = {};
	//debugprint('** KanColleTimerShipFilterSlotitem(' + param.toSource() + ')');
	if (param.itemlv) {
	    let param_itemlv = param.itemlv[0];
	    let slotitemowner = KanColleDatabase.slotitem.get(param.itemid,'owner') || {};
	    let list = null;
	    let slotitemowner_base = isNaN(param_itemlv) ? slotitemowner : slotitemowner.lv.api_level[param_itemlv];
	    if (slotitemowner_base) {
		let items = slotitemowner_base.item;
		let itemalvs = items.reduce(function(p,c) {
						let alv = c.api_alv || 0;
						if (!p[alv])
						    p[alv] = { num: 0, totalnum: 0, };
						if (KanColleDatabase.ship.find_slotitem(c.api_id))
						    p[alv].num++;
						p[alv].totalnum++;
						return p;
					    }, {});
		let alvs = Object.keys(itemalvs).map(function(e) { return e | 0; });
		alvs.sort(function(a,b) { return a - b; });
		list = alvs.map(function(e) {
		    let nparam = Object.create(param);
		    let name = param._item_label + ((alvs.length > 1 || alvs[0]) ? '[+' + e + ']' : '');
		    nparam.itemalv = [ e ];
		    nparam._item_label = name;
		    return {
			label: name + '(' + itemalvs[e].num + '/' + itemalvs[e].totalnum + ')',
			entry: null,
			param: nparam,
		    };
		});
		if (list.length > 1) {
		    let nparam = Object.create(param);
		    let name = param._item_label;
		    nparam.itemalv = [ Number.NaN ];
		    nparam._item_label = name;
		    list.unshift({
			label: name + '(' + slotitemowner_base.num + '/' + slotitemowner_base.totalnum + ')',
			entry: null,
			param: nparam,
		    });
		}
	    }
	    return list;
	} else if (param.itemid) {
	    let slotitemowner = KanColleDatabase.slotitem.get(param.itemid,'owner') || {};
	    let list = [];
	    let lvs = [];
	    for (let lv in slotitemowner.lv.api_level)
		lvs.push(lv | 0);
	    //debugprint(slotitemowner.name + ': lvs = ' + lvs.toSource());
	    for (let lv of lvs) {
		let nparam = Object.create(param);
		let name = slotitemowner.name + ((lvs.length > 1 || lv) ? '[*' + lv + ']' : '');
		nparam.itemlv = [ lv ];
		nparam._item_label = name;
		list.push({
		    label: name + '(' + slotitemowner.lv.api_level[lv].num + '/' + slotitemowner.lv.api_level[lv].totalnum + ')',
		    list: null,
		    param: nparam,
		});
	    }
	    if (lvs.length > 1) {
		let nparam = Object.create(param);
		let name = slotitemowner.name;
		nparam.itemlv = [ Number.NaN ];
		nparam._item_label = name;
		list.unshift({
		    label: name + '(' + slotitemowner.num + '/' + slotitemowner.totalnum + ')',
		    list: null,
		    param: nparam,
		});
	    }
	    return list;
	} else if (param.itemtype) {
	    let slotitemowners = KanColleDatabase.slotitem.get(null,'owner') || {};
	    return Object.keys(slotitemowners)
		    .map(function(e) {
			return slotitemowners[e];
		    })
		    .filter(function(e) {
				return e.type[2] == param.itemtype;
			    })
		    .map(function(e) {
			    let nparam = Object.create(param);
			    nparam.itemid = e.id;
			    return {
				label: e.name + '(' + e.num + '/' + e.totalnum + ')',
				list: null,
				param: nparam,
			    }
			 });
	} else if (param.stype || param._item_all) {
	    let slotitemowners = KanColleDatabase.slotitem.get(null,'owner') || {};
	    let eqtype_ok = param.stype ? param.stype.reduce(function(p,c) {
			    let st = KanColleDatabase.masterStype.get(c);
			    if (st && st.api_equip_type) {
				for (let k in st.api_equip_type) {
				    if (!p[k])
					p[k] = false;
				    if (st.api_equip_type[k])
					p[k] = true;
				}
			    }
			    return p;
			}, {}) : null;
	    let eqtypehash = Object.keys(slotitemowners)
				.reduce(function(p,c) {
					    let type = slotitemowners[c].type[2];
					    if (!eqtype_ok || eqtype_ok[type])
						p[type] = true;
					    return p;
					}, {});
	    let eqtypes = Object.keys(eqtypehash);

	    eqtypes.sort(function(a,b) { return a - b; });

	    return eqtypes.map(function(e) {
		let eqtype = KanColleDatabase.masterSlotitemEquiptype.get(e);
		let eqname = eqtype ? eqtype.api_name : ('UNKNOWN_' + e);
		let nparam = Object.create(param);
		nparam.itemtype = e;
		return {
		    label: eqname,
		    list: null,
		    param: nparam,
		};
	    });
	} else if (param._item_preset > 0) {
	    let preset = KanColleDatabase.presetslot.get(param._item_preset);
	    let sublist = preset.api_slot_item.map(function(e,i) {
		    let nparam = Object.create(param);
		    let item = KanColleDatabase.masterSlotitem.get(e.api_id);
		    let name = item ? item.api_name : `UNKNOWN_${ e }`;
		    let lv_str = e.api_level ? `[*${ e.api_level }]` : '';
		    nparam.itemid = e.api_id;
		    return {
			label: `${ name }${ lv_str }`,
			list: null,
			param: nparam,
		    }
		});
	    if (preset.api_slot_item_ex) {
		let e = preset.api_slot_item_ex;
		let nparam = Object.create(param);
		let item = KanColleDatabase.masterSlotitem.get(e.api_id);
		let name = item ? item.api_name : `UNKNOWN_${ e }`;
		let lv_str = e.api_level ? `[*${ e.api_level }]` : '';
		nparam.itemid = preset.api_slot_item_ex.api_id;
		sublist.push({
		    label: `${ name }${ lv_str }`,
		    list: null,
		    param: nparam,
		});
	    }
	    return sublist;
	} else if (param._item_preset < 0) {
	    return KanColleDatabase.presetslot.list().map(function(e) {
		return KanColleDatabase.presetslot.get(e);
	    }).filter(function(e) {
		return e.api_slot_item.length || e.api_slot_item_ex;
	    }).map(function(e) {
		return {
		    label: `${ e.api_name }装備`,
		    list: null,
		    param: { _item_preset: e.api_preset_no, },
		};
	    });
	} else {
	    // List sub-root
	    return [
		{
		    label: '全て',
		    list: null,
		    param: { _item_all: true, },
		},{
		    label: '艦種',
		    list: KanColleTimerShipFilterStype,
		    param: { _stype_sublist: this, },
		},{
		    label: '装備プリセット',
		    list: null,
		    param: { _item_preset: -1, },
		},
	    ];
	}
    },
    get_filter: function(param) {
	let itemid = param.itemid;
	let itemlv = param.itemlv ? param.itemlv[0] : Number.NaN;
	let itemalv = param.itemalv ? param.itemalv[0] : Number.NaN;
	return function(e) {
	    let ret = false;
	    let ship = KanColleDatabase.ship.get(e);
	    if (!ship)
		return false;
	    let ship_slot = ship.api_slot.slice();
	    if (ship.api_slot_ex)
		ship_slot.push(ship.api_slot_ex);
	    return ship_slot.some(function(f) {
		let item = KanColleDatabase.slotitem.get(f);
		if (!item)
		    return false;
		return item.api_slotitem_id == itemid &&
		       (item.api_level === undefined || isNaN(itemlv) || item.api_level == itemlv) &&
		       (item.api_alv === undefined || isNaN(itemalv) || item.api_alv == itemalv);
	    });
	};
    },
};

// 改修
var KanColleTimerShipFilterUpgrade = {
    _list: [
	{
	    label: '改修可能',
	    entry: null,
	    param: { upgrade: ['karyoku', 'raisou', 'taiku', 'soukou'], },
	},{
	    label: '最高改造段階で改修可能',
	    entry: null,
	    param: { upgrade: ['karyoku', 'raisou', 'taiku', 'soukou'], endmodel: true },
	},{
	    label: '運改修可能',
	    entry: null,
	    param: { upgrade: ['lucky'], },
	},{
	    label: '最高改造段階で運改修可能',
	    entry: null,
	    param: { upgrade: ['lucky'], endmodel: true },
	},
    ],
    get_list: function() {
	return this._list;
    },
    get_filter: function(param) {
	let props = param.upgrade;
	let endmodel = param.endmodel;
	return function(e) {
	    let ship = KanColleDatabase.ship.get(e);
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    return (endmodel === undefined || (shiptype.api_afterlv == 0) == endmodel) &&
		   props.some(function(e) {
				let p = getShipProperties(ship,e);
				return (p && p.remain);
			      });
	};
    },
};

// 素材
var KanColleTimerShipFilterPowerup = {
    _list: [
	{
	    label: '火力強化',
	    entry: null,
	    param: { powerup: 'karyoku' },
	},{
	    label: '火力非強化',
	    entry: null,
	    param: { powerup: 'karyoku', powerup_inv: true },
	},{
	    label: '雷装強化',
	    entry: null,
	    param: { powerup: 'raisou' },
	},{
	    label: '雷装非強化',
	    entry: null,
	    param: { powerup: 'raisou', powerup_inv: true },
	},{
	    label: '対空強化',
	    entry: null,
	    param: { powerup: 'taiku' },
	},{
	    label: '対空非強化',
	    entry: null,
	    param: { powerup: 'taiku', powerup_inv: true },
	},{
	    label: '装甲強化',
	    entry: null,
	    param: { powerup: 'soukou' },
	},{
	    label: '装甲非強化',
	    entry: null,
	    param: { powerup: 'soukou', powerup_inv: true },
	},
    ],
    get_list: function() {
	return this._list;
    },
    get_filter: function(param) {
	let props = param.powerup;
	let inv = !!param.powerup_inv;
	return function(e) {
	    let ship = KanColleDatabase.ship.get(e);
	    let p = getShipProperties(ship, props);
	    return !ship.api_locked && ((p.powup > 0) != inv);
	}
    },
};

// 改造
var KanColleTimerShipFilterEvolution = {
    _list: [
	{
	    label: '非最高改造段階',
	    entry: null,
	    param: {},
	},{
	    label: '保護下で非最高改造段階',
	    entry: null,
	    param: { locked: true, },
	},
    ],
    get_list: function() {
	return this._list;
    },
    get_filter: function(param) {
	let locked = param.locked;
	return function(e) {
	    let ship = KanColleDatabase.ship.get(e);
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    return (locked === undefined || locked == !!ship.api_locked) &&
		   (shiptype.api_afterlv != 0);
	};
    },
};

// 状態
var KanColleTimerShipFilterMisc = {
    _list: [
	{
	    label: '非保護艦',
	    entry: null,
	    param: { locked: false },
	},{
	    label: '保護艦',
	    entry: null,
	    param: { locked: true },
	},{
	    label: 'キラ無',
	    entry: null,
	    param: { cond: false },
	},{
	    label: 'キラ有',
	    entry: null,
	    param: { cond: true },
	},
    ],
    get_list: function() {
	return this._list;
    },
    get_filter: function(param) {
	let locked = param.locked;
	let cond = param.cond;
	return function(e) {
	    let ship = KanColleDatabase.ship.get(e);
	    return (locked === undefined || locked == !!ship.api_locked) &&
		   (cond === undefined || cond == (ship.api_cond >= 50));
	};
    },
};

// 出撃
var KanColleTimerShipFilterSallyarea = {
    get_list: function() {
	const num_area = KanColleTimerShipMaxSallyarea();
	let list = [];

	for (let i = 1; i < (1 << (num_area + 1)) - 1; i++) {
	    let b = i.toString(2);
	    let label, spec;
	    let bits = b.split("").map(function(val, idx) {
		return {
		    idx: idx,
		    val: val,
		}
	    }).filter(function(val) {
		return val.val != 0;
	    }).map(function(val) {
		return b.length - val.idx - 1;
	    });
	    list.push({
		label: '海域' + bits.join('+'),
		entry: null,
		param: { sallyarea: bits },
	    });
	}

	return list;
    },
    get_filter: function(param) {
	let sareatable = param.sallyarea.reduce(function(p,c) {
	    p[c] = true;
	    return p;
	}, {});
	return function(e) {
	    let ship = KanColleDatabase.ship.get(e);
	    if (!ship)
		return false;
	    return sareatable[ship.api_sally_area || 0] || false;
	};
    },
};

var KanColleTimerShipFilterPreset = {
    get_list: function() {
	let decks = KanColleDatabase.preset.list();
	if (!decks.length)
	    return;
	decks.sort(function(a,b) { return a - b; });
	return decks.map(function(e) {
	    let deck = KanColleDatabase.preset.get(e);
	    return {
		label: deck.api_preset_no + ': ' + deck.api_name,
		entry: null,
		param: { preset_deck: deck.api_preset_no | 0, },
	    };
	});
    },
    get_filter: function(param) {
	let preset_deck = param.preset_deck;
	return function(e) {
	    let decks = KanColleDatabase.preset.lookup(e);
	    return decks && decks.indexOf(param.preset_deck) != -1;
	}
    },
};

// Root
var KanColleTimerShipFilterRoot = {
    _dynamic_list: [],
    _list: [
	{
	    label: 'すべて',
	    entry: null,
	},{
	    label: '艦種',
	    list: KanColleTimerShipFilterStype,
	},{
	    label: '装備',
	    list: KanColleTimerShipFilterSlotitem,
	},{
	    label: '近代化',
	    list: KanColleTimerShipFilterUpgrade,
	},{
	    label: '素材',
	    list: KanColleTimerShipFilterPowerup,
	},{
	    label: '改造',
	    list: KanColleTimerShipFilterEvolution,
	},{
	    label: '状態',
	    list: KanColleTimerShipFilterMisc,
	},{
	    label: '出撃',
	    list: KanColleTimerShipFilterSallyarea,
	},{
	    label: 'プリセット',
	    list: KanColleTimerShipFilterPreset,
	},
    ],
    set_menu: function(entry, param) {
	//debugprint('set_menu');
	if (entry != this)
	    this._dynamic_list = [ { label: param._label, entry: entry, param: param, } ];

	if (param._list) {
	    debugprint('sublist is not implemented yet.');
	}
    },
    get_list: function() {
	let that = this;
	return [].concat(that._dynamic_list,
			 [ that._dynamic_list.length ? null : [] ],
			 that._list);
    },
};

// Command handler
function KanColleTimerShipFilterCommandHandler(node, entry, param) {
    this.handleEvent = function(event) {
	return this._handleEvent([node, entry, param], event);
    };
}
KanColleTimerShipFilterCommandHandler.prototype = {
    expandParameter: function(param) {
	let temp = {};

	if (!param)
	    return null;

	while (param && (param instanceof Object)) {
	    for (let k in param) {
		let v = param[k];
		if (!temp[k])
		    temp[k] = v;
	    }
	    param = Object.getPrototypeOf(param);
	}

	return temp;
    },
    _handleEvent: function([node, entry, param], event) {
	let filter;

	if (!param)
	    param = {};

	//debugprint('command: ' + (this.expandParameter(param)).toSource());

	KanColleTimerShipFilterRoot.set_menu(entry, param);

	// Set filter and redraw
	filter = entry.get_filter ? entry.get_filter(param) : null;
	if (!filter)
	    debugprint('null filter');

	KanColleTimerShipFilter.set_filter(filter);
	KanColleShipInfoSetView();

	return 0;
    },
};

// Popup handler
function KanColleTimerShipFilterPopupHandler(node, list, param) {
    this.handleEvent = function(event) {
	return this._handleEvent([node, list, param], event);
    };
}
KanColleTimerShipFilterPopupHandler.prototype = {
    check_children: function(list, param, depth) {
	//debugprint('check_children()');
	let cur = {
	    items: [],
	    list: list,
	    param: param,
	};
	let children = list.get_list(param) || [];
	//debugprint('children = ' + (children ? children.toSource() : 'null'));
	let items = [];
	if (children && children.length) {
	    //debugprint('children.length = ' + children.length);
	    for (let child of children) {
		let item = null;
		if (!child) {
		    item = { leaf: CreateElement('menuseparator'), };
		} else if (child.entry !== undefined) {
		    //debugprint('entry: ' + child.label);
		    let centry = child.entry || list;
		    let cparam = child.param ? Object.create(child.param) : {};
		    let leafitem = CreateElement('menuitem');
		    leafitem.setAttribute('label', child.label);
		    cparam._list = child.list !== undefined ? child.list || list : null;
		    cparam._label = child.label;
		    leafitem.addEventListener('command', new KanColleTimerShipFilterCommandHandler(leafitem, centry, cparam), false);
		    item = { leaf: leafitem };
		} else if (child.list !== undefined) {
		    //debugprint('list: ' + child.label);
		    let clist = child.list || list;
		    let cparam = child.param ? Object.create(child.param) : {};
		    cparam._label = child.label;
		    item = this.check_children(clist, cparam, depth + 1);
		    if (item) {
			item.label = child.label;
			if (depth && item.length > 1)
			    break;
		    }
		}
		if (item) {
		    cur.items.push(item);
		    if (depth && cur.items.length > 1)
			break;
		}
	    }
	}
	//debugprint('items = ' + cur.items.length);
	if (!cur.items.length)
	    return null;
	if (cur.items.length == 1)
	    return cur.items[0];
	return cur;
    },
    _handleEvent: function([node, list, param], event) {
	let count = 0;
	let children;

	if (event.target != node || event.type != 'popupshowing')
	    return;

	if (!param)
	    param = {};

	//debugprint('POPUP list: ' + list.toSource());
	//debugprint('POPUP param: ' + param.toSource());

	while(node.hasChildNodes())
	    node.removeChild(node.childNodes[0]);

	children = this.check_children(list, param, 0);
	if (children) {
	    for (let child of children.items) {
		let item;
		if (child.leaf)
		    item = child.leaf;
		else {
		    let menupopup = CreateElement('menupopup');
		    menupopup.addEventListener('popupshowing', new KanColleTimerShipFilterPopupHandler(menupopup, child.list, child.param), false);
		    item = CreateElement('menu');
		    item.appendChild(menupopup);
		    item.setAttribute('label', child.label);
		}
		node.appendChild(item);
	    }
	}
	return children && children.length > 0;
    },
};

// Ship Filter
var KanColleTimerShipFilter = {
    _filter: null,

    set_filter: function(filter) {
	this._filter = filter;
    },
    get_filter: function() {
	return this._filter;
    },
    // init
    restore: function() {
	let node = $('shipinfo-filter-root');
	if (!node)
	    return;

	node.addEventListener('popupshowing', new KanColleTimerShipFilterPopupHandler(node, KanColleTimerShipFilterRoot, null), false);
    },
};
KanColleTimerShipFilter.__proto__ = __KanColleTimerPanel;

function KanColleSortMenuPopup(that){
    let value = that.value;
    debugprint('KanColleSortMenuPopup(' + value + ')');

    if (value.match(/:/)) {
	let key = RegExp.leftContext;
	let order = RegExp.rightContext;
	let spec = null;

	if (key.match(/@/)) {
	    spec = RegExp.leftContext;
	    key = RegExp.rightContext;
	} else
	    spec = key;

	if (ShipInfoTree.sortkey != key ||
	    ShipInfoTree.sortspec != spec ||
	    ShipInfoTree.sortorder != order) {
	    ShipInfoTree.sortold = ShipInfoTree._sortold;
	} else
	    ShipInfoTree.sortold = {};

	ShipInfoTree.sortkey = key;
	ShipInfoTree.sortspec = spec;
	ShipInfoTree.sortorder = order;

	ShipInfoTreeSort();
    }
}

function KanColleBuildSortMenuPopup(id,key){
    let menupopup;
    let idx;
    let colinfo;
    let sortspecs;

    menupopup  = document.createElementNS(XUL_NS, 'menupopup');
    menupopup.setAttribute('id', id);
    menupopup.setAttribute('position', 'overlap');

    colinfo = ShipInfoTree.columns.get(key);
    sortspecs = colinfo.sortspecs;
    if (!sortspecs)
	sortspecs = [{ sortspec: key, label: colinfo.label, }];

    for (let i = 0; i < sortspecs.length; i++) {
	let ad = [ { val:  1, label: '昇順', },
		   { val: -1, label: '降順', },
	];
	for (let j = 0; j < ad.length; j++) {
	    let menuitem = document.createElementNS(XUL_NS, 'menuitem');

	    //debugprint('key=' + key + ', spec = ' + sortspecs[i].sortspec);

	    menuitem.setAttribute('type', 'radio');
	    menuitem.setAttribute('name', id);
	    menuitem.setAttribute('label', sortspecs[i].label + ad[j].label);
	    menuitem.setAttribute('value', sortspecs[i].sortspec + '@' + key + ':' + ad[j].val);
	    menuitem.addEventListener('command', function() { KanColleSortMenuPopup(this); }, true);
	    menupopup.appendChild(menuitem);
	}
    }

    return menupopup;
}

function KanColleCreateSortMenuPopup(box,id,key)
{
    let oldmenupopup = $(id);
    let menupopup = KanColleBuildSortMenuPopup(id,key);

    // Replace existing one or add new one.
    if (oldmenupopup)
	box.replaceChild(menupopup, oldmenupopup);
    else
	box.appendChild(menupopup);
}

function KanColleCreateShipTree(){
    let tree;
    let oldtree;
    let treecols;
    let treechildren;
    let box;
    let firstcolumn = true;

    //debugprint('KanColleCreateShipTree()');

    // outer box
    box = $('shipinfo-box');

    if (!$('shipinfo-filter-root')) {
	// Build filter menu popup
	KanColleCreateFilterMenuList(box,'shipinfo-filtermenu');
    }

    // Build sort menu
    for (let key of ShipInfoTree.columns.keys())
	KanColleCreateSortMenuPopup(box, 'shipinfo-sortmenu-' + key, key);

    // Treecols
    treecols = document.createElementNS(XUL_NS, 'treecols');
    treecols.setAttribute('context', 'shipinfo-colmenu');
    treecols.setAttribute('id', 'shipinfo-tree-columns');

    // Check selected items and build menu
    for (let [id, colinfo] of ShipInfoTree.columns.entries()) {
	let treecol;
	let node = $('shipinfo-colmenu-' + id);
	let ischecked = node &&
			node.hasAttribute('checked') &&
			node.getAttribute('checked') == 'true';

	treecol = document.createElementNS(XUL_NS, 'treecol');
	treecol.setAttribute('id', 'shipinfo-tree-column-' + id);
	treecol.setAttribute('label', colinfo.label);
	if (colinfo.flex)
	    treecol.setAttribute('flex', colinfo.flex);
	treecol.setAttribute('popup', 'shipinfo-sortmenu-' + id);
	treecol.setAttribute('class', 'sortDirectionIndicator');
	if (ShipInfoTree.sortkey && id == ShipInfoTree.sortkey) {
	    treecol.setAttribute('sortDirection',
				 ShipInfoTree.sortorder > 0 ? 'ascending' : 'descending');
	}
	treecol.setAttribute('hidden', ischecked ? 'false' : 'true');

	if (!firstcolumn) {
	    let splitter = document.createElementNS(XUL_NS, 'splitter');
	    splitter.setAttribute('class', 'tree-splitter');
	    treecols.appendChild(splitter);
	}
	treecols.appendChild(treecol);
	firstcolumn = false;
    }

    // Treechildren
    treechildren = document.createElementNS(XUL_NS, 'treechildren');
    treechildren.setAttribute('id', 'shipinfo-tree-children');

    // Build tree
    tree = document.createElementNS(XUL_NS, 'tree');
    tree.setAttribute('flex', '1');
    tree.setAttribute('hidecolumnpicker', 'true');
    tree.setAttribute('id', 'shipinfo-tree');

    tree.appendChild(treecols);
    tree.appendChild(treechildren);

    // Replace existing tree, or append one.
    oldtree = $('shipinfo-tree');
    if (oldtree)
	box.replaceChild(tree, oldtree);
    else
	box.appendChild(tree);
}

function ShipInfoTreeSort(){
    let order;
    let id;
    let key;
    let dir;

    debugprint('ShipInfoSort()');

    dir = ShipInfoTree.sortorder > 0 ? 'ascending' : 'descending';

    for (let colid of ShipInfoTree.columns.keys()) {
	if (colid == ShipInfoTree.sortkey)
	    $('shipinfo-tree-column-' + colid).setAttribute('sortDirection', dir);
	else
	    $('shipinfo-tree-column-' + colid).removeAttribute('sortDirection');
    }

    //debugprint('key=' + ShipInfoTree.sortkey + ', order=' + ShipInfoTree.sortorder);

    KanColleShipInfoSetView();
}

function DefaultSortFunc(ship_a,ship_b,order){
    let shiptype_a = KanColleDatabase.masterShip.get(ship_a.api_ship_id);
    let shiptype_b = KanColleDatabase.masterShip.get(ship_b.api_ship_id);
    let ret;
    if (shiptype_a === undefined || shiptype_b === undefined)
	return ((shiptype_a !== undefined ? 1 : 0) - (shiptype_b !== undefined ? 1 : 0)) * order;
    ret = shiptype_a.api_stype - shiptype_b.api_stype;
    if (ret)
	return ret;
    //ゲーム内ソートは艦種と艦船の順位づけが逆
    return ship_b.api_sortno - ship_a.api_sortno;
}

function TreeView(){
    var that = this;
    var shiplist;

    key = null;
    spec = null;
    order = null;
    let id;

    let filtermenu;

    var key = ShipInfoTree.sortkey;
    var spec = ShipInfoTree.sortspec;
    var order = ShipInfoTree.sortorder;

    // getCellText function table by column ID
    var shipcellfunc = {
	fleet: function(ship) {
	    let fleet = KanColleDatabase.deck.lookup(ship.api_id);
	    if (fleet)
		return fleet.fleet;
	    return '';
	},
	id: function(ship) {
	    return ship.api_id;
	},
	stype: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    if (!shiptype)
		return -1;
	    return KanColleDatabase.masterStype.get(shiptype.api_stype).api_name;
	},
	name: function(ship) {
	    return FindShipNameByCatId(ship.api_ship_id);
	},
	//_sortno: function(ship) {
	//    return ship.api_sortno;
	//},
	_yomi: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id)
	    if (!shiptype)
		return 0;
	    return shiptype.api_yomi;
	},
	lv: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let nextlv = shiptype ? shiptype.api_afterlv : 0;
	    if (!nextlv)
		nextlv = '-';
	    return ship.api_lv + '/' + nextlv;
	},
	_lv: function(ship) {
	    return ship.api_lv;
	},
	_lvupg: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let nextlv = shiptype ? shiptype.api_afterlv : 0;
	    if (!nextlv)
		nextlv = '';
	    return nextlv;
	},
	_lvupgremain: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let nextlv = shiptype ? shiptype.api_afterlv : 0;
	    if (!nextlv)
		return '';
	    return nextlv - ship.api_lv;
	},
	exp: function(ship) {
	    let nextlvexp = ShipNextLvExp(ship);
	    let nextupgexp = ShipUpgradeableExp(ship);
	    let ship_exp = ShipExp(ship);

	    if (nextlvexp === undefined)
		nextlvexp = '?';
	    else if (nextlvexp == Number.POSITIVE_INFINITY)
		nextlvexp = '-';

	    if (nextupgexp === undefined)
		nextupgexp = '?';
	    else if (nextupgexp === Number.POSITIVE_INFINITY)
		nextupgexp = '-';

	    return ship_exp + '/' + nextlvexp + '/' + nextupgexp;
	},
	_exp: function(ship) {
	    return ShipExp(ship);
	},
	_expnext: function(ship) {
	    let expnext = ShipNextLvExp(ship);
	    if (expnext != Number.POSITIVE_INFINITY)
		return expnext;
	    return '';
	},
	_expnextremain: function(ship) {
	    let expnextremain = ShipNextLvExp(ship) - ShipExp(ship);
	    if (expnextremain != Number.POSITIVE_INFINITY)
		return expnextremain;
	    return '';
	},
	_expupg: function(ship) {
	    let expnext = ShipUpgradeableExp(ship);
	    if (expnext != Number.POSITIVE_INFINITY)
		return expnext;
	    return '';
	},
	_expupgremain: function(ship) {
	    let expnextremain = ShipUpgradeableExp(ship) - ShipExp(ship);
	    if (expnextremain != Number.POSITIVE_INFINITY)
		return expnextremain;
	    return '';
	},
	hp: function(ship) {
	    let info = FindShipStatus(ship.api_id);
	    return info ? info.nowhp + '/' + info.maxhp : '';
	},
	_hp: function(ship) {
	    let info = FindShipStatus(ship.api_id);
	    return info ? info.nowhp : '';
	},
	_maxhp: function(ship) {
	    let info = FindShipStatus(ship.api_id);
	    return info ? info.maxhp : '';
	},
	karyoku:	function(ship) { return getShipProperties(ship,'karyoku').str; },
	_karyoku:	function(ship) { return getShipProperties(ship,'karyoku').cur; },
	_karyokumin:	function(ship) { return getShipProperties(ship,'karyoku').min; },
	_karyokumax:	function(ship) { return getShipProperties(ship,'karyoku').max; },
	_karyokuremain:	function(ship) { return getShipProperties(ship,'karyoku').remain; },
	_karyokupowup:	function(ship) { return getShipProperties(ship,'karyoku').powup; },
	raisou:		function(ship) { return getShipProperties(ship,'raisou').str; },
	_raisou:	function(ship) { return getShipProperties(ship,'raisou').cur; },
	_raisoumin:	function(ship) { return getShipProperties(ship,'raisou').min; },
	_raisoumax:	function(ship) { return getShipProperties(ship,'raisou').max; },
	_raisouremain:	function(ship) { return getShipProperties(ship,'raisou').remain; },
	_raisoupowup:	function(ship) { return getShipProperties(ship,'raisou').powup; },
	taiku:		function(ship) { return getShipProperties(ship,'taiku').str; },
	_taiku:		function(ship) { return getShipProperties(ship,'taiku').cur; },
	_taikumin:	function(ship) { return getShipProperties(ship,'taiku').min; },
	_taikumax:	function(ship) { return getShipProperties(ship,'taiku').max; },
	_taikuremain:	function(ship) { return getShipProperties(ship,'taiku').remain; },
	_taikupowup:	function(ship) { return getShipProperties(ship,'taiku').powup; },
	soukou:		function(ship) { return getShipProperties(ship,'soukou').str; },
	_soukou:	function(ship) { return getShipProperties(ship,'soukou').cur; },
	_soukoumin:	function(ship) { return getShipProperties(ship,'soukou').min; },
	_soukoumax:	function(ship) { return getShipProperties(ship,'soukou').max; },
	_soukouremain:	function(ship) { return getShipProperties(ship,'soukou').remain; },
	_soukoupowup:	function(ship) { return getShipProperties(ship,'soukou').powup; },
	kaihi: function(ship) { return ship.api_kaihi[0]; },
	taisen: function(ship) { return ship.api_taisen[0]; },
	sakuteki: function(ship) { return ship.api_sakuteki[0]; },
	soku: function(ship) { return KanColleDatabase.masterShip.get(ship.api_ship_id).api_soku; },
	length: function(ship) { return ship.api_leng; },
	lucky:		function(ship) { return getShipProperties(ship,'lucky').str; },
	_lucky:		function(ship) { return getShipProperties(ship,'lucky').cur; },
	_luckymax:	function(ship) { return getShipProperties(ship,'lucky').max; },
	_luckyremain:	function(ship) { return getShipProperties(ship,'lucky').remain; },
	ndock: function(ship) {
	    let ndocktime = ship.api_ndock_time;
	    let hour;
	    let min;
	    if (!ndocktime)
		return '-';
	    min = Math.floor(ndocktime / 60000);
	    hour = Math.floor(min / 60);
	    min -= hour * 60;
	    if (min < 10)
		min = '0' + min;
	    return hour + ':' + min;
	},
	_ndock: function(ship) {
	    return ship.api_ndock_time;
	},
	cond: function(ship) {
	    return FindShipCond(ship.api_id);
	},
	fuel: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxfuel = shiptype ? shiptype.api_fuel_max : 0;
	    if (!maxfuel)
		maxfuel = '-';
	    return ship.api_fuel + '/' + maxfuel;
	},
	_fuel: function(ship) {
	    return ship.api_fuel;
	},
	_fuelmax: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxfuel = shiptype ? shiptype.api_fuel_max : 0;
	    if (!maxfuel)
		maxfuel = '-';
	    return maxfuel;
	},
	_fuelremain: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxfuel = shiptype ? shiptype.api_fuel_max : 0;
	    if (!maxfuel)
		maxfuel = '-';
	    return maxfuel - ship.api_fuel;
	},
	bull: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxbull = shiptype ? shiptype.api_bull_max : 0;
	    if (!maxbull)
		maxbull = '-';
	    return ship.api_bull + '/' + maxbull;
	},
	_bull: function(ship) {
	    return ship.api_bull;
	},
	_bullmax: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxbull = shiptype ? shiptype.api_bull_max : 0;
	    if (!maxbull)
		maxbull = '-';
	    return maxbull;
	},
	_bullremain: function(ship) {
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    let maxbull = shiptype ? shiptype.api_bull_max : 0;
	    if (!maxbull)
		maxbull = '-';
	    return maxbull - ship.api_bull;
	},
	slotitem1: function(ship) {
	    return getShipSlotitem(ship,1);
	},
	slotitem2: function(ship) {
	    return getShipSlotitem(ship,2);
	},
	slotitem3: function(ship) {
	    return getShipSlotitem(ship,3);
	},
	slotitem4: function(ship) {
	    return getShipSlotitem(ship,4);
	},
	slotitem5: function(ship) {
	    return getShipSlotitem(ship,5);
	},
	slotitemx1: function(ship) {
	    return getShipSlotitem(ship,ship.api_slot.length + 1);
	},
    };

    var shippropfunc = {
	'karyoku': function(ship) {
	    let props = [];
	    if (getShipProperties(ship,'karyoku').remain == 0)
		props.push('shipinfo-tree-cell-karyoku-full');
	    return props.join(' ');
	},
	'raisou': function(ship) {
	    let props = [];
	    if (getShipProperties(ship,'raisou').remain == 0)
		props.push('shipinfo-tree-cell-raisou-full');
	    return props.join(' ');
	},
	'taiku': function(ship) {
	    let props = [];
	    if (getShipProperties(ship,'taiku').remain == 0)
		props.push('shipinfo-tree-cell-taiku-full');
	    return props.join(' ');
	},
	'soukou': function(ship) {
	    let props = [];
	    if (getShipProperties(ship,'soukou').remain == 0)
		props.push('shipinfo-tree-cell-soukou-full');
	    return props.join(' ');
	},
	'*': function(ship) {
	    let ship_info;
	    let props = [];

	    ship_info = FindShipStatus(ship.api_id);
	    if (ship_info && ship_info.nowhp >= ship_info.maxhp) {
		props.push('shipinfo-tree-row-hp-ok');
	    }
	    if (ship_info &&
		ship_info.bull >= ship_info.bull_max &&
		ship_info.fuel >= ship_info.fuel_max) {
		props.push('shipinfo-tree-row-fill-ok');
	    }

	    ship_cond_str = 'none';
	    if (ship.api_cond >= 50) {
		ship_cond_str = 'good';
	    } else if (ship.api_cond == 49) {
		ship_cond_str = 'normal';
	    } else if (ship.api_cond >= 40) {
		ship_cond_str = 'hidden';
	    } else if (ship.api_cond >= 30) {
		ship_cond_str = 'tired';
	    } else if (ship.api_cond >= 20) {
		ship_cond_str = 'orange';
	    } else if (ship.api_cond >= 0) {
		ship_cond_str = 'red';
	    }
	    props.push('shipinfo-tree-row-cond-' + ship_cond_str);

	    if (ship.api_sally_area && parseInt(ship.api_sally_area, 10) != Number.NaN && ship.api_sally_area > 0)
		props.push('shipinfo-tree-row-sallyarea-' + ship.api_sally_area);

	    return props.join(' ');
	},
	'+': function(ship) {
	    let props = [];

	    if (KanColleDatabase.ndock.find(ship.api_id))
		props.push('shipinfo-tree-row-ndock-repairing');

	    return props.join(' ');
	},
    };

    // Ship list
    shiplist = KanColleDatabase.ship.list().slice();

    //
    // Sort ship list
    //
    // default comparison function
    var objcmp = function(a,b) {
	if (a > b)
	    return 1;
	else if (a < b)
	    return -1;
	return 0;
    };

    // special comparision function: each function takes two 'ship's
    var shipcmpfunc = {
	fleet: function(ship_a,ship_b,defres){
	    let fleet_a = KanColleDatabase.deck.lookup(ship_a.api_id);
	    let fleet_b = KanColleDatabase.deck.lookup(ship_b.api_id);
	    let ret;
	    if (!fleet_a || !fleet_b)
		return ((fleet_b ? 1 : 0) - (fleet_a ? 1 : 0)) * order;
	    if (!fleet_a.fleet || !fleet_b.fleet)
		return ((fleet_b.fleet ? 1 : 0) - (fleet_a.fleet ? 1 : 0)) * order;
	    ret = fleet_a.fleet - fleet_b.fleet;
	    if (ret)
		return ret;
	    ret = (fleet_a.pos - fleet_b.pos) * order;
	    if (ret)
		return ret;
	    if (defres)
		return defres;
	    return DefaultSortFunc(ship_b,ship_a,order);
	},
	_stype: function(ship_a,ship_b,defres){
	    let shiptype_a = KanColleDatabase.masterShip.get(ship_a.api_ship_id);
	    let shiptype_b = KanColleDatabase.masterShip.get(ship_b.api_ship_id);
	    let ret;
	    if (shiptype_a === undefined || shiptype_b === undefined)
		return ((shiptype_a !== undefined ? 1 : 0) - (shiptype_b !== undefined ? 1 : 0)) * order;
	    ret = shiptype_a.api_stype - shiptype_b.api_stype;
	    if (ret)
		return ret;
	    if (defres)
		return defres;
	    return DefaultSortFunc(ship_a,ship_b,order);
	},
	_lv: function(ship_a,ship_b,defres){
	    let ret = ship_a.api_lv - ship_b.api_lv;
	    if (!ret)
		ret = defres;
	    if (!ret)
		ret = ship_b.api_sortno - ship_a.api_sortno;
	    return ret;
	},
	_lvupg: function(ship_a,ship_b,defres){
	    let shiptype_a = KanColleDatabase.masterShip.get(ship_a.api_ship_id);
	    let lv_a = shiptype_a ? shiptype_a.api_afterlv : 0;
	    let shiptype_b = KanColleDatabase.masterShip.get(ship_b.api_ship_id);
	    let lv_b = shiptype_b ? shiptype_b.api_afterlv : 0;
	    let ret;
	    if (!lv_a)
		lv_a = Number.POSITIVE_INFINITY;
	    if (!lv_b)
		lv_b = Number.POSITIVE_INFINITY;
	    if (lv_a == Number.POSITIVE_INFINITY &&
		lv_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = lv_a - lv_b;
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_lvupgremain: function(ship_a,ship_b,defres){
	    let shiptype_a = KanColleDatabase.masterShip.get(ship_a.api_ship_id);
	    let lv_a = shiptype_a ? shiptype_a.api_afterlv : 0;
	    let shiptype_b = KanColleDatabase.masterShip.get(ship_b.api_ship_id);
	    let lv_b = shiptype_b ? shiptype_b.api_afterlv : 0;
	    let ret;
	    if (!lv_a)
		lv_a = Number.POSITIVE_INFINITY;
	    if (!lv_b)
		lv_b = Number.POSITIVE_INFINITY;
	    if (lv_a == Number.POSITIVE_INFINITY &&
		lv_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = (lv_a - ship_a.api_lv) - (lv_b - ship_b.api_lv);
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_expnext: function(ship_a,ship_b,defres) {
	    let nextexp_a = ShipNextLvExp(ship_a);
	    let nextexp_b = ShipNextLvExp(ship_b);
	    let ret;
	    if (nextexp_a === undefined)
		nextexp_a = Number.POSITIVE_INFINITY;
	    if (nextexp_b === undefined)
		nextexp_b = Number.POSITIVE_INFINITY;
	    if (nextexp_a == Number.POSITIVE_INFINITY &&
	        nextexp_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = nextexp_a - nextexp_b;
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_expnextremain: function(ship_a,ship_b,defres) {
	    let nextexp_a = ShipNextLvExp(ship_a);
	    let nextexp_b = ShipNextLvExp(ship_b);
	    let ret;
	    if (nextexp_a === undefined)
		nextexp_a = Number.POSITIVE_INFINITY;
	    if (nextexp_b === undefined)
		nextexp_b = Number.POSITIVE_INFINITY;
	    if (nextexp_a == Number.POSITIVE_INFINITY &&
	        nextexp_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = (nextexp_a - ShipExp(ship_a)) - (nextexp_b - ShipExp(ship_b));
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_expupg: function(ship_a,ship_b,defres) {
	    let nextexp_a = ShipUpgradeableExp(ship_a);
	    let nextexp_b = ShipUpgradeableExp(ship_b);
	    let ret;
	    if (nextexp_a === undefined)
		nextexp_a = Number.POSITIVE_INFINITY;
	    if (nextexp_b === undefined)
		nextexp_b = Number.POSITIVE_INFINITY;
	    if (nextexp_a == Number.POSITIVE_INFINITY &&
	        nextexp_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = nextexp_a - nextexp_b;
	    if (!ret)
		ret = defres;
	    return ret;
	},
	_expupgremain: function(ship_a,ship_b,defres) {
	    let nextexp_a = ShipUpgradeableExp(ship_a);
	    let nextexp_b = ShipUpgradeableExp(ship_b);
	    let ret;
	    if (nextexp_a === undefined)
		nextexp_a = Number.POSITIVE_INFINITY;
	    if (nextexp_b === undefined)
		nextexp_b = Number.POSITIVE_INFINITY;
	    if (nextexp_a == Number.POSITIVE_INFINITY &&
	        nextexp_b == Number.POSITIVE_INFINITY)
		ret = 0;
	    else
		ret = (nextexp_a -= ShipExp(ship_a)) - (nextexp_b -= ShipExp(ship_b));
	    if (!ret)
		ret = defres;
	    return ret;
	},
    };

    // default
    if (key === undefined)
	key = 'id';
    if (spec === undefined)
	spec = key;
    if (order === undefined)
	order = 1;

    shiplist = shiplist.sort(function(a, b) {
	let res = 0;

	let ship_a = KanColleDatabase.ship.get(a);
	let ship_b = KanColleDatabase.ship.get(b);
	let old_a = ShipInfoTree.sortold[a];
	let old_b = ShipInfoTree.sortold[b];
	let oldres;

	if (old_a === undefined || old_b === undefined)
	    oldres = (old_a !== undefined ? 1 : 0) - (old_b !== undefined ? 1 : 0);
	else
	    oldres = objcmp(old_a, old_b);
	oldres *= order;

	if (!ship_a || !ship_b)
	    res = (ship_a ? 1 : 0) - (ship_b ? 1 : 0);
	else if (shipcmpfunc[spec] !== undefined)
	    res = shipcmpfunc[spec](ship_a,ship_b,oldres);
	else if (shipcellfunc[spec] !== undefined) {
	    let va = shipcellfunc[spec](ship_a);
	    let vb = shipcellfunc[spec](ship_b);
	    res = objcmp(va,vb);
	}

	if (!res)
	    res = oldres;

	return res * order;
    });

    ShipInfoTree._sortold = shiplist.map(function(e,i) {
	return { idx: i, key: e, };
    }).reduce(function(p, c) {
	p[c.key] = c.idx;
	return p;
    }, {});

    filtermenu = $('shipinfo-filtermenu');
    if (filtermenu) {
	let filterspec = filtermenu.getAttribute('value');
	if (filterspec) {
	    let filter = null;
	    KanColleShipFilter.forEach(function(e) {
		let m;
		if (filter)
		    return;
		m = e.regexp.exec(filterspec);
		if (!m)
		    return;
		filter = e.get_filter(m);
	    });

	    if (filter) {
		shiplist = shiplist.filter(filter);
	    } else {
		debugprint('invalid filterspec "' + filterspec + '"; ignored');
	    }
	}
    } else {
	let filter = KanColleTimerShipFilter.get_filter();
	if (filter)
	    shiplist = shiplist.filter(filter);
    }

    // our local interface
    this.saveShipList = function(){
	const nsIFilePicker = Components.interfaces.nsIFilePicker;
	let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
	let rv;
	let cos;

	fp.init(window, "艦船リストの保存...", MODE_SAVE);
	fp.defaultExtension = 'txt';
	fp.appendFilter("テキストCSV","*.csv; *.txt");
	fp.appendFilters(nsIFilePicker.filterText);
	fp.appendFilters(nsIFilePicker.filterAll);
	rv = fp.show();

	if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace)
	    cos = KanColleTimerUtils.file.openWriter(fp.file, 0, 0);
	else
	    return;

	// ZERO-WIDTH NO-BREAK SPACE (used as BOM)
	// とある表計算ソフトでは、UTF-8な.csvファイルにはこれがないと
	// "文字化け"する。一方、.txtなら問題ない。
	//cos.writeString('\ufeff');
	for (let i = -1; i < shiplist.length; i++) {
	    let a = [];
	    let ship = KanColleDatabase.ship.get(shiplist[i]);
	    for (let [id, colinfo] of ShipInfoTree.columns.entries()) {
		if (!colinfo.subdump) {
		    let val;
		    if (i < 0)
			val = colinfo.label;
		    else
			val = '' + shipcellfunc[id](ship);
		    a.push(val.replace(/,/g,'_').replace(/"/g,'_'));
		}
		if (!colinfo.sortspecs)
		    continue;
		for (let k = 0; k < colinfo.sortspecs.length; k++) {
		    let val;
		    if (colinfo.sortspecs[k].skipdump)
			continue;
		    if (!shipcellfunc[colinfo.sortspecs[k].sortspec]) {
			a.push('<'+colinfo.sortspecs[k].sortspec+'>');
			continue;
		    }
		    if (i < 0)
			val = colinfo.sortspecs[k].label;
		    else
			val = '' + shipcellfunc[colinfo.sortspecs[k].sortspec](ship);
		    val = val.replace(/[\r\n]+/, ' ');
		    a.push(val.match(/[",]/) ? '"' + val.replace(/"/g,'""') + '"' : val);
		}
	    }
	    cos.writeString(a.join(',')+'\n');
	}
	cos.close();
    };

    //
    // the nsITreeView object interface
    //
    this.rowCount = shiplist.length;
    this.getCellText = function(row,column){
	let colid = column.id.replace(/^shipinfo-tree-column-/, '');
	let ship;
	let func;

	if (row >= this.rowCount)
	    return 'N/A';

	ship = KanColleDatabase.ship.get(shiplist[row]);
	if (!ship)
	    return 'N/A';

	func = shipcellfunc[colid];
	if (func)
	    ret = func(ship);
	else
	    ret = colid + '_' + row;
	return ret;
    };
    this.getColumnProperties = function(col) {};
    this.setTree = function(treebox){ this.treebox = treebox; };
    this.isContainer = function(row){ return false; };
    this.isSeparator = function(row){ return false; };
    this.isSorted = function(){ return false; };
    this.getLevel = function(row){ return 0; };
    this.getImageSrc = function(row,col){ return null; };
    this.getRowProperties = function(row,props){
	let ship;
	let func;
	if (row >= this.rowCount)
	    return '';
	ship = KanColleDatabase.ship.get(shiplist[row]);
	if (!ship)
	    return;
	func = shippropfunc['*'];
	return func(ship);
    };
    this.getCellProperties = function(row,column,props){
	let colid = column.id.replace(/^shipinfo-tree-column-/, '');
	let ship;
	let func;
	let prop;

	if (row >= this.rowCount)
	    return prop;
	ship = KanColleDatabase.ship.get(shiplist[row]);
	if (!ship)
	    return column.id;

	func = shippropfunc['+'];
	prop = func(ship);

	func = shippropfunc[colid];
	if (func)
	    return column.id + ' ' + prop + ' ' + func(ship);
	return column.id + ' ' + prop;
    };
    this.getColumnProperties = function(col,props){};
    this.cycleHeader = function(col,elem){};
};

function KanColleShipInfoSetView(){
    let menu = $('saveshiplist-menu');
    //debugprint('KanColleShipInfoSetView()');
    ShipListView = new TreeView();
    if (menu)
	menu.setAttribute('disabled', 'false');
    $('shipinfo-tree').view = ShipListView;
}

function ShipInfoTreeMenuPopup(){
    //debugprint('ShipInfoTreeMenuPopup()');
    KanColleCreateShipTree();
    KanColleShipInfoSetView();
}

function KanColleTimerShipTableShow() {
    let toggle = $('shipinfo-toggle');
    if (toggle) {
	let checked = toggle.getAttribute('checked') == 'true';
	$('shipinfo-box').collapsed = !checked;
    }
}

function KanColleTimerShipInfoHandler(){
    KanColleCreateShipTree();
    KanColleShipInfoSetView();
}

var KanColleTimerShipTable = {
    update: {
	masterSlotitem: 'ship',
	masterShip: 'ship',
	ship: function() {
	    KanColleCreateShipTree();
	    KanColleShipInfoSetView();
	},
	slotitem: 'ship',
    },
    restore: function() {
	KanColleCreateShipTree();
	KanColleShipInfoSetView();
	KanColleTimerShipTableShow();
    },
};
KanColleTimerShipTable.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerShipFilter.load(); }, false);
window.addEventListener('load', function(e) { KanColleTimerShipTable.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerShipTable.unload(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerShipFilter.unload(); }, false);
