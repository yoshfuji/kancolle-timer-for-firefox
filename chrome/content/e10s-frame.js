/* -*- mode: js2;-*- */
// vim: set ts=8 sw=4 sts=4 ff=dos :

/*
 * KanColleTimer E10S Support - Frame Script
 *
 * Author:
 *  YOSHIFUJI Hideaki <hideaki@yoshifuji.org>, 2016.
 */

Components.utils.import("resource://gre/modules/Promise.jsm");
Components.utils.import("chrome://kancolletimer/content/utils.jsm");

function E10SDEBUG(msg) {
    //KanColleTimerUtils.console.log('[E10SFRAME] ' + msg);
}

var KanColleTimerE10SFrame = {
    _req_state: new Map,

    _createCanvas: function(win) {
	return win.document.createElementNS('http://www.w3.org/1999/xhtml', 'canvas');
    },
    _drawCanvas: function(canvas, size, region, masks) {
	let ctx;
	let scale = { x: 1.0, y: 1.0, };

	if (!canvas)
	    throw new TypeError('canvas not defined.');
	if (!region)
	    throw new TypeError('region not defined.');

	if (!size)
	    size = {};
	if (!size.x)
	    size.x = region.w;
	if (!size.y)
	    size.y = region.h;

	scale.x = size.x / region.w;
	scale.y = size.y / region.h;

	canvas.style.display = 'inline';
	canvas.width = size.x;
	canvas.height = size.y;

	ctx = canvas.getContext('2d');
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	ctx.save();
	ctx.scale(scale.x, scale.y);

	// x,y,w,h
	ctx.drawWindow(region.win, region.x, region.y, region.w, region.h, 'rgb(255,255,255)');

	if (masks) {
	    for (let i = 0; i < masks.length; i++) {
		let mask = masks[i];
		ctx.fillStyle = KanColleTimerUtils.color.rgb(mask);
		ctx.fillRect(mask.x, mask.y, mask.w, mask.h);
	    }
	}
	ctx.restore();

	return canvas;
    },
    _get_kancolle_region: function() {
	let region = null;
	try {
	    let win = content;
	    let game_frame = win.document.getElementById('game_frame');
	    let rect = game_frame.getBoundingClientRect();
	    let flash = game_frame.contentWindow.document.getElementById('flashWrap');

	    region = {
		win: win,
		w: flash.clientWidth,
		h: flash.clientHeight,
		x: rect.left + win.pageXOffset + flash.offsetLeft,
		y: rect.top + win.pageYOffset + flash.offsetTop,
	    };
	} catch(x) {
	}
	return region;
    },
    _init_kancolle_screenshot: function(h, d) {
	let canvas;
	let region;
	let masks = [];

	if (d.privacy) {
	    masks.push({
			r: 0, g: 0, b: 0,
			x: 110, y: 5, w: 145, h: 20,
		       });
	}

	region = this._get_kancolle_region();
	if (!region)
	    throw new Error('Region not found.');
	canvas = this._createCanvas(region.win);

	return Promise.resolve({
	    h: h,
	    canvas: canvas,
	    size: d.size,
	    region: region,
	    masks: masks,
	    format: d.format || [],
	    framerate: d.framerate || 0,
	});
    },
    /*
     * Message Handler
     */
    _message_handlers: new Map(
	[
	    [
		KanColleUtils.URL,
		{
		    screenshot: function(h, d) {
			let that = this;
			this._init_kancolle_screenshot(h, d)
			    .then(function(state) {
				let now = Date.now();
				that._drawCanvas(state.canvas, state.size, state.region, state.masks);
				that._reply(state.h, {
				    data: {
					url: state.canvas.toDataURL.apply(state.canvas, state.format),
					timestamp: now,
				    },
				    error: null,
				});
			    })
			    .catch(function(e) {
				KanColleTimerUtils.console.log('[FRAME] screenshot error: ' + e);
				that._reply(h, {
				    data: null,
				    error: '' + e,
				});
			    });
		    },
		    multi_screenshot: function(h, d) {
			let that = this;
			let state = this._req_state.get(h.seq);
			if (d.cmd == 'start') {
			    if (state) {
				this._reply(h, { data: null, error: 'duplicate request.', });
				return;
			    }
			    this._req_state.set(h.seq, {}); //lock
			    this._init_kancolle_screenshot(h, d)
				.catch(function(e) {
				    that._req_state.delete(h.seq);
				    that._reply(h, { data: null, error: '' + e, });
				})
				.then(function(state) {
				    if (!state)
					return;
				    if (state.framerate)
					state._interval = 1000 / state.framerate;
				    state.frames = [];
				    that._req_state.set(h.seq, state);

				    state.timer = KanColleTimerUtils.timer.startRepeatingEvent(that, state._interval, state);
				});
			} else if (d.cmd == 'stop') {
			    if (!state) {
				this._reply(h, { data: null, error: 'unknown request.', });
				return;
			    }
			    if (state.timer) {
				KanColleTimerUtils.timer.stop(state.timer);
				state.timer = null;
			    }
			    this._reply(h, { data: { cmd: 'stop' }});
			} else if (d.cmd == 'get') {
			    let rep = {
				'cmd': 'get',
			    };
			    let frame;
			    if (!state) {
				this._reply(h, { data: null, error: 'unknown request.', });
				return;
			    }
			    frame = state.frames[d.frame_no];
			    rep.frame_no = d.frame_no;
			    if (frame) {
				rep.timestamp = frame.timestamp;
				if (frame.canvas)
				    rep.url = frame.canvas.toDataURL.apply(frame.canvas, state.format);
			    }
			    this._reply(h, { data: rep, error: null, });
			} else if (d.cmd == 'clear') {
			    if (!state) {
				this._reply(h, { data: null, error: 'unknown request.', });
				return;
			    }
			    if (state.timer) {
				clearTimeout(state.timer);
				state.timer = null;
			    }
			    this._req_state.delete(h.seq);
			    this._reply(h, { data: { cmd: 'clear' }});
			} else {
			    this._reply(h, { data: null, error: 'command not found.', });
			    return;
			}
		    },
		}
	    ],
	]
    ),
    _current_message_handler: {},
    _classify_message_handler: function(url) {
	for (let [key,entry] of this._message_handlers.entries()) {
	     if (url.indexOf(key) != -1)
		return entry;
	}
	return {};
    },

    receiveMessage: function(message) {
	let d = message.data;
	let n = d.$header.name;

	if (message.name.replace(/^[^:]+:/, '') != n)
	    return;

	//KanColleTimerUtils.console.log('[FRAME] recv: '+ JSON.stringify(d));

	if (!this._current_message_handler[n])
	    return;
	this._current_message_handler[n].call(this, d.$header, d.$body);
    },
    _reply: function(h, r) {
	let name = 'kancolletimer-kai@st-paulia.net:' + h.name;
	//KanColleTimerUtils.console.log('[FRAME] reply:' + name + ', message = ' + JSON.stringify(r));
	sendAsyncMessage(name + '#' + h.seq,
			 {
			    $header: h,
			    $body: r || null,
			 }
			);
    },

    _register_message_handlers: function() {
	//KanColleTimerUtils.console.log('[FRAME] URL = ' + content.location.href);

	found = this._classify_message_handler(content.location.href);
	for (let k in this._current_message_handler) {
	    if (found[k] && found[k] == this._current_message_handler[k])
		continue;
	    //KanColleTimerUtils.console.log('[FRAME] remove message handler: ' + k);
	    removeMessageListener('kancolletimer-kai@st-paulia.net:' + k, this);
	}
	for (let k in found) {
	    if (found[k] == this._current_message_handler[k])
		continue;
	    //KanColleTimerUtils.console.log('[FRAME] add message handler: ' + k);
	    addMessageListener('kancolletimer-kai@st-paulia.net:' + k, this);
	}
	this._current_message_handler = found;
    },

    /*
     * Event handler
     */
    handleEvent: function(event) {
	let found;
	let doc = event.originalTarget;

	switch (event.type) {
	case 'DOMContentLoaded':
	    if (doc.nodeName != "#document")
		break;
	    this._register_message_handlers();
	    break;
	default:;
	}
    },
    observe: function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    let state = data;
	    if (!state.abort) {
		let canvas = this._createCanvas(state.region.win);
		let now = Date.now();
		this._drawCanvas(canvas, state.size, state.region, state.masks);
		state.frames.push({
				    canvas: canvas,
				    timestamp: now,
				  });
		this._reply(state.h,
			    {
				data: { timestamp: now, },
				error: null,
			    });
	    }
	    break;
	default:;
	}
    },

    /*
     * Initializer/Finalizer
     */
    init: function() {
	this._register_message_handlers();
	addEventListener("DOMContentLoaded", this);
    },
    exit: function() {
    },
};

KanColleTimerE10SFrame.init();

