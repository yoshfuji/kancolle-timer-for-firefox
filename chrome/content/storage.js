// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("chrome://kancolletimer/content/utils.jsm");

var Storage = {
    dirname: "kancolletimer.dat",

    /**
     * ファイルに書き込む
     * @param k ファイル名(key)
     * @param v オブジェクト(value)
     */
    writeObject: function( k, v ){
	KanColleUtils.writeObject(k, v);
    },

    /**
     * ファイルから読み込む
     * @param k ファイル名(key)
     * @param defvalue デフォルト値
     */
    readObject: function( k, defvalue ){
	return KanColleUtils.readObject(k, defvalue);
    },

    /**
     * プロフィールのディレクトリを返す.
     * @return プロフィールディレクトリへのnsIFileを返す
     */
    getProfileDir: function(){
	return KanColleTimerUtils.file.profileDir();
    },

    /**
     * データの保存先を返す
     * @return データ保存先をnsIFileで返す
     */
    getDefaultSaveDir: function(){
	let profdir = this.getProfileDir();
	profdir.append( this.dirname );
	return profdir;
    },

    getFilePath: function(f) {
	return KanColleUtils.getDataFile(f);
    },

    getSaveDir: function() {
	return KanColleUtils.getDataDir();
    },

    /**
     * データ保存ディレクトリを作成する.
     */
    createSaveDir: function(){
	let profdir = this.getSaveDir();
	KanColleTimerUtils.file.createDirectory(profdir, 0);
    },

    init: function(){
	this.createSaveDir();
    }
};

Storage.init();
