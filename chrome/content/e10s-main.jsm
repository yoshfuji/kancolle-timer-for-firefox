/* -*- mode: js2;-*- */
// vim: set ts=8 sw=4 sts=4 ff=dos :

/*
 * KanColleTimer E10S Support
 * e10s-main provides e10s support framework for chrome script
 *
 * Author:
 *  YOSHIFUJI Hideaki <hideaki@yoshifuji.org>, 2016.
 */

var EXPORTED_SYMBOLS = ["KanColleTimerE10S"];

Components.utils.import("chrome://kancolletimer/content/utils.jsm");

var globalMM = Components.classes["@mozilla.org/globalmessagemanager;1"]
		.getService(Components.interfaces.nsIMessageListenerManager);

const ID = 'kancolletimer-kai@st-paulia.net';
const SEQ_MASK = 0x7fffffff;
const SEQ_NUM = 0x80000000;

var seq = (Date.now() ^ (Math.random() * SEQ_NUM)) & SEQ_MASK;
var sessions = new Map;

function E10SDEBUG(msg) {
    //KanColleTimerUtils.console.log('[E10S] ' + msg);
}

var KanColleTimerE10S = {
    _open: function(manager, type, callback) {
	E10SDEBUG('_open: ' + type);
	let _type = ID + ':' + type;
	let _seq = seq++ & SEQ_MASK;
	let _type_seq = _type + '#' + _seq;
	let cb = callback ? function(cx) { return callback(seq, cx) } : function() {};
	let listener = { receiveMessage: cb, };
	let _manager = manager || globalMM;
	let session = {
	    _type: _type,
	    _type_seq: _type_seq,
	    $header: {
		name: type,
		seq: _seq,
	    },
	    listener: listener,
	    manager: _manager,
	};
	sessions.set(_seq, session);
	_manager.addMessageListener(_type_seq, listener);
	E10SDEBUG('_open: seq = ' + _seq);
	return _seq;
    },
    /**
     * Establish new connection with frame script.
     * This function opens new connection with frame script and
     * registers callback function.
     * @param manager	The message manager to use (or null, for global message manager)
     * @param type	The message type string.
     * @param callback	The callback function or null.
     * @returns		New handle for further operation.
     */
    open: function(manager, type, callback) {
	let cb = callback ? function(seq, cx) { return callback(cx.data.$header, cx.data.$body); } : null;
	return this._open(manager, type, cb);
    },
    /**
     * Close connection.
     * @param seq	The handle.
     */
    close: function(seq) {
	E10SDEBUG('close: ' + seq);
	let session = sessions.get(seq);
	if (!session)
	    throw new Error('session not found.');
	session.manager.removeMessageListener(session._type_seq, session.listener);
	sessions.delete(seq);
    },
    /**
     * Send data.
     * @param seq	The handle.
     * @param data	The data to send.
     */
    send: function(seq, data = null) {
	E10SDEBUG('E10S send: ' + seq + ', data = ' + (data ? data.toSource() : 'null'));
	let session = sessions.get(seq);
	if (!session)
	    throw new Error('session not found.');
	session.manager.sendAsyncMessage(session._type,
					 {
					    $header: session.$header,
					    $body: data,
					 });
    },
    /**
     * Send data and wait reply.
     * This opens new connection and sends data.
     * Then it waits reply and calls callback (if any).
     * @param message	The message manager to use (or null for global message manager).
     * @param type	The message type string.
     * @param data	The data to send.
     * @param callback	The callback function or null.
     */
    request: function(manager, type, data, callback) {
	let that = this;
	let seq = this.open(manager, type,
			    function(h, d) {
				that.close(seq);
				return callback ? callback(h, d) : null;
			    });
	this.send(seq, data);
    },
    _load_frame_script: function() {
	globalMM.loadFrameScript("chrome://kancolletimer/content/e10s-frame.js", true);
    },
    init: function() {
	this._load_frame_script();
    },
};

KanColleTimerE10S.init();
