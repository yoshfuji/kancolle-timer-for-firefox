// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("chrome://kancolletimer/content/utils.jsm");

var __KanColleDataAdd = {
    /*
     主砲 #d15b5b
     副砲 #ffea00
     対空砲 #66cc77
     魚雷 #5887ab
     艦載機 #39b74e
     偵察機 #8fcc99
     電探 #e89a35
     対潜 #7eccd8
     タービン #fdc24c
     三式弾 #71cd7e
     徹甲弾 #d15b5b
     機銃 #66cc77
     ダメコン #ffffff
     大発 #9aa55d
     カ号 #66cc77
     三式指揮 #7fccd8
     バルジ #9a7eaa
     ドラム缶 #a3a3a3
     */
    slotitem_color: {
	1:  '#d15b5b',	// 主砲 1-16が高角砲
	2:  '#d15b5b',	// 主砲
	3:  '#d15b5b',	// 主砲
	4:  '#ffea00',	// 副砲 4-16が高角砲
	5:  '#5887ab',	// 魚雷
	6:  '#39b74e',	// 制空戦闘機
	7:  '#39b74e',	// 艦爆
	8:  '#39b74e',	// 艦攻
	9:  '#39b74e',	// 彩雲
	10: '#8fcc99',	// 偵察機・観測機
	11: '#8fcc99',	// 瑞雲・晴嵐
	12: '#e89a35',	// 電探
	13: '#e89a35',	// 電探
	14: '#7eccd8',	// 対潜兵器
	15: '#7eccd8',	// 対潜兵器
	17: '#fdc24c',	// タービン
	18: '#71cd7e',	// 三式弾
	19: '#d15b5b',	// 徹甲弾
	21: '#66cc77',	// 機銃
	22: '#5887ab',	// 甲標的
	23: '#ffffff',	// ダメコン
	24: '#9aa55d',	// 大発
	25: '#66cc77',	// カ号
	26: '#7fccd8',	// 三式式連絡機
	27: '#9a7eaa',	// バルジ
	28: '#9a7eaa',	// バルジ
	29: '#f28a47',  // 探照灯
	30: '#a3a3a3',  // ドラム缶
	31: '#b09d7f',  // 艦艇修理施設
	32: '#5887ab',  // 潜水艦艦首魚雷
	33: '#f28a47',  // 照明弾
	34: '#c8aaff',	// 艦隊司令部施設
	35: '#cda269', // 熟練艦載機整備員
	36: '#899a4d', // 91式高射装置
	37: '#ff3636', // WG42
	38: '',
	39: '#bfeb9f', // 熟練見張員
	41: '#8fcc99', // 二式大艇
	42: '#f28a47', // 大型探照灯
	43: '#ffffff',	// 戦闘糧食
	44: '#78dcb5', // 洋上補給

	99: ''
    },
};

/*
 * data.uri のデフォルトは
 *  chrome://kancolletimer/content/data.json
 *
 * 例えば
 *  https://bitbucket.org/yoshfuji/kancolle-timer-for-firefox/raw/dev/chrome/content/data.js?at=dev&fileviewer=file-view-default
 * にすると bitbucket 上の dev ブランチの最新になる
 */
var [KanColleData, TouRabuData] = (function() {
    let uris = [
	KanColleUtils.getUnicharPref('data.uri'),
	'chrome://kancolletimer/content/data.json',
    ];
    let data = null;

    function get_data_json(uri) {
	let req = Components.classes["@mozilla.org/xmlextras/xmlhttprequest;1"]
		    .createInstance();
	let data = null;
	let header = null;

	if (!uri)
	    return null;

	debugprint('Loading data: ' + uri);

	try {
	    req.open('GET', uri, false);
	    req.overrideMimeType('text/x-json-with-comments');
	    req.send();
	} catch(e) {
	    debugprint('Failed to send request to ' + uri);
	    return null;
	}
	// req.status may become 0 for non-HTTP URLs.
	// format will be checked later.
	if (req.status != 200 && req.status != 0) {
	    debugprint('Failed to load data from ' + uri);
	    return null;
	}

	try {
	    // //-style コメントを削除して正規の JSON に変換
	    data = JSON.parse(req.responseText.split(/[\r\n]+/)
				.map(function(v) {
					return v.replace(/^(([^/"]|"([^"\\]|\\.)*")*)\/\/.*$/g, "$1");
				     })
				.join('')
			     );
	    header = data.HEADER;
	} catch(e) {
	}

	if (!header) {
	    debugprint('invalid data.');
	    return null;
	}

	debugprint('data[' + header.name + ']: version ' + header.ver + ', release ' + header.rel);

	if (header.name != 'kancolletimer-kai@st-paulia.net' ||
	    header.ver != 1 ||
	    header.rel < 10) {
	    debugprint('version mismatch\n');
	    return null;
	}

	return data;
    }

    for (let uri of uris) {
	data = get_data_json(uri);
	if (data)
	    break;
    }

    if (!data) {
	debugprint('Failed to load json.data.');
	return [null, null];
    }

    Object.keys(__KanColleDataAdd).forEach(function(e) {
	data.KANCOLLE[e] = __KanColleDataAdd[e];
    });

    return [data.KANCOLLE, data.TOURABU];
})();

function GetEquipmentColor( d ){
    let color = KanColleData.slotitem_color[ d.api_type[2] ];
    if( (d.api_type[2] == 1 || d.api_type[2] == 4) && d.api_type[3] == 16 ){
       // 主砲・副砲扱いの高角砲たち
       color = "#66cc77";
     }
    return color;
}

/**
 * 装備アイテムのサブカラーを返す
 * @param d
 */
function GetEquipmentSubColor( d ){
    let subcolor = {
       6:  '#39b74e',  // 制空戦闘機
       7:  '#ea6a6a',  // 艦爆
       8:  '#65bcff',  // 艦攻
       9:  '#ffc000'   // 彩雲
    };
    let color = subcolor[ d.api_type[2] ];
    return color;
}

