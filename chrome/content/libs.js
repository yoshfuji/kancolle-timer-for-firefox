// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("resource://gre/modules/ctypes.jsm");
Components.utils.import("chrome://kancolletimer/content/httpobserve.jsm");
Components.utils.import("chrome://kancolletimer/content/utils.jsm");

/**
 * いろいろと便利関数などを.
 */

const Cc = Components.classes;
const Ci = Components.interfaces;

const MODE_SAVE = Ci.nsIFilePicker.modeSave;

const XUL_NS = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";
const HTML_NS= "http://www.w3.org/1999/xhtml";

const ADDON_ID = "kancolletimer-kai@st-paulia.net";

var __KanColleTimerPanel = {
    _alarm: null,
    _set_alarm_time_node: function(node, timeout) {
	if (!node)
	    return;
	node.value = isNaN(timeout) ? '' : GetDateString(timeout);
    },
    _update_alarm_time_node: function(node, diff) {
	if (!node)
	    return;
	node.value = isNaN(diff) ? '' : GetTimeString(Math.round(diff / 1000));
    },
    _update_alarm_style_node: function(node, diff) {
	let style = null;
	if (!node)
	    return;
	if (!isNaN(style)) {
	    if (diff <= 60000)
		style = 'red';
	    else if (diff <= 300000)
		style = 'orange';
	}
	SetStyleProperty(node, 'color', style);
    },
    _alarm_set: function(id, timeout) {
	if (!isNaN(timeout) && !timeout)
	    timeout = (new Date).getTime();

	//debugprint('set timer: id = ' + id + '; timeout = ' + timeout);

	this._set_alarm_time(id, timeout);

	this._alarm.timeout.set(id, timeout);
	this._alarm_update();
    },
    _alarm_update: function() {
	let now = Date.now();
	for (let [id, timeout] of this._alarm.timeout.entries()) {
	    let diff = timeout - now;

	    if (diff < 0)
		diff = 0;

	    //debugprint('update timer: id = ' + id + '; left = ' + diff);
	    if (this._update_alarm_time)
		this._update_alarm_time(id, diff);

	    if (!diff)
		this._alarm.timeout.delete(id);
	}
	if (this._alarm.timeout.size)
	    this._alarm_start();
	else
	    this._alarm_stop();
    },
    _alarm_stop: function() {
	debugprint('STOP TIMER');
	if (this._alarm.timeout.size)
	    debugprint('time is still running: ' + this._alarm.timeout.size);
	KanColleTimerUtils.timer.stop(this._alarm.timer);
	this._alarm.timer = null;
    },
    _alarm_start: function() {
	if (!this._alarm.timeout.size)
	    return;
	if (!this.observe || this._alarm.timer)
	    return;
	debugprint('START TIMER');
	this._alarm.timer = KanColleTimerUtils.timer.startRepeatingEvent(this, 1000, { type: 'alarm' });
    },
    _alarm_exit: function() {
	for (let id of this._alarm.timeout.keys())
	    this._alarm_stop(id);
	if (this._alarm.timer)
	    KanColleTimerUtils.timer.stop(this._alarm.timer);
	this._alarm = null;
    },
    _alarm_init: function() {
	this._alarm = {
	    timer: null,
	    timeout: new Map,
	};
    },

    _pref: null,
    _pref_branch: null,
    _prefs_init: function() {
	let pref = this._pref || '';
	if (this._pref_branch || !this.observe)
	    return;
	this._pref_branch = KanColleUtils.getPrefBranch();
	KanColleTimerUtils.prefs.addObserver(this._pref_branch, pref, this, false);
	this.observe(null, 'nsPref:changed', null);
    },
    _prefs_exit: function() {
	let pref = this._pref || '';
	if (!this._pref_branch)
	    return;
	KanColleTimerUtils.prefs.removeObserver(this._pref_branch, pref, this);
	this._pref_branch = null;
    },

    update: null,
    _update_bound: null,

    _update_start: function() {
	for (let [k, f] in Iterator(this._update_bound))
	    KanColleDatabase[k].appendCallback(f);
    },
    _update_stop: function() {
	for (let [k, f] in Iterator(this._update_bound))
	    KanColleDatabase[k].removeCallback(f);
    },

    _update_init: function() {
	if (!this._update_bound) {
	    this._update_bound = {};
	    if (this.update) {
		for (let k in this.update) {
		    let f = this.update[k];
		    let visited = {};   // loop detection
		    while (typeof(f) == 'string' && !visited[f]) {
			visited[f] = true;
			f = this.update[f];
		    }
		    this._update_bound[k] = f.bind(this);
		}
	    }
	}
    },
    _update_exit: function() {
	this._update_bound = null;
    },

    start: function() {
	this._update_start();
    },
    stop: function() {
	this._update_stop();
    },

    init: function() {
	KanColleDatabase.init();
	this._update_init();
	this._prefs_init();
	this._alarm_init();
    },
    exit: function() {
	this._alarm_exit();
	this._prefs_exit();
	this._update_exit();
	KanColleDatabase.exit();
    },

    load: function() {
	this.init();
	if (this.restore)
	    this.restore();
	this.start();
    },
    unload: function() {
	this.stop();
	this.exit();
    },
};

/*
 * 所有艦娘情報2
 *  member/ship2	: api_data
 */

function AddLog(str){
    $('log').value = str + $('log').value;
}

function OpenShipList(){
    let feature="chrome,resizable=yes";
    let w = window.open("chrome://kancolletimer/content/shiplist.xul","KanColleTimerShipList",feature);
    w.focus();
}

function OpenNewShipList(){
    if( KanColleUtils.getBoolPref( 'tab-open.shiplist2' ) )
	OpenDefaultBrowser( 'chrome://kancolletimer/content/shiplist/shiplist.xul', true);
    else {
	let feature="chrome,resizable=yes";
	let w = window.open("chrome://kancolletimer/content/shiplist/shiplist.xul","KanColleTimerNewShipList",feature);
	w.focus();
    }
}

function OpenResourceGraph(){
    if( KanColleUtils.getBoolPref( 'tab-open.resourcegraph' ) )
	OpenDefaultBrowser( 'chrome://kancolletimer/content/resourcegraph.xul', true);
    else
	window.open('chrome://kancolletimer/content/resourcegraph.xul','KanColleTimerResourceGraph','chrome,resizable=yes').focus();
}

function OpenAboutDialog(){
    var f='chrome,toolbar,modal=no,resizable=no,centerscreen';
    var w = window.openDialog('chrome://kancolletimer/content/about.xul','KanColleTimerAbout',f);
    w.focus();
}

function OpenSettingsDialog(){
    var f='chrome,toolbar,modal=no,resizable=yes,centerscreen';
    var w = window.openDialog('chrome://kancolletimer/content/preferences.xul','KanColleTimerPreference',f);
    w.focus();
}

function OpenTweetDialog(param){
    var f = 'chrome,toolbar,modal=no,resizable=no,centerscreen';
    var w = window.openDialog('chrome://kancolletimer/content/sstweet.xul','KanColleTimerTweet',f,param);
    w.focus();
}

function OpenPowerup() {
    window.open('chrome://kancolletimer/content/powerup.xul','KanColleTimerPowerUp','chrome,resizable=yes').focus();
}

function OpenDropShipList() {
    window.open('chrome://kancolletimer/content/droplist.xul','KanColleTimerDropShipList','chrome,resizable=yes').focus();
}

function OpenVideoRecorder(){
    let feature = "chrome,resizable=yes";
    let w = window.open( "chrome://kancolletimer/content/videorecorder.xul", "KanColleTimerVideoRecorder", feature );
    w.focus();
}

function OpenEquipmentList() {
    if( KanColleUtils.getBoolPref( 'tab-open.equipmentlist' ) )
	OpenDefaultBrowser( 'chrome://kancolletimer/content/equipment/equipmentlist.xul', true);
    else
	window.open('chrome://kancolletimer/content/equipment/equipmentlist.xul','KanColleTimerEquipmentList','chrome,resizable=yes').focus();
}

function OpenDeckExporter(){
    var f = 'chrome,toolbar,modal=no,resizable=no,centerscreen';
    let feature = "chrome,resizable=yes";
    var w = window.openDialog('chrome://kancolletimer/content/deckexporter.xul','KanColleTimerDeckExporter',f);
    w.focus();
}

function TakeKanColleScreenshot_canvas(isjpeg){
    var canvas = document.createElementNS( "http://www.w3.org/1999/xhtml", "canvas" );
    let region = KanColleUtils.getRegion();
    let masks = [];
    let url;

    if (!canvas || !region)
	return null;

    if (!KanColleTimerUtils.screenshot.drawCanvas(canvas, null, region, masks))
	return null;

    return canvas;
}

/*

 */
function FindSlotItemNameById( api_id ){
    let item = KanColleDatabase.slotitem.get(api_id);
    let itemname = '[Unknown]';
    if (item) {
	let itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id);
	if (itemtype)
	    itemname = itemtype.api_name;
    }
    return itemname;
}

function FindShipNameByCatId( id ){
    try{
	// 全艦データから艦艇型IDをキーに艦名を取得
	return KanColleDatabase.masterShip.get(id).api_name;
    } catch (x) {
    }
    return "";
}

/**
 * 自分の保有している艦のデータを返す.
 */
function FindOwnShipData( ship_id ){
    return KanColleDatabase.ship.get(ship_id);
}

/**
 * 艦のデータを返す
 */
function FindShipData( ship_id ){
    let ship = KanColleDatabase.ship.get(ship_id);
    if (ship)
	return KanColleDatabase.masterShip.get(ship.api_ship_id);
    return undefined;
}

/**
 * 艦艇の名前を返す
 */
function FindShipName( ship_id ){
    try{
	// member/ship2 には艦名がない。艦艇型から取得
	let ship = KanColleDatabase.ship.get(ship_id);
	return FindShipNameByCatId( ship.api_ship_id );
    } catch (x) {
    }
    return "";
}

function FindShipCond( ship_id ){
    try{
	let ship = KanColleDatabase.ship.get(ship_id);
	return parseInt(ship.api_cond, 10);
    } catch (x) {
    }
    return undefined;
}

function FindShipStatus( ship_id ){
    try{
	let info = {
	    fuel: undefined,
	    fuel_max: undefined,
	    bull: undefined,
	    bull_max: undefined,
	    nowhp: undefined,
	    maxhp: undefined,
	};

	// member/ship には fuel, bull, nowhp, maxhp
	let ship = KanColleDatabase.ship.get(ship_id);

	info.fuel = parseInt(ship.api_fuel, 10);
	info.bull = parseInt(ship.api_bull, 10);
	info.nowhp = parseInt(ship.api_nowhp, 10);
	info.maxhp = parseInt(ship.api_maxhp, 10);

	// fuel_max と bull_max は master/shipから
	ship = KanColleDatabase.masterShip.get(ship.api_ship_id);

	info.fuel_max = parseInt(ship.api_fuel_max, 10);
	info.bull_max = parseInt(ship.api_bull_max, 10);

	return info;
    } catch (x) {
    }
    return undefined;
}

function ShipCalcSearchBase(shipid) {
    let ship = KanColleDatabase.ship.get(shipid);
    let search = 0;

    if (!ship || !ship.api_sakuteki)
	return -1;

    search = ship.api_sakuteki[0];

    return search -
	   ship.api_slot.map(function(e) {
	let item = KanColleDatabase.slotitem.get(e);
	let itemtype = item ? KanColleDatabase.masterSlotitem.get(item.api_slotitem_id) : null;
	return itemtype ? itemtype.api_saku : 0;
    }).reduce(function(prev, cur) {
	return prev + cur;
    }, 0);
}

function ShipCalcSearch(shipid) {
    let ship = KanColleDatabase.ship.get(shipid);
    let search = 0;

    if (!ship || !ship.api_sakuteki)
	return -1;

    return ship.api_sakuteki[0];
}

function __ShipCalcSearchV4(factors, param, shipid) {
    let nomials;
    let base = ShipCalcSearchBase(shipid);
    let ship = KanColleDatabase.ship.get(shipid);

    if (base < 0 || !ship)
	return null;
    base = Math.sqrt(base);

    nomials = ship.api_slot
		.map(function(e) {
		    let item = KanColleDatabase.slotitem.get(e);
		    let itemtype;
		    if (!item)
			return { v: 0, f: { f2: 0 }, };
		    itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id);
		    if (!itemtype)
			return { v: 0, f: { f2: 0 }, };

		    let fac = factors.slotitem[itemtype.api_type[2]] || factors.slotitem["*"];

		    let val = (itemtype.api_saku + fac.f4 * Math.sqrt(item.api_level || 0)) * param;
		    let obj = { v: val, f: fac, };
		    return obj;
		});
    nomials.unshift({ v: base, f: factors.base, });

    return nomials.reduce(function(p,c) { return p + c.v * c.f.f2; }, 0);
}

function FleetCalcSearchV4(ships, escaped, param) {
    let factors = KanColleData.search_factors;
    let nomials;

    // 索敵値 = Sl + El + Ch + Cq
    //	Sl = Σ(√(艦船素索敵))
    //	El = Σ((装備索敵 + 改修係数 * √★) * 装備係数 * 分岐点係数
    //	Ch = 司令部Lv補正
    //	Cq = 2 * (6 - 到達艦数)

    let lv = KanColleDatabase.record.get().api_level;
    if (isNaN(lv))
	return null;

    // 到達艦は本体と装備の索敵を参入する(Sl+El)
    // いない場合到達艦補正(Cq)
    nomials = ships.map(function(e,i) {
			    let ret = escaped[i] ? null : __ShipCalcSearchV4(factors, param, e);
			    return ret !== null ? ret : 2;
			});

    // Ch 過去の経緯でパラメータは負で与えている
    nomials.push(-Math.ceil(lv * (-factors.lv.f2)));

    //debugprint(nomials.toSource());

    return nomials.reduce(function(p,c) {
	return p + c;
    }, 0);
}

function ShipCalcAirPower(shipid) {
    let ship = KanColleDatabase.ship.get(shipid);
    let ap = 0;

    if (!ship)
	return -1;

    for (let j = 0; j < ship.api_slot.length && j < ship.api_onslot.length; j++) {
	let item = KanColleDatabase.slotitem.get(ship.api_slot[j]);
	let itemtype;

	if (!item)
	    continue;

	itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id);
	if (itemtype && itemtype.api_tyku) {
	    // 制空値 ＝ [(対空値 ＋ 装備別補正値 × ★改修値) × √(搭載数) ＋ 熟練度補正]
	    //	装備別補正値：艦戦、水戦、陸戦は0.2、爆戦は0.25
	    //  熟練度補正＝内部熟練ボーナス＋制空ボーナス(艦戦・水戦・局戦/水爆)
	    //   内部熟練ボーナス＝√(内部熟練度/10)
	    let item_level_bonus_ratio = KanColleData.air_factors.item_bonus_ratio.id[item.api_slotitem_id] ||
					 KanColleData.air_factors.item_bonus_ratio.type[itemtype.api_type[2]] ||
					 0;
	    let item_level_bonus = (item.api_level || 0) * item_level_bonus_ratio;
	    let item_tyku = itemtype.api_tyku + item_level_bonus;
	    let item_skill_value = KanColleData.air_factors.base[item.api_alv || 0] || [0,0];
	    let item_type_bonus = KanColleData.air_factors.slotitem[itemtype.api_type[2]];
	    if (!item_type_bonus)
		continue;
	    let item_skill_bonus = Math.sqrt(item_skill_value[0] / 10) + item_type_bonus[item.api_alv || 0];
	    let _ap = Math.floor(item_tyku * Math.sqrt(ship.api_onslot[j]) + item_skill_bonus);
	    ap += _ap;
	}
    }
    return ap;
}

function getShipProperties(ship,name)
{
    const propmap = {
	karyoku: { kidx: 0, master: 'houg', },
	raisou:  { kidx: 1, master: 'raig', },
	taiku:   { kidx: 2, master: 'tyku', },
	soukou:  { kidx: 3, master: 'souk', },
	lucky:   { kidx: 4, master: 'luck', },
    };
    let prop = {
	cur: null,
	max: null,
	str: null,
	remain: null,
	powup: null,
    };
    let shiptype;
    shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
    prop.cur = ship['api_' + name][0];
    prop.min = prop.cur - ship.api_kyouka[propmap[name].kidx];
    prop.max = prop.min +
	       shiptype['api_' + propmap[name].master][1] -
	       shiptype['api_' + propmap[name].master][0];
    prop.str = prop.cur + '/' + prop.max;
    prop.remain = prop.max - prop.cur;
    prop.powup = shiptype.api_powup[propmap[name].kidx];
    return prop;
}

function getShipSlotitem(ship,slot){
    let idx = slot - 1;
    let name;
    let itemid;

    if (idx >= ship.api_slot.length) {
	itemid = ship.api_slot_ex;
    } else {
	if (idx >= ship.api_slotnum)
	    return '';
	itemid = ship.api_slot[idx];
    }

    if (itemid < 0)
	return '-';

    item = KanColleDatabase.slotitem.get(itemid);
    if (!item)
	return '!' + itemid;

    if (item.api_name)
	name = item.api_name;
    else {
	let itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id);
	if (!itemtype)
	    return '?';
	name = itemtype.api_name;
    }
    return name;
}

function ShipExp(ship){
    let ship_exp;
    if (typeof(ship.api_exp) == 'object') {
	// 2013/12/11よりAPI変更
	// 0: 現在経験値
	// 1: 次Lvまでの必要経験値
	// 2: 現在Lvでの獲得経験値(%)
	return ship.api_exp[0];
    } else
	return ship.api_exp;
}

function ShipNextLvExp(ship){
    if (typeof(ship.api_exp) == 'object') {
	// Lv99: [1000000,0,0]
	return ship.api_exp[1] > 0 ? ship.api_exp[0] + ship.api_exp[1] : Number.POSITIVE_INFINITY;
    } else {
	let nextexp = KanColleData.level_accumexp[ship.api_lv];
	if (nextexp === undefined)
	    return undefined;
	else if (nextexp < 0)
	    return Number.POSITIVE_INFINITY;
	return nextexp;
    }
}

function ShipUpgradeableExp(ship){
    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
    let nextlv = shiptype ? shiptype.api_afterlv : 0;
    let nextexp;
    if (nextlv > 0) {
	nextexp = KanColleData.level_accumexp[nextlv - 1];
	if (nextexp === undefined || nextexp < 0)
	    return undefined;
    } else
	nextexp = Number.POSITIVE_INFINITY;
    return nextexp;
}
/*
 * とうらぶ
 */

function TouRabuParseTime(s) {
    if (s && s.match(/^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)$/)) {
	let year = parseInt(RegExp.$1, 10),
	    month = parseInt(RegExp.$2, 10),
	    day = parseInt(RegExp.$3, 10),
	    hour = parseInt(RegExp.$4, 10),
	    min = parseInt(RegExp.$5, 10),
	    sec = parseInt(RegExp.$6, 10);
	let d = new Date;
	d.setUTCFullYear(year, month - 1, day);
	d.setUTCHours(hour);
	d.setUTCMinutes(min);
	d.setUTCSeconds(sec);
	return d.getTime() - 9 * 3600000;
    } else {
	return Number.NaN;
    }
}

function TouRabuSwordName(id) {
    let swords = KanColleDatabase.tourabuSword.get();
    let sword = swords ? swords[id] : null;
    let sword_name = null;
    if (sword) {
	let sword_id = sword.sword_id;
	let sword_data = sword_id ? TouRabuData.sword[sword_id] : null;
	sword_name = sword_data ? sword_data.name : null;
	if (!sword_name)
	    sword_name = '#' + sword_id;
    }
    if (!sword_name)
	sword_name = '?(' + id + ')';
    return sword_name;
}

function GetFleetNo(ship_id) {
    let fleet = KanColleDatabase.deck.lookup( ship_id );
    return fleet ? fleet.fleet : 0;
}

function SaveCheckPreference( elem ){
    let name = elem.getAttribute("prefname");
    let checked = elem.hasAttribute('checked');
    KanColleUtils.setBoolPref( name, checked );
}

/**
 * Style
 */
function SetStyleProperty(node, prop, value, prio){
    if (value === undefined || value === null)
	node.style.removeProperty(prop);
    else {
	if (prio === undefined || prio === null)
	    prio = '';
	node.style.setProperty(prop,value,prio);
    }
}

/**
 * 指定のURLを開く.
 * @param url URL
 * @param hasfocus 開いたタブがフォーカスを得るか
 */
function OpenDefaultBrowser(url, hasfocus){
    return KanColleTimerUtils.window.openTab(url, hasfocus);
}

/**
 * Windowの最前面表示設定をする.
 * @note Windows/Firefox 17以降でのみ有効
 * @param win Window
 * @param istop 最前面にするならtrue
 */
function WindowOnTop(win, istop){
    try{
	let baseWin = win.QueryInterface(Ci.nsIInterfaceRequestor)
	    .getInterface(Ci.nsIWebNavigation)
	    .QueryInterface(Ci.nsIDocShellTreeItem)
	    .treeOwner
	    .QueryInterface(Ci.nsIInterfaceRequestor)
	    .nsIBaseWindow;
	let nativeHandle = baseWin.nativeHandle;

	let lib = ctypes.open('user32.dll');

	let HWND_TOPMOST = -1;
	let HWND_NOTOPMOST = -2;
	let SWP_NOMOVE = 2;
	let SWP_NOSIZE = 1;

	/*
	 WINUSERAPI BOOL WINAPI SetWindowPos(
	 __in HWND hWnd,
	 __in_opt HWND hWndInsertAfter,
	 __in int X,
	 __in int Y,
	 __in int cx,
	 __in int cy,
	 __in UINT uFlags );
	 */
	let SetWindowPos = lib.declare("SetWindowPos",
				       ctypes.winapi_abi, // abi
				       ctypes.int32_t,     // return type
				       ctypes.int32_t,     // hWnd arg 1 HWNDはint32_tでOK
				       ctypes.int32_t,     // hWndInsertAfter
				       ctypes.int32_t,     // X
				       ctypes.int32_t,     // Y
				       ctypes.int32_t,     // cx
				       ctypes.int32_t,     // cy
				       ctypes.uint32_t);   // uFlags
	SetWindowPos( parseInt(nativeHandle), istop?HWND_TOPMOST:HWND_NOTOPMOST,
		      0, 0, 0, 0,
		      SWP_NOMOVE | SWP_NOSIZE);
	lib.close();
    } catch (x) {
    }
}

/**
 * サウンド再生をする.
 * @param path ファイルのパス
 */
function PlaySound( path ){
    try{
	//debugprint(path);
	let IOService = Cc['@mozilla.org/network/io-service;1'].getService(Ci.nsIIOService);
	let localFile = Cc['@mozilla.org/file/local;1'].createInstance(Ci.nsILocalFile);
	let sound = Cc["@mozilla.org/sound;1"].createInstance(Ci.nsISound);
	localFile.initWithPath( path );
	sound.play(IOService.newFileURI(localFile));
	//sound.playEventSound(0);
    } catch (x) {
    }
}


function $(tag){
    return document.getElementById(tag);
}

function $$(tag){
    return document.getElementsByTagName(tag);
}

/**
 * オブジェクトをマージする.
 * @param a オブジェクト1
 * @param b オブジェクト2
 * @param aにbをマージしたオブジェクトを返す
 */
function MergeSimpleObject(a,b)
{
    for(let k in b){
	a[k] = b[k];
    }
    return a;
}

/**
 * 配列をシャッフルする.
 * @param list 配列
 */
function ShuffleArray( list ){
    let i = list.length;
    while(i){
	let j = Math.floor(Math.random()*i);
	let t = list[--i];
	list[i] = list[j];
	list[j] = t;
    }
}

/**
 * ユーザーのProfileディレクトリを返す
 */
function GetProfileDir(){
    return KanColleTimerUtils.file.profileDir();
}

function GetAddonVersion()
{
    let version;
    try{
	let em = Components.classes["@mozilla.org/extensions/manager;1"].getService(Components.interfaces.nsIExtensionManager);
	let addon = em.getItemForID(ADDON_ID);
	version = addon.version;
    } catch (x) {
	// Fx4
	let addon = KanColleTimerUtils.addon.get(ADDON_ID);
	version = addon.version;
    }
    return version;
}

function GetXmlText(xml,path){
    try{
	let tmp = evaluateXPath(xml,path);
	if( tmp.length<=0 ) return null;
	return tmp[0].textContent;
    } catch (x) {
	debugprint(x);
	return null;
    }
}

// 特定の DOM ノードもしくは Document オブジェクト (aNode) に対して
// XPath 式 aExpression を評価し、その結果を配列として返す。
// 最初の作業を行った wanderingstan at morethanwarm dot mail dot com に感謝します。
function evaluateXPath(aNode, aExpr) {
    let xpe = new XPathEvaluator();
    let nsResolver = xpe.createNSResolver(aNode.ownerDocument == null ?
					  aNode.documentElement : aNode.ownerDocument.documentElement);
    let result = xpe.evaluate(aExpr, aNode, nsResolver, 0, null);
    let found = [];
    let res;
    while (res = result.iterateNext())
	found.push(res);
    return found;
}
function evaluateXPath2(aNode, aExpr) {
    let xpe = new XPathEvaluator();
    let nsResolver = function(){ return XUL_NS; };
    let result = xpe.evaluate(aExpr, aNode, nsResolver, 0, null);
    let found = [];
    let res;
    while (res = result.iterateNext())
	found.push(res);
    return found;
}

function CreateElement(part){
    let elem;
    elem = document.createElementNS(XUL_NS,part);
    return elem;
}
function CreateHTMLElement(part){
    let elem;
    elem = document.createElementNS(HTML_NS,part);
    return elem;
}

/**
 * 指定の要素を削除する.
 * @param elem 削除したい要素
 */
function RemoveElement(elem){
    if (elem) {
	elem.parentNode.removeChild(elem);
	return true;
    }
    return false;
}

/**
 * 指定の要素の子要素を全削除する.
 * @param elem 対象の要素
 */
function RemoveChildren(elem){
    while(elem.hasChildNodes()) {
	elem.removeChild(elem.childNodes[0]);
    }
}

function ClearListBox( list ){
    while( list.getRowCount() ){
	list.removeItemAt( 0 );
    }
}

function CreateMenuItem(label,value){
    let elem;
    elem = document.createElementNS(XUL_NS,'menuitem');
    elem.setAttribute('label',label);
    elem.setAttribute('value',value);
    return elem;
};

function CreateButton(label){
    let elem;
    elem = document.createElementNS(XUL_NS,'button');
    elem.setAttribute('label',label);
    return elem;
}

function ToggleCheckbox(event,func) {
    let target = event.originalTarget;
    let checked = target.getAttribute('checked') == 'true';
    target.checked = !checked;
    return func();
}

function CreateLabel(label){
    let elem;
    elem = document.createElementNS(XUL_NS,'label');
    elem.setAttribute('value',label);
    return elem;
}

function CreateListCell(label){
    let elem;
    elem = document.createElementNS(XUL_NS,'listcell');
    elem.setAttribute('label',label);
    return elem;
}

function GetInputStream( file ){
    return KanColleTimerUtils.file.openRawReader(file);
}

/** ディレクトリを作成する.
 * ディレクトリ掘ったらtrue、掘らなかったらfalseを返す.
 */
function CreateFolder(path){
    let file = KanColleTimerUtils.file.initWithPath(path);
    return KanColleTimerUtils.file.createDirectory(file, 0);
}

/**
 * ファイルを開く
 */
function OpenFile(path){
    return KanColleTimerUtils.file.initWithPath(path);
}

function CreateFile( file ){
    return KanColleTimerUtils.file.openRawWriter(file, 0, 0);
}

// NicoLiveHelperのインストールパスを返す.
function GetExtensionPath(){
    let ext;
    try{
	ext = Components.classes["@mozilla.org/extensions/manager;1"]
            .getService(Components.interfaces.nsIExtensionManager)
            .getInstallLocation(ADDON_ID)
            .getItemLocation(ADDON_ID);
    } catch (x) {
	let _addon = KanColleTimerUtils.addon.get(ADDON_ID);
	ext = _addon.getResourceURI('/').QueryInterface(Components.interfaces.nsIFileURL).file.clone();
    }
    return ext;
}

function PlayAlertSound(){
    let sound = Components.classes["@mozilla.org/sound;1"].createInstance(Components.interfaces.nsISound);
    sound.playSystemSound("_moz_alertdialog");
}

function AlertPrompt(text,caption){
    let prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
    let result = prompts.alert(window, caption, text);
    return result;
}

function ConfirmPrompt(text,caption){
    let prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
    let result = prompts.confirm(window, caption, text);
    return result;
}

function InputPrompt(text,caption,input){
    let check = {value: false};
    let input_ = {value: input};

    let prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
    let result = prompts.prompt(window, caption, text, input_, null, check);
    if( result ){
	return input_.value;
    }else{
	return null;
    }
}

function InputPromptWithCheck(text,caption,input,checktext){
    let check = {value: false};
    let input_ = {value: input};

    let prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
    let result = prompts.prompt(window, caption, text, input_, checktext, check);
    if( result ){
	return input_.value;
    }else{
	return null;
    }
}

/**
 * @return nsIFileを返す
 */
function OpenFileDialog( caption, mode )
{
    const nsIFilePicker = Components.interfaces.nsIFilePicker;
    let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
    fp.init( window, caption, mode );
    fp.appendFilters(nsIFilePicker.filterAll);
    let rv = fp.show();
    if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
	let file = fp.file;
	return file;
    }
    return null;
}

function SaveUrlToFile(url, file)
{
    return KanColleTimerUtils.file.saveURI(file, url);
}

/**
 * HTML canvasを nsIURL に変換して返す
 * @param canvas
 * @param format
 * @returns {nsIURL}
 */
function CanvasToURI( canvas, format ){
    format = format || "image/png";
    let url = canvas.toDataURL( format );
    const IO_SERVICE = Components.classes['@mozilla.org/network/io-service;1']
       .getService( Components.interfaces.nsIIOService );
    let newurl = IO_SERVICE.newURI( url, null, null );
    return newurl;
}

function DrawSVGToCanvas( svg ){
    var canvas = document.createElementNS( "http://www.w3.org/1999/xhtml", "canvas" );
    let rect = svg.getBoundingClientRect();
    let region = {
	win: window,
	x: rect.left,
	y: rect.top,
	w: rect.width,
	h: rect.height,
    };

    return KanColleTimerUtils.screenshot.drawCanvas(canvas, null, region, null);
}

/**
 *  Javascriptオブジェクトをファイルに保存する.
 * @param obj Javascriptオブジェクト
 * @param caption ファイル保存ダイアログに表示するキャプション
 */
function SaveObjectToFile(obj,caption)
{
    const nsIFilePicker = Components.interfaces.nsIFilePicker;
    let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
    fp.init(window, caption, MODE_SAVE);
    fp.appendFilters(nsIFilePicker.filterAll);
    let rv = fp.show();
    if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
	KanColleTimerUtils.file.writeObject(fp.file, 0, 0, obj);
    }
}

/**
 *  ファイルからJavascriptオブジェクトを読み込む.
 * @param caption ファイル読み込みダイアログに表示するキャプション
 */
function LoadObjectFromFile(caption)
{
    const nsIFilePicker = Components.interfaces.nsIFilePicker;
    let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
    fp.init(window, caption, nsIFilePicker.modeOpen);
    fp.appendFilters(nsIFilePicker.filterAll);
    let rv = fp.show();
    if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
	let obj = KanColleTimerUtils.file.readObject(fp.file);
	if (obj === undefined)
	    obj = null;
	return obj;
    }
    return null;
}


/**
 * 指定タグを持つ親要素を探す.
 * @param elem 検索の起点となる要素
 * @param tag 親要素で探したいタグ名
 */
function FindParentElement(elem,tag){
    //debugprint("Element:"+elem+" Tag:"+tag);
    while(elem.parentNode &&
	  (!elem.tagName || (elem.tagName.toUpperCase()!=tag.toUpperCase()))){
	elem = elem.parentNode;
    }
    return elem;
}

function GetSignedValue(v)
{
    return (!isNaN(v) && v > 0) ? '+' + v : v;
}

/**
 * クリップボードにテキストをコピーする.
 * @param str コピーする文字列
 */
function CopyToClipboard(str){
    if(str.length<=0) return;
    let gClipboardHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"].
	getService(Components.interfaces.nsIClipboardHelper);
    gClipboardHelper.copyString(str);
}

function htmlspecialchars(ch){
    ch = ch.replace(/&/g,"&amp;");
    ch = ch.replace(/"/g,"&quot;");
    //ch = ch.replace(/'/g,"&#039;");
    ch = ch.replace(/</g,"&lt;");
    ch = ch.replace(/>/g,"&gt;");
    return ch ;
}

function restorehtmlspecialchars(ch){
    ch = ch.replace(/&quot;/g,"\"");
    ch = ch.replace(/&amp;/g,"&");
    ch = ch.replace(/&lt;/g,"<");
    ch = ch.replace(/&gt;/g,">");
    ch = ch.replace(/&nbsp;/g," ");
    ch = ch.replace(/&apos;/g,"'");
    return ch;
}

function syslog(txt){
    let tmp = GetDateString( GetCurrentTime()*1000 );
    txt = tmp + " " +txt;
    if( $('syslog-textbox') )
	$('syslog-textbox').value += txt + "\n";
}

function debugprint(txt){
    /*
    if( $('debug-textbox') )
	$('debug-textbox').value += txt + "\n";
     */
    KanColleTimerUtils.console.log(txt);
}

function debugconsole(txt){
    KanColleTimerUtils.console.log(txt);
}

function debugalert(txt){
    AlertPrompt(txt,'');
}

function ShowPopupNotification(imageURL,title,text,cookie){
    KanColleTimerUtils.alert.show(imageURL, title, text, false, cookie, null);
}


function GetUTF8ConverterInputStream(istream)
{
    let cis = Components.classes["@mozilla.org/intl/converter-input-stream;1"].createInstance(Components.interfaces.nsIConverterInputStream);
    cis.init(istream,"UTF-8",0,Components.interfaces.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER);
    return cis;
}

function GetUTF8ConverterOutputStream(os)
{
    let cos = Components.classes["@mozilla.org/intl/converter-output-stream;1"].createInstance(Components.interfaces.nsIConverterOutputStream);
    cos.init(os,"UTF-8",0,Components.interfaces.nsIConverterOutputStream.DEFAULT_REPLACEMENT_CHARACTER);
    return cos;
}


/**
 *  現在時刻を秒で返す(UNIX時間).
 */
function GetCurrentTime(){
    let d = new Date();
    return Math.floor(d.getTime()/1000);
}

function GetDateString(ms){
    let d = new Date(ms);
    return d.toLocaleFormat("%m-%d %H:%M:%S");
}

function GetFormattedDateString(format,ms){
    let d = new Date(ms);
    return d.toLocaleFormat(format);
}

// string bundleから文字列を読みこむ.
function LoadString(name){
    return $('string-bundle').getString(name);
}
function LoadFormattedString(name,array){
    return $('string-bundle').getFormattedString(name,array);
}

// [day+]hour:min:sec の文字列を返す.
function GetTimeString(sec,days){
    sec = Math.abs(sec);

    let hour = Math.floor( sec / 60 / 60 );
    let min = Math.floor( sec / 60 ) - hour*60;
    let s = sec % 60;
    let day = Math.floor(hour / 24);
    let str = '';

    if (days && day > 0) {
	hour -= day * 24;
	str = day + 'd+';
    }
    str += hour<10?"0"+hour:hour;
    str += ":";
    str += min<10?"0"+min:min;
    str += ":";
    str += s<10?"0"+s:s;
    return str;
}

// min以上、max以下の範囲で乱数を返す.
function GetRandomInt(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


// LCGの疑似乱数はランダム再生専用のため、他の用途では使用禁止.
var g_randomseed = GetCurrentTime();
function srand(seed)
{
    g_randomseed = seed;
}
function rand()
{
    g_randomseed = (g_randomseed * 214013 + 2531011) & 0x7fffffff;
    return g_randomseed;
}
// min以上、max以下の範囲で乱数を返す.
function GetRandomIntLCG(min,max)
{
    let tmp = rand() >> 4;
    return (tmp % (max-min+1)) + min;
}

function ZenToHan(str){
    return str.replace(/[ａ-ｚＡ-Ｚ０-９－（）＠]/g,
		       function(s){ return String.fromCharCode(s.charCodeAt(0)-65248); });
}

function HiraToKana(str){
    return str.replace(/[\u3041-\u3094]/g,
		      function(s){ return String.fromCharCode(s.charCodeAt(0)+0x60); });
}

/*
 *  convertKana JavaScript Library beta4
 *
 *  MIT-style license.
 *
 *  2007 Kazuma Nishihata [to-R]
 *  http://www.webcreativepark.net
 *
 * よりアルゴリズムを拝借.
 */
function HanToZenKana(str){
    let fullKana = new Array("ヴ","ガ","ギ","グ","ゲ","ゴ","ザ","ジ","ズ","ゼ","ゾ","ダ","ヂ","ヅ","デ","ド","バ","ビ","ブ","ベ","ボ","パ","ピ","プ","ペ","ポ","゛","。","「","」","、","・","ヲ","ァ","ィ","ゥ","ェ","ォ","ャ","ュ","ョ","ッ","ー","ア","イ","ウ","エ","オ","カ","キ","ク","ケ","コ","サ","シ","ス","セ","ソ","タ","チ","ツ","テ","ト","ナ","ニ","ヌ","ネ","ノ","ハ","ヒ","フ","ヘ","ホ","マ","ミ","ム","メ","モ","ヤ","ユ","ヨ","ラ","リ","ル","レ","ロ","ワ","ン","゜");
    let halfKana = new Array("ｳﾞ","ｶﾞ","ｷﾞ","ｸﾞ","ｹﾞ","ｺﾞ","ｻﾞ","ｼﾞ","ｽﾞ","ｾﾞ","ｿﾞ","ﾀﾞ","ﾁﾞ","ﾂﾞ","ﾃﾞ","ﾄﾞ","ﾊﾞ","ﾋﾞ","ﾌﾞ","ﾍﾞ","ﾎﾞ","ﾊﾟ","ﾋﾟ","ﾌﾟ","ﾍﾟ","ﾎﾟ","ﾞ","｡","｢","｣","､","･","ｦ","ｧ","ｨ","ｩ","ｪ","ｫ","ｬ","ｭ","ｮ","ｯ","ｰ","ｱ","ｲ","ｳ","ｴ","ｵ","ｶ","ｷ","ｸ","ｹ","ｺ","ｻ","ｼ","ｽ","ｾ","ｿ","ﾀ","ﾁ","ﾂ","ﾃ","ﾄ","ﾅ","ﾆ","ﾇ","ﾈ","ﾉ","ﾊ","ﾋ","ﾌ","ﾍ","ﾎ","ﾏ","ﾐ","ﾑ","ﾒ","ﾓ","ﾔ","ﾕ","ﾖ","ﾗ","ﾘ","ﾙ","ﾚ","ﾛ","ﾜ","ﾝ","ﾟ");
    for(let i = 0; i < 89; i++){
	let re = new RegExp(halfKana[i],"g");
	str=str.replace(re, fullKana[i]);
    }
    return str;
}

function FormatCommas(str){
    try{
	return str.toString().replace(/(\d)(?=(?:\d{3})+$)/g,"$1,");
    } catch (x) {
	return str;
    }
}

function clearTable(tbody)
{
   while(tbody.rows.length>0){
      tbody.deleteRow(0);
   }
}

function IsWINNT()
{
    let osString = Components.classes["@mozilla.org/xre/app-info;1"]
        .getService(Components.interfaces.nsIXULRuntime).OS;
    if(osString=="WINNT"){
	return true;
    }
    return false;
}

function IsDarwin()
{
    let osString = Components.classes["@mozilla.org/xre/app-info;1"]
        .getService(Components.interfaces.nsIXULRuntime).OS;
    if(osString=="Darwin"){
	return true;
    }
    return false;
}

function IsLinux()
{
    let osString = Components.classes["@mozilla.org/xre/app-info;1"]
        .getService(Components.interfaces.nsIXULRuntime).OS;
    if(osString=="Linux"){
	return true;
    }
    return false;
}
