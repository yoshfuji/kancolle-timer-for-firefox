// vim: set ts=8 sw=4 sts=4 ff=dos :

var TouRabuTimerDeckInfo = {
    update: {
	tourabuParty: function(extradata) {
	    let offset = KanColleDatabase.tourabuParty.timestamp() - KanColleDatabase.tourabuParty.now();
	    let list = KanColleDatabase.tourabuParty.list();
	    for (k of list) {
		let box = $('tourabu-mission' + k);
		let d = KanColleDatabase.tourabuParty.get(k);
		let goal = d._finished_at + offset;
		let field_name = d._summary ? ('#' + d._summary.field_id) : '?';
		if (!box)
		    continue;
		if (!isNaN(goal)) {
		    $('tourabu-mission' + k + '-name').value = field_name;
		} else {
		    $('tourabu-mission' + k + '-name').value = '';
		}
	    }
	    if (extradata) {
		let state = extradata.state || 0;
		let id = extradata.id || 0;
		debugprint(extradata.toSource());
		if (state > 1)
		    AddLog(extradata.message + '\n');
		if (id > 0)
		    this._alarm_set(id, extradata.time);
	    }
	},
	tourabuLoginStart: function() {
	    this.restore();
	},
    },

    _set_alarm_time: function(id, timeout) {
	let node = $('tourabu-mission' + id + '-time');
	this._set_alarm_time_node(node, timeout);
    },
    _update_alarm_time: function(id, diff) {
	let node = $('tourabu-mission' + id + '-time');
	let rnode = $('tourabu-mission' + id + '-remain');
	this._update_alarm_time_node(rnode, diff);
	this._update_alarm_style_node(node, diff);
	this._update_alarm_style_node(rnode, diff);
    },

    observe: function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm_update();
	    break;
	default:;
	}
    },

    restore: function() {
	let t = KanColleDatabase.tourabuLoginStart.timestamp();
	let d = KanColleDatabase.tourabuLoginStart.get();
	for (let i = 1; i <= 4; i++) {
	    let box = $('tourabu-mission' + i);
	    if (!box)
		continue;
	    if (t && i <= d.max_party)
		$('tourabu-mission' + i).collapsed = false;
	    else
		$('tourabu-mission' + i).collapsed = true;
	}
    },
};
TouRabuTimerDeckInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { TouRabuTimerDeckInfo.load(); }, false);
window.addEventListener('unload', function(e) { TouRabuTimerDeckInfo.unload(); }, false);
