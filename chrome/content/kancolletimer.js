// vim: set ts=8 sw=4 sts=4 ff=dos :

// http://www.dmm.com/netgame/social/application/-/detail/=/app_id=854854/

Components.utils.import("chrome://kancolletimer/content/httpobserve.jsm");
Components.utils.import("chrome://kancolletimer/content/utils.jsm");
Components.utils.import("resource://gre/modules/Promise.jsm");
Components.utils.import("chrome://kancolletimer/content/e10s-main.jsm");

var KanColleTimer = {
    // ウィンドウを最前面にする
    setWindowOnTop:function(){
	let checkbox = $('window-stay-on-top');
	if (checkbox)
	    WindowOnTop( window, checkbox.hasAttribute('checked') );
    },

    setGeneralTimerByTime: function(timeout){
	KanColleTimerHeadQuarterInfo.start_general_timer(timeout);
    },

    setGeneralTimer: function(sec){
	KanColleTimerHeadQuarterInfo.start_general_timer_by_delay(sec);
    },

    getNowDateString: function(){
	var d = new Date();
	var month = d.getMonth()+1;
	month = month<10 ? "0"+month : month;
	var date = d.getDate()<10 ? "0"+d.getDate() : d.getDate();
	var hour = d.getHours()<10 ? "0"+d.getHours() : d.getHours();
	var min = d.getMinutes()<10 ? "0"+d.getMinutes() : d.getMinutes();
	var sec = d.getSeconds()<10 ? "0"+d.getSeconds() : d.getSeconds();
	var ms = d.getMilliseconds();
	if( ms<10 ){
	    ms = "000" + ms;
	}else if( ms<100 ){
	    ms = "00" + ms;
	}else if( ms<1000 ){
	    ms = "0" + ms;
	}
	return "" + d.getFullYear() + month + date + hour + min + sec + ms;
    },

    /**
     * スクリーンショット撮影
     * @param isjpeg JPEGかどうか
     */
    _PtakeScreenshot: function(isjpeg){
	return new Promise(function(resolve, reject) {
	    let format = isjpeg ? [ 'image/jpeg' ] : [ 'image/png' ];
	    let tab = KanColleTimerUtils.window.findTab(KanColleUtils.URL);
	    if (!tab) {
		AlertPrompt("艦隊これくしょんのページが見つかりませんでした。","艦これタイマー");
		reject(new Error('Failed to find tab.'));
		return;
	    }
	    KanColleTimerE10S.request(tab.linkedBrowser.messageManager,
					'screenshot',
					{
					    format: format,
					    privacy: KanColleUtils.getBoolPref("screenshot.mask-name"),
					},
					function(header, msg) {
					    if (msg.error)
						reject(new Error(msg.error));
					    else {
						resolve(Components.classes['@mozilla.org/network/io-service;1']
							.getService(Components.interfaces.nsIIOService)
							.newURI(msg.data.url, null, null));
					    }
					}
	    );
	});
    },

    /**
     * スクリーンショット撮影
     */
    takeScreenshot: function(){
	let that = this;
	let isjpeg = KanColleUtils.getBoolPref("screenshot.jpeg");

	this._PtakeScreenshot(isjpeg)
	    .then(function(url) {
		let ret;
		let defaultdir = KanColleUtils.getUnicharPref("screenshot.path");
		let nsIFilePicker = Components.interfaces.nsIFilePicker;
		let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
		let file = null;

		fp.init(window, "保存ファイルを選んでください", nsIFilePicker.modeSave);
		if (defaultdir) {
		    let file = KanColleTimerUtils.file.initWithPath(defaultdir);
		    if (file.exists() && file.isDirectory())
			fp.displayDirectory = file;
		}
		fp.appendFilters(nsIFilePicker.filterImages);
		fp.defaultString = "screenshot-"+ that.getNowDateString() + (isjpeg?".jpg":".png");
		fp.defaultExtension = isjpeg ? "jpg" : "png";
		ret = fp.show();
		if (ret == nsIFilePicker.returnOK || ret == nsIFilePicker.returnReplace)
		    file = fp.file;

		if (!file)
		    return Promise.reject(new Error('File not selected.'));

		return new Promise(function(resolve, reject) {
		    KanColleTimerUtils.file.saveURI(file, url, function() {
			resolve();
		    });
		});
	    })
	    .catch(function(error) {
		debugprint(error);
	    });
    },

    /**
     * スクリーンショット連続撮影用フォルダ選択
     * @return nsIFile
     */
    takeScreenshotSeriographySelectFolder: function(){
	let defaultdir = KanColleUtils.getUnicharPref("screenshot.path");
	let ret;
	let nsIFilePicker = Components.interfaces.nsIFilePicker;
	let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);

	fp.init(window, "保存フォルダを選んでください", nsIFilePicker.modeGetFolder);
	if (defaultdir) {
	    let file = KanColleTimerUtils.file.initWithPath(defaultdir);
	    if (file.exists() && file.isDirectory())
		fp.displayDirectory = file;
	}
	ret = fp.show();
	if (ret != nsIFilePicker.returnOK || !fp.file)
	    return null;

	KanColleUtils.setUnicharPref("screenshot.path", fp.file.path);

	return fp.file;
    },

    /**
     * スクリーンショット連続撮影
     */
    takeScreenshotSeriography: function(){
	let that = this;
	let isjpeg = KanColleUtils.getBoolPref("screenshot.jpeg");

	this._PtakeScreenshot(isjpeg)
	    .then(function(url) {
		let file = null;
		let dir;

		dir = KanColleUtils.getUnicharPref("screenshot.path");
		if (dir)
		    file = KanColleTimerUtils.file.initWithPath(dir);

		// フォルダのチェック。フォルダでなければ、(再)選択
		do {
		    if (file && file.exists() && file.isDirectory())
			break;
		    file = that.takeScreenshotSeriographySelectFolder();
		} while(file);

		if (file)
		    file.append("screenshot-" + that.getNowDateString() + (isjpeg ? '.jpg' : '.png'));

		if (!file)
		    return Promise.reject(new Error('File not selected.'));

		return new Promise(function(resolve, reject) {
		    KanColleTimerUtils.file.saveURI(file, url, function() {
			resolve();
		    });
		});
	    })
	    .catch(function(error) {
		debugprint(error);
	    });
    },

    /**
     * 艦これタイマーを開く
     */
    open:function(){
	let feature="chrome,resizable=yes";

	let win = KanColleUtils.findMainWindow();
	if(win){
	    win.focus();
	}else{
	    let w = window.open("chrome://kancolletimer/content/mainwindow.xul","KanColleTimer",feature);
	    w.focus();
	}
    },

    _loadFonts:function(){
	let col = KanColleUtils.getUnicharPref('display.font-color') || "";
	let font = KanColleUtils.getUnicharPref("display.font") || "";
	let fontsize = KanColleUtils.getUnicharPref("display.font-size") || "";

	for (let key of ['sbKanColleTimerSidebar', 'kancolletimermainwindow', 'log']) {
	    let node = $(key);
	    if (!node)
		continue;
	    node.style.color = col;
	    node.style.fontFamily = font;
	    node.style.fontSize = fontsize;
	}
    },

    _setWallpaper: function() {
	let wallpaper = KanColleUtils.getUnicharPref('wallpaper');
	if( wallpaper ){
	    let alpha = KanColleUtils.getIntPref('wallpaper.alpha') / 100.0;
	    let sheet = document.styleSheets[1];
	    wallpaper = wallpaper.replace(/\\/g,'/');
	    let rule = "background-image: url('file://"+wallpaper+"'); opacity: "+alpha+";";
	    $('wallpaper').setAttribute('style',rule);
	    //sheet.insertRule(rule,1);
	}
    },

    observe: function(aSubject, aTopic, aData) {
	switch (aTopic) {
	case 'nsPref:changed':
	    this._loadFonts();
	    this._setWallpaper();
	    break;
	default:;
	}
    },

    _init_prefs: function() {
	if (this._pref_branch)
	    return;
	this._pref_branch = KanColleUtils.getPrefBranch();
	KanColleTimerUtils.prefs.addObserver(this._pref_branch, '', this, false);
	this.observe(null, 'nsPref:changed', null);
    },
    _exit_prefs: function() {
	if (!this._pref_branch)
	    return;
	KanColleTimerUtils.prefs.removeObserver(this._pref_branch, '', this);
	this._pref_branch = null;
    },

    init: function(){
	this._init_prefs();
	this.setWindowOnTop();
    },

    destroy: function(){
	this._exit_prefs();
    }
};


window.addEventListener("load", function(e){ KanColleTimer.init(); }, false);
window.addEventListener("unload", function(e){ KanColleTimer.destroy(); }, false);
