/* -*- mode: js2;-*- */
// vim: set ts=8 sw=4 sts=4 ff=dos :

var EXPORTED_SYMBOLS = ["KanColleRemainInfo", "KanColleDatabase"];

Components.utils.import("chrome://kancolletimer/content/utils.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");

/*
 * Database
 */
//
// タイムスタンプ管理
//
var Timestamp = function() {};
Timestamp.prototype = {
    _ts: 0,
    get: function() { return this._ts; },
    set: function() { this._ts = (new Date).getTime(); return this._ts; },
};

//
// ハンドラ管理
//
var Callback = function(opt) {
    this._cb = [];
    if (opt && opt.match)
	this._match = opt.match;
};
Callback.prototype = {
    _cb: null,
    _match: function(f1, o1, f2, o2) {
	return f1 == f2;
    },
    notify: function(now, data, extradata) {
	for (let i = 0; i < this._cb.length; i++) {
	    let e = this._cb[i];
	    if (e.opt === true || (e.opt && e.opt.compat))
		e.func(Math.floor(now / 1000), data, extradata);
	    else
		e.func(extradata);
	}
    },
    append: function(f, o) {
	this._cb.push({ func: f, opt: o, });
    },
    remove: function(f, o) {
	let count = 0;
	for (let i = 0; i < this._cb.length; i++) {
	    if (this._match(this._cb[i].func, this._cb[i].opt,
			    f, o)) {
		this._cb.splice(i, 1);
		i--;
	    }
	}
	return count;
    },
    flush: function() {
	let count = 0;
	while (this._cb.length > 0) {
	    let e = this._cb.shift();
	    if (e)
		count++;
	}
	return count;
    },
};

//
// データベース
//
var KanColleDB = function() {
    this._init.apply(this, arguments);
};
KanColleDB.prototype = {
    _cb: null,
    _ts: null,
    _req: null,
    _raw: null,
    _db: null,
    _keys: null,
    _pkey: null,

    timestamp: function() { return this._ts.get(); },

    prepare: function(data) {
	this._req = data;
    },

    update: function(data, extradata) {
	let now = this._ts.set();

	this._raw = data;
	this._db = null;
	this._keys = null;

	this._cb.notify(now, data, extradata);
    },

    _parse: function() {
	let hash = {};
	if (!this._raw || this._db)
	    return this._db;
	for (let i = 0; i < this._raw.length; i++)
	    hash[this._raw[i][this._pkey]] = this._raw[i];
	this._db = hash;
	this._keys = null;
	return hash;
    },

    get: function(key) {
	if (key) {
	    let db = this._parse();
	    return db ? db[key] : null;
	}
	return this._raw;
    },

    get_req: function() {
	return this._req;
    },

    _list: function() {
	if (!this._keys)
	    this._keys = Object.keys(this._parse());
	return this._keys;
    },

    list: function() {
	return this._raw ? this._list() : [];
    },

    count: function() {
	return this._raw ? this._list().length : undefined;
    },

    appendCallback: function(f, c) { this._cb.append(f, c); },
    removeCallback: function(f) { this._cb.remove(f); },

    _init: function(opt) {
	this._ts = new Timestamp;
	this._cb = new Callback;
	this._pkey = 'api_' + (opt && opt.primary_key ? opt.primary_key : 'id');
    },
};

//
// 複合データベース
//
var KanColleCombinedDB = function() {
    this._init.apply(this);
};
KanColleCombinedDB.prototype = {
    _cb: null,
    _ts: null,
    _db: null,
    _timer: null,

    // 更新タイムスタンプの取得
    timestamp: function() { return this._ts; },

    // [オーバーライド可] 依存データベースと処理の列挙。
    // 更新を知りたいデータベースをキーに、処理すべき
    // 関数(配列)を指定する。
    // 各関数の返値は、このデータベースの変更の通知方法。
    // 配列オブジェクトまたは false と評価されるもの。
    // 配列オブジェクトの場合は第0要素をextradataとする。
    // false の場合、通知されない。
    _update: null,

    // 内部実装
    _update_list: null,
    _update_init: function() {
	let _this = this;
	let _update_list;

	if (this._update_list || !this._update)
	    return;

	_update_list = {};
	for (let [k, f] in Iterator(this._update)) {
	    if (!Array.isArray(f))
		f = [ f ];
	    _update_list[k] = f.map(function(_f) {
					return function(extra) {
					    let _ret = _f.apply(_this, [extra]);
					    if (_ret)
						_this._cb.notify.apply(_this._cb, [_this._ts, _this.get()].concat(_ret));
					};
				    });
	};
	this._update_list = _update_list;
    },

    // 通知用タイマー
    _alarm_prototype: {
	_owner: null,
	_data: null,
	_notice: 60000,
	set_timeout: function(id, timeout) {
	    let changed = 0;
	    if (!this._data[id])
		this._data[id] = {};
	    if (this._data[id].timeout != timeout) {
		debugprint('timeout: ' + this._data[id].timeout + ' => ' + timeout);
		changed = 1
	    }
	    this._data[id].timeout = timeout;
	    return changed;
	},
	get_state: function(id) {
	    return (this._data[id] || {}).state;
	},
	update: function(force) {
	    let now = (new Date).getTime();
	    let next = Number.POSITIVE_INFINITY;
	    let list = this._owner.list();
	    let forces = (force ? (Array.isArray(force) ? force : list) : []).reduce(function(p,c) {
		p[c] = true;
		return p;
	    }, {});
	    for (let k of list) {
		let t = (this._data[k] || {}).timeout;
		let state = (this._data[k] || {}).state || -1;

		debugprint('id = ' + k + ', timeout = ' + t + ', old state = ' + state);

		if (isNaN(t)) {
		    if (state > 0 || forces[k]) {
			// notify
			if (state < 3)
			    this._notify(k, 3);
			state = -1;
			this._notify(k, state);
		    }
		} else if (t <= now) {
		    if (state < 3 || forces[k]) {
			// notify
			state = 3;
			this._notify(k, state);
		    }
		} else if (t - this._notice <= now) {
		    if (state < 2 || forces[k]) {
			// notify
			state = 2;
			this._notify(k, state);
		    }
		    if (next > t)
			next = t;
		} else {
		    if (state < 1 || forces[k]) {
			state = 1;
			this._notify(k, state);
		    }
		    if (next > t - this._notice)
			next = t - this._notice;
		}
		if (forces[k] || this._data[k].state != state)
		    debugprint('=> state = ' + state);
		this._data[k].state = state;
	    }
	    this._stop();
	    debugprint('now = ' + now + ', next = ' + next);
	    this._start(next)
	},
	reset: function() {
	    this._stop();
	    this._data = {};
	},
	init: function(_this, params) {
	    this._owner = _this;
	    this._data = {};
	},
	exit: function() {
	    this._stop();
	    this._data = null;
	},
	_start: function(next) {
	    if (next != Number.POSITIVE_INFINITY)
		this._timer = KanColleTimerUtils.timer.startDelayedEventAt(this._owner, next);
	},
	_stop: function() {
	    KanColleTimerUtils.timer.stop(this._timer);
	    this._timer = null;
	},
	_notify: function(id, state) {
	    let conf = this._owner._timer_notify_set(id, state);
	    let timeout = 0;

	    debugprint(`_notify(id=${ id }, state=${ state }): conf=${ JSON.stringify(conf) }`);
	    if (conf.popup) {
		let callback_notify = conf.popup_click_url ? {
		    observe: function(subject, topic, data) {
			if (topic != 'alertclickcallback')
			    return;
			if (!KanColleTimerUtils.window.selectTab(conf.popup_click_url))
			    KanColleTimerUtils.window.openTab(conf.popup_click_url, true)
		    },
		} : null;
		KanColleTimerUtils.alert.show(conf.icon || KanColleUtils.IconURL,
					      conf.title || KanColleUtils.Title,
					      conf.msg, !!callback_notify,
					      conf.msg, callback_notify);
	    }
	    if (conf.sound)
		KanColleTimerUtils.sound.play(conf.sound);

	    if (conf.ntfy_prio && conf.ntfy_prio > 0) {
		let ntfy_uri = KanColleUtils.getUnicharPref('ntfy.uri', '');
		let ntfy_topic = KanColleUtils.getUnicharPref('ntfy.topic', '');
		let obj = {
		    'topic': ntfy_topic,
		    'message': conf.msg,
		    'title': conf.title || KanColleUtils.Title,
		    'priority': conf.ntfy_prio || 3,
		    'tags': conf.ntfy_tags || [],
		};
		debugprint(`ntfy: url=${ ntfy_uri }, obj=${ JSON.stringify(obj) }`);
		if (ntfy_uri != '' && ntfy_topic != '') {
		    let xhr = KanColleTimerUtils.net.createXMLHttpRequest();
		    KanColleUtils.sendObject(xhr, 'POST', ntfy_uri, obj);
		}
	    }

	    if (state == 1 || state == 2)
		timeout = this._data[id].timeout;
	    else if (state == 3)
		timeout = 0;
	    else
		timeout = Number.NaN;

	    this._owner._cb.notify(this._owner._ts, this._owner.get(),
				   {
					id: id,
					state: state,
					message: conf.msg,
					time: timeout,
				   });
	},
    },

    // [オーバーライド可] データベースを得る。
    get: function() { return this._db; },

    // 通知の追加削除
    appendCallback: function(f, c) { this._cb.append(f, c); },
    removeCallback: function(f) { this._cb.remove(f); },

    // 初期化
    init: function() {
	if (this._preinit_hook)
	    this._preinit_hook();
	if (!this._update_list) {
	    this._update_init();
	    for (let [k,fs] in Iterator(this._update_list)) {
		if (!KanColleDatabase[k]) {
		    debugprint('KanColleDatabase["' + k + '"] is not initialized yet.  Please fix it.');
		    continue;
		}
		for (let f of fs)
		    KanColleDatabase[k].appendCallback(f);
	    }
	}
	this._alarm.init(this);
	if (this._postinit_hook)
	    this._postinit_hook();
    },

    // 後始末
    exit: function() {
	if (this._preexit_hook)
	    this._preexit_hook();
	this._alarm.exit();
	if (this._update_list) {
	    for (let [k,fs] in Iterator(this._update_list)) {
		if (!KanColleDatabase[k])
		    continue;
		for (let f of fs)
		    KanColleDatabase[k].removeCallback(f);
	    }
	    this._update_list = null;
	}
	if (this._postexit_hook)
	    this._postexit_hook();
    },

    // コンストラクタ
    _init: function() {
	this._ts = 0;
	this._cb = new Callback;
	this._alarm = Object.create(this._alarm_prototype);
    },
};

/**
 * 母港データベース。
 * 主に泊地修理タイマー、資源/士気更新時間を管理する。
 */
var KanCollePortDB = function() {
    this._init();

    this._factor = {
	fuel: 3,
	bullet: 3,
	steel: 3,
	bauxite: 1,
    };

    this._db = {
	repair_timer: Number.NaN,
	now: null,
	last: null,
    };

    this._update = {
	memberBasic: function() {
	    let notify = this._db.repair_timer_notify;
	    delete this._db.repair_timer_notify;
	    return notify;
	},
	material: function() {
	    let that = this;
	    let now = this._db.now;
	    let last = this._db.last && this._db.last.material;
	    let list = KanColleDatabase.material.list();
	    if (!now)
		return;
	    if (!KanColleDatabase.material.timestamp())
		return;
	    this._db.now.material = list.map(function(e) {
		return KanColleDatabase.material.get(e);
	    });
	    if (!last)
		return;
	    this._db.now.material_cycles =
		Math.max.apply(Math, Object.keys(this._db.now.material).map(function(e) {
				let factor = that._factor[e];
				return factor ? Math.ceil((that._db.now.material[e] - that._db.last.material[e]) / factor) : 0;
			       }));
	    this._countcycles();
	},
	ship: function() {
	    let that = this;
	    let now = this._db.now;
	    let last = this._db.last && this._db.last.ship;
	    let list = KanColleDatabase.ship.list();
	    if (!now)
		return;
	    if (!KanColleDatabase.ship.timestamp())
		return;
	    this._db.now.ship = list.map(function(e) {
		let ship = KanColleDatabase.ship.get(e);
		if (!ship)
		    return;
		return {
		    // condと泊地修理
		    api_id: ship.api_id,
		    api_cond: ship.api_cond,
		    api_nowhp: ship.api_nowhp,
		    api_maxhp: ship.api_nowhp,
		};
	    }).filter(function(e) {
		return e !== null;
	    }).reduce(function(p,c) {
		p[c.api_id] = c;
		return p;
	    }, {});
	    if (!last)
		return;
	    this._db.now.ship_cycles =
		Math.max.apply(Math, Object.keys(that._db.now.ship).map(function(e) {
				/* 修理から戻ると cond が 40 になるのを誤認識
				 * するのを避けるため、40未満同士、または 40以上同士
				 * のみに限定
				 */
				if ((that._db.last.ship[e].api_cond < 40 && that._db.last.ship[e].api_cond < 40) ||
				    (that._db.last.ship[e].api_cond >= 40 && that._db.last.ship[e].api_cond >= 40)) {
				    return Math.ceil((that._db.now.ship[e].api_cond - that._db.last.ship[e].api_cond) / 3);
				} else {
				    return 0;
				}
			       }));
	    this._countcycles();
	},
	deck: function(extradata) {
	    let time = (new Date).getTime();
	    // extradata: true => reset repair timer
	    if (!extradata || !extradata.restart_repair_timer)
		return;
	    debugprint('(re)starting repair timer.');
	    this._db.repair_timer = time;
	    return [];
	},
    };

    this._countcycles = function() {
	if (!this._db.now.elapsed_time) {
	    let time_offset = KanColleUtils.getIntPref('time-offset', Number.NaN);
	    let time_sub = !isNaN(time_offset) ? (this._db.last.time - time_offset) % 180000 : 0;
	    let last_time = this._db.last.time - time_sub;
	    this._db.now.elapsed_time = this._db.now.time - last_time;
	    this._db.now.elapsed_cycles = Math.floor(this._db.now.elapsed_time / 180000);
	    this._db.now.elapsed_reminder = this._db.now.elapsed_time % 180000;
	    debugprint('time offset: ' + time_offset);
	    debugprint('Port: ' +
		       this._db.now.elapsed_time + ' msec [' +
		       this._db.now.elapsed_cycles + ' + ' +
		       this._db.now.elapsed_reminder + ' msec]');
	}
	if (isNaN(this._db.now.material_cycles) &&
	    isNaN(this._db.now.ship_cycles)) {
	    this._db.now.cycles = Number.NaN;
	} else {
	    this._db.now.cycles = Math.max(
		isNaN(this._db.now.material_cycles) ? 0 : this._db.now.material_cycles,
		isNaN(this._db.now.ship_cycles) ? 0 : this._db.now.ship_cycles
	    );
	}
	debugprint('cycles = ' + this._db.now.cycles);
	if (this._db.now.cycles > this._db.now.elapsed_cycles) {
	    let time_offset = this._db.now.time % 180000;
	    debugprint('time offset (new): ' + time_offset);
	    KanColleUtils.setIntPref('time-offset', time_offset);
	}
    };

    this.mark = function(path) {
	let now = (new Date).getTime();
	if (path === null) {
	    // port: set
	    let last;
	    //debugprint('set');
	    this._ts = now;
	    last = this._db.now;
	    this._db.now = { time: this._ts };
	    this._db.last = last;
	    // 泊地修理のチェック
	    if (isNaN(this._db.repair_timer)) {
		this._db.repair_timer = now;
		this._db.repair_timer_notify = [];
	    } else {
		debugprint('repair timer = ' + (now - this._db.repair_timer) + 'msec');
		if (now - this._db.repair_timer >= 1200000) {
		    debugprint('reset repair timer.');
		    this._db.repair_timer = now;
		    this._db.repair_timer_notify = [];
		}
	    }
	} else if (path.match(/\/kcsapi\/api_member\/(record|preset_deck|picture_book|payitem|questlist|ndock)/)) {
	    // safe API, just ignore
	} else {
	    // not port: unset
	    //debugprint('reset');
	    this._ts = now;
	    this._db.last = this._db.now = null;
	}
    };

    this.get = function() {
	let data = {};
	if (this._db.last) {
	    data.time = this._db.last.time;
	    data.elapsed_time = this._db.last.elapsed_time;
	    data.elapsed_cycles = this._db.last.elapsed_cycles;
	    data.elapsed_reminder = this._db.last.elapsed_reminder;
	}
	data.repair_timer = this._db.repair_timer;
	return data;
    };
};
KanCollePortDB.prototype = new KanColleCombinedDB();

/**
 * 記録データベース。
 * 司令部レベル、艦船/装備数を管理する。
 */
var KanColleRecordDB = function() {
    this._init();

    this._db = null;
    this._special_slotitems = [];

    this._deepcopy = function(force) {
	if (!this._db || force) {
	    let d = KanColleDatabase.memberRecord.get();
	    if (d)
		this._db = JSON.parse(JSON.stringify(d));
	    else {
		this._db = {
		    api_ship: [],
		    api_slotitem: [],
		    api_level: undefined,
		};
	    }
	}
	return this._db;
    };

    this._update = {
	memberRecord: function() {
	    let t = KanColleDatabase.memberRecord.timestamp();

	    this._deepcopy(true);

	    this._ts = t;
	    return [];
	},

	memberBasic: function() {
	    let t = KanColleDatabase.memberBasic.timestamp();
	    let d = KanColleDatabase.memberBasic.get();

	    this._deepcopy();

	    this._db.api_ship[1] = d.api_max_chara;
	    this._db.api_slotitem[1] = d.api_max_slotitem;
	    this._db.api_level = d.api_level;

	    this._ts = t;
	    return [];
	},

	masterSlotitem: function() {
	    const special_slotitems = [
		23, // 応急修理要員
		43, // 戦闘糧食
		44, // 補給物資
	    ];
	    this._special_slotitems = KanColleDatabase.masterSlotitem.list()
					.map(function(id) {
					    return KanColleDatabase.masterSlotitem.get(id);
					})
					.filter(function(e) { return special_slotitems.some(function(type) { return e.api_type[2] == type; }); })
					.map(function(e) { return e.api_id; });
	    return [];
	},

	ship: function() {
	    let t = KanColleDatabase.ship.timestamp();
	    let n = KanColleDatabase.ship.count();

	    this._deepcopy();

	    this._db.api_ship[0] = n;

	    this._ts = t;
	    return [];
	},

	slotitem: function() {
	    let t = KanColleDatabase.slotitem.timestamp();
	    let n = KanColleDatabase.slotitem.count();
	    let n2 = this._special_slotitems
			.reduce(function(p,c) { return p + KanColleDatabase.slotitem.count(c); }, 0);

	    this._deepcopy();

	    this._db.api_slotitem[0] = n - n2;

	    this._ts = t;

	    return [];
	},
    };

    this.get = function() {
	return this._deepcopy();
    };
};
KanColleRecordDB.prototype = new KanColleCombinedDB();

/**
 * 資源データベース。
 * 資源/資材を管理する。
 */
var KanColleMaterialDB = function() {
    this._init();

    this._db = {
	fuel: Number.NaN,
	bullet: Number.NaN,
	steel: Number.NaN,
	bauxite: Number.NaN,
	burner: Number.NaN,
	bucket: Number.NaN,
	devkit: Number.NaN,
	screw: Number.NaN,
    };
    this._log = null;

    this._update = {
	memberBasic: function() {
	    this._readResourceData();
	    return [];
	},
	memberMaterial: function() {
	    let t = KanColleDatabase.memberMaterial.timestamp();
	    let keys = {
		fuel: 1,
		bullet: 2,
		steel: 3,
		bauxite: 4,
		burner: 5,
		bucket: 6,
		devkit: 7,
		screw: 8,
	    };
	    let ok = 0;

	    for (let k in keys) {
		let d = KanColleDatabase.memberMaterial.get(keys[k]);
		if (typeof(d) != 'object' || d == null)
		    continue;
		this._update_resource_one(t, k, d.api_value, false);
		ok++;
	    }

	    if (!ok)
		return;

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqAirCorpsSetPlane: function() {
	    let t = KanColleDatabase.reqAirCorpsSetPlane.timestamp();
	    let data = KanColleDatabase.reqAirCorpsSetPlane.get();

	    if (!this._ts)
		return;

	    // change_deployment_base does not consume bauxite
	    if (data.api_after_bauxite === undefined)
		return;

	    this._update_resource_one(t, 'bauxite', data.api_after_bauxite, false);

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqAirCorpsSupply: function() {
	    let t = KanColleDatabase.reqAirCorpsSupply.timestamp();
	    let data = KanColleDatabase.reqAirCorpsSupply.get();

	    if (!this._ts)
		return;

	    this._update_resource_one(t, 'fuel',    data.api_after_fuel,    false);
	    this._update_resource_one(t, 'bauxite', data.api_after_bauxite, false);

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqHokyuCharge: function() {
	    let t = KanColleDatabase.reqHokyuCharge.timestamp();
	    let data = KanColleDatabase.reqHokyuCharge.get();

	    if (!this._ts)
		return;

	    let data_api_material = data.api_after_material || data.api_material;
	    this._update_resource_one(t, 'fuel',    data_api_material[0], false);
	    this._update_resource_one(t, 'bullet',  data_api_material[1], false);
	    this._update_resource_one(t, 'steel',   data_api_material[2], false);
	    this._update_resource_one(t, 'bauxite', data_api_material[3], false);

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqNyukyoStart: function() {
	    let t = KanColleDatabase.reqNyukyoStart.timestamp();
	    let req = KanColleDatabase.reqNyukyoStart.get_req();

	    if (!this._ts || isNaN(req._highspeed) || !req._highspeed)
		return;

	    this._update_resource_one(t, 'bucket', 1, -1);

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqKousyouCreateShipSpeedChange: function() {
	    let t = KanColleDatabase.reqKousyouCreateShipSpeedChange.timestamp();
	    let req = KanColleDatabase.reqKousyouCreateShipSpeedChange.get_req();
	    let kdock;
	    let delta_burner = 1;

	    if (!this._ts || isNaN(req._kdock_id))
		return;

	    kdock = KanColleDatabase.kdock.get(req._kdock_id);
	    if (kdock &&
		(kdock.api_item1 >= 1000 || kdock.api_item2 >= 1000 ||
		 kdock.api_item3 >= 1000 || kdock.api_item4 >= 1000 ||
		 kdock.api_item5 > 1)) {
		// 大型建造
		delta_burner = 10;
	    }

	    this._update_resource_one(t, 'burner', delta_burner, -1);

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqKousyouRemodelSlot: function() {
	    let t = KanColleDatabase.reqKousyouRemodelSlot.timestamp();
	    let data = KanColleDatabase.reqKousyouRemodelSlot.get();

	    if (!this._ts || !data)
		return;

	    this._update_resource_one(t, 'fuel',    data.api_after_material[0], false);
	    this._update_resource_one(t, 'bullet',  data.api_after_material[1], false);
	    this._update_resource_one(t, 'steel',   data.api_after_material[2], false);
	    this._update_resource_one(t, 'bauxite', data.api_after_material[3], false);
	    this._update_resource_one(t, 'burner',  data.api_after_material[4], false);
	    this._update_resource_one(t, 'bucket',  data.api_after_material[5], false);
	    this._update_resource_one(t, 'devkit',  data.api_after_material[6], false);
	    this._update_resource_one(t, 'screw',   data.api_after_material[7], false);

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},

	reqNyukyoSpeedChange: function() {
	    let t = KanColleDatabase.reqNyukyoStart.timestamp();

	    if (!this._ts)
		return;

	    this._update_resource_one(t, 'bucket', 1, -1);

	    this._ts = t;
	    this._updateResourceData();
	    return [];
	},
    };

    this._update_resource_one = function(t, name, value, relative) {
	if (!t)
	    return;

	let v = this._db[name];

	if (relative) {
	    if (isNaN(v))
		return;
	    v += relative * value;
	} else {
	    v = value;
	}

	this._db[name] = v;
    };

    this._updateResourceData = function() {
	let now = Math.floor(KanColleDatabase.material.timestamp() / 1000);
	let res = this.get_log();
	let last_data = res[ res.length-1 ];
	let data = new Object();
	let resnames = {
	    fuel: 1,
	    bullet: 2,
	    steel: 3,
	    bauxite: 4,
	    bucket: 6,
	};
	let count = 0;
	let flush = false;

	if (!now)
	    return;

	for (let k in resnames) {
	    let v = KanColleDatabase.material.get(k);
	    if (isNaN(v))
		continue;
	    data[k] = v;
	    if (!res.length || last_data[k] != data[k])
		count++;
	    if (!res.length || (!last_data[k] && !data[k]) ||
		Math.abs(last_data[k] - data[k]) / Math.max(last_data[k], data[k]) > 0.001)
		flush = true;
	}

	data.recorded_time = now; // 記録日時

	if (count)
	    res.push( data );

	this._flushResourceData(flush);
    };
    this._readResourceData = function(){
	let data = KanColleUtils.getBoolPref('record.resource-history') ?
		    KanColleUtils.readObject('resourcehistory', []) : [];
	let d = this._log;

	if (d) {
	    let t1 = data.length && data[ data.length-1 ].recorded_time;
	    let t2 = d.length && d[ d.length-1 ].recorded_time;
	    if (t2 >= t1)
		return;
	}
	this._log = data;
    };
    this._writeResourceData = function(){
	let data = this._log;

	if (!KanColleUtils.getBoolPref('record.resource-history'))
	    return;

	if( data.length > 15000 ){
	    // 自然回復が一日480回あるので、それを最低1ヶ月分記録するとしたら
	    // 15000件保存できればいいので。
	    data = data.slice(-15000);
	}
	KanColleUtils.writeObject('resourcehistory', data );
    };
    this._flushResourceData = function(forced) {
	let ts_mm = KanColleDatabase.memberMaterial.timestamp(),
	    ts_m = KanColleDatabase.material.timestamp();
	if (!ts_mm || !ts_m || ts_mm != ts_m)
	    return;
	if (!forced && this._last_flush &&
	    ts_m - this._last_flush <= 1800000)
	    return;
	this._last_flush = ts_m;
	this._writeResourceData();
    };

    this.list = function() {
	return Object.keys(this._db);
    };
    this.get = function(key) { return this._db[key]; };
    this.get_log = function() {
	if (!this._log)
	    this._readResourceData();
	return this._log;
    };
};
KanColleMaterialDB.prototype = new KanColleCombinedDB();

/**
 * 艦船データベース。
 * 所持各艦船を管理する。
 * member/ship[23] をもとに、他の操作による更新を追跡。
 * 新たに入手した艦船のファイルへの記録も行う。
 */
var KanColleShipDB = function() {
    this._init();

    this._db = {
	ship: null,
	dead: {},   // 艦船解体/改装時において、消滅する艦船の装備を
		    // 削除する必要がある。艦船を先に削除してしまうと
		    // 装備がわからなくなってしまうので、完全には削除
		    // せず、暫時IDで検索できるようにする。
		    // 次の全体更新で完全削除。
	list: null,
	ndock: null,
	slotitem: null,
    };

    this._deepcopy = function() {
	if (!this._db.ship) {
	    let ships = KanColleDatabase._memberShip2.list();
	    if (!ships)
		return;

	    this._db.ship = new Object;

	    for (let i = 0; i < ships.length; i++)
		this._db.ship[ships[i]] = JSON.parse(JSON.stringify(KanColleDatabase._memberShip2.get(ships[i])));

	    this._db.list = Object.keys(this._db.ship);

	    //debugprint('hash: ' + this._db.ship.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._update = {
	_memberShip2: function() {
	    this._ts = KanColleDatabase._memberShip2.timestamp();
	    this._db.ship = null;
	    this._db.list = null;
	    this._db.slotitem = null;
	    this._db.dead = {};
	    this._deepcopy();
	    this._save();
	    return [];
	},

	// リクエストに api_shipid が含まれる場合のみ呼ばれる
	//  XXX: リクエストの api_shipid と整合性を確認すべき?
	_memberShip3: function() {
	    let t = KanColleDatabase._memberShip3.timestamp();
	    let data = KanColleDatabase._memberShip3.get();

	    if (!this._ts)
		return;

	    this._deepcopy();

	    for (let i = 0; i < data.length; i++) {
		let ship_id = data[i].api_id;
		this._db.ship[ship_id] = data[i];
	    }

	    this._db.list = null;
	    this._db.slotitem = null;
	    this._db.dead = {};

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqHokyuCharge: function() {
	    let t = KanColleDatabase.reqHokyuCharge.timestamp();
	    let data = KanColleDatabase.reqHokyuCharge.get().api_ship;

	    if (!this._ts)
		return;

	    this._deepcopy();

	    // Update
	    for (let i = 0; i < data.length; i++) {
		let ship_id = data[i].api_id;
		for (let k in data[i])
		    this._db.ship[ship_id][k] = data[i][k];
	    }

	    this._ts = t;
	    this._save();

	    // Notification
	    return [];
	},

	reqHenseiLock: function() {
	    let t = KanColleDatabase.reqHenseiLock.timestamp();
	    let req = KanColleDatabase.reqHenseiLock.get_req();
	    let data = KanColleDatabase.reqHenseiLock.get();

	    if (!this._ts || isNaN(req._ship_id))
		return;

	    this._deepcopy();

	    this._db.ship[req._ship_id].api_locked = data.api_locked;

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqKaisouPowerup: function() {
	    let t = KanColleDatabase.reqKaisouPowerup.timestamp();
	    let req = KanColleDatabase.reqKaisouPowerup.get_req();

	    if (!this._ts || !req._id_items)
		return;

	    this._deepcopy();

	    this._delete_ships(req._id_items);
	    this._db.list = null;
	    this._db.slotitem = null;

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqKaisouSlotExchangeIndex: function() {
	    let data = KanColleDatabase.reqKaisouSlotExchangeIndex.get();
	    let t = KanColleDatabase.reqKaisouSlotExchangeIndex.timestamp();
	    let req = KanColleDatabase.reqKaisouSlotExchangeIndex.get_req();
	    let ship;

	    if (!this._ts || isNaN(req._id))
		return;

	    this._deepcopy();

	    ship = this._db.ship[req._id];
	    if (!ship)
		return;

	    ship.api_slot = (data.api_ship_data || data).api_slot.slice();

	    this._db.slotitem = null;

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqKousyouDestroyShip: function() {
	    let t = KanColleDatabase.reqKousyouDestroyShip.timestamp();
	    let req = KanColleDatabase.reqKousyouDestroyShip.get_req();
	    let fleet;

	    if (!this._ts || !req._ship_ids.length)
		return;

	    this._ts = KanColleDatabase.reqKousyouDestroyShip.timestamp();

	    this._deepcopy();

	    this._delete_ships(req._ship_ids);
	    this._db.list = null;
	    this._db.slotitem = null;

	    this._ts = t;

	    return [];
	},

	reqKousyouGetShip: function() {
	    let t = KanColleDatabase.reqKousyouGetShip.timestamp();
	    let data = KanColleDatabase.reqKousyouGetShip.get().api_ship;
	    let ship;

	    if (!this._ts)
		return;

	    this._deepcopy();

	    this._db.ship[data.api_id] = data;
	    this._db.list = null;
	    this._db.slotitem = null;

	    ship = KanColleDatabase.masterShip.get(data.api_ship_id);

	    this.recordNewShip(t,
			       'Created', '',
			       KanColleDatabase.masterStype.get(ship.api_stype).api_name,
			       ship.api_name, '');

	    this._ts = t;
	    this._save();
	    return [];
	},

	reqNyukyoStart: function() {
	    let t = KanColleDatabase.reqNyukyoStart.timestamp();
	    let req = KanColleDatabase.reqNyukyoStart.get_req();
	    let ship;

	    if (!this._ts ||
		isNaN(req._ndock_id) || isNaN(req._ship_id) || isNaN(req._highspeed))
		return;

	    this._deepcopy();

	    ship = this._db.ship[req._ship_id];
	    if (!ship)
		return;

	    // 高速修復 または 1分以下
	    if (req._highspeed || ship.api_ndock_time <= 60000)
		this._repair_ship(ship);

	    this._ts = t;
	    this._save();

	    return [];
	},

	ndock: function() {
	    let t = KanColleDatabase.ndock.timestamp();
	    let notify = false;

	    if (!this._db.ndock) {
		let dock = KanColleDatabase.ndock.list();
		this._db.ndock = {};
		for (let i = 0; i < dock.length; i++) {
		    this._db.ndock[dock[i]] = JSON.parse(JSON.stringify(KanColleDatabase.ndock.get(dock[i])));
		}

		return;
	    }

	    if (!this._ts)
		return;

	    for (let dock in this._db.ndock) {
		let ndock = KanColleDatabase.ndock.get(dock);
		let shipid;
		let ship = null;

		if (ndock &&
		    this._db.ndock[dock].api_state == 1 &&
		    ndock.api_state == 0) {

		    // 修復完了(api_state: 1 => 0)
		    this._deepcopy();

		    shipid = this._db.ndock[dock].api_ship_id;
		    ship = this._db.ship[shipid];
		    if (!ship) {
			debugprint('ship not found: ' + shipid)
			continue;
		    }

		    this._repair_ship(ship);
		    notify = true;
		}

		this._db.ndock[dock] = JSON.parse(JSON.stringify(ndock));
	    }

	    if (!notify)
		return;

	    this._ts = t;
	    this._save();

	    return [];
	},

	battle: function(state) {
	    let that = this;
	    let t = KanColleDatabase.battle.timestamp();
	    let decks = KanColleDatabase.battle.list();
	    let notify = false;

	    if (state != -2)
		return;

	    this._deepcopy();

	    decks.forEach(function(e) {
		let deck = KanColleDatabase.deck.get(e);
		let battle = KanColleDatabase.battle.get(e);
		let ship;

		if (!deck || !battle || !battle.consumed)
		    return;

		ship = that._db.ship[deck.api_ship[0]];
		if (!ship) {
		    debugprint('no ship: ' + deck.api_ship[0]);
		    return;
		}

		Object.keys(battle.consumed).forEach(function(itemid) {
		    if (ship.api_slot_ex == itemid) {
			ship.api_slot_ex = -1;
			notify = true;
		    } else {
			let idx = ship.api_slot.indexOf(itemid);
			if (idx >= 0) {
			    ship.api_slot = ship.api_slot.splice(idx, 1);
			    ship.api_slot.push(-1);
			    notify = true;
			}
		    }
		    if (notify)
			ship.api_nowhp = battle.consumed[itemid].nowhp;
		});
	    });

	    if (!notify)
		return;

	    this._ts = t;
	    this._save();

	    return [];
	},

	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [];
	},
    };

    this._delete_ships = function(ids) {
	let count = 0;
	if (ids) {
	    for (let i = 0; i < ids.length; i++) {
		let id = ids[i];
		if (!this._db.ship[id])
		    continue;
		debugprint('deleting ship: ' + id);
		this._db.dead[id] = this._db.ship[id];
		delete(this._db.ship[id]);
		count++;
	    }
	}
	return count;
    };

    this._repair_ship = function(ship) {
	// HP, Condなどを補正
	ship.api_nowhp = ship.api_maxhp;
	ship.api_ndock_time = 0;
	for (let i = 0; i < ship.api_ndock_item.length; i++)
	    ship.api_ndock_item[i] = 0;
	if (ship.api_cond < 40)
	    ship.api_cond = 40;
    };

    this._recordNewShip = function(time, area, enemy, stype_name, ship_name, win_rank) {
	let file = KanColleUtils.getDataFile('getship.dat');
	let writer = KanColleTimerUtils.file.openWriter(file,
							FileUtils.MODE_WRONLY|FileUtils.MODE_CREATE|FileUtils.MODE_APPEND,
							0);
	let line = [ area, enemy, stype_name, ship_name, time / 1000, win_rank ]
		    .map(function(s) {
			s = ("" + s).replace(/[\r\n]/g, ' ');
			if (s.match(/[",]/)) {
			    return '"' + s.replace(/"/g, '""') + '"';
			} else
			    return s;
		    })
		    .join(',');
	writer.writeString(line + '\n');
	writer.close();
    };
    this.recordNewShip = function(time, area, enemy, stype_name, ship_name, win_rank) {
	if (!KanColleUtils.getBoolPref('record.ships'))
	   return;
	this._recordNewShip(time, area, enemy, stype_name, ship_name, win_rank);
    };

    this._save = function() {
	let that = this;
	let hash;

	if (!this._ts || !KanColleUtils.getBoolPref('record.internalstate'))
	    return;

	// shipは大きすぎるので、DeckDB, NdockDB,PresetDB に
	// 関連するものに限定する
	hash = Object.keys(this._db.ship)
	    .map(function(e) { return that._db.ship[e]; })
	    .filter(function(e) {
		// Check DeckDB
		if (KanColleDatabase.deck.lookup(e.api_id))
		    return true;
		// Check Ndock
		if (KanColleDatabase.ndock.find(e.api_id))
		    return true;
		if (KanColleDatabase.preset.lookup(e.api_id))
		    return true;
		return false;
	    }).reduce(function(p,c) {
		p[c.api_id] = c;
		return p;
	    }, {});

	debugprint('ship = ' + (Object.keys(this._db.ship).length) + ', draft = ' + (Object.keys(hash).length));

	KanColleUtils.writeObject('ship', {
	    hash: hash,
	    ndock: this._db.ndock,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.getBoolPref('record.internalstate') && KanColleUtils.readObject('ship', null);
	if (!obj)
	    return;
	// We will get real data later.
	this._db.ship = null;
	this._db.dead = obj.hash;
	this._db.list = null;
	this._db.slotitem = null;
	this._db.fleet = {};
	this._db.ndock = obj.ndock;
	// We do NOT update timestamp here.
	//this._ts = obj._ts;
	return true;
    };

    this.get = function(id, key) {
	if (key == null) {
	    let ret;
	    if (this._db.ship) {
		ret = this._db.ship[id];
		if (!ret)
		    ret = this._db.dead[id];
	    } else
		ret = KanColleDatabase._memberShip2.get(id);
	    return ret;
	}
    };

    this.list = function() {
	if (!this._db.ship)
	    return KanColleDatabase._memberShip2.list();
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.ship);
	return this._db.list;
    };

    this.find_slotitem = function(itemid) {
	let id;
	if (!this._db.slotitem) {
	    let list = this.list();
	    if (!list)
		return;
	    this._db.slotitem = {};
	    for (id of list) {
		let ship = this.get(id);
		let ship_slot = ship.api_slot.slice();
		if (ship.api_slot_ex)
		    ship_slot.push(ship.api_slot_ex);
		for (item of ship_slot) {
		    if (item < 0)
			continue;
		    this._db.slotitem[item] = id;
		}
	    }
	}
	return this._db.slotitem[itemid];
    };

    this.count = function() {
	if (!this._db.ship)
	    return KanColleDatabase._memberShip2.count();
	return this.list().length;
    };
};
KanColleShipDB.prototype = new KanColleCombinedDB();

/**
 * デッキデータベース。
 * 主に艦隊編成、遠征状態を管理。
 */
var KanColleDeckDB = function() {
    this._init();

    this._db = {
	fleet: {},
	deck: null,
	list: null,
	mission: {},
	timer: {},
    };

    this._update_fleet = function() {
	let db = {};
	let ids = this.list();
	for (let i = 0; i < ids.length; i++) {
	    let deck = this.get(ids[i]);
	    for (let j = 0; j < deck.api_ship.length; j++) {
		if (deck.api_ship[j] < 0)
		    continue;
		db[deck.api_ship[j]] = {
		    fleet: deck.api_id,
		    pos: j,
		};
	    }
	}
	this._db.fleet = db;
    };

    this._check_repair_ship = function(deckid) {
	let deck = this._db.deck[deckid];
	let shipid = deck.api_ship[0];
	let ship = shipid >= 0 ? KanColleDatabase.ship.get(shipid) : null;
	let shiptype = ship ? KanColleDatabase.masterShip.get(ship.api_ship_id) : null;
	if (!shiptype)
	    return false;
	return shiptype.api_stype == 19;	//工作艦
    };

    this._deepcopy = function() {
	if (!this._db.deck) {
	    let decks = KanColleDatabase.memberDeck.list();
	    if (!decks)
		return;

	    this._db.deck = new Object;

	    for (let i = 0; i < decks.length; i++)
		this._db.deck[decks[i]] = JSON.parse(JSON.stringify(KanColleDatabase.memberDeck.get(decks[i])));

	    this._db.list = Object.keys(this._db.deck);

	    //debugprint('hash: ' + this._db.deck.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._update = {
	memberDeck: function() {
	    this._ts = KanColleDatabase.memberDeck.timestamp();
	    this._db.deck = null;
	    this._db.list = null;
	    this._update_fleet();
	    this._deepcopy();
	    this._update_mission();
	    if (this._update_timeout(this._ts, this.list()))
		this._alarm.update();
	    this._save();
	    return [{}];
	},
	__memberDeck: function() {
	    let decks;

	    if (!this._ts)
		return;

	    this._ts = KanColleDatabase.__memberDeck.timestamp();
	    this._deepcopy();
	    this._db.list = null;

	    decks = KanColleDatabase.__memberDeck.list();
	    for (let i = 0; i < decks.length; i++) {
		let deck = KanColleDatabase.__memberDeck.get(decks[i]);
		this._db.deck[deck.api_id] = deck;
	    }

	    this._update_fleet();
	    if (this._update_timeout(this._ts, decks))
		this._alarm.update();
	    this._save();
	    return [{}];
	},
	reqHenseiChange: function() {
	    let notify_data = {};
	    let req = KanColleDatabase.reqHenseiChange.get_req();
	    let deck;

	    if (!this._ts ||
		isNaN(req._id) || isNaN(req._ship_id) || isNaN(req._ship_idx))
		return;

	    this._ts = KanColleDatabase.reqHenseiChange.timestamp();

	    this._deepcopy();

	    // 編成
	    //	req._id: 艦隊ID(or -1)
	    //	req._ship_idx: 艦隊でのindex(or -1)
	    //	req._ship_id: 変更後のID(or -1 or -2)

	    deck = this._db.deck[req._id];
	    if (!deck)
		return;

	    if (req._ship_id == -2) {
		// 随伴艦解除
		for (let i = 1; i < deck.api_ship.length; i++)
		    deck.api_ship[i] = -1;
	    } else if (req._ship_id == -1) {
		// 解除
		deck.api_ship.splice(req._ship_idx, 1);
		deck.api_ship.push(-1);
		if (this._check_repair_ship(req._id))
		    notify_data.restart_repair_timer = true;
	    } else if (req._ship_id >= 0) {
		// 配置換え

		// 現在艦隊に所属する艦船ID
		let ship_id = deck.api_ship[req._ship_idx];
		// 配置しようとする艦の所属艦隊
		let ship_fleet = req._ship_id >= 0 ? KanColleDatabase.deck.lookup(req._ship_id) : null;

		// 新しい艦を配置
		deck.api_ship[req._ship_idx] = req._ship_id;
		if (ship_fleet) {
		    // 所属元艦隊での処理
		    let odeck = this._db.deck[ship_fleet.fleet];
		    if (ship_id >= 0) {
			// 配置
			odeck.api_ship[ship_fleet.pos] = ship_id;
		    } else {
			// 解除
			odeck.api_ship.splice(ship_fleet.pos, 1);
			odeck.api_ship.push(-1);
		    }
		    if (ship_fleet.fleet != req._id &&
			this._check_repair_ship(ship_fleet.fleet)) {
			notify_data.restart_repair_timer = true;
		    }
		}
		if (this._check_repair_ship(req._id))
		    notify_data.restart_repair_timer = true;
	    }
	    this._update_fleet();
	    this._save();
	    return [notify_data];
	},
	reqHenseiPresetSelect: function() {
	    let t = KanColleDatabase.reqHenseiPresetSelect.timestamp();
	    let req = KanColleDatabase.reqHenseiPresetSelect.get_req();
	    let data = KanColleDatabase.reqHenseiPresetSelect.get();
	    let d;

	    if (!this._ts || isNaN(req._deck_id))
		return;

	    this._deepcopy();

	    this._db.deck[req._deck_id] = JSON.parse(JSON.stringify(data));

	    this._ts = t;
	    this._update_fleet();

	    this._save();
	    return [{}];
	},
	reqKousyouDestroyShip: function() {
	    let req = KanColleDatabase.reqKousyouDestroyShip.get_req();
	    let that = this;
	    let updated = false;

	    if (!this._ts || !req._ship_ids.length)
		return;

	    req._ship_ids.forEach(function(ship_id) {
		let fleet = KanColleDatabase.deck.lookup(ship_id);
		if (!fleet)
		    return;

		if (!updated)
		    that._deepcopy();

		that._db.deck[fleet.fleet].api_ship.splice(fleet.pos,1);
		that._db.deck[fleet.fleet].api_ship.push(-1);

		updated = true;
	    });

	    if (!updated)
		return;

	    this._ts = KanColleDatabase.reqKousyouDestroyShip.timestamp();
	    this._update_fleet();
	    this._save();
	    return [{}];
	},
	reqMemberUpdateDeckName: function() {
	    let req = KanColleDatabase.reqMemberUpdateDeckName.get_req();
	    let deck;

	    if (!this._ts || isNaN(req._deck_id))
		return;

	    this._deepcopy();

	    deck = this.get(req._deck_id);
	    if (!deck)
		return;

	    deck.api_name = req.api_name;

	    this._ts = KanColleDatabase.reqMemberUpdateDeckName.timestamp();
	    this._save();
	    return [{}];
	},
	reqMissionReturnInstruction: function() {
	    let req = KanColleDatabase.reqMissionReturnInstruction.get_req();
	    let deck;
	    let d;

	    if (!this._ts || isNaN(req._deck_id))
		return;

	    d = KanColleDatabase.reqMissionReturnInstruction.get();

	    this._deepcopy();

	    deck = this.get(req._deck_id);
	    if (!deck)
		return;

	    deck.api_mission = JSON.parse(JSON.stringify(d.api_mission));

	    this._ts = KanColleDatabase.reqMissionReturnInstruction.timestamp();
	    if (this._update_timeout(this._ts, [req._deck_id]))
		this._alarm.update([req._deck_id]);

	    this._save();

	    return [{}];
	},

	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [{ init: true }];
	},
    };

    this._update_mission = function() {
	for (let k of this.list()) {
	    let d = this.get(k);
	    if (d.api_mission[0] > 0)
		this._db.mission[k] = { id: d.api_mission[1], name: KanColleDatabase.mission.get(d.api_mission[1]).api_name };
	}
    };

    this._update_timeout = function(now, decks) {
	let count = 0;
	debugprint('mission: update timeout: ' + decks);
	for (let k of decks) {
	    let d = this.get(k);
	    let t;

	    if (d.api_mission[0] > 0)
		t = d.api_mission[2];
	    else
		t = Number.NaN;

	    count += this._alarm.set_timeout(k, t);
	}
	return count;
    };

    this._timer_notify_set = function(id, state) {
	debugprint('mission: notify(deck = ' + id + ', state = ' + state + ')');
	//0        1  2    3  4  5    6        7          8  9    10   11
	//まもなく|第|艦隊|が|の|遠征|していま|が完了しま|す|した|せん|。
	const _m = '\u307e\u3082\u306a\u304f|\u7b2c|\u8266\u968a|\u304c|\u306e|\u9060\u5f81|\u3057\u3066\u3044\u307e|\u304c\u5b8c\u4e86\u3057\u307e|\u3059|\u3057\u305f|\u305b\u3093|\u3002'.split(/\|/);
	let msg = null;
	let popup = KanColleUtils.getBoolPref('popup.mission');
	let sound = null;
	let deck = this.get(id);
	let deckname = deck ? deck.api_name : (_m[1] + id + _m[2]);
	let ntfy_prio = KanColleUtils.getBoolPref('ntfy.mission') ? 3 : -1;
	let ntfy_tags = ['anchor','ship'];
	let mission_name = this._db.mission[id] ? this._db.mission[id].name : '';
	switch (state) {
	case 3:
	    msg =         deckname         + _m[4] + _m[5] + ' ' + mission_name               + _m[7]         + _m[9]          + _m[11];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.mission');
	    break;
	case 2:
	    msg = _m[0] + deckname         + _m[4] + _m[5] + ' ' + mission_name               + _m[7] + _m[8]                  + _m[11];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.1min.mission');
	    popup = KanColleUtils.getBoolPref('popup.mission-1min');
	    ntfy_prio = KanColleUtils.getBoolPref('ntfy.mission-1min') ? 3 : -1;
	    break;
	case 1:
	    msg =         deckname + _m[3]         + _m[5] + '(' + mission_name + ')' + _m[6]         + _m[8]                  + _m[11];
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	    break;
	case -1:
	    msg =         deckname + _m[3]         + _m[5]                            + _m[6]                         + _m[10] + _m[11];
	    /*FALLTHROUGH*/
	default:
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	}
	return { popup: popup, sound: sound, msg: msg,
		 ntfy_prio: ntfy_prio, ntfy_tags: ntfy_tags,
		 popup_click_url: KanColleUtils.URL };
    };

    this.observe = function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm.update();
	    break;
	default:;
	}
    };

    this._save = function() {
	if (!this._ts || !KanColleUtils.getBoolPref('record.internalstate'))
	    return;
	KanColleUtils.writeObject('deck', {
	    hash: this._db.deck,
	    mission: this._db.mission,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.getBoolPref('record.internalstate') && KanColleUtils.readObject('deck', null);
	if (!obj)
	    return;
	this._db.deck = obj.hash;
	this._db.list = null;
	this._db.fleet = null;
	this._db.mission = obj.mission || {};
	this._db.timer = {};
	this._ts = obj._ts;
	this._update_fleet();

	this._alarm.reset();
	this._update_timeout(this._ts, this.list());
	this._alarm.update(true);

	return true;
    };

    this.lookup = function(ship_id) {
	return this._db.fleet[ship_id];
    };

    this.get = function(id, key) {
	if (key == null) {
	    return this._db.deck ? this._db.deck[id] : KanColleDatabase.memberDeck.get(id);
	}
    };
    this.get_last_mission = function(id) {
	return this._db.mission[id];
    };

    this.list = function() {
	if (!this._db.deck)
	    return KanColleDatabase.memberDeck.list();
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.deck);
	return this._db.list;
    };

    this.count = function() {
	return this._db.deck ? this._db.list.length : KanColleDatabase.memberDeck.count();
    };
};
KanColleDeckDB.prototype = new KanColleCombinedDB();

/**
 * 編成プリセットデータベース。
 */
var KanCollePresetDB = function() {
    this._init();

    this._db = {
	hash: {},
	list: null,
	preset: null,
    };

    this._update = {
	memberPresetDeck: function() {
	    let data = KanColleDatabase.memberPresetDeck.get();
	    let t = KanColleDatabase.memberPresetDeck.timestamp();
	    this._db.hash = JSON.parse(JSON.stringify(data.api_deck));
	    this._db.list = null;
	    this._db.preset = null;
	    this._ts = t;
	    this._save();
	    return [];
	},
	reqHenseiPresetDelete: function() {
	    let req = KanColleDatabase.reqHenseiPresetDelete.get_req();
	    let t = KanColleDatabase.reqHenseiPresetDelete.timestamp();
	    delete(this._db.hash[req._preset_no]);
	    this._db.list = null;
	    this._db.preset = null;
	    this._ts = t;
	    this._save();
	    return [];
	},
	reqHenseiPresetRegister: function() {
	    let data = KanColleDatabase.reqHenseiPresetRegister.get();
	    let t = KanColleDatabase.reqHenseiPresetRegister.timestamp();
	    this._db.hash[data.api_preset_no] = JSON.parse(JSON.stringify(data));
	    this._db.list = null;
	    this._db.preset = null;
	    this._ts = t;
	    this._save();
	    return [];
	},

	// Note: 改装や解体で削除された艦は「除籍艦」表示なので対策不要

	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [];
	},
    };

    this._update_preset = function() {
	let that = this;
	this._db.preset = this.list().reduce(function(p,c) {
	    let ships = that._db.hash[c].api_ship;
	    ships.forEach(function(e) {
		if (!p[e])
		    p[e] = [];
		p[e].push(c | 0);
	    });
	    return p;
	}, {});
    };

    this._save = function() {
	if (!this._ts || !KanColleUtils.getBoolPref('record.internalstate'))
	    return;
	KanColleUtils.writeObject('preset', {
	    hash: this._db.hash,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.getBoolPref('record.internalstate') && KanColleUtils.readObject('preset', null);
	if (!obj)
	    return;
	this._db.hash = obj.hash;
	this._db.list = null;
	this._db.preset = null;
	this._ts = obj._ts;

	return true;
    };
    this.get = function(id) {
	return this._db.hash[id];
    };

    this.lookup = function(ship_id) {
	if (!this._db.preset)
	    this._update_preset();
	return this._db.preset[ship_id];
    };

    this.list = function() {
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.hash);
	return this._db.list;
    };

    this.count = function() {
	return this.list().length;
    };
};
KanCollePresetDB.prototype = new KanColleCombinedDB();

/**
 * 装備データベース。
 * 各所持装備を管理。強化状態毎などに分類。
 */
var KanColleSlotitemDB = function() {
    this._init();

    this._stat = {
	create: 0,
	destroy: 0,
    };

    this._db = {
	owner: {},
	hash: null,
	list: null,
    };

    this._levelkeys = [
	{ key: 'api_level', label: '*', },
	{ key: 'api_alv',   label: '+', },
    ];

    this._deepcopy = function() {
	if (!this._db.hash) {
	    let ids = KanColleDatabase._memberSlotitem.list();
	    if (!ids)
		return;

	    this._db.hash = new Object;

	    for (let i = 0; i < ids.length; i++)
		this._db.hash[ids[i]] = JSON.parse(JSON.stringify(KanColleDatabase._memberSlotitem.get(ids[i])));

	    this._db.list = Object.keys(this._db.hash);

	    //debugprint('hash: ' + this._db.hash.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._shipname = function(ship_id) {
	try{
	    // member/ship2 には艦名がない。艦艇型から取得
	    let ship = KanColleDatabase.ship.get(ship_id);
	    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
	    return shiptype.api_name;
	} catch (x) {
	}
	return "";
    },

    this._reset_owner = function() {
	this._db.owner = null;
	if (!KanColleDatabase.ship.timestamp() ||
	    !KanColleDatabase.slotitem.timestamp() ||
	    !KanColleDatabase.masterSlotitem.timestamp()) {
	    return -1;
	}
	return 0;
    };

    this._update_owner = function() {
	let db = {};
	let items;
	let ships;

	if (!this._ts || !KanColleDatabase.ship.timestamp() ||
	    !KanColleDatabase.masterSlotitem.timestamp())
	    return -1;

	items = KanColleDatabase.slotitem.list();
	ships = KanColleDatabase.ship.list();

	for (let i = 0; i < items.length; i++) {
	    let item = KanColleDatabase.slotitem.get(items[i]);
	    let itemtypeid = item.api_slotitem_id;
	    let itemtype = KanColleDatabase.masterSlotitem.get(itemtypeid);
	    if (!db[itemtypeid]) {
		db[itemtypeid] = {
				    id: itemtypeid,
				    name: itemtype.api_name,
				    type: itemtype.api_type,
				    list: {},
				    item: [],
				    totalnum: 0,
				    num: 0,
				    lv: {},
		};
		for (let ki = 0; ki < this._levelkeys.length; ki++) {
		    db[itemtypeid].lv[this._levelkeys[ki].key] = [];
		}
	    }
	    db[itemtypeid].totalnum++;
	    db[itemtypeid].item.push(item);

	    for (let ki = 0; ki < this._levelkeys.length; ki++) {
		let key = this._levelkeys[ki].key;
		let itemlv = item[key] || 0;
		if (!db[itemtypeid].lv[key][itemlv]) {
		    db[itemtypeid].lv[key][itemlv] = {
			list: {},
			item: [],
			totalnum: 0,
			num: 0,
		    };
		}
		db[itemtypeid].lv[key][itemlv].totalnum++;
		db[itemtypeid].lv[key][itemlv].item.push(item);
	    }
	}

	for (let i = 0; i < ships.length; i++) {
	    let ship = KanColleDatabase.ship.get(ships[i]);
	    let ship_slot = ship.api_slot.slice();
	    if (ship.api_slot_ex)
		ship_slot.push(ship.api_slot_ex);

	    //debugprint(this._shipname(ship.api_id) + ': ');

	    for (let j = 0; j < ship_slot.length; j++) {
		let item;
		let itemtypeid;

		if (ship_slot[j] < 0)
		    continue;

		item = KanColleDatabase.slotitem.get(ship_slot[j]);
		// member/slotitem might be out-of-date for a while.
		if (!item)
		    return -1;

		itemtypeid = item.api_slotitem_id;

		db[itemtypeid].list[ship.api_id] = (db[itemtypeid].list[ship.api_id] || 0) + 1;
		db[itemtypeid].num++;

		for (let ki = 0; ki < this._levelkeys.length; ki++) {
		    let key = this._levelkeys[ki].key;
		    let itemlv = item[key] || 0;

		    //debugprint(itemtypeid + ': ' + itemtype.api_name + ': ' + key + '=' + itemlv);

		    db[itemtypeid].lv[key][itemlv].list[ship.api_id] = (db[itemtypeid].lv[key][itemlv].list[ship.api_id] || 0) + 1;
		    db[itemtypeid].lv[key][itemlv].num++;
		}
	    }
	}

	for ( let k in db ){
	    let s = [];
	    for ( let l in db[k].list ){
		s.push(this._shipname(parseInt(l, 10)));
	    }
	    //debugprint(db[k].name + ': ' + s.join(','));
	}

	//debugprint(db.toSource());
	this._db.owner = db;

	return 0;
    };

    this._update = {
	ship: function() {
	    let t = KanColleDatabase.ship.timestamp();

	    if (!this._ts)
		return;

	    if (this._reset_owner() < 0)
		return;

	    this._ts = t;
	    return [];
	},

	_memberSlotitem: function() {
	    let t = KanColleDatabase._memberSlotitem.timestamp();
	    this._db.hash = null;
	    this._db.list = null;
	    this._ts = t;
	    this._reset_owner();
	    return [];
	},

	reqKaisouPowerup: function() {
	    let t = KanColleDatabase.reqKaisouPowerup.timestamp();
	    let req = KanColleDatabase.reqKaisouPowerup.get_req();

	    if (!this._ts || !req._id_items || !req._slot_dest_flag)
		return;

	    this._deepcopy();

	    for (let i = 0; i < req._id_items.length; i++)
		this._delete_ships([req._id_items[i]]);

	    this._db.list = Object.keys(this._db.hash);

	    this._ts = t;
	    this._reset_owner();
	    return [];
	},

	reqKousyouCreateItem: function() {
	    let t = KanColleDatabase.reqKousyouCreateItem.timestamp();
	    let data = KanColleDatabase.reqKousyouCreateItem.get();
	    let getitems = data.api_get_items;

	    if (!this._ts || !getitems)
		return;

	    this._deepcopy();

	    for (let d of getitems) {
		if (d.api_id < 0)
		    continue;
		this._db.hash[d.api_id] = d;
		this._stat.create++;
	    }
	    this._db.list = Object.keys(this._db.hash);

	    this._ts = t;
	    this._reset_owner();
	    return [];
	},

	reqKousyouDestroyItem2: function() {
	    let t = KanColleDatabase.reqKousyouDestroyItem2.timestamp();
	    let req = KanColleDatabase.reqKousyouDestroyItem2.get_req();

	    if (!this._ts || !req._slotitem_ids)
		return;

	    this._deepcopy();

	    this._delete_slotitems(req._slotitem_ids);

	    this._db.list = Object.keys(this._db.hash);

	    this._stat.destroy++;

	    this._ts = t;
	    this._reset_owner();
	    return [];
	},

	reqKousyouDestroyShip: function() {
	    let t = KanColleDatabase.reqKousyouDestroyShip.timestamp();
	    let req = KanColleDatabase.reqKousyouDestroyShip.get_req();
	    let ship;
	    let ship_slot;
	    let that = this;
	    let updated = false;

	    if (!this._ts || !req._ship_ids.length || !req._slot_dest_flag)
		return;

	    this._deepcopy();
	    this._delete_ships(req._ship_ids);
	    this._db.list = Object.keys(this._db.hash);
	    this._ts = t;
	    this._reset_owner();
	    return [];
	},

	reqKousyouGetShip: function() {
	    let data = KanColleDatabase.reqKousyouGetShip.get().api_slotitem;
	    let t = KanColleDatabase.reqKousyouGetShip.timestamp();

	    if (!this._ts || !data)
		return;

	    this._deepcopy();
	    for (let i = 0; i < data.length; i++)
		this._db.hash[data[i].api_id] = data[i];
	    this._db.list = Object.keys(this._db.hash);
	    this._ts = t;
	    this._reset_owner();
	    return [];
	},

	reqKousyouRemodelSlot: function() {
	    let data = KanColleDatabase.reqKousyouRemodelSlot.get();
	    let t = KanColleDatabase.reqKousyouRemodelSlot.timestamp();

	    if (!this._ts || !data)
		return;

	    this._deepcopy();

	    this._delete_slotitems(data.api_use_slot_id);

	    if (data.api_after_slot && data.api_after_slot.api_id)
		this._db.hash[data.api_after_slot.api_id] = data.api_after_slot;

	    this._db.list = Object.keys(this._db.hash);
	    this._ts = t;
	    this._reset_owner();
	    return [];
	},

	battle: function(stage) {
	    let t = KanColleDatabase.battle.timestamp();
	    let decks;
	    let that = this;
	    let updated = 0;

	    if (stage != -2 && stage != 2)
		return;

	    if (!this._ts)
		return;

	    this._deepcopy();

	    decks = KanColleDatabase.battle.list();
	    decks.forEach(function(e) {
		let ids = (KanColleDatabase.battle.get(e) || {}).consumed;
		if (!ids)
		    return;
		updated += that._delete_slotitems(Object.keys(ids));
	    });

	    if (!updated)
		return;

	    this._db.list = Object.keys(this._db.hash);

	    this._ts = t;
	    this._reset_owner();
	    return [];
	},
    };

    this._delete_slotitems = function(ids) {
	let count = 0;
	if (ids) {
	    for (let i = 0; i < ids.length; i++) {
		let id = ids[i];
		if (id < 0 || !this._db.hash[id])
		    continue;
		debugprint('deleting slotitem: ' + id);
		delete(this._db.hash[id]);
		count++;
	    }
	}
	return count;
    };

    this._delete_ships = function(ids) {
	let count = 0;
	for (let i = 0; i < ids.length; i++) {
	    let ship = KanColleDatabase.ship.get(ids[i]);
	    if (ship) {
		count += this._delete_slotitems([ship.api_slot_ex]);
		count += this._delete_slotitems(ship.api_slot);
	    }
	}
	return count;
    };

    this.get = function(id, key) {
	if (key == 'owner') {
	    if (!this._db.owner)
		this._update_owner();
	    if (!this._db.owner)
		return null;
	    return id ? this._db.owner[id] : this._db.owner;
	} else if (key == null) {
	    return this._db.hash ? this._db.hash[id] : KanColleDatabase._memberSlotitem.get(id);
	}
    };

    this.list = function() {
	if (!this._db.hash)
	    return KanColleDatabase._memberSlotitem.list();
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.hash);
	return this._db.list;
    };

    this.find_type = function(type) {
	let list = this.list();
	let that = this;
	return [].concat(Object.keys(this._db.owner)
			    .reduce(function(p,c) {
					let ownerinfo = that._db.owner[c];
					if (ownerinfo.type[2] == type)
					    p.push(ownerinfo.item);
					return p;
				    }, [])
			);
    };

    this.count = function(id) {
	if (id) {
	    if (!this._db.owner)
		this._update_owner();
	    if (!this._db.owner)
		return Number.NaN;
	    return (this._db.owner[id] || {}).totalnum;
	}
	return this._db.hash ? this._db.list.length : KanColleDatabase._memberSlotitem.count();
    };

    this.stat = function() {
	return this._stat;
    };
};
KanColleSlotitemDB.prototype = new KanColleCombinedDB();

/**
 * 装備プリセットデータベース。
 */
var KanCollePresetSlotDB = function() {
    this._init();

    this._db = {
	hash: {},
	list: null,
    };

    this._update = {
	memberPresetSlot: function() {
	    let data = KanColleDatabase.memberPresetSlot.get();
	    let t = KanColleDatabase.memberPresetSlot.timestamp();
	    let d = {};
	    for (let preset of data.api_preset_items) {
		d[preset.api_preset_no] = JSON.parse(JSON.stringify(preset));
	    }
	    this._db.hash = d;
	    this._db.list = null;
	    this._ts = t;
	    this._save();
	    return [];
	},
	reqKaisouPresetSlotDelete: function() {
	    let req = KanColleDatabase.reqKaisouPresetSlotDelete.get_req();
	    let t = KanColleDatabase.reqKaisouPresetSlotDelete.timestamp();
	    this._db.hash[req._preset_id].api_slot_item = [];
	    this._db.hash[req._preset_id].api_slot_item_ex = null;
	    this._db.list = null;
	    this._ts = t;
	    this._save();
	    return [];
	},
	reqKaisouPresetSlotRegister: function() {
	    let req = KanColleDatabase.reqKaisouPresetSlotRegister.get_req();
	    let t = KanColleDatabase.reqKaisouPresetSlotRegister.timestamp();
	    let ship = KanColleDatabase.ship.get(req._ship_id);
	    let d = this._db.hash[req._preset_id] || {
		api_preset_no: req._preset_id,
		api_name: `\u7b2c${ ('00' + req._preset_id ).slice(-2) }`,  // 第XX
	    };
	    let slot;

	    function slotitem_data(id) {
		if (id < 0)
		    return null;
		let item = KanColleDatabase.slotitem.get(id);
		if (!item)
		    return null;
		return {
		    api_id: item.api_slotitem_id,
		    api_level: item.api_level,
		    //api_alv: item.api_alv,	    // not yet
		};
	    }

	    d.api_selected_mode = 0;
	    d.api_slot_item = []

	    for (let itemid of ship.api_slot) {
		if (itemid < 0)
		    continue;
		let item = slotitem_data(itemid);
		if (!item)
		    continue;
		d.api_slot_item.push(item);
	    }

	    if (ship.api_slot_ex)
		d.api_slot_item_ex = slotitem_data(ship.api_slot_ex);

	    this._db.hash[req._preset_id] = d;
	    this._db.list = null;
	    this._ts = t;
	    this._save();
	    return [];
	},
	reqKaisouPresetSlotSelect: function() {
	    let req = KanColleDatabase.reqKaisouPresetSlotSelect.get_req();
	    let t = KanColleDatabase.reqKaisouPresetSlotSelect.timestamp();

	    this._db.hash[req._preset_id].api_selected_mode = req._equip_mode;
	    this._ts = t;
	    this._save();
	    return [];
	},
	reqKaisouPresetSlotUpdateName: function() {
	    let req = KanColleDatabase.reqKaisouPresetSlotUpdateName.get_req();
	    let t = KanColleDatabase.reqKaisouPresetSlotUpdateName.timestamp();
	    this._db.hash[req._preset_id].api_name = req.api_name;
	    this._ts = t;
	    this._save();
	    return [];
	},
	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [];
	},
    };

    this._save = function() {
	if (!this._ts || !KanColleUtils.getBoolPref('record.internalstate'))
	    return;
	KanColleUtils.writeObject('preset-slot', {
	    hash: this._db.hash,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.getBoolPref('record.internalstate') && KanColleUtils.readObject('preset-slot', null);
	if (!obj)
	    return;
	this._db.hash = obj.hash;
	this._db.list = null;
	this._ts = obj._ts;

	return true;
    };

    this.get = function(id) {
	return this._db.hash[id];
    };

    this.list = function() {
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.hash);
	return this._db.list;
    };

    this.count = function() {
	return this.list().length;
    };
};
KanCollePresetSlotDB.prototype = new KanColleCombinedDB();

/**
 * アイテムデータベース。
 */
var KanColleUseitemDB = function() {
    this._init();

    this._db = null;

    this._update = {
	masterUseitem: function() {
	    return [];
	},
	memberUseitem: function() {
	    return [];
	},
    };

    this._useitem_filter = function(f, id) {
        let d = KanColleDatabase.masterUseitem.get(id);
	if (!d || !f)
	    return true;
	if ((f.category === 0 || f.category) && d.api_category != f.category)
	    return false;
	if ((f.type === 0 || f.type) && d.api_type != f.type)
	    return false;
	return true;
    };

    this.get = function(id) {
	let res = null;
	let d = KanColleDatabase.masterUseitem.get(id);
	if (!d)
	    return;
	res = JSON.parse(JSON.stringify(d));
	d = KanColleDatabase.memberUseitem.get(id);
	if (d)
	    res.api_count = d.api_count;
	return res;
    };

    this.list = function(filter) {
	let items = KanColleDatabase.memberUseitem.list();
	if (!items)
	    return;

	let that = this;

	return items.filter(function(e) {
				return that._useitem_filter(filter, e);
			    });
    };

    this.count = function(f) {
	return this.list(f).length;
    };
};
KanColleUseitemDB.prototype = new KanColleCombinedDB();

/*
 * 基地航空隊
 */
var KanColleBaseAirCorpsDB = function() {
    this._init();

    this._ts = null;
    this._db = {
	last_area: 0,
	area: {},
    };
    this._update = {
	memberBaseAirCorps: function() {
	    let t = KanColleDatabase.memberBaseAirCorps.timestamp();
	    let data = KanColleDatabase.memberBaseAirCorps.get();
	    for (let i = 0; i < data.length; i++) {
		let d = data[i];
		if (!this._db.area[d.api_area_id])
		    this._db.area[d.api_area_id] = { hash: {}, timer: {} };

		this._db.area[d.api_area_id].hash[d.api_rid] = JSON.parse(JSON.stringify(d));

		if (!this._db.area[d.api_area_id].timer[d.api_rid])
		    this._db.area[d.api_area_id].timer[d.api_rid] = {};
		for (let j = 0; j < d.api_plane_info.length; j++) {
		    if (d.api_state == 0 || d.api_state == 1)
			delete this._db.area[d.api_area_id].timer[d.api_rid][d.api_squadron_id];
		}

		if (!this._db.last_area)
		    this._db.last_area = d.api_area_id;
	    }
	    this._db.list = null;
	    this._ts = t;
	    return [];
	},

	reqAirCorpsChangeName: function() {
	    let t = KanColleDatabase.reqAirCorpsChangeName.timestamp();
	    let req = KanColleDatabase.reqAirCorpsChangeName.get_req();

	    this._db.area[req._area_id].hash[req._base_id].api_name = req.api_name;
	    this._db.last_area = req._area_id;

	    this._ts = t;
	    return [];
	},
	reqAirCorpsSetAction: function() {
	    let that = this;
	    let t = KanColleDatabase.reqAirCorpsSetAction.timestamp();
	    let req = KanColleDatabase.reqAirCorpsSetAction.get_req();

	    if (req._base_ids.length != req._action_kinds.length) {
		debugprint(`set action length mismatch: ${ req._base_ids.length } vs ${ req._action_kinds.length }`);
		return;
	    }

	    req._base_ids.forEach(function(e,idx) {
		that._db.area[req._area_id].hash[e].api_action_kind = req._action_kinds[idx];
	    });

	    this._db.last_area = req._area_id;

	    this._ts = t;
	    return [];
	},
	reqAirCorpsSetPlane: function() {
	    let t = KanColleDatabase.reqAirCorpsSetPlane.timestamp();
	    let data = KanColleDatabase.reqAirCorpsSetPlane.get();
	    let req = KanColleDatabase.reqAirCorpsSetPlane.get_req();

	    let base_items = data.api_base_items || [ data ];

	    for (let d of base_items) {
		let rid = d.api_rid ? d.api_rid : req._base_id;
		this._db.area[req._area_id].hash[rid].api_distance = d.api_distance;
		this._update_base_plane_info(req._area_id, rid, d, t);
	    }

	    this._db.last_area = req._area_id;

	    this._ts = t;
	    return [];
	},
	reqAirCorpsSupply: function() {
	    let t = KanColleDatabase.reqAirCorpsSupply.timestamp();
	    let data = KanColleDatabase.reqAirCorpsSupply.get();
	    let req = KanColleDatabase.reqAirCorpsSupply.get_req();

	    this._update_base_plane_info(req._area_id, req._base_id, data, t);

	    this._db.last_area = req._area_id;

	    this._ts = t;
	    return [];
	},
    };

    this._update_base_plane_info = function(area_id, base_id, data, t) {
	let base_info = this._db.area[area_id].hash[base_id];
	let idx;

	if (!this._db.area[area_id].timer[base_id])
	    this._db.area[area_id].timer[base_id] = {};

	for (let i = 0; i < data.api_plane_info.length; i++) {
	    let d = data.api_plane_info[i];
	    let j;
	    for (j = 0; j < base_info.api_plane_info.length; j++) {
		if (base_info.api_plane_info[j].api_squadron_id == d.api_squadron_id)
		    break;
	    }
	    base_info.api_plane_info[j] = JSON.parse(JSON.stringify(d));
	    this._db.area[area_id].timer[base_id][d.api_squadron_id] = t;
	}
	return [];
    };

    this.get = function(id, area_id) {
	if (!area_id)
	    area_id = this._db.last_area || 0;

	return area_id ? this._db.area[area_id].hash[id] : {};
    };

    this.list = function(area_id) {
	if (!area_id)
	    area_id = this._db.last_area || 0;
	return area_id ? Object.keys(this._db.area[area_id].hash) : [];
    };

    this.count = function(area_id) {
	return this.list(area_id).length;
    };
};
KanColleBaseAirCorpsDB.prototype = new KanColleCombinedDB();

/**
 * 任務（クエスト）データベース。
 */
var KanColleQuestDB = function() {
    this._init();

    this._db = {
	hash: {},
	timestamp: {},
	list: [],
    };

    this._update = {
	memberQuestlist: function() {
	    let t = KanColleDatabase.memberQuestlist.timestamp();
	    let req = KanColleDatabase.memberQuestlist.get_req();
	    let d = KanColleDatabase.memberQuestlist.get();
	    let quests = this._db;
	    let oldest = t;

	    if (!t)
		return;

	    if (!quests.pages)
		quests.pages = [];

	    if (!req._tab_id && d.api_page_count !== undefined) {
		// 「全(All)」時のみ削除
		quests.pages[d.api_disp_page] = t;

		debugprint('num = ' + d.api_page_count + ', pages = ' + quests.pages.toSource());

		for (let i = 1; i <= d.api_page_count; i++) {
		    let v = quests.pages[i] || 0;
		    if (v < oldest)
			oldest = v;
		}
	    }

	    if (d.api_list) {
		if (Array.isArray(d.api_list)) {
		    d.api_list.forEach(function(q) {
			if (typeof(q) != 'object')
			    return;
			quests.hash[q.api_no] = q;
			quests.timestamp[q.api_no] = t;
		    });
		} else {
		    debugprint('q.api_list is non-array.');
		}
	    } else {
		debugprint('d.api_list is null: ' + d.toSource());
	    }

	    if (!req._tab_id) {
		// 「全(All)」時のみ削除
		// Clean-up obsolete quests.
		debugprint('t=' + t + ', oldest=' + oldest);
		Object.keys(quests.hash).filter(function(e) {
		    // - エントリの最終更新が全ページの更新より古ければ、
		    //   もう表示されないエントリ。
		    //debugprint(' ' + e + ': ' + quests.timestamp[e]);
		    return quests.timestamp[e] < oldest;
		}).forEach(function(e) {
		    debugprint('delete obsolete quest: ' + e);
		    delete quests.hash[e];
		});
	    }

	    this._db.list = null;
	    this._ts = t;

	    this._save_quest();

	    return [];
	},
	reqQuestClearitemget: function() {
	    let req = KanColleDatabase.reqQuestClearitemget.get_req();
	    let t = KanColleDatabase.reqQuestClearitemget.timestamp();
	    debugprint('delete cleared quest: ' + req._quest_id);
	    delete this._db.hash[req._quest_id];
	    delete this._db.timestamp[req._quest_id];

	    this._db.list = null;
	    this._ts = t;

	    this._save_quest();

	    return [];
	},
	reqQuestStop: function() {
	    let req = KanColleDatabase.reqQuestStop.get_req();
	    let t = KanColleDatabase.reqQuestStop.timestamp();
	    let quests = this._db;

	    debugprint('stop quest: ' + req._quest_id);
	    if (quests.hash[req._quest_id]) {
		quests.hash[req._quest_id].api_state = 1;
		quests.timestamp[req._quest_id] = t;
	    }

	    this._db.list = null;
	    this._ts = t;

	    this._save_quest();

	    return [];
	},
	reqMemberGetIncentive: function() {
	    if (!this._load_quest())
		return;
	    return [];
	},
    };

    this._load_quest = function() {
	let obj = KanColleUtils.getBoolPref('record.internalstate') && KanColleUtils.readObject('quest', null);
	if (!obj)
	    return;

	this._db.hash = obj.hash;
	this._db.list = null;
	this._db.timestamp = obj.timestamp;

	this._ts = obj._ts;

	return true;
    };

    this._save_quest = function() {
	if (!this._ts || !KanColleUtils.getBoolPref('record.internalstate'))
	    return;

	KanColleUtils.writeObject('quest', {
	    hash: this._db.hash,
	    timestamp: this._db.timestamp,
	    _ts: this._ts,
	});
    };

    this.list = function() {
	if (!this._db.list) {
	    let list = Object.keys(this._db.hash);
	    list.sort(function(a,b) { return a - b; });
	    this._db.list = list;
	}
	return this._db.list;
    };

    this.get = function(id) {
	return this._db.hash[id];
    };

    this.timestamp = function(id) {
	if (id !== undefined)
	    return this._db.timestamp[id];
	return this._ts;
    };
};
KanColleQuestDB.prototype = new KanColleCombinedDB();

/**
 * 遠征データベース。
 */
var KanColleMissionDB = function() {
    this._init();

    this._stat = {};
    this._db = {};

    this._update = {
	masterMission: function() {
	    let list = KanColleDatabase.masterMission.list();
	    for (let i = 0; i < list.length; i++) {
		this._db[list[i]] = KanColleDatabase.masterMission.get(list[i]);
	    }
	    return [];
	},
	reqMissionResult: function() {
	    let t = KanColleDatabase.reqMissionResult.timestamp();
	    let req = KanColleDatabase.reqMissionResult.get_req();
	    let d = KanColleDatabase.reqMissionResult.get();
	    let deck;
	    let mission;

	    if (isNaN(req._deck_id))
		return;

	    deck = KanColleDatabase.deck.get(req._deck_id);
	    if (!deck)
		return;

	    mission = deck.api_mission;
	    if (mission && mission[0] && mission[1]) {
		if (!this._stat[mission[1]])
		    this._stat[mission[1]] = {};
		if (!this._stat[mission[1]][d.api_clear_result])
		    this._stat[mission[1]][d.api_clear_result] = 0
		this._stat[mission[1]][d.api_clear_result]++;
	    }
	    return [];
	},
    };

    this.list = function() {
	return Object.keys(this._db);
    };

    this.get = function(id) {
	return this._db[id];
    };

    this.stat = function() {
	return this._stat;
    };
};
KanColleMissionDB.prototype = new KanColleCombinedDB();

/**
 * 演習データベース。
 */
var KanCollePracticeDB = function() {
    this._init();

    this._db = {
	hash: {},
	info: {},
    };

    this._update = {
	memberPractice: function() {
	    let t = KanColleDatabase.memberPractice.timestamp();
	    let list = KanColleDatabase.memberPractice.list();

	    // 演習は3時,15時(6時,18時UTC)に更新
	    if (!this._ts || Math.floor((this._ts - 21600000) / 43200000) < Math.floor((t - 21600000) / 43200000)) {
		this._db.hash = {};
		this._db.info = {};
	    };

	    for (let i = 0; i < list.length; i++) {
		let p = KanColleDatabase.memberPractice.get(list[i]);
		this._db.hash[p.api_id] = p;
	    }

	    this._ts = t;
	    return [];
	},

	reqMemberGetPracticeEnemyInfo: function() {
	    let t = KanColleDatabase.reqMemberGetPracticeEnemyInfo.timestamp();
	    let info = KanColleDatabase.reqMemberGetPracticeEnemyInfo.get();

	    if (!this._ts)
		return;

	    this._db.info[info.api_member_id] = info;

	    this._ts = t;
	    return [];
	},
    };

    this.get = function(id) {
	return this._db.hash[id];
    };

    this.list = function() {
	return Object.keys(this._db.hash);
    };

    this.find = function(id) {
	return this._db.info[id];
    };
};
KanCollePracticeDB.prototype = new KanColleCombinedDB();

/**
 * 建造ドックデータベース。
 */
var KanColleKdockDB = function() {
    this._init();

    this._db = {
	timestamp: {},
	hash: null,
	list: null,
	timer: {},
    };

    this._deepcopy = function() {
	if (!this._db.hash) {
	    let ids = KanColleDatabase._memberKdock.list();
	    if (!ids)
		return;

	    this._db.hash = new Object;

	    for (let i = 0; i < ids.length; i++)
		this._db.hash[ids[i]] = JSON.parse(JSON.stringify(KanColleDatabase._memberKdock.get(ids[i])));

	    this._db.list = Object.keys(this._db.hash);

	    //debugprint('hash: ' + this._db.hash.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._update = {
	_memberKdock: function() {
	    let that = this;
	    let t = KanColleDatabase._memberKdock.timestamp();
	    let list = KanColleDatabase._memberKdock.list();

	    this._db.hash = null;
	    this._db.list = null;

	    this._deepcopy();

	    list.forEach(function(e) {
		that._update_timestamp(e, t);
	    });

	    if (this._update_timeout(t, list))
		this._alarm.update();

	    this._ts = t;
	    this._save();

	    return [{}];
	},

	reqKousyouCreateShipSpeedChange: function() {
	    let t = KanColleDatabase.reqKousyouCreateShipSpeedChange.timestamp();
	    let req = KanColleDatabase.reqKousyouCreateShipSpeedChange.get_req();

	    if (!this._ts)
		return;

	    this._deepcopy();

	    kdock = this._db.hash[req._kdock_id];
	    kdock.api_state = 3;	    // completed
	    kdock.api_complete_time = 0;

	    this._update_timestamp(req._kdock_id, t);
	    if (this._update_timeout(t, [req._kdock_id]))
		this._alarm.update();

	    this._ts = t;
	    this._save();

	    return [{}];
	},
	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [{ init: true }];
	}
    };

    this._update_timestamp = function(e, t) {
	let d = this._db.hash[e];
	if (!d)
	    return;
	if (!this._db.timestamp[e])
	    this._db.timestamp[e] = {};

	if (d.api_state == 1 || d.api_state == 2) {
	    if (!this._db.timestamp[e].start) {
		this._db.timestamp[e].start = t;
		this._db.timestamp[e].complete = d.api_complete_time;
	    }
	} else if (d.api_state == 0) {
	    this._db.timestamp[e].start = 0;
	}

	this._db.timestamp[e].updated = t;
    };

    this._update_timeout = function(now, docks) {
	let count = 0;
	debugprint('kdock: update timeout: ' + docks);
	for (let k of docks) {
	    let d = this.get(k);
	    let t;

	    if (d.api_state > 0)
		t = d.api_complete_time;
	    else
		t = Number.NaN;

	    count += this._alarm.set_timeout(k, t);
	}
	return count;
    };

    this._timer_notify_set = function(id, state) {
	debugprint('kdock: notify(dock = ' + id + ', state = ' + state + ')');
	//0        1      2  3  4    5        6          7  8    9    10
	//まもなく|ドック|で|の|建造|していま|が完了しま|す|した|せん|。
	const _m = '\u307e\u3082\u306a\u304f|\u30c9\u30c3\u30af|\u3067|\u306e|\u5efa\u9020|\u3057\u3066\u3044\u307e|\u304c\u5b8c\u4e86\u3057\u307e|\u3059|\u3057\u305f|\u305b\u3093|\u3002'.split(/\|/);
	let msg = null;
	let popup = KanColleUtils.getBoolPref('popup.kdock');
	let sound = null;
	let ntfy_prio = KanColleUtils.getBoolPref('ntfy.kdock') ? 3 : -1;
	let ntfy_tags = ['anchor','hammer_and_wrench'];
	switch (state) {
	case 3:
	    msg =         _m[1] + id         + _m[3] + _m[4]         + _m[6]         + _m[8]         + _m[10];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.kdock');
	    break;
	case 2:
	    msg = _m[0] + _m[1] + id         + _m[3] + _m[4]         + _m[6] + _m[7]                 + _m[10];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.1min.kdock');
	    popup = KanColleUtils.getBoolPref('popup.kdock-1min');
	    ntfy_prio = KanColleUtils.getBoolPref('ntfy.kdock-1min') ? 3 : -1;
	    break;
	case 1:
	    msg =         _m[1] + id + _m[2]         + _m[4] + _m[5]         + _m[7]                 + _m[10];
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	    break;
	case -1:
	    msg =         _m[1] + id + _m[2]         + _m[4] + _m[5]                         + _m[9] + _m[10];
	    /*FALLTHROUGH*/
	default:
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	}
	return { popup: popup, sound: sound, msg: msg,
		 ntfy_prio: ntfy_prio, ntfy_tags: ntfy_tags,
		 popup_click_url: KanColleUtils.URL };
    };

    this.observe = function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm.update();
	    break;
	default:;
	}
    };

    this._save = function() {
	if (!this._ts || !KanColleUtils.getBoolPref('record.internalstate'))
	    return;

	KanColleUtils.writeObject('kdock', {
	    hash: this._db.hash,
	    timestamp: this._db.timestamp,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.getBoolPref('record.internalstate') && KanColleUtils.readObject('kdock', null);
	if (!obj)
	    return;
	this._db.hash = obj.hash;
	this._db.timer = {};
	this._db.list = null;
	this._db.timestamp = obj.timestamp;
	this._ts = obj._ts;

	this._alarm.reset();
	this._update_timeout(this._ts, this.list());
	this._alarm.update(true);

	return true;
    };

    this.get = function(id) {
	return this._db.hash ? this._db.hash[id] : KanColleDatabase._memberKdock.get(id);
    };

    this.list = function() {
	if (!this._db.hash)
	    return KanColleDatabase._memberKdock.list();
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.hash);
	return this._db.list;
    };

    this.count = function() {
	return this._db.hash ? this._db.list.length : KanColleDatabase._memberKdock.count();
    };

    this.timestamp = function(id) {
	if (!id)
	    return this._ts;
	return this._db.timestamp[id];
    };
};
KanColleKdockDB.prototype = new KanColleCombinedDB();

/**
 * 入渠ドックデータベース。
 */
var KanColleNdockDB = function() {
    this._init();

    this._db = {
	ship: null,
	hash: null,
	list: null,
	timer: {},
    };
    this._timer = null;

    this._deepcopy = function() {
	if (!this._db.hash) {
	    let ids = KanColleDatabase._memberNdock.list();
	    if (!ids)
		return;

	    this._db.hash = new Object;

	    for (let i = 0; i < ids.length; i++)
		this._db.hash[ids[i]] = JSON.parse(JSON.stringify(KanColleDatabase._memberNdock.get(ids[i])));

	    //debugprint('hash: ' + this._db.hash.toSource());
	    //debugprint('list: ' + this._db.list.toSource());
	}
    };

    this._update = {
	_memberNdock: function() {
	    let t = KanColleDatabase._memberNdock.timestamp();

	    this._db.hash = null;
	    this._db.list = null;
	    this._db.ship = null;

	    this._deepcopy();

	    this._ts = t;

	    if (this._update_timeout(this._ts, this.list()))
		this._alarm.update();

	    this._save();

	    return [{}];
	},

	reqNyukyoSpeedChange: function() {
	    let t = KanColleDatabase.reqNyukyoSpeedChange.timestamp();
	    let req = KanColleDatabase.reqNyukyoSpeedChange.get_req();

	    if (!this._ts || isNaN(req._ndock_id))
		return;

	    this._deepcopy();

	    kdock = this._db.hash[req._ndock_id];
	    kdock.api_state = 0;	    //empty
	    kdock.api_complete_time = 0;

	    this._db.ship = null;

	    this._ts = t;

	    if (this._update_timeout(this._ts, [req._ndock_id]))
		this._alarm.update();

	    this._save();

	    return [{}];
	},

	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [{ init: true }];
	},
    };

    this._update_timeout = function(now, docks) {
	let count = 0;
	debugprint('ndock: update timers');
	for (let k of docks) {
	    let d = this.get(k);
	    let state = this._alarm.get_state(k) || 0;
	    let t;

	    if (d.api_state > 0)
		t = d.api_complete_time;
	    else if (state > 0)
		t = Number.NaN;	//now;
	    else
		t = Number.NaN;

	    count += this._alarm.set_timeout(k, t);
	}
	return count;
    };
    this._timer_notify_set = function(id, state) {
	debugprint('ndock: notify(dock = ' + id + ', state = ' + state + ')');
	//0        1      2  3  4    5        6          7  8    9    10 11
	//まもなく|ドック|で|の|修復|していま|が完了しま|す|した|せん|。|を
	const _m = '\u307e\u3082\u306a\u304f|\u30c9\u30c3\u30af|\u3067|\u306e|\u4fee\u5fa9|\u3057\u3066\u3044\u307e|\u304c\u5b8c\u4e86\u3057\u307e|\u3059|\u3057\u305f|\u305b\u3093|\u3002|\u3092'.split(/\|/);
	let msg = null;
	let popup = KanColleUtils.getBoolPref('popup.ndock');
	let sound = null;
	let ntfy_prio = KanColleUtils.getBoolPref('ntfy.ndock') ? 3 : -1;
	let ntfy_tags = ['anchor','bath'];
	let name = '?';
	let ndock = this.get(id) || {};
	if (ndock) {
	    let ship_id = ndock.api_ship_id;
	    let d = KanColleDatabase.ship.get(ship_id);
	    let ship = d ? KanColleDatabase.masterShip.get(d.api_ship_id) : null;
	    if (ship)
		    name = `${ ship.api_name } Lv${ d.api_lv }`;
	    else if (d)
		    name = `#${ d.api_ship_id } Lv ${ d.api_lv }`;
	    else
		    name = '';
	}
	switch (state) {
	case 3:
	    msg =         _m[1] + id  + ' '  + name + _m[3]  + _m[4]         + _m[6]         + _m[8]         + _m[10];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.ndock');
	    break;
	case 2:
	    msg = _m[0] + _m[1] + id  + ' '  + name + _m[3]  + _m[4]         + _m[6] + _m[7]                 + _m[10];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.1min.ndock');
	    popup = KanColleUtils.getBoolPref('popup.ndock-1min');
	    ntfy_prio = KanColleUtils.getBoolPref('ntfy.ndock-1min') ? 3 : -1;
	    break;
	case 1:
	    msg =         _m[1] + id + _m[2] + name + _m[11] + _m[4] + _m[5]         + _m[7]                 + _m[10];
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	    break;
	case -1:
	    msg =         _m[1] + id + _m[2]                 + _m[4] + _m[5]                         + _m[9] + _m[10];
	    /*FALLTHROUGH*/
	default:
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	}
	return { popup: popup, sound: sound, msg: msg,
		 ntfy_prio: ntfy_prio, ntfy_tags: ntfy_tags,
		 popup_click_url: KanColleUtils.URL };
    };

    this.observe = function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm.update();
	    break;
	default:;
	}
    };

    this._save = function() {
	if (!this._ts || !KanColleUtils.getBoolPref('record.internalstate'))
	    return;
	KanColleUtils.writeObject('ndock', {
	    hash: this._db.hash,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.getBoolPref('record.internalstate') && KanColleUtils.readObject('ndock', null);
	if (!obj)
	    return;
	this._db.hash = obj.hash;
	this._db.ship = null;
	this._db.list = null;
	this._db.timer = {};
	this._ts = obj._ts;

	this._alarm.reset();
	this._update_timeout(this._ts, this.list());
	this._alarm.update(true);

	return true;
    };

    this.get = function(id) {
	return this._db.hash ? this._db.hash[id] : KanColleDatabase._memberNdock.get(id);
    };

    this._find = function() {
	if (!this._db.ship) {
	    let that = this;
	    this._db.ship = this.list().map(function(e) {
		return that.get(e);
	    }).reduce(function(p,c) {
		if (c && c.api_ship_id)
		    p[c.api_ship_id] = c.api_id;
		return p;
	    }, {});
	}
	return this._db.ship;
    };

    this.find = function(shipid) {
	return this._find()[shipid];
    };

    this._list = function() {
	if (!this._db.list)
	    this._db.list = Object.keys(this._db.hash);
	return this._db.list;
    };

    this.list = function() {
	if (!this._db.hash)
	    return KanColleDatabase._memberNdock.list();
	return this._list();
    };

    this.count = function() {
	return this._db.hash ? this._db._list().length : KanColleDatabase._memberNdock.count();
    };
};
KanColleNdockDB.prototype = new KanColleCombinedDB();

/**
 * マップデータベース。
 */
var KanColleMapDB = function() {
    this._init();

    this._ts = null;
    this._db = {
	dirs: {},
	latest: null,
	battleinfo: {},
	path: [],
	pending: null,
    };

    this._update = {
	reqMapStart: function(start) {
	    let t = KanColleDatabase.reqMapStart.timestamp();
	    let req = KanColleDatabase.reqMapStart.get_req();
	    let d = KanColleDatabase.reqMapStart.get();
	    let latest_data;
	    let path;
	    let area_name = d.api_maparea_id + '-' + d.api_mapinfo_no;
	    let allcount;
	    // + '-' + d.api_no;

	    this._ts = t;

	    if (start) {
		this._db.latest = null;
		this._db.path = [];
		this._db.pending = null;
	    } else
		this._commit_pending();

	    path = this._db.path.join('-');

	    //let cell_info = this._get(area_name, path);
	    //debugprint(`cellinfo = ${ JSON.stringify(cell_info) }`);

	    if (!this._db.dirs[area_name])
		this._db.dirs[area_name] = {};

	    if (this._db.dirs[area_name][path]) {
		let that = this;
		let allcount = Object.keys(this._db.dirs[area_name][path]).reduce(function(p,c) {
		    return p + that._db.dirs[area_name][path][c];
		}, 0);
		debugprint(`Total: ${ allcount }`);
		Object.keys(this._db.dirs[area_name][path]).forEach(function(e) {
		    let ratio = that._db.dirs[area_name][path][e] / allcount;
		    let cell_name = `${ area_name }-${ e }`;
		    let battleinfo = that._db.battleinfo[cell_name] || {};
		    let etotal;

		    debugprint(`Direction: ${ path } -> ${ e }: ${ (100 * ratio).toFixed(2) }%`);

		    etotal = Object.keys(battleinfo).reduce(function(p,c) {
			return p + battleinfo[c].count;
		    }, 0);
		    Object.keys(battleinfo).forEach(function(ee) {
			let eeo = that._deserialize_battledata(ee);
			r = battleinfo[ee].count / etotal;
			debugprint(`${ (100 * r).toFixed(2) }%[${ (100 * r * ratio).toFixed(2) }%]: ${ eeo.deck_name }: ${
			    ['e1', 'e2'].map(function(en) {
				return `${en}: [${ ((eeo.ships || {})[en] || []).map(function(ees) { return `${ eeo.ship_data[ees].api_name }[${ eeo.ship_data[ees].api_yomi}]`}).join(",") }]`;
			    }).join(", ")
			}`);
		    });
		});
	    } else {
		this._db.dirs[area_name][path] = {};
		debugprint(`Direction: ${ path } -> unknown`);
	    }

	    latest_data = JSON.parse(JSON.stringify(d));
	    delete latest_data.api_cell_data;
	    this._db.latest = latest_data;

	    this._db.pending = [ area_name, path, d.api_no ];

	    return [ [area_name, path] ];
	},
	battle: function(state) {
	    let t = KanColleDatabase.battle.timestamp();

	    if (state != -1)
		return;

	    let ret = this._commit_pending(t);

	    return [ret];
	},
	reqSortieBattleResult: function(mode) {
	    let that = this;
	    let t = KanColleDatabase.reqSortieBattleResult.timestamp();
	    let d = KanColleDatabase.reqSortieBattleResult.get();

	    if (mode.practice)
		return;

	    this._commit_pending(t);

	    let latest = this._db.latest || {};
	    let cell_name = `${ latest.api_maparea_id }-${ latest.api_mapinfo_no }-${ latest.api_no }`;
	    let quest_name = d.api_quest_name;
	    let deck_name = d.api_enemy_info.api_deck_name;
	    let obj = { deck_name: deck_name };

	    obj.ship_data = {};

	    obj.ships = {};
	    for (let en of ['e1', 'e2']) {
		let d = KanColleDatabase.battle.get(en);
		obj.ships[en] = ((d || {}).ships || []).filter(function(e) { return e >= 0; });
		if (obj.ships[en]) {
		    obj.ships[en].forEach(function(e) {
			if (e < 0)
			    return;
			if (obj.ship_data[e])
			    return;
			let ship = KanColleDatabase.masterShip.get(e);
			if (ship)
			    obj.ship_data[e] = { api_name: ship.api_name, api_yomi: ship.api_yomi };
		    });
		}
	    }

	    obj_str = this._serialize_battledata(obj);
 
	    if (!this._db.battleinfo[cell_name])
		this._db.battleinfo[cell_name] = {};

	    if (!this._db.battleinfo[cell_name][obj_str]) {
		this._db.battleinfo[cell_name][obj_str] = {};
		this._db.battleinfo[cell_name][obj_str].count = 0;
		this._db.battleinfo[cell_name][obj_str].first = t;
	    }
	    this._db.battleinfo[cell_name][obj_str].count++;
	    this._db.battleinfo[cell_name][obj_str].last = t;

	    debugprint(`debug: battleinfo = ${ JSON.stringify(this._db.battleinfo[cell_name]) }`);

	    that._ts = t;

	    return [];
	},
	memberBasic: function() {
	    let t = KanColleDatabase.memberBasic.timestamp();

	    if (!this._ts || !this._db.latest)
		return;

	    this._commit_pending(t);

	    this._save();
	    return [];
	},
	reqMemberGetIncentive: function() {
	    if (!this._load())
		return;
	    return [];
	},
    };

    this._serialize_battledata = function(obj) {
	// [ name, [ [id,name,yomi], [id,name,yomi], ... ], [ [id,name,yomi], .... ]]
	let o = [];

	debugprint(`serialize: ${ JSON.stringify(obj) }`);

	o.push(obj.deck_name);
	for (let en of ['e1', 'e2']) {
	    let done = {};
	    o.push(obj.ships[en].map(function(ship_id) {
		if (done[ship_id]) {
		    return [ship_id];
		} else {
		    done[ship_id] = true;
		    return [ ship_id, obj.ship_data[ship_id].api_name, obj.ship_data[ship_id].api_yomi ];
		}
	    }));
	}

	debugprint(`serialize: => ${ JSON.stringify(o) }`);

	return JSON.stringify(o);
    };

    this._deserialize_battledata = function(s) {
	let [name,e1,e2] = JSON.parse(s) || [null,[],[]];

	if (name === null)
	    return;

	let obj = { deck_name: name, ship_data: {}, ships: { e1: null, e2: null } };

	for (const [k,v] of Object.entries({e1: e1, e2: e2})) {
	    obj.ships[k] = [];
	    for (let e of v) {
		let [ship_id, name, yomi] = e;
		if (typeof name == 'string')
		    obj.ship_data[ship_id] = { api_name: name, api_yomi: yomi };
		obj.ships[k].push(ship_id);
	    }
	}

	return obj;
    };

    this._commit_pending = function(t) {
	if (!this._db.pending)
	    return;

	let [ area_name, path, api_no ] = this._db.pending;

	debugprint(`Record ${area_name}: ${path} -> ${api_no}`);

	if (!this._db.dirs[area_name][path])
	    throw new Error("assert");

	if (!this._db.dirs[area_name][path][api_no])
	    this._db.dirs[area_name][path][api_no] = 0;

	this._db.dirs[area_name][path][api_no]++;
	this._db.path.push(api_no);

	this._db.pending = null;

	if (t)
	    this._ts = t;

	return [area_name, path, api_no];
    };
    this._save = function() {
	if (!this._ts || !KanColleUtils.getBoolPref('record.internalstate'))
	    return;
	KanColleUtils.writeObject('map', {
	    dirs: this._db.dirs,
	    battleinfo: this._db.battleinfo,
	    _ts: this._ts,
	});
    };

    this._load = function() {
	let obj = KanColleUtils.getBoolPref('record.internalstate') && KanColleUtils.readObject('map', null);
	if (!obj)
	    return;

	this._db.dirs = obj.dirs || {};
	this._db.battleinfo = obj.battleinfo || {};
	this._ts = obj._ts;

	return true;
    };

    this.list = function(area_name) {
	if (!area_name)
	    return Object.keys(this._db.dirs);

	let paths = this._db.dirs[area_name];
	if (!paths)
	    return;

	Object.keys(paths).map(function(e) {
	    return e.replace(/^.*-/,'');
	});

	return;
    };
    this._get = function(area_name, id) {
	let that = this;
	if (!area_name) {
	    let d = this._db.latest;
	    if (!d)
		return;
	    area_name = `${ d.api_maparea_id }-${ d.api_mapinfo_no }`;
	}

	let obj = {};

	let next_cells = (this._db.dirs[area_name] || {})[id];
	if (next_cells) {
	    obj['next_total'] = Object.keys(next_cells)
		.reduce(function(p,c) {
		    return p + next_cells[c];
		}, 0);
	    obj['next'] = Object.keys(next_cells)
		.reduce(function(p2,c2) {
		    p2[c2] = {};
		    p2[c2]['count'] = next_cells[c2];

		    let battleinfo = that._db.battleinfo[`${ area_name }-${ c2 }`];
		    if (battleinfo) {
			p2[c2]['battleinfo_total'] = Object.keys(battleinfo)
			    .reduce(function(p3, c3) {
				return p3 + battleinfo[c3].count;
			    }, 0);
			p2[c2]['battleinfo'] = Object.keys(battleinfo)
			    .reduce(function(p3, c3) {
				let o = JSON.parse(JSON.stringify(battleinfo[c3]));
				o['data'] = that._deserialize_battledata(c3);
				p3[c3] = o;
				return p3;
			    }, {});
		    }

		    return p2;
		}, {});
	}
	obj['battleinfo'] = this._db.battleinfo[`${ area_name }-${id}`] || null;

	return obj;
    };
    this.get = function(area_name, id) {
	let ret = this._get(area_name, id);
	if (ret)
	    ret = JSON.parse(JSON.stringify(ret));
	return ret;
    };
};
KanColleMapDB.prototype = new KanColleCombinedDB();

/**
 * 戦闘/対戦データベース。
 * 戦闘の進行に応じて何度かnotifyする。
 *	-3: reset
 *	-2: enemy information (partial)
 *	-1: enemy information
 *	 0: before update
 *	 1: after update
 *	 2: battle finished
 *	 3: battle finished, final
 */
var KanColleBattleDB = function() {
    this._init();

    this._stat = {
	first: 0,
	map: {
	    // practice, boss, count, rank by map (e.g., 1-1-1)
	},
	type: {},
    };
    this._db = {
	deckid: 0,
	deck: {},
    };

    this._update = {
	// 敵/対戦情報
	memberPractice: function() {
	    let t = KanColleDatabase.memberPractice.timestamp();
	    this._init_battle();
	    this._ts = t;
	    return [-3];
	},
	memberMission: function() {
	    let t = KanColleDatabase.memberMission.timestamp();
	    this._init_battle();
	    this._ts = t;
	    return [-3];
	},
	memberMapinfo: function() {
	    let t = KanColleDatabase.memberMapinfo.timestamp();
	    this._init_battle();
	    this._ts = t;
	    return [-3];
	},
	reqMemberGetPracticeEnemyInfo: [
	    function() {
		let t = KanColleDatabase.reqMemberGetPracticeEnemyInfo.timestamp();
		this._init_battle();
		this._ts = t;
		return [-3];
	    },
	    function() {
		let d = KanColleDatabase.reqMemberGetPracticeEnemyInfo.get();

		this._db.deck['e1'].practice = true;
		this._db.deck['e1'].first = false;
		this._db.deck['e1']._first = false;
		this._db.deck['e1'].boss = false;
		this._db.deck['e1'].last = false;
		this._db.deck['e1'].name = '\u6f14\u7fd2';	//演習
		this._db.deck['e1'].fullname = d.api_deckname;
		this._db.deck['e1'].ships = null;

		return [-2];
	    },
	    function() {
		let d = KanColleDatabase.reqMemberGetPracticeEnemyInfo.get();
		let ships = (d.api_deck.api_ships || []).map(function(e) {
		    return e.api_ship_id || -1;
		});
		ships.unshift(-1);

		this._db.deck['e1'].ships = ships;

		return [-1];
	    },
	],
	reqMapStart: [
	    function(start) {
		let t = KanColleDatabase.reqMapStart.timestamp();

		if (start)
		    return;

		this._reset_battle();

		this._ts = t;
		return [-3];
	    },
	    function(start) {
		let t = KanColleDatabase.reqMapStart.timestamp();
		let req = KanColleDatabase.reqMapStart.get_req();
		let d = KanColleDatabase.reqMapStart.get();

		this._db.deck['e1'].practice = false;
		this._db.deck['e1'].first = start;
		this._db.deck['e1'].boss = d.api_event_id == 5;  // d.api_bosscell_no does not work
		this._db.deck['e1'].last = !d.api_next;
		this._db.deck['e1'].name = d.api_maparea_id + '-' + d.api_mapinfo_no + '-' + d.api_no;
		this._db.deck['e1'].fullname = null;
		this._db.deck['e1'].ships = null;	// will be updated later
		this._db.deck['e2'].name = this._db.deck['e1'].name + '#2';
		this._db.deck['e2'].ships = null;

		if (start)
		    this._db.deck['e1']._first = true;

		if (req._deck_id)
		    this._db.deckid = req._deck_id;

		if (req._recovery_type) {
		    let useitem = ([ 0, 42, 43 ])[req._recovery_type];
		    this._db.deck[this._db.deckid].consumed = {},
		    this._revive_ship(this._db.deckid, 1, useitem);
		}
		this._ts = t;
		return [-2];
	    },
	],
	reqSortieBattle: [
	    function(mode) {
		let t = KanColleDatabase.reqSortieBattle.timestamp();
		let data = KanColleDatabase.reqSortieBattle.get();

		if (mode.practice)
		    return;

		this._db.deck['e1'].ships = this._get_compat_array(data.api_ship_ke);
		if (mode.ecombined)
		    this._db.deck['e2'].ships = this._get_compat_array(data.api_ship_ke_combined);

		this._ts = t;
		return [-1];
	    },
	    function(mode) {
		let t = KanColleDatabase.reqSortieBattle.timestamp();
		let data = KanColleDatabase.reqSortieBattle.get();
		let deckid = data.api_deck_id || data.api_dock_id;  //typo
		let deckid2 = mode.combined ? (deckid % 4) + 1 : deckid;
		let data_f_nowhps;
		let data_f_maxhps;
		let data_e_nowhps;
		let data_e_maxhps;
		let data_f_nowhps_combined;
		let data_f_maxhps_combined;
		let data_e_nowhps_combined;
		let data_e_maxhps_combined;

		debugprint('data = ' + JSON.stringify(data));

		if (data.api_f_nowhps) {
		    data_f_nowhps = this._get_compat_array(data.api_f_nowhps);
		} else
		    data_f_nowhps = data.api_nowhps.slice(0,7);

		if (data.api_f_maxhps)
		    data_f_maxhps = this._get_compat_array(data.api_f_maxhps);
		else
		    data_f_maxhps = data.api_maxhps.slice(0,7);

		this._update_escape(deckid, data.api_escape_idx);
		if (deckid != deckid2)
		    this._update_escape(deckid2, data.api_escape_idx_combined);

		this._start_battle(deckid, data_f_nowhps, data_f_maxhps);

		debugprint('f_maxhps/f_nowhps:   ' + data_f_nowhps.toSource() + '/' + data_f_maxhps.toSource());

		if (mode.combined) {
		    if (data.api_f_nowhps_combined)
			data_f_nowhps_combined = this._get_compat_array(data.api_f_nowhps_combined);
		    else
			data_f_nowhps_combined = data.api_nowhps_combined.slice(0,7);

		    if (data.api_f_maxhps_combined)
			data_f_maxhps_combined = this._get_compat_array(data.api_f_maxhps_combined);
		    else
			data_f_maxhps_combined = data.api_maxhps_combined.slice(0,7);

		    this._start_battle(deckid2, data_f_nowhps_combined, data_f_maxhps_combined);

		    debugprint('f_maxhps2/f_nowhps2: ' + data_f_nowhps_combined.toSource() + '/' + data_f_maxhps_combined.toSource());
		}

		if (data.api_e_nowhps)
		    data_e_nowhps = this._get_compat_array(data.api_e_nowhps);
		else
		    data_e_nowhps = data.api_nowhps.slice(6);

		if (data.api_e_maxhps)
		    data_e_maxhps = this._get_compat_array(data.api_e_maxhps);
		else
		    data_e_maxhps = data.api_maxhps.slice(6);

		this._start_battle('e1', data_e_nowhps, data_e_maxhps);
		this._fixup_battle('e1');

		debugprint('e_maxhps/e_nowhps: ' + data_e_nowhps.toSource() + '/' + data_e_maxhps.toSource());

		if (mode.ecombined) {
		    if (data.api_e_nowhps_combined)
			data_e_nowhps_combined = this._get_compat_array(data.api_e_nowhps_combined);
		    else
			data_e_nowhps_combined = data.api_nowhps_combined.slice(6);

		    if (data.api_e_maxhps_combined)
			data_e_maxhps_combined = this._get_compat_array(data.api_e_maxhps_combined);
		    else
			data_e_maxhps_combined = data.api_maxhps_combined.slice(6);

		    this._start_battle('e2', data_e_nowhps_combined, data_e_maxhps_combined);
		    this._fixup_battle('e2');

		    debugprint('e_maxhps2/e_nowhps2: ' + data_e_nowhps_combined.toSource() + '/' + data_e_maxhps_combined.toSource());
		}

		this._ts = t;
		return [0];
	    },
	    function(mode) {
		let data = KanColleDatabase.reqSortieBattle.get();
		let deckid = data.api_deck_id || data.api_dock_id;  //typo
		let deckid2 = mode.combined ? (deckid % 4) + 1 : deckid;
		let edeckid2 = mode.ecombined ? 'e2' : 'e1';
		let newapi = !!data.api_f_nowhps;

		debugprint('deckid = ' + deckid + ', deckid2 = ' + deckid2 + ', data = ' + JSON.stringify(data));

		// 夜戦開始マス
		if (data.api_n_support_flag) {
		    this._parse_battle_support(data.api_n_support_flag, data.api_n_support_info, deckid);
		}
		if (data.api_n_hourai_flag ||
		    data.api_n_hougeki1 || data.api_n_hougeki2 ||
		    data.api_n_hougeki3 || data.api_n_raigeki) {
		    this._parse_battle_hourai(mode,
					      data.api_n_hourai_flag,
					      data.api_n_hougeki1, data.api_n_hougeki2,
					      data.api_n_hougeki3, data.api_n_raigeki,
					      deckid, deckid2, newapi);
		}

		// 基地航空隊噴式強襲
		if (data.api_air_base_injection)
		    this._parse_battle_kouku(null, data.api_air_base_injection, deckid, deckid2);

		// 噴式強襲
		if (data.api_injection_kouku)
		    this._parse_battle_kouku(null, data.api_injection_kouku, deckid, deckid2);

		if (data.api_air_base_attack) {
		    for (let d of data.api_air_base_attack) {
			this._parse_battle_kouku(d.api_stage_flag, d, deckid, deckid2);
		    }
		}

		// 索敵
		this._parse_battle_kouku(data.api_stage_flag, data.api_kouku, deckid, deckid2);

		// 支援
		if (data.api_support_flag)
		    this._parse_battle_support(data.api_support_flag, data.api_support_info, deckid);

		// 開幕雷撃
		if (data.api_opening_taisen_flag)
			this._update_battle_hourai(data.api_opening_taisen, deckid2);

		// 開幕: 第2(随伴)=>全体
		if (data.api_opening_flag)
		    this._update_battle_raibak(data.api_opening_atack, deckid, 'e1', deckid2, 'e2', deckid2, edeckid2); // attackではない

		// 航空戦闘マス
		this._parse_battle_kouku(data.api_stage_flag2, data.api_kouku2, deckid, deckid2);

		if (data.api_hourai_flag) {
		    this._parse_battle_hourai(mode,
					      data.api_hourai_flag,
					      data.api_hougeki1, data.api_hougeki2,
					      data.api_hougeki3, data.api_raigeki,
					      deckid, deckid2, newapi);
		}

		this._fixup_battle(deckid);
		if (deckid != deckid2)
		    this._fixup_battle(deckid2);
		this._fixup_battle('e1');
		if (mode.ecombined)
		    this._fixup_battle('e2');

		return [1];
	    },
	],
	reqBattleMidnightBattle: [
	    function(mode) {
		let t = KanColleDatabase.reqBattleMidnightBattle.timestamp();
		let data = KanColleDatabase.reqBattleMidnightBattle.get();

		if (mode.practice)
		    return;

		this._db.deck['e1'].ships = this._get_compat_array(data.api_ship_ke);
		if (mode.ecombined)
		    this._db.deck['e2'].ships = this._get_compat_array(data.api_ship_ke_combined);

		this._ts = t;
		return [-1];
	    },
	    function(mode) {
		let t = KanColleDatabase.reqBattleMidnightBattle.timestamp();
		let data = KanColleDatabase.reqBattleMidnightBattle.get();
		let deckid = data.api_deck_id || data.api_dock_id;  // typo?
		let deckid2 = mode.combined ? (deckid % 4) + 1 : deckid;
		let data_f_nowhps;
		let data_f_maxhps;
		let data_e_nowhps;
		let data_e_maxhps;
		let data_f_nowhps_combined;
		let data_f_maxhps_combined;
		let data_e_nowhps_combined;
		let data_e_maxhps_combined;

		debugprint('data = ' + JSON.stringify(data));

		this._update_escape(deckid, data.api_escape_idx);
		if (deckid != deckid2)
		    this._update_escape(deckid2, data.api_escape_idx_combined);

		if (data.api_f_nowhps)
		    data_f_nowhps = this._get_compat_array(data.api_f_nowhps);
		else
		    data_f_nowhps = data.api_nowhps.slice(0,7);

		if (data.api_f_maxhps)
		    data_f_maxhps = this._get_compat_array(data.api_f_maxhps);
		else
		    data_f_maxhps = data.api_maxhps.slice(0,7);

		this._start_battle(deckid, data_f_nowhps, data_f_maxhps);

		debugprint('f_maxhps/f_nowhps:   ' + data_f_nowhps.toSource() + '/' + data_f_maxhps.toSource());

		if (mode.combined || mode.ecombined) {
		    if (data.api_f_nowhps_combined)
			data_f_nowhps_combined = this._get_compat_array(data.api_f_nowhps_combined);
		    else
			data_f_nowhps_combined = data.api_nowhps_combined.slice(0,7);

		    if (data.api_f_maxhps_combined)
			data_f_maxhps_combined = this._get_compat_array(data.api_f_maxhps_combined);
		    else
			data_f_maxhps_combined = data.api_maxhps_combined.slice(0,7);

		    this._start_battle(deckid2, data_f_nowhps_combined, data_f_maxhps_combined);

		    debugprint('f_maxhps2/f_nowhps2: ' + data_f_nowhps_combined.toSource() + '/' + data_f_maxhps_combined.toSource());
		}

		if (data.api_e_nowhps)
		    data_e_nowhps = this._get_compat_array(data.api_e_nowhps);
		else
		    data_e_nowhps = data.api_nowhps.slice(6);

		if (data.api_e_maxhps)
		    data_e_maxhps = this._get_compat_array(data.api_e_maxhps);
		else
		    data_e_maxhps = data.api_maxhps.slice(6);

		this._start_battle('e1', data_e_nowhps, data_e_maxhps);
		this._fixup_battle('e1');

		debugprint('e_maxhps/e_nowhps: ' + data_e_nowhps.toSource() + '/' + data_e_maxhps.toSource());

		if (mode.ecombined) {
		    if (data.api_e_nowhps_combined)
			data_e_nowhps_combined = this._get_compat_array(data.api_e_nowhps_combined);
		    else
			data_e_nowhps_combined = data.api_nowhps_combined.slice(6);

		    if (data.api_e_maxhps_combined)
			data_e_maxhps_combined = this._get_compat_array(data.api_e_maxhps_combined);
		    else
			data_e_maxhps_combined = data.api_maxhps_combined.slice(6);

		    this._start_battle('e2', data_e_nowhps_combined, data_e_maxhps_combined);
		    this._fixup_battle('e2');

		    debugprint('e_maxhps2/e_nowhps2: ' + data_e_nowhps_combined.toSource() + '/' + data_e_maxhps_combined.toSource());
		}

		this._ts = t;
		return [0];
	    },
	    function(mode) {
		let data = KanColleDatabase.reqBattleMidnightBattle.get();
		let deckid = data.api_deck_id || data.api_dock_id;  // typo?
		let deckid2 = mode.combined ? (deckid % 4) + 1 : deckid;
		let deckida = (data.api_active_deck || [deckid])[0];
		let en = 'e' + (data.api_active_deck || [-1,1])[1];
		let newapi = !!data.api_f_nowhps;

		// 友軍艦隊
		if (data.api_friendly_battle) {
		    debugprint('*** Friendly battle ***');
		    this._update_battle_hourai(data.api_friendly_battle.api_hougeki, -1);
		}

		// 支援
		if (data.api_n_support_flag)
		    this._parse_battle_support(data.api_n_support_flag, data.api_n_support_info, deckid);

		// 索敵
		if (data.api_hougeki) {
		    if (newapi)
			this._update_battle_hourai(data.api_hougeki, deckid, deckid2, en);
		    else
			this._update_battle_hourai(data.api_hougeki, deckida, 0, en);
		}

		this._fixup_battle(deckid);
		if (newapi) {
		    if (deckid != deckid2)
			this._fixup_battle(deckid2);
		} else if (deckid != deckida) {
		    this._fixup_battle(deckida);
		}
		this._fixup_battle('e1');
		if (mode.ecombined && en != 'e1')
		    this._fixup_battle(en);

		return [1];
	    },
	],
	reqSortieGobackPort: function(mode) {
	    let t = KanColleDatabase.reqSortieGobackPort.timestamp();
	    let battle = KanColleDatabase.reqSortieBattleResult.get();
	    let escape = battle ? battle.api_escape : null;
	    let deckid = this._db.deckid;
	    let deckid2 = mode.combined ? (deckid % 4) + 1 : deckid;
	    let escape_ships = [];

	    if (!escape) {
		debugprint('no escape information:' + (battle ? battle.toSource() : 'null'));
		return;
	    }

	    debugprint('escape ships: ' + escape.api_escape_idx.toSource());
	    escape_ships.push(escape.api_escape_idx[0]);

	    if (escape.api_tow_idx) {
		debugprint('tow    ships: ' + escape.api_tow_idx.toSource());
		escape_ships.push(escape.api_tow_idx[0]);
	    }

	    this._fixup_escape(deckid, deckid2, escape_ships);

	    this._ts = t;
	    return [0];
	},
	reqSortieBattleResult: [
	    function(mode) {
	        let t = KanColleDatabase.reqSortieBattleResult.timestamp();
		// Notify listeners and update slotitems.
		let that = this;
		Object.keys(this._db.deck).forEach(function(deckid) {
		    if (that._db.deck[deckid].consumed)
			debugprint('Consumed items: ' + that._db.deck[deckid].consumed.toSource());
		});
		this._ts = t;
		return [2];
	    },
	    function(mode) {
		let deckid = this._db.deckid;
		let deckid2 = mode.combined ? (deckid % 4) + 1 : deckid;
		let that = this;
		let warn = [];

		//if (mode.practice)
		//    return;
		if (!KanColleUtils.getBoolPref('popup.damage-warning'))
		    return;

		this._check_warn(warn, deckid);
		if (deckid != deckid2)
		    this._check_warn(warn, deckid2);
		KanColleTimerUtils.console.log('warn = ' + warn.toSource());
		if (warn.length) {
		    const txt_s = '\u306E';	//の
		    const txt_tail = '\u304c\u5927\u7834\u3057\u3066\u3044\u307e\u3059\u3002';	// が大破しています。
		    let txt = warn.map(function(e) {
			return e.name + txt_s + e.ships.map(function(f) { return f.name }).join(', ')
		    }).join(', ') + txt_tail;
		    let sound_conf = KanColleUtils.getUnicharPrefFileURI('sound.damage-warning');
		    if (sound_conf)
			KanColleTimerUtils.sound.play(sound_conf);
		    KanColleTimerUtils.alert.show(KanColleUtils.IconURL, KanColleUtils.Title,
						  txt, false, 'damage-warning', null);

		    let ntfy_uri = KanColleUtils.getUnicharPref('ntfy.uri', '');
		    let ntfy_topic = KanColleUtils.getUnicharPref('ntfy.topic', '');
		    let obj = {
			'topic': ntfy_topic,
			'message': txt,
			'title': KanColleUtils.Title,
			'priority': 5,
			'tags': ['anchor','rotating_light'],
		    };
		    debugprint(`ntfy: url=${ ntfy_uri }, obj=${ JSON.stringify(obj) }`);
		    if (ntfy_uri != '' && ntfy_topic != '') {
			let xhr = KanColleTimerUtils.net.createXMLHttpRequest();
			KanColleUtils.sendObject(xhr, 'POST', ntfy_uri, obj);
		    }
		}
		return;
	    },
	    function(mode) {
	        let t = KanColleDatabase.reqSortieBattleResult.timestamp();
		let data = KanColleDatabase.reqSortieBattleResult.get();
		this._recordNewShip(t, data);
		this._update_battlestat(data.api_win_rank);
		return [3];
	    },
	],
	memberBasic: function() {
	    let t = KanColleDatabase.memberBasic.timestamp();
	    this._init_battle();
	    this._ts = t;
	    return [-3];
	},
    };

    this._get_compat_array = function(v) {
	let a = [ -1, -1, -1, -1, -1, -1, -1 ];
	for (let i = 0; i < v.length; i++)
	    a[i + 1] = v[i];
	return a;
    };

    this._check_warn = function(warn, deckid) {
	let deck = KanColleDatabase.deck.get(deckid);
	let d = this._db.deck[deckid];
	let warns = [];

	if (!deck || !d)
	    return;

	for (let i in d.result) {
	    let res = d.result[i];
	    let ratio = res.cur / res.maxhp;
	    let ship = i >= 1 ? KanColleDatabase.ship.get(deck.api_ship[i - 1]) : null;
	    let flags = 0;

	    // 旗艦
	    if (i <= 1)
		flags |= 0x800;
	    // 待避
	    if (d.escape[i])
		flags |= 0x400;
	    // 大破未満
	    if (ratio > 0.25)
		flags |= 0x100;
	    // 装備
	    slotitem = ship.api_slot.slice();
	    slotitem.push(isNaN(ship.api_slot_ex) ? Number.NaN : ship.api_slot_ex);
	    slotitem.forEach(function(e,i) {
		let item = isNaN(e) ? null : KanColleDatabase.slotitem.get(e);
		let itemtype = item ? KanColleDatabase.masterSlotitem.get(item.api_slotitem_id) : null;
		if (!itemtype)
		    return;
		if (itemtype.api_type[2] == 23)
		    flags |= (1 << i);
	    });
	    // 出力
	    KanColleTimerUtils.console.log(deckid + '#' + i + ': ' + ('000000000000' + flags.toString(2)).slice(-12));
	    if (!flags) {
		let name = (KanColleDatabase.masterShip.get(ship.api_ship_id) || {}).api_name;
		let lv = ship.api_lv;
		warns.push({
		    pos: i,
		    _name: name,
		    _lv: lv,
		    name: name + ' Lv' + lv,
		});
	    }
	}
	if (warns.length)
	    warn.push({ _deck: deckid, name: deck.api_name, ships: warns, });
    };

    this._recordNewShip = function(time, data) {
	if (!data.api_get_ship)
	    return;
	KanColleDatabase.ship.recordNewShip(
	    time,
	    data.api_quest_name,
	    data.api_enemy_info.api_deck_name,
	    data.api_get_ship.api_ship_type,
	    data.api_get_ship.api_ship_name,
	    data.api_win_rank
	);
    };

    this._parse_battle_kouku = function(api_stage_flag, api_kouku, deckid, deckid2) {
	if (!api_kouku)
	    api_kouku = [];
	if (!api_stage_flag)
	    api_stage_flag = [ !!api_kouku.api_stage1, !!api_kouku.api_stage2, !!api_kouku.api_stage3 ];

	// 索敵
	if (api_stage_flag[0])
	    this._update_battle_raibak(api_kouku.api_stage1, deckid);
	if (api_stage_flag[1])
	    this._update_battle_raibak(api_kouku.api_stage2, deckid);
	if (api_stage_flag[2])
	    this._update_battle_raibak(api_kouku.api_stage3, deckid);
	// 索敵(連合艦隊用フラグなし？)
	if (api_kouku.api_stage3_combined)
	    this._update_battle_raibak(api_kouku.api_stage3_combined, deckid2, 'e2');
    };

    this._parse_battle_support = function(api_support_flag, api_support_info, deckid) {
	switch (api_support_flag) {
	case undefined:
	case 0:
	    break;
	case 1: /* 航空支援 */
	case 4:	/* 対潜支援 */
	    if (api_support_info.api_support_airatack.api_stage_flag[2])
		this._update_battle_raibak(api_support_info.api_support_airatack.api_stage3, deckid);
	    break;
	case 2: /* 支援射撃 */
	case 3: /* 支援長距離雷撃 */
	    this._update_battle_support(api_support_info.api_support_hourai);
	    break;
	default:
	    debugprint('support: unknown ' + api_support_flag);
	}
    };

    this._parse_battle_hourai = function(mode, api_hourai_flag, api_hougeki1, api_hougeki2, api_hougeki3, api_raigeki, deckid, deckid2, newapi) {
	if (!api_hourai_flag || api_hourai_flag.length != 4)
	    api_hourai_flag = [1,1,1,1];

	if (!mode.ecombined) {
	    if (!mode.combined || mode.water) {
		// 通常艦隊砲雷撃
		// 水上連合艦隊砲雷撃 (第一艦隊砲撃(x2)->第二艦隊砲撃->第二艦隊雷撃)
		if (api_hourai_flag[0])
		    this._update_battle_hourai(api_hougeki1, deckid);
		if (api_hourai_flag[1])
		    this._update_battle_hourai(api_hougeki2, deckid);
		if (api_hourai_flag[2]) {
		    if (newapi)
			this._update_battle_hourai(api_hougeki3, deckid, deckid2);
		    else
			this._update_battle_hourai(api_hougeki3, deckid2);
		}
		if (api_hourai_flag[3]) {
		    if (newapi)
			this._update_battle_raibak(api_raigeki, deckid, 'e1', deckid2, 'e2');
		    else
			this._update_battle_raibak(api_raigeki, deckid2);
		}
	    } else {
		// {機動,輸送}連合艦隊砲雷撃 (第二艦隊砲撃->第二艦隊雷撃->第一艦隊砲撃(x2))
		if (api_hourai_flag[0]) {
		    if (newapi)
			this._update_battle_hourai(api_hougeki1, deckid, deckid2);
		    else
			this._update_battle_hourai(api_hougeki1, deckid2);
		}
		if (api_hourai_flag[1]) {
		    if (newapi)
			this._update_battle_raibak(api_raigeki, deckid, 'e1', deckid2, 'e2');
		    else
			this._update_battle_raibak(api_raigeki, deckid2);
		}
		if (api_hourai_flag[2])
		    this._update_battle_hourai(api_hougeki2, deckid);
		if (api_hourai_flag[3])
		    this._update_battle_hourai(api_hougeki3, deckid);
	    }
	} else {
	    // 連合vs連合
	    if (mode.combined) {
		if (mode.water) {
		    // 砲撃#1,砲撃#2,砲撃#2,雷撃#2
		    if (api_hourai_flag[0])
			this._update_battle_hourai(api_hougeki1, deckid, deckid2);
		    if (api_hourai_flag[1])
			this._update_battle_hourai(api_hougeki2, deckid, deckid2);
		    if (api_hourai_flag[2])
			this._update_battle_hourai(api_hougeki3, deckid, deckid2);
		    if (api_hourai_flag[3])
			this._update_battle_raibak(api_raigeki, deckid, 'e1', deckid2, 'e2');
		} else {
		    // 砲撃#1,砲撃#2,雷撃#2,砲撃#1
		    if (api_hourai_flag[0])
			this._update_battle_hourai(api_hougeki1, deckid, deckid2);
		    if (api_hourai_flag[1])
			this._update_battle_hourai(api_hougeki2, deckid, deckid2);
		    if (api_hourai_flag[2])
			this._update_battle_raibak(api_raigeki, deckid, 'e1', deckid2, 'e2');
		    if (api_hourai_flag[3])
			this._update_battle_hourai(api_hougeki3, deckid, deckid2);
		}
	    } else {
		if (api_hourai_flag[0])
		    this._update_battle_hourai(api_hougeki1, deckid, deckid2);
		if (api_hourai_flag[1])
		    this._update_battle_raibak(api_raigeki, deckid, 'e1', deckid2, 'e2');
		if (api_hourai_flag[2])
		    this._update_battle_hourai(api_hougeki2, deckid, deckid2);
		if (api_hourai_flag[3])
		    this._update_battle_hourai(api_hougeki3, deckid, deckid2);
	    }
	}
    };

    this._update_battle_raibak = function(data, deckid, edeckid, deckid2, edeckid2, deckid1, edeckid1) {
	let that = this;

	if (!data)
	    return;

	debugprint('====RAIBAK====');

	if (!edeckid)
	    edeckid = 'e1';
	if (!deckid1)
	    deckid1 = deckid;
	if (!deckid2)
	    deckid2 = deckid;
	if (!edeckid1)
	    edeckid1 = edeckid;
	if (!edeckid2)
	    edeckid2 = edeckid;

	debugprint('deckid = ' + deckid + ', deckid2 = ' + (deckid2 || 'null') + ', ' +
		   'edeckid = ' + edeckid + ', edeckid2 = ' + (edeckid2 || 'null') + ', ' +
		   'deckid1 = ' + deckid1 + ', edeckid1 = ' + edeckid1);
	debugprint('frai = ' + JSON.stringify(data.api_frai || null) +
		   ', erai = ' + JSON.stringify(data.api_erai || null) +
		   ', fdam = ' + JSON.stringify(data.api_fdam || null) +
		   ', edam = ' + JSON.stringify(data.api_edam || null));

	function __parse_xdam(damages, sdeck, ddeck) {
	    if (!damages)
		return;
	    for (let i = 0; i < damages.length; i++) {
		if (damages[i] === undefined || damages[i] < 0)
		    continue;
		that._update_battle(sdeck, -1, ddeck, i + 1, Math.floor(damages[i]));
	    }
	}

	function __parse_xydam(dposs, damages, sdeck, ddeck, sdeck2, ddeck2) {
	    if (!dposs || !damages)
		return;
	    for (let i = 0; i < damages.length; i++) {
		let sd, sp, dd, dp;
		if (damages[i] === undefined || damages[i] < 0 || dposs[i] < 0)
		    continue;
		if (sdeck == sdeck2 || i < 6) {
		    sd = sdeck;
		    sp = i + 1;
		} else {
		    sd = sdeck2;
		    sp = i - 6 + 1;
		}
		if (ddeck == ddeck2 || dposs[i] < 6) {
		    dd = ddeck;
		    dp = dposs[i] + 1;
		} else {
		    dd = ddeck2;
		    dp = dposs[i] - 6 + 1;
		}
		that._update_battle(sd, sp, dd, dp, Math.floor(damages[i]));
	    }
	}

	//自軍 => 敵軍
	if (data.api_frai)
	    __parse_xydam(data.api_frai, data.api_fydam, deckid1, edeckid, deckid2, edeckid2);
	else
	    __parse_xdam(data.api_edam, deckid1, edeckid);

	//敵軍 => 自軍
	if (data.api_erai)
	    __parse_xydam(data.api_erai, data.api_eydam, edeckid1, deckid, edeckid2, deckid2);
	else
	    __parse_xdam(data.api_fdam, edeckid1, deckid);

	this._check_downed_ships(deckid);
    };

    this._update_battle_hourai = function(data, deckid, deckid2, edeck) {
	if (!data)
	    return;

	debugprint('====HOURAI====');

	if (!edeck)
	    edeck = 'e1';
	if (!deckid2)
	    deckid2 = deckid;

	for (let i = 0; i < data.api_at_list.length; i++) {
	    let sdeck, spos;

	    spos = data.api_at_list[i] + 1;
	    if (data.api_at_eflag) {
		// 敵連合艦隊
		if (data.api_at_eflag[i]) {
		    if (spos > 6) {
			sdeck = 'e2';
			spos -= 6;
		    } else {
			sdeck = 'e1';
		    }
		} else {
		    if (deckid != deckid2 && spos > 6) {
			sdeck = deckid2;
			spos -= 6;
		    } else {
			sdeck = deckid;
		    }
		}
	    } else {
		sdeck = deckid;
		if (spos > 6) {
		    sdeck = edeck;
		    spos -= 6;
		}
	    }

	    for (let j = 0; j < data.api_df_list[i].length; j++) {
		let ddeck, dpos, damage;

		dpos = data.api_df_list[i][j] + 1;
		if (data.api_at_eflag) {
		    // 敵連合艦隊
		    if (data.api_at_eflag[i]) {
			if (deckid != deckid2 && dpos > 6) {
			    ddeck = deckid2;
			    dpos -= 6;
			} else {
			    ddeck = deckid;
			}
		    } else {
			if (dpos > 6) {
			    ddeck = 'e2';
			    dpos -= 6;
			} else {
			    ddeck = 'e1';
			}
		    }
		} else {
		    ddeck = deckid;
		    if (dpos > 6) {
			ddeck = edeck;
			dpos -= 6;
		    }
		}

		damage = data.api_damage[i][j];
		if (damage === undefined)
		    continue;

		this._update_battle(sdeck, spos, ddeck, dpos, Math.floor(damage));
		if (ddeck != 'e1' && ddeck != 'e2' && deckid != -1)
		    this._check_downed_ships(deckid);
	    }
	}
    };

    this._update_battle_support = function(data) {
	let poffset = 1;
	debugprint('====SUPPORT====');
	for (let i = 0; i < data.api_damage.length; i++) {
	    let damage = data.api_damage[i];
	    let enemy = (i + poffset <= 6 ? 'e1' : 'e2');
	    let pos = (i + poffset <= 6 ? i : i - 6) + poffset;
	    if (damage > 0)
		this._update_battle(-1, -1, enemy, pos, Math.floor(damage));
	}
    };

    this._update_battle = function(sdeck, spos, ddeck, dpos, damage) {
	let oldhp, newhp;

	if (damage < 0)
	    return -1;

	if (!this._db.deck[ddeck]) {
	    debugprint('no deck info: ' + ddeck);
	    return -1;
	}

	oldhp = this._db.deck[ddeck].nowhps[dpos];
	newhp = oldhp - damage;
	if (newhp < 0)
	    newhp = 0;

	this._db.deck[ddeck].damages[dpos] += damage;
	this._db.deck[ddeck].nowhps[dpos] = newhp;

	if (ddeck == 'e1' || ddeck == 'e2') {
	    debugprint(sdeck + '[' + spos + '] --> ' + ddeck + '[' + dpos + ']: ' +
		       damage + ' (' + oldhp + ' => ' + newhp + ')');
	} else {
	    debugprint(ddeck + '[' + dpos + '] <-- ' + sdeck + '[' + spos + ']: ' +
		       damage + ' (' + oldhp + ' => ' + newhp + ')');
	}

	return newhp > 0 ? 1 : 0;
    };

    this._check_downed_ships = function(deckid) {
	let deck = KanColleDatabase.deck.get(deckid);
	if (!deck)
	    return;
	if (this._db.deck['e1'].practice)
	    return;
	debugprint('--- CHECK DOWNED SHIPS ---');
	for (let i = 2; i <= 7; i++) {
	    let maxhp = this._db.deck[deckid].maxhps[i];
	    if (maxhp === undefined || maxhp < 0)
		continue;
	    if (this._db.deck[deckid].nowhps[i] <= 0) {
		let newhp = this._revive_ship(deckid, i);
		if (newhp > 0)
		    this._db.deck[deckid].nowhps[i] = newhp;
	    }
	}
    };

    this._revive_ship = function(deckid, pos, useitem) {
	let deck = KanColleDatabase.deck.get(deckid);
	let ship = KanColleDatabase.ship.get(deck.api_ship[pos - 1]);
	let slotitem;
	let slot = Number.NaN;
	let item = null;
	let itemid = -1;
	let newhp = -1;

	debugprint(deckid + '#' + pos);

	slotitem = ship.api_slot.slice();
	slotitem.unshift(ship.api_slot_ex);

	for (let i = 0; i < slotitem.length; i++) {
	    let itemtype;

	    item = KanColleDatabase.slotitem.get(slotitem[i]);
	    itemid = item ? item.api_slotitem_id : -1;
	    itemtype = item ? KanColleDatabase.masterSlotitem.get(itemid) : null;

	    if (!itemtype || itemtype.api_type[2] != 23 ||
		(useitem && itemid != useitem)) {
		item = null;
		itemid = -1;
		continue;
	    }

	    //1戦闘で2度轟沈はない

	    slot = i - 1;
	    break;
	}

	if (!isNaN(slot)) {
	    let dcinfo = { ship: pos, slot: slot, item: item.api_id };

	    switch (itemid) {
	    case 42:	//応急修理要員
		newhp = Math.floor(maxhp * (useitem ? 0.5 : 0.2));
		break;
	    case 43:	//応急修理女神
		newhp = maxhp;
		break;
	    default:
		newhp = 0;
	    }
	    dcinfo.nowhp = newhp;

	    this._db.deck[deckid].consumed[item.api_id] = dcinfo;
	}
	return newhp;
    };

    this._update_escape = function(deckid, data) {
	if (!data)
	    data = [];
	debugprint('escape' + deckid + ':' + data.toSource());
	if (!Array.isArray(data))
	    return;
	if (!this._db.deck[deckid])
	    this._db.deck[deckid] = {};
	this._db.deck[deckid].escape = data.reduce(function(p,c) {
	    p[c] = 1;
	    return p;
	}, [-1,0,0,0,0,0,0,0]);
    };
    this._fixup_escape = function(deckid1, deckid2, data) {
	let deckid;
	for (let i = 0; i < data.length; i++) {
	    let v = data[i];
	    if (deckid1 == deckid2 || v <= 6) {
		deckid = deckid1;
	    } else {
		deckid = deckid2;
		v -= 6;
	    }
	    this._db.deck[deckid].escape[v] = 1;
	}
    };

    this._init_battle = function() {
	let decks = KanColleDatabase.deck.list().slice();
	decks.push('e1');
	decks.push('e2');

	this._db.deckid = 0;
	this._db.deck = {};
	for (let i = 0; i < decks.length; i++) {
	    let deck = decks[i];
	    this._db.deck[deck] = {
		escape: [-1,0,0,0,0,0,0,0],
		damages: [-1,0,0,0,0,0,0,0],
		result: [],
		consumed: {},
	    };
	}
    };

    this._reset_battle = function(softreset) {
	let decks = KanColleDatabase.deck.list().slice();
	decks.push('e1');

	for (let i = 0; i < decks.length; i++) {
	    let deckid = decks[i];
	    let _escape = this._db.deck[deckid] ? this._db.deck[deckid].escape : [-1,0,0,0,0,0,0,0];
	    let _first = this._db.deck[deckid] ? !!this._db.deck[deckid]._first : false;
	    this._db.deck[deckid] = {
		escape: _escape,
		_first: _first,
		damages: [-1,0,0,0,0,0,0,0],
		result: [],
		consumed: {},
	    };
	}
    };

    this._start_battle = function(deckid, nowhps, maxhps) {
	let old_result = this._db.deck[deckid].result;
	let new_nowhps;

	this._db.deck[deckid].nowhps = nowhps.slice();
	this._db.deck[deckid].nowhps[0] = -1;
	this._db.deck[deckid].maxhps = maxhps.slice();
	this._db.deck[deckid].maxhps[0] = -1;

	new_nowhps = this._db.deck[deckid].nowhps;

	if (old_result.length && old_result.length == new_nowhps.length) {
	    for (let i = 0; i < old_result.length; i++) {
		if (!old_result[i] || !old_result[i].cur)
		    continue;
		debugprint(`deck[${ deckid }] #${ i }: ${ old_result[i].cur } => ${ new_nowhps[i] } : ${ old_result[i].cur == new_nowhps[i] ? 'ok' : 'ng' }`);
	    }
	}
    };

    this._fixup_battle = function(deckid) {
	let s = '';

	for (let i = 0; i <= 7; i++) {
	    let cur;
	    let ratio;
	    let nowhps;
	    let maxhps;
	    let battle_data;

	    maxhp = this._db.deck[deckid].maxhps[i];
	    if (maxhp === undefined || maxhp < 0)
		continue;
	    cur = this._db.deck[deckid].nowhps[i];

	    ratio = cur / maxhp;

	    s += deckid + '#' + i + ': ' + cur + '/' + maxhp + ' = ' +
		 ratio.toFixed(3) +
		 (Math.floor(ratio * 1000) != ratio * 1000 ? '+' : '') +
		 '\n';

	    this._db.deck[deckid].result[i] = {
		maxhp: maxhp,
		cur: cur,
		damage: this._db.deck[deckid].damages[i],
	    };
	}
	debugprint(s);
    };

    this._update_battlestat = function(rank) {
	let ships;
	let map;

	if (!this._db.deck['e1'].practice) {
	    if (this._db.deck['e1']._first) {
		this._stat.first++;
		delete this._db.deck['e1']._first;
	    }

	    ships = this._db.deck['e1'].ships;
	    for (let i = 0; i < ships.length; i++) {
		let ship = ships[i];
		let type = KanColleDatabase.masterShip.get(ship);
		let res = this._db.deck['e1'].result[i];
		if (!type)
		    continue;
		if (res.cur == 0) {
		    let stype = type.api_stype;
		    if (!this._stat.type[stype])
			this._stat.type[stype] = 0;
		    this._stat.type[stype]++;
		}
	    }
	}

	map = this._stat.map;
	if (!map[this._db.deck['e1'].name]) {
	    map[this._db.deck['e1'].name] = {
		practice: this._db.deck['e1'].practice,
		boss: this._db.deck['e1'].boss,
		count: 0,
		rank: {},
	    };
	}

	map[this._db.deck['e1'].name].count++;

	if (!map[this._db.deck['e1'].name].rank[rank])
	     map[this._db.deck['e1'].name].rank[rank] = 0;
	map[this._db.deck['e1'].name].rank[rank]++;

	debugprint(this._stat.toSource());
    };

    this.get = function(deckid) {
	return this._db.deck[deckid];
    };

    this.list = function() {
	return Object.keys(this._db.deck);
    };

    this.stat = function() {
	return this._stat;
    };
};
KanColleBattleDB.prototype = new KanColleCombinedDB();

/*
 * TouRabu
 */
function TouRabuParseTime(s) {
    if (s && s.match(/^(\d+)-(\d+)-(\d+) (\d+):(\d+):(\d+)$/)) {
	let year = parseInt(RegExp.$1, 10),
	    month = parseInt(RegExp.$2, 10),
	    day = parseInt(RegExp.$3, 10),
	    hour = parseInt(RegExp.$4, 10),
	    min = parseInt(RegExp.$5, 10),
	    sec = parseInt(RegExp.$6, 10);
	let d = new Date;
	d.setUTCFullYear(year, month - 1, day);
	d.setUTCHours(hour);
	d.setUTCMinutes(min);
	d.setUTCSeconds(sec);
	return d.getTime() - 9 * 3600000;
    } else {
	return Number.NaN;
    }
}

var TouRabuPartyDB = function() {
    this._init();

    this._db = {
	hash: {},
	list: [],
    };
    this._ts = null;
    this._now = null;

    this._update = {
	tourabuConquest: function() {
	    let t = KanColleDatabase.tourabuConquest.timestamp();
	    let d = KanColleDatabase.tourabuConquest.get();

	    this._update_party(d.party);

	    if (d.summary) {
		for (let k of Object.keys(d.summary))
		    this._db.hash[k]._summary = JSON.parse(JSON.stringify(d.summary[k]));
	    }

	    this._now = TouRabuParseTime(d.now);
	    this._ts = t;

	    debugprint('party = ' + this._db.hash.toSource());

	    if (this._update_timeout(this.list()))
		this._alarm.update();

	    return [];
	},
	tourabuLoginStart: function() {
	    let t = KanColleDatabase.tourabuLoginStart.timestamp();
	    let d = KanColleDatabase.tourabuLoginStart.get();

	    this._update_party(d.party);

	    this._now = TouRabuParseTime(d.now);
	    this._ts = t;

	    debugprint('party = ' + this._db.hash.toSource());

	    if (this._update_timeout(this.list()))
		this._alarm.update();

	    return [];
	},
    };

    this._update_party = function(party) {
	let hash = JSON.parse(JSON.stringify(party));
	let list = Object.keys(hash);
	for (let k of list) {
	    hash[k]._summary = (this._db.hash[k] || {})._summary || null;
	    hash[k]._finished_at = TouRabuParseTime(hash[k].finished_at);
	}
	this._db.hash = hash;
	this._db.list = list;
    };

    this._update_timeout = function(docks) {
	let count = 0;
	let offset = this._ts - this._now;
	debugprint('party: update timeout: ' + docks);
	for (let k of docks)
	    count += this._alarm.set_timeout(k, offset + this.get(k)._finished_at);
	return count;
    };

    this._timer_notify_set = function(id, state) {
	debugprint('mission: notify(deck = ' + id + ', state = ' + state + ')');
	//0        1  2    3  4  5    6        7          8  9    10   11
	//まもなく|第|部隊|が|の|遠征|していま|が帰還しま|す|した|せん|。
	const _m = '\u307e\u3082\u306a\u304f|\u7b2c|\u90e8\u968a|\u304c|\u306e|\u9060\u5f81|\u3057\u3066\u3044\u307e|\u304c\u5e30\u9084\u3057\u307e|\u3059|\u3057\u305f|\u305b\u3093|\u3002'.split(/\|/);
	let msg = null;
	let popup = KanColleUtils.getBoolPref('popup.mission');
	let sound = null;
	let deck = this.get(id);
	let deckname = deck ? deck.party_name : (_m[1] + id + _m[2]);
	let ntfy_prio = KanColleUtils.getBoolPref('ntfy.mission') ? 3 : -1;
	let ntfy_tags = ['crossed_swords','house'];
	switch (state) {
	case 3:
	    msg =         deckname         + _m[4] + _m[5]         + _m[7]         + _m[9]          + _m[11];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.mission');
	    break;
	case 2:
	    msg = _m[0] + deckname         + _m[4] + _m[5]         + _m[7] + _m[8]                  + _m[11];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.1min.mission');
	    popup = KanColleUtils.getBoolPref('popup.mission-1min');
	    ntfy_prio = KanColleUtils.getBoolPref('ntfy.mission-1min') ? 3 : -1;
	    break;
	case 1:
	    msg =         deckname + _m[3]         + _m[5] + _m[6]         + _m[8]                  + _m[11];
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	    break;
	case -1:
	    msg =         deckname + _m[3]         + _m[5] + _m[6]                         + _m[10] + _m[11];
	    /*FALLTHROUGH*/
	default:
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	}
	return { popup: popup, sound: sound, msg: msg,
		 ntfy_prio: ntfy_prio, ntfy_tags: ntfy_tags,
		 icon: KanColleUtils.TouRabuIconURL,
		 popup_click_url: KanColleUtils.TouRabuURL };
    };

    this.observe = function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm.update();
	    break;
	default:;
	}
    };
    this.now = function() {
	return this._now;
    };

    this.get = function(id) {
	return this._db.hash[id];
    };

    this.list = function() {
	return this._db.list;
    };
};
TouRabuPartyDB.prototype = new KanColleCombinedDB();

var TouRabuRepairDB = function() {
    this._init();

    this._db = {
	hash: {},
	list: [],
    };
    this._ts = null;
    this._now = null;

    this._update = {
	_tourabuRepair: function() {
	    let t = KanColleDatabase._tourabuRepair.timestamp();
	    let d = KanColleDatabase._tourabuRepair.get();

	    this._update_repair(d);

	    this._now = d._now;
	    this._ts = t;

	    debugprint('repair = ' + this._db.hash.toSource());

	    if (this._update_timeout(this.list()))
		this._alarm.update();

	    return [];
	},
    };

    this._update_repair = function(repair) {
	let hash = (Array.isArray(repair) && !repair.length) ? {} : JSON.parse(JSON.stringify(repair));
	let list;
	for (let k of this._db.list) {
	    if (!hash[k])
		hash[k] = {};
	}
	list = Object.keys(hash).filter(function(v) { return !isNaN(parseInt(v, 10)); });
	for (let k of list) {
	    hash[k]._finished_at = TouRabuParseTime(hash[k].finished_at);
	}
	this._db.hash = hash;
	this._db.list = list;
    };

    this._update_timeout = function(docks) {
	let count = 0;
	let offset = this._ts - this._now;
	debugprint('repair: update timeout: ' + docks);
	for (let k of docks)
	    count += this._alarm.set_timeout(k, offset + this.get(k)._finished_at);
	return count;
    };

    this._timer_notify_set = function(id, state) {
	debugprint('repair: notify(deck = ' + id + ', state = ' + state + ')');
	//0        1  2    3  4  5    6        7          8  9    10   11
	//まもなく|第|工房|が|の|手入|していま|が完了しま|す|した|せん|。
	const _m = '\u307e\u3082\u306a\u304f|\u7b2c|\u5de5\u623f|\u304c|\u306e|\u624b\u5165|\u3057\u3066\u3044\u307e|\u304c\u5b8c\u4e86\u3057\u307e|\u3059|\u3057\u305f|\u305b\u3093|\u3002'.split(/\|/);
	let msg = null;
	let popup = KanColleUtils.getBoolPref('popup.ndock');
	let sound = null;
	let deck = this.get(id);
	let deckname = _m[1] + id + _m[2];
	let ntfy_prio = KanColleUtils.getBoolPref('ntfy.ndock') ? 3 : -1;
	let ntfy_tags = ['crossed_swords','sleeping_bed'];
	switch (state) {
	case 3:
	    msg =         deckname         + _m[4] + _m[5]         + _m[7]         + _m[9]          + _m[11];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.ndock');
	    break;
	case 2:
	    msg = _m[0] + deckname         + _m[4] + _m[5]         + _m[7] + _m[8]                  + _m[11];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.1min.ndock');
	    popup = KanColleUtils.getBoolPref('popup.ndock-1min');
	    ntfy_prio = KanColleUtils.getBoolPref('ntfy.ndock-1min') ? 3 : -1;
	    break;
	case 1:
	    msg =         deckname + _m[3]         + _m[5] + _m[6]         + _m[8]                  + _m[11];
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	    break;
	case -1:
	    msg =         deckname + _m[3]         + _m[5] + _m[6]                         + _m[10] + _m[11];
	    /*FALLTHROUGH*/
	default:
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	}
	return { popup: popup, sound: sound, msg: msg,
		 ntfy_prio: ntfy_prio, ntfy_tags: ntfy_tags,
		 icon: KanColleUtils.TouRabuIconURL,
		 popup_click_url: KanColleUtils.TouRabuURL };
    };

    this.observe = function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm.update();
	    break;
	default:;
	}
    };
    this.now = function() {
	return this._now;
    };

    this.get = function(id) {
	return this._db.hash[id];
    };

    this.list = function() {
	return this._db.list;
    };
};
TouRabuRepairDB.prototype = new KanColleCombinedDB();

var TouRabuForgeDB = function() {
    this._init();

    this._db = {
	hash: {},
	list: [],
    };
    this._ts = null;
    this._now = null;

    this._update = {
	_tourabuForge: function() {
	    let t = KanColleDatabase._tourabuForge.timestamp();
	    let d = KanColleDatabase._tourabuForge.get();

	    this._update_forge(d);

	    this._now = d._now;
	    this._ts = t;

	    debugprint('forge = ' + this._db.hash.toSource());

	    if (this._update_timeout(this.list()))
		this._alarm.update();

	    return [];
	},
    };

    this._update_forge = function(forge) {
	let hash = JSON.parse(JSON.stringify(forge));
	let list = Object.keys(hash).filter(function(v) { return !isNaN(parseInt(v, 10)); });
	for (let k of list) {
	    hash[k]._finished_at = TouRabuParseTime(hash[k].finished_at);
	}
	this._db.hash = hash;
	this._db.list = list;
    };

    this._update_timeout = function(docks) {
	let count = 0;
	let offset = this._ts - this._now;
	debugprint('forge: update timeout: ' + docks);
	for (let k of docks)
	    count += this._alarm.set_timeout(k, offset + this.get(k)._finished_at);
	return count;
    };

    this._timer_notify_set = function(id, state) {
	debugprint('forge: notify(deck = ' + id + ', state = ' + state + ')');
	//0        1  2    3  4  5    6        7          8  9    10   11
	//まもなく|第|工房|が|の|錬刀|していま|が完了しま|す|した|せん|。
	const _m = '\u307e\u3082\u306a\u304f|\u7b2c|\u5de5\u623f|\u304c|\u306e|\u932c\u5200|\u3057\u3066\u3044\u307e|\u304c\u5b8c\u4e86\u3057\u307e|\u3059|\u3057\u305f|\u305b\u3093|\u3002'.split(/\|/);
	let msg = null;
	let popup = KanColleUtils.getBoolPref('popup.kdock');
	let sound = null;
	let deck = this.get(id);
	let deckname = _m[1] + id + _m[2];
	let ntfy_prio = KanColleUtils.getBoolPref('ntfy.kdock') ? 3 : -1;
	let ntfy_tags = ['crossed_swords','hammer'];
	switch (state) {
	case 3:
	    msg =         deckname         + _m[4] + _m[5]         + _m[7]         + _m[9]          + _m[11];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.kdock');
	    break;
	case 2:
	    msg = _m[0] + deckname         + _m[4] + _m[5]         + _m[7] + _m[8]                  + _m[11];
	    sound = KanColleUtils.getUnicharPrefFileURI('sound.1min.kdock');
	    popup = KanColleUtils.getBoolPref('popup.kdock-1min');
	    ntfy_prio = KanColleUtils.getBoolPref('ntfy.kdock-1min') ? 3 : -1;
	    break;
	case 1:
	    msg =         deckname + _m[3]         + _m[5] + _m[6]         + _m[8]                  + _m[11];
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	    break;
	case -1:
	    msg =         deckname + _m[3]         + _m[5] + _m[6]                         + _m[10] + _m[11];
	    /*FALLTHROUGH*/
	default:
	    popup = false;
	    sound = null;
	    ntfy_prio = -1;
	}
	return { popup: popup, sound: sound, msg: msg,
		 ntfy_prio: ntfy_prio, ntfy_tags: ntfy_tags,
		 icon: KanColleUtils.TouRabuIconURL,
		 popup_click_url: KanColleUtils.TouRabuURL };
    };

    this.observe = function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm.update();
	    break;
	default:;
	}
    };
    this.now = function() {
	return this._now;
    };

    this.get = function(id) {
	return this._db.hash[id];
    };

    this.list = function() {
	return this._db.list;
    };
};
TouRabuForgeDB.prototype = new KanColleCombinedDB();

/*
 * ハンドラ一覧。
 * 2段階で要求/応答に応じたハンドラを定義。
 */
var KanColleTimerHandlers = [
    {
	regexp: /^https?:\/\/.*(\/kcsapi\/.*)/,
	request: null,
	response: function(s, match) {
	    let data;
	    try {
		let d = JSON.parse(s.substring(s.indexOf('svdata=') + 7));
		if (d.api_result == 1)
		    data = d;
	    } catch(x) {}
	    if (data) {
		// portの場合 null, それ以外は match[1]
		KanColleDatabase.port.mark(match[1].match(/kcsapi\/api_port\/port/) ? null : match[1]);
	    }
	    return data;
	},
	handlers: [
	    {
		regexp: /kcsapi\/api_start2/,
		response: function(data, match) {
		    this.masterMaparea.update(data.api_data.api_mst_maparea);
		    this.masterMapinfo.update(data.api_data.api_mst_mapinfo);
		    this.masterMission.update(data.api_data.api_mst_mission);
		    this.masterShip.update(data.api_data.api_mst_ship);
		    this.masterSlotitem.update(data.api_data.api_mst_slotitem);
		    this.masterSlotitemEquiptype.update(data.api_data.api_mst_slotitem_equiptype);
		    this.masterUseitem.update(data.api_data.api_mst_useitem);
		    this.masterEquipExslot.update(data.api_data.api_mst_equip_exslot);
		    this.masterEquipExslotShip.update(data.api_data.api_mst_equip_exslot_ship);
		    this.masterEquipShip.update(data.api_data.api_mst_equip_ship);
		    this.masterStype.update(data.api_data.api_mst_stype);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/maparea/,
		response: function(data, match) {
		    this.masterMaparea.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/mission/,
		response: function(data, match) {
		    this.masterMission.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/ship/,
		response: function(data, match) {
		    this.masterShip.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/slotitem/,
		response: function(data, match) {
		    this.masterSlotitem.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_master\/stype/,
		response: function(data, match) {
		    this.masterStype.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/base_air_corps/,
		response: function(data, match) {
		    this.memberBaseAirCorps.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/basic/,
		response: function(data, match) {
		    this.memberBasic.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/deck(_port)?/,
		response: function(data, match) {
		    this.memberDeck.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/kdock/,
		response: function(data, match) {
		    this._memberKdock.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/mission/,
		response: function(data, match) {
		    this.memberMission.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/mapinfo/,
		response: function(data, match) {
		    this.memberMapinfo.update(data.api_data.api_map_info || data.api_data);
		    if (data.api_data.api_air_base)
			this.memberBaseAirCorps.update(data.api_data.api_air_base);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/material/,
		response: function(data, match) {
		    this.memberMaterial.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/ndock/,
		response: function(data, match) {
		    this._memberNdock.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/practice/,
		response: function(data, match) {
		    this.memberPractice.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/preset_deck/,
		response: function(data, match) {
		    this.memberPresetDeck.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/preset_slot/,
		response: function(data, match) {
		    this.memberPresetSlot.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/questlist/,
		request: function(data, match) {
		    data._tab_id = parseInt(data.api_tab_id, 10);
		    //data._page_no = parseInt(data.api_page_no, 10);
		    this.memberQuestlist.prepare(data);
		},
		response: function(data, match) {
		    this.memberQuestlist.update(data.api_data);
		}
	    },{
		// 2016/04/01
		regexp: /kcsapi\/api_get_member\/require_info/,
		response: function(data, match) {
		    this._memberSlotitem.update(data.api_data.api_slot_item);
		    this.memberUnsetslot.update(data.api_data.api_unsetslot);
		    this.memberUseitem.update(data.api_data.api_useitem);
		    this._memberKdock.update(data.api_data.api_kdock);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/record/,
		response: function(data, match) {
		    this.memberRecord.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/ship2/,
		request: function(data, match) {
		    this._memberShip2.prepare(data);
		},
		response: function(data, match) {
		    // もし request に api_shipid が含まれていたら、
		    // その艦船についてのみ送られてくる
		    if (this._memberShip2.get_req().api_shipid)
			this._memberShip3.update(data.api_data);
		    else
			this._memberShip2.update(data.api_data);
		    this.memberDeck.update(data.api_data_deck);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/ship3/,
		request: function(data, match) {
		    this._memberShip3.prepare(data);
		},
		response: function(data, match) {
		    // もし request に api_shipid が含まれていたら、
		    // その艦船についてのみ送られてくる
		    if (this._memberShip3.get_req().api_shipid)
			this._memberShip3.update(data.api_data.api_ship_data);
		    else
			this._memberShip2.update(data.api_data.api_ship_data);
		    this.memberDeck.update(data.api_data.api_deck_data);
		    this.memberUnsetslot.update(data.api_data.api_slot_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/ship_deck/,
		response: function(data, match) {
		    // 2015/05/18
		    this._memberShip3.update(data.api_data.api_ship_data);
		    this.__memberDeck.update(data.api_data.api_deck_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/slotitem/,
		response: function(data, match) {
		    this._memberSlotitem.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/slot_item/,
		response: function(data, match) {
		    this._memberSlotitem.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/unsetslot/,
		response: function(data, match) {
		    this.memberUnsetslot.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_get_member\/useitem/,
		response: function(data, match) {
		    this.memberUseitem.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_port\/port/,
		response: function(data, match) {
		    this.memberBasic.update(data.api_data.api_basic);
		    this.reqHenseiCombined.update({ api_combined: data.api_data.api_combined_flag });
		    this.memberDeck.update(data.api_data.api_deck_port);
		    this.memberMaterial.update(data.api_data.api_material);
		    this._memberShip2.update(data.api_data.api_ship);
		    this._memberNdock.update(data.api_data.api_ndock);
		}
	    },{
		regexp: /kcsapi\/api_req_air_corps\/change_deployment_base/,
		request: function(data, match) {
		    data._area_id = parseInt(data.api_area_id, 10);
		    data._base_id_src = parseInt(data.api_base_id_src, 10);
		    data._base_id = parseInt(data.api_base_id, 10);
		    data._squadron_id = parseInt(data.api_squadron_id, 10);
		    data._item_id = parseInt(data.api_item_id, 10);
		    this.reqAirCorpsSetPlane.prepare(data);
		},
		response: function(data, match) {
		    this.reqAirCorpsSetPlane.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_air_corps\/change_name/,
		request: function(data, match) {
		    data._area_id = parseInt(data.api_area_id, 10);
		    data._base_id = parseInt(data.api_base_id, 10);
		    this.reqAirCorpsChangeName.prepare(data);
		},
		response: function(data, match) {
		    this.reqAirCorpsChangeName.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_air_corps\/set_action/,
		request: function(data, match) {
		    data._area_id = parseInt(data.api_area_id, 10);
		    data._base_ids = data.api_base_id.split(/,/).map(function(e) { return parseInt(e, 10); });
		    data._action_kinds = data.api_action_kind.split(/,/).map(function(e) { return parseInt(e, 10); });
		    this.reqAirCorpsSetAction.prepare(data);
		},
		response: function(data, match) {
		    this.reqAirCorpsSetAction.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_air_corps\/set_plane/,
		request: function(data, match) {
		    data._area_id = parseInt(data.api_area_id, 10);
		    data._base_id = parseInt(data.api_base_id, 10);
		    data._squadron_id = parseInt(data.api_squadron_id, 10);
		    data._item_id = parseInt(data.api_item_id, 10);
		    this.reqAirCorpsSetPlane.prepare(data);
		},
		response: function(data, match) {
		    this.reqAirCorpsSetPlane.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_air_corps\/supply/,
		request: function(data, match) {
		    data._area_id = parseInt(data.api_area_id, 10);
		    data._squadron_id = data.api_squadron_id.split(/,/)
					.map(function(e) { return parseInt(e, 10); });
		    data._base_id = parseInt(data.api_base_id, 10);
		    this.reqAirCorpsSupply.prepare(data);
		},
		response: function(data, match) {
		    this.reqAirCorpsSupply.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_battle_midnight\/(sp_)?(battle|midnight)/,
		response: function(data, match) {
		    this.reqBattleMidnightBattle.update(data.api_data, {});
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/battleresult/,
		response: function(data, match) {
		    this.reqSortieBattleResult.update(data.api_data, { combined: true });
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/(each_)?battle_water/,
		response: function(data, match) {
		    let ecombined = (match[1] == 'each_');
		    this.reqSortieBattle.update(data.api_data, { combined: true, water: true, ecombined: ecombined });
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/(ld_air|air|each_|ec_)?battle/,
		response: function(data, match) {
		    let ecombined = (match[1] == 'each_' || match[1] == 'ec_');
		    let combined = (match[1] != 'ec_');
		    this.reqSortieBattle.update(data.api_data, { combined: combined, ecombined: ecombined });
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/ec_night_to_day/,
		response: function(data, match) {
		    this.reqSortieBattle.update(data.api_data, { combined: false, ecombined: true });
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/goback_port/,
		response: function(data, match) {
		    this.reqSortieGobackPort.update(data.api_data, { combined: true });
		}
	    },{
		regexp: /kcsapi\/api_req_combined_battle\/(midnight_battle|sp_midnight|ec_midnight_battle)/,
		response: function(data, match) {
		    let ecombined = (match[1] == 'ec_midnight_battle') && !data.api_data.api_e_nowhps_combined;
		    let combined = !!data.api_data.api_f_nowhps_combined;
		    this.reqBattleMidnightBattle.update(data.api_data, { combined: combined, ecombined: ecombined });
		}
	    },{
		regexp: /kcsapi\/api_req_hensei\/change/,
		request: function(data, match) {
		    data._id = parseInt(data.api_id, 10);
		    data._ship_id = parseInt(data.api_ship_id, 10);
		    data._ship_idx = parseInt(data.api_ship_idx, 10);
		    this.reqHenseiChange.prepare(data);
		},
		response: function(data, match) {
		    this.reqHenseiChange.update();
		}
	    },{
		regexp: /kcsapi\/api_req_hensei\/combined/,
		response: function(data, match) {
		    this.reqHenseiCombined.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_hensei\/lock/,
		request: function(data, match) {
		    data._ship_id = parseInt(data.api_ship_id, 10);
		    this.reqHenseiLock.prepare(data);
		},
		response: function(data, match) {
		    this.reqHenseiLock.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_hensei\/preset_delete/,
		request: function(data, match) {
		    data._preset_no = parseInt(data.api_preset_no, 10);
		    this.reqHenseiPresetDelete.prepare(data);
		},
		response: function(data, match) {
		    this.reqHenseiPresetDelete.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_hensei\/preset_register/,
		//request: function(data, match) {
		//    data._preset_no = parseInt(data.api_preset_no, 10);
		//    data._deck_id = parseInt(data.api_deck_id, 10);
		//    this.reqHenseiPresetRegister.prepare(data);
		//},
		response: function(data, match) {
		    this.reqHenseiPresetRegister.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_hensei\/preset_select/,
		request: function(data, match) {
		    data._deck_id = parseInt(data.api_deck_id, 10);
		    this.reqHenseiPresetSelect.prepare(data);
		},
		response: function(data, match) {
		    this.reqHenseiPresetSelect.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_hokyu\/charge/,
		response: function(data, match) {
		    this.reqHokyuCharge.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kaisou\/marriage/,
		response: function(data, match) {
		    this._memberShip3.update([data.api_data]);
		}
	    },{
		regexp: /kcsapi\/api_req_kaisou\/powerup/,
		request: function(data, match) {
		    data._id_items = data.api_id_items.split(/,/)
					.map(function(v) {
					    return parseInt(v, 10);
					});
		    data._slot_dest_flag = parseInt(data.api_slot_dest_flag, 10);
		    this.reqKaisouPowerup.prepare(data);
		},
		response: function(data, match) {
		    this.reqKaisouPowerup.update(data.api_data);
		    this.memberDeck.update(data.api_data.api_deck);
		    if (data.api_data.api_ship)
			this._memberShip3.update([data.api_data.api_ship]);
		}
	    },{
		regexp: /kcsapi\/api_req_kaisou\/preset_slot_delete/,
		request: function(data, match) {
		    data._preset_id = parseInt(data.api_preset_id, 10);
		    this.reqKaisouPresetSlotDelete.prepare(data);
		},
		response: function(data, match) {
		    this.reqKaisouPresetSlotDelete.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kaisou\/preset_slot_register/,
		request: function(data, match) {
		    data._preset_id = parseInt(data.api_preset_id, 10);
		    data._ship_id = parseInt(data.api_ship_id, 10);
		    this.reqKaisouPresetSlotRegister.prepare(data);
		},
		response: function(data, match) {
		    this.reqKaisouPresetSlotRegister.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kaisou\/preset_slot_select/,
		request: function(data, match) {
		    data._preset_id = parseInt(data.api_preset_id, 10);
		    data._ship_id = parseInt(data.api_ship_id, 10);
		    data._equip_mode = parseInt(data.api_equip_mode, 10);
		    this.reqKaisouPresetSlotSelect.prepare(data);
		},
		response: function(data, match) {
		    this.reqKaisouPresetSlotSelect.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kaisou\/preset_slot_update_name/,
		request: function(data, match) {
		    data._preset_id = parseInt(data.api_preset_id, 10);
		    this.reqKaisouPresetSlotUpdateName.prepare(data);
		},
		response: function(data, match) {
		    this.reqKaisouPresetSlotUpdateName.update(data.api_data);
		}
	    },{
		// 2016/06/01
		regexp: /kcsapi\/api_req_kaisou\/slot_deprive/,
		//request: function(data, match) {
		//    for (let k of ['slot_kind', 'idx', 'ship']) {
		//	data['_set_' + k] = parseInt(data['api_set_' + k], 10);
		//	data['_unset_' + k] = parseInt(data['api_unset_' + k], 10);
		//    }
		//    this.reqKaisouSlotDeprive.prepare(data);
		//},
		response: function(data, match) {
		    this._memberShip3.update([data.api_data.api_ship_data.api_unset_ship,
					      data.api_data.api_ship_data.api_set_ship]);
		    // TODO: Update unsetslot DB
		    // type = data.api_data.api_unset_list.api_type3No
		    // list = data.api_data.api_unset_list.api_slot_list
		    // unsetslot["api_slottype" + type] = list
		}
	    },{
		regexp: /kcsapi\/api_req_kaisou\/slot_exchange_index/,
		request: function(data, match) {
		    data._id = parseInt(data.api_id, 10);
		    this.reqKaisouSlotExchangeIndex.prepare(data);
		},
		response: function(data, match) {
		    this.reqKaisouSlotExchangeIndex.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/createitem/,
		response: function(data, match) {
		    this.reqKousyouCreateItem.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/createship_speedchange/,
		request: function(data, match) {
		    data._kdock_id = parseInt(data.api_kdock_id, 10);
		    this.reqKousyouCreateShipSpeedChange.prepare(data);
		},
		response: function(data, match) {
		    this.reqKousyouCreateShipSpeedChange.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/destroyitem2/,
		request: function(data, match) {
		    data._slotitem_ids = data.api_slotitem_ids.split(/,/)
					    .map(function(v) {
						return parseInt(v, 10);
					    });
		    this.reqKousyouDestroyItem2.prepare(data);
		},
		response: function(data, match) {
		    this.reqKousyouDestroyItem2.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/destroyship/,
		request: function(data, match) {
		    data._ship_ids = data.api_ship_id.split(/,/).
					map(function(v) {
					    return parseInt(v, 10);
					});
		    data._slot_dest_flag = parseInt(data.api_slot_dest_flag, 10);
		    this.reqKousyouDestroyShip.prepare(data);
		},
		response: function(data, match) {
		    this.reqKousyouDestroyShip.update();
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/getship/,
		response: function(data, match) {
		    this.reqKousyouGetShip.update(data.api_data);
		    this._memberKdock.update(data.api_data.api_kdock);
		}
	    },{
		regexp: /kcsapi\/api_req_kousyou\/remodel_slot$/,
		response: function(data, match) {
		    this.reqKousyouRemodelSlot.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_map\/(start|next)$/,
		request: function(data, match) {
		    // req_map/start
		    if (data.api_deck_id !== undefined)
			data._deck_id = parseInt(data.api_deck_id, 10);
		    if (data.api_maparea_id !== undefined)
			data._maparea_id = parseInt(data.api_maparea_id, 10);
		    if (data.api_mapinfo_no !== undefined)
			data._mapinfo_no = parseInt(data.api_mapinfo_no, 10);
		    if (data.api_formation_id !== undefined)
			data._formation_id = parseInt(data.api_formation_id, 10);

		    // req_map/next
		    if (data.api_recovery_type !== undefined)
			data._recovery_type = parseInt(data.api_recovery_type, 10);
		    this.reqMapStart.prepare(data);
		},
		response: function(data, match) {
		    this.reqMapStart.update(data.api_data, match[1] == 'start');
		}
	    },{
		// 緊急泊地修理
		regexp: /kcsapi\/api_req_map\/anchorage_repair$/,
		response: function(data, match) {
		    this._memberShip3.update(data.api_data.api_ship_data);
		}
	    },{
		regexp: /kcsapi\/api_req_member\/get_incentive/,
		response: function(data, match) {
		    this.reqMemberGetIncentive.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_member\/get_practice_enemyinfo/,
		response: function(data, match) {
		    this.reqMemberGetPracticeEnemyInfo.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_member\/updatedeckname/,
		request: function(data, match) {
		    data._deck_id = parseInt(data.api_deck_id, 10);
		    this.reqMemberUpdateDeckName.prepare(data);
		},
		response: function(data, match) {
		    this.reqMemberUpdateDeckName.update();
		}
	    },{
		regexp: /kcsapi\/api_req_mission\/result/,
		request: function(data, match) {
		    data._deck_id = parseInt(data.api_deck_id, 10);
		    this.reqMissionResult.prepare(data);
		},
		response: function(data, match) {
		    this.reqMissionResult.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_mission\/return_instruction/,
		request: function(data, match) {
		    data._deck_id = parseInt(data.api_deck_id, 10);
		    this.reqMissionReturnInstruction.prepare(data);
		},
		response: function(data, match) {
		    this.reqMissionReturnInstruction.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_nyukyo\/speedchange/,
		request: function(data, match) {
		    data._ndock_id = parseInt(data.api_ndock_id, 10);
		    this.reqNyukyoSpeedChange.prepare(data);
		},
		response: function(data, match) {
		    this.reqNyukyoSpeedChange.update();
		}
	    },{
		regexp: /kcsapi\/api_req_nyukyo\/start/,
		request: function(data, match) {
		    data._ndock_id = parseInt(data.api_ndock_id, 10);
		    data._ship_id = parseInt(data.api_ship_id, 10);
		    data._highspeed = parseInt(data.api_highspeed, 10);
		    this.reqNyukyoStart.prepare(data);
		},
		response: function(data, match) {
		    this.reqNyukyoStart.update();
		}
	    },{
		regexp: /kcsapi\/api_req_practice\/battle_result/,
		response: function(data, match) {
		    this.reqSortieBattleResult.update(data.api_data, { practice: true });
		}
	    },{
		regexp: /kcsapi\/api_req_practice\/battle/,
		response: function(data, match) {
		    this.reqSortieBattle.update(data.api_data, { practice: true });
		}
	    },{
		regexp: /kcsapi\/api_req_practice\/midnight_battle/,
		response: function(data, match) {
		    this.reqBattleMidnightBattle.update(data.api_data, { practice: true });
		}
	    },{
		regexp: /kcsapi\/api_req_sortie\/battleresult/,
		response: function(data, match) {
		    this.reqSortieBattleResult.update(data.api_data, {});
		}
	    },{
		regexp: /kcsapi\/api_req_sortie\/(ld_air|air)?battle/,
		response: function(data, match) {
		    this.reqSortieBattle.update(data.api_data, {});
		}
	    },{
		regexp: /kcsapi\/api_req_sortie\/goback_port/,
		response: function(data, match) {
		    this.reqSortieGobackPort.update(data.api_data, {});
		}
	    },{
		regexp: /kcsapi\/api_req_sortie\/night_to_day/,
		response: function(data, match) {
		    this.reqSortieBattle.update(data.api_data, {});
		}
	    },{
		regexp: /kcsapi\/api_req_quest\/clearitemget/,
		request: function(data, match) {
		    data._quest_id = parseInt(data.api_quest_id, 10);
		    this.reqQuestClearitemget.prepare(data);
		},
		response: function(data, match) {
		    this.reqQuestClearitemget.update(data.api_data);
		}
	    },{
		regexp: /kcsapi\/api_req_quest\/stop/,
		request: function(data, match) {
		    data._quest_id = parseInt(data.api_quest_id, 10);
		    this.reqQuestStop.prepare(data);
		},
		response: function(data, match) {
		    this.reqQuestStop.update();
		}
	    }
	]
    },{
	regexp: /^https?:\/\/.*\.touken-ranbu\.jp\//,
	request: null,
	response: function(s, match) {
	    let data;
	    try {
		d = JSON.parse(s);
		if (d.status == 0)
		    data = d;
	    } catch(x) {
	    }
	    return data;
	},
	handlers: [
	    {
		regexp: /\/conquest\/complete/,
		response: function(data, match) {
		    let d = JSON.parse(JSON.stringify(data));
		    d._now = TouRabuParseTime(data.now);
		    this.tourabuConquest.update(d);
		}
	    },{
		regexp: /\/conquest\/start/,
		response: function(data, match) {
		    let d = JSON.parse(JSON.stringify(data));
		    d._now = TouRabuParseTime(data.now);
		    this.tourabuConquest.update(d);
		}
	    },{
		regexp: /\/conquest/,
		response: function(data, match) {
		    let d = JSON.parse(JSON.stringify(data));
		    d._now = TouRabuParseTime(data.now);
		    this.tourabuConquest.update(d);
		}
	    },{
		regexp: /\/forge\/complete/,
		request: function(data, match) {
		    let d = JSON.parse(JSON.stringify(data));
		    d._now = TouRabuParseTime(data.now);
		    this._tourabuForge.prepare(d);
		},
		response: function(data, match) {
		    let d = {};
		    let req = KanColleDatabase._tourabuForge.get_req();
		    d[req.slot_no] = data;
		    d._now = TouRabuParseTime(data.now);
		    this._tourabuForge.update(d);
		}
	    },{
		regexp: /\/forge\/start/,
		response: function(data, match) {
		    let d = {};
		    d[data.slot_no] = data;
		    d._now = TouRabuParseTime(data.now);
		    this._tourabuForge.update(d);
		}
	    },{
		regexp: /\/forge/,
		response: function(data, match) {
		    let d = JSON.parse(JSON.stringify(data.forge));
		    d._now = TouRabuParseTime(data.now);
		    this._tourabuForge.update(d);
		}
	    },{
		regexp: /\/login\/start/,
		response: function(data, match) {
		    let d = JSON.parse(JSON.stringify(data));
		    d._now = TouRabuParseTime(data.now);
		    this.tourabuLoginStart.update(d);
		}
	    },{
		regexp: /\/repair\/repair/,
		response: function(data, match) {
		    let d = JSON.parse(JSON.stringify(data.repair));
		    d._now = TouRabuParseTime(data.now);
		    this._tourabuRepair.update(d);
		}
	    },{
		regexp: /\/repair\/complete/,
		response: function(data, match) {
		    // 完了時刻に手入れ画面にいると呼ばれる。
		    // 通知とのraceがあるのでここでは何もしない。
		}
	    },{
		regexp: /\/repair/,
		response: function(data, match) {
		    let d = JSON.parse(JSON.stringify(data));
		    d.sword._now = d.repair._now = TouRabuParseTime(data.now);
		    this.tourabuSword.update(d.sword);
		    this._tourabuRepair.update(d.repair);
		}
	    },{
		regexp: /\/sarry/,
		response: function(data, match) {
		    let d = JSON.parse(JSON.stringify(data));
		    d.sword._now = d.repair._now = TouRabuParseTime(data.now);
		    this.tourabuSword.update(d.sword);
		    this._tourabuRepair.update(d.repair);
		}
	    }
	]
    }
];

/*
 * 全データベース。
 */
var KanColleDatabase = {
    // Internal variable
    _refcnt: null,

    _db: [
	{   key: 'masterMaparea',		    persist: false },	// master/maparea
	{   key: 'masterMapinfo',		    persist: false },	// master/mapinfo
	{   key: 'masterMission',		    persist: false },	// master/mission
	{   key: 'masterShip',			    persist: true},	// master/ship
	{   key: 'masterSlotitem',		    persist: true },	// master/slotiem
	{   key: 'masterSlotitemEquiptype',	    persist: true },	// api_start2[mst_slotitem_equip_type]
	{   key: 'masterEquipExslot',		    persist: true },	// api_start2[api_mst_equip_exslot]
	{   key: 'masterEquipExslotShip',	    persist: true },	// api_start2[api_mst_equip_exslot_ship]
	{   key: 'masterEquipShip',		    persist: true },	// api_start2[api_mst_equip_ship]
	{   key: 'masterStype',			    persist: true },	// master/stype
	{   key: 'masterUseitem',		    persist: true },	// master/useitem
	{   key: 'memberBaseAirCorps',		    persist: false },	// member/base_air_corps
	{   key: 'memberBasic',			    persist: false },	// member/basic
	{   key: 'memberDeck',			    persist: false },	// member/deck, member/deck_port
									// or member/ship2[api_data_deck]
									// or member/ship3[api_deck_data]
	{   key: '__memberDeck',		    persist: false },	// member/ship_deck[api_deck_data]
	{   key: '_memberKdock',		    persist: false },	// member/kdock
	{   key: 'memberMaterial',		    persist: false },	// member/material
	{   key: 'memberMapinfo',		    persist: false },	// member/mapinfo
	{   key: 'memberMission',		    persist: false },	// member/mission
	{   key: '_memberNdock',		    persist: false },	// member/ndock
	{   key: 'memberPractice',		    persist: false },	// member/practice
	{   key: 'memberPresetDeck',		    persist: false },	// member/preset_deck
	{   key: 'memberPresetSlot',		    persist: false },	// member/preset_slot
	{   key: 'memberQuestlist',		    persist: false },	// member/questlist
	{   key: 'memberRecord',		    persist: false },	// member/record
	{   key: '_memberShip2',		    persist: false },	// member/ship2 or member/ship3, full data
	{   key: '_memberShip3',		    persist: false },	// member/ship2 or member/ship3, partial data
	{   key: '_memberSlotitem',		    persist: false },	// member/slotitem
	{   key: 'memberUnsetslot',		    persist: false },	// member/unsetslot
									// or member/ship3[api_slot_data]
	{   key: 'memberUseitem',		    persist: false },	// member/useitem
	{   key: 'reqAirCorpsChangeName',	    persist: false },	// req_air_corps/change_name
	{   key: 'reqAirCorpsSetAction',	    persist: false },	// req_air_corps/set_action
	{   key: 'reqAirCorpsSetPlane',		    persist: false },	// req_air_corps/set_plane, req_air_corps/change_deployment_base
	{   key: 'reqAirCorpsSupply',		    persist: false },	// req_air_corps/supply
	{   key: 'reqHenseiChange',		    persist: false },	// req_hensei/change
	{   key: 'reqHenseiCombined',		    persist: false },	// req_hensei/combined
	{   key: 'reqHenseiLock',		    persist: false },	// req_hensei/lock
	{   key: 'reqHenseiPresetDelete',	    persist: false },	// req_hensei/preset_delete
	{   key: 'reqHenseiPresetRegister',	    persist: false },	// req_hensei/preset_register
	{   key: 'reqHenseiPresetSelect',	    persist: false },	// req_hensei/preset_select
	{   key: 'reqKaisouPowerup',		    persist: false },	// req_kaisou/powerup
	{   key: 'reqKaisouSlotExchangeIndex',	    persist: false },	// req_kaisou/slot_exchange_index
	{   key: 'reqKaisouPresetSlotDelete',	    persist: false },	// req_kaisou/preset_slot_delete
	{   key: 'reqKaisouPresetSlotRegister',	    persist: false },	// req_kaisou/preset_slot_register
	{   key: 'reqKaisouPresetSlotSelect',	    persist: false },	// req_kaisou/preset_slot_select
	{   key: 'reqKaisouPresetSlotUpdateName',   persist: false },	// req_kaisou/preset_slot_update_name
	{   key: 'reqKousyouCreateItem',	    persist: false },	// req_kousyou/create_item
	{   key: 'reqKousyouCreateShipSpeedChange', persist: false },	// req_kousyou/createship_speedchange
	{   key: 'reqKousyouDestroyItem2',	    persist: false },	// req_kousyou/destroyitem2
	{   key: 'reqKousyouDestroyShip',	    persist: false },	// req_kousyou/destroyship
	{   key: 'reqKousyouGetShip',		    persist: false },	// req_kousyou/get_ship
	{   key: 'reqKousyouRemodelSlot',	    persist: false },	// req_kousyou/remodel_slot
	{   key: 'reqHokyuCharge',		    persist: false },	// req_hokyu/charge
	{   key: 'reqMapStart',			    persist: false },	// req_map/start, req_map/next
	{   key: 'reqMemberGetIncentive',	    persist: false },	// req_member/get_incentive
	{   key: 'reqMemberGetPracticeEnemyInfo',   persist: false },	// req_member/get_practice_enemyinfo
	{   key: 'reqMemberUpdateDeckName',	    persist: false },	// req_member/updatedeckname
	{   key: 'reqMissionResult',		    persist: false },	// req_mission/result
	{   key: 'reqMissionReturnInstruction',	    persist: false },	// req_mission/return_instruction
	{   key: 'reqNyukyoSpeedChange',	    persist: false },	// req_nyukyo/speedchange
	{   key: 'reqNyukyoStart',		    persist: false },	// req_nyukyo/start
	{   key: 'reqQuestClearitemget',	    persist: false },	// req_quest/clearitemget
	{   key: 'reqQuestStop',		    persist: false },	// req_quest/stop
	{   key: 'reqSortieBattle',		    persist: false },	// req_sortie/battle
	{   key: 'reqSortieBattleResult',	    persist: false },	// req_sortie/battle_result
	{   key: 'reqSortieGobackPort',		    persist: false },	// req_sortie/goback_port, req_combined_battle/go_back_port
	{   key: 'reqBattleMidnightBattle',	    persist: false },	// req_battle/midnight_battle
	{   key: 'ndock',			    persist: false,	func: KanColleNdockDB },	// 入渠ドック
	{   key: 'battle',			    persist: false,	func: KanColleBattleDB },	// 戦闘/対戦
	{   key: 'ship',			    persist: false,	func: KanColleShipDB },		// 艦船
	{   key: 'deck',			    persist: false,	func: KanColleDeckDB },		// デッキ
	{   key: 'slotitem',			    persist: false,	func: KanColleSlotitemDB },	// 装備保持艦船
	{   key: 'record',			    persist: false,	func: KanColleRecordDB },	// 艦船/装備
	{   key: 'quest',			    persist: false,	func: KanColleQuestDB },	// 任務(クエスト)
	{   key: 'mission',			    persist: false,	func: KanColleMissionDB },	// 遠征一覧
	{   key: 'practice',			    persist: false,	func: KanCollePracticeDB },	// 演習
	{   key: 'material',			    persist: false,	func: KanColleMaterialDB },	// 資源/資材
	{   key: 'kdock',			    persist: false,	func: KanColleKdockDB },	// 建造ドック
	{   key: 'port',			    persist: false,	func: KanCollePortDB },		// 母港
	{   key: 'preset',			    persist: false,	func: KanCollePresetDB },	// 編成プリセット
	{   key: 'presetslot',			    persist: false,	func: KanCollePresetSlotDB },	// 装備プリセット
	{   key: 'useitem',			    persist: false,	func: KanColleUseitemDB },	// アイテム
	{   key: 'baseaircorps',		    persist: false,	func: KanColleBaseAirCorpsDB },	// 基地航空隊
	{   key: 'map',				    persist: false,	func: KanColleMapDB },		// マップ
	// TouRabu
	{   key: 'tourabuConquest',		    persist: false },	// conquest, conquest/start, conquest/complete
	{   key: '_tourabuForge',		    persist: false },	// forge, forge/start, forge/complete
	{   key: 'tourabuLoginStart',		    persist: false },	// login/start
	{   key: '_tourabuRepair',		    persist: false },	// repair, repair/repair, repair/complete
	{   key: 'tourabuSword',		    persist: false },	// sword information
	{   key: 'tourabuParty',		    persist: false,	func: TouRabuPartyDB },
	{   key: 'tourabuRepair',		    persist: false,	func: TouRabuRepairDB },
	{   key: 'tourabuForge',		    persist: false,	func: TouRabuForgeDB },
    ],

    // Callback
    _callback: function(req, s, mode) {
	let that = KanColleDatabase;
	let url = req.name;
	let m;

	if (!mode || mode == 'http-on-examine-response') {
	    m = 'response';
	} else if (mode == 'http-on-modify-request') {
	    m = 'request';
	} else {
	    return;
	}

	KanColleTimerHandlers.find(function(domain) {
	    let data = undefined;
	    let match = domain.regexp.exec(url);
	    if (!match)
		return false;
	    //debugprint('Domain match: ' + match.toSource());
	    if (domain[m]) {
		//debugprint('Parser: ' + m);
		data = domain[m].call(that, s, match);
	    } else {
		//debugprint('Parser(default): ' + m);
		if (m == 'request') {
		    let postdata = s.substring(s.indexOf('\r\n\r\n') + 4).split('&');
		    let k,v,t;
		    let d = new Object();
		    for (let i = 0; i < postdata.length; i++){
			let idx;
			let e;

			t = postdata[i];
			idx = t.indexOf('=');
			try{
			    if (idx >= 0) {
				k = decodeURIComponent(t.substring(0, idx));
				v = decodeURIComponent(t.substring(idx + 1));
			    }
			    if (d[k])
				debugprint('overriding data for ' + k + '; ' + d[k]);
			    d[k] = v;
			} catch(e) {
			}
		    }
		    data = d;
		} else {
		    // no default handler.
		}
	    }
	    if (data !== undefined) {
		if (KanColleUtils.getBoolPref('record.api')) {
		    let file = KanColleUtils.getUnicharPrefFile('record.api_dir');
		    if (file) {
			file.append(encodeURIComponent(req.name) + "." + m);
			KanColleTimerUtils.file.writeObject(file, 0, 0, data, 0);
		    }
		}
		domain.handlers.find(function(handler) {
		    let match = handler.regexp.exec(url);
		    if (!match)
			return false;
		    //debugprint('Handler match: ' + match.toSource());
		    if (handler[m])
			handler[m].call(that, data, match);
		    return true;
		});
	    }
	    return true;
	});
	//debugprint('done');
    },

    // database registration
    _registerDatabase: function() {
	for (let i = 0; i < this._db.length; i++) {
	    let param = this._db[i];
	    let func;
	    let obj;

	    if (this[param.key] && param.persist)
		continue;

	    debugprint("Registering database " + param.key);

	    func = param.func || KanColleDB;
	    obj = this[param.key] = new func;
	    if (obj.init)
		obj.init();
	    this._db[param.key] = param.persist === true;
	}
	debugprint("KanColleDatabase initialized.");
    },
    _unregisterDatabase: function() {
	for (let i = this._db.length - 1; i >= 0; i--) {
	    let param = this._db[i];
	    if (!this[param.key] || param.persist)
		return;

	    debugprint("Unregistering database " + param.key);

	    if (this[param.key].exit)
		this[param.key].exit();
	    delete this[param.key];
	}
	debugprint("KanColleDatabase cleared.");
    },

    // Initialization
    init: function() {
	if (this._refcnt === null) {
	    this._callback = this._callback.bind(this);
	    this._refcnt = 0;
	}

	if (!this._refcnt++) {
	    // Initialize
	    KanColleHttpRequestObserver.init();
	    this._registerDatabase();
	    // Start
	    KanColleHttpRequestObserver.addCallback(this._callback);
	}
    },

    exit: function() {
	if (!--this._refcnt) {
	    // Stop
	    KanColleHttpRequestObserver.removeCallback(this._callback);
	    // Unregister
	    this._unregisterDatabase();
	    KanColleHttpRequestObserver.destroy();
	}
    },
};

/*
 * 雑多な共有情報。
 * 本当はなくしたい。
 */
var KanColleRemainInfo = {
    ndock_memo: [], // 入渠ドック用のメモ
};

/*
 * ライブラリ
 */
const Cc = Components.classes;
const Ci = Components.interfaces;

function debugprint(str){
    KanColleTimerUtils.console.log(str);
}

function CCIN(cName, ifaceName) {
    return Cc[cName].createInstance(Ci[ifaceName]);
}

/*
 * 通信傍受
 */
var callback = new Array();

function TracingListener() {
    this.originalListener = null;
}
TracingListener.prototype =
{
    onDataAvailable: function(request, context, inputStream, offset, count) {
        var binaryInputStream = CCIN("@mozilla.org/binaryinputstream;1",
				     "nsIBinaryInputStream");
        var storageStream = CCIN("@mozilla.org/storagestream;1", "nsIStorageStream");
        var binaryOutputStream = CCIN("@mozilla.org/binaryoutputstream;1",
				      "nsIBinaryOutputStream");

        binaryInputStream.setInputStream(inputStream);
        storageStream.init(8192, count, null);
        binaryOutputStream.setOutputStream(storageStream.getOutputStream(0));

        // Copy received data as they come.
        var data = binaryInputStream.readBytes(count);
        this.receivedData.push(data);

        binaryOutputStream.writeBytes(data, count);

        this.originalListener.onDataAvailable(request, context,
					      storageStream.newInputStream(0), offset, count);
    },

    onStartRequest: function(request, context) {
        this.originalListener.onStartRequest(request, context);
	this.receivedData = new Array();
    },

    onStopRequest: function(request, context, statusCode) {
        this.originalListener.onStopRequest(request, context, statusCode);

	var s = this.receivedData.join('');
	for( var k in  callback ){
	    var f = callback[k];
	    if( typeof f=='function' ){
		f( request, s, 'http-on-examine-response' );
	    }
	}
    },

    QueryInterface: function (aIID) {
        if (aIID.equals(Ci.nsIStreamListener) ||
            aIID.equals(Ci.nsISupports)) {
            return this;
        }
        throw Components.results.NS_NOINTERFACE;
    }
};

var KanColleHttpRequestObserver =
{
    counter: 0,

    observe: function(aSubject, aTopic, aData){
        if (aTopic == "http-on-examine-response"){
	    var httpChannel = aSubject.QueryInterface(Components.interfaces.nsIHttpChannel);

	    if (KanColleTimerHandlers.some(function(domain) {
						return !!domain.regexp.exec(httpChannel.URI.spec);
					   })
	       ) {
		//debugprint(httpChannel.URI.spec);
		var newListener = new TracingListener();
		aSubject.QueryInterface(Ci.nsITraceableChannel);
		newListener.originalListener = aSubject.setNewListener(newListener);
	    }
	}

	if( aTopic=="http-on-modify-request" ){
	    var httpChannel = aSubject.QueryInterface(Components.interfaces.nsIHttpChannel);
	    if (httpChannel.requestMethod == 'POST' &&
	        KanColleTimerHandlers.some(function(domain) {
						return !!domain.regexp.exec(httpChannel.URI.spec);
					   })
	       ) {
		httpChannel.QueryInterface(Components.interfaces.nsIUploadChannel);
		var us = httpChannel.uploadStream;
		us.QueryInterface(Components.interfaces.nsISeekableStream);
		var ss = CCIN("@mozilla.org/scriptableinputstream;1",
			      "nsIScriptableInputStream");
		ss.init(us);
		us.seek(0, 0);
		var n = ss.available();
		var postdata = ss.read(n);
		us.seek(0, 0);

		for( let k in callback ){
		    let f = callback[k];
		    if (typeof(f) == 'function')
			f({ name: httpChannel.URI.spec }, postdata, aTopic);
		}

	    }
        }
    },

    QueryInterface : function (aIID) {
        if (aIID.equals(Ci.nsIObserver) || aIID.equals(Ci.nsISupports)){
            return this;
        }
        throw Components.results.NS_NOINTERFACE;
    },

    addCallback: function(f){
	callback.push( f );
	debugprint( 'add callback='+callback.length );
    },

    removeCallback: function( f ){
	for( var k in callback ){
	    if( callback[k]==f ){
		callback.splice(k,1);
		debugprint( 'remove callback='+callback.length );
	    }
	}
    },

    init: function(){
	if( this.counter==0 ){
	    this.observerService = Components.classes["@mozilla.org/observer-service;1"]
		.getService(Components.interfaces.nsIObserverService);
	    this.observerService.addObserver(this, "http-on-examine-response", false);
	    this.observerService.addObserver(this, "http-on-modify-request", false);
	    debugprint("start kancolle observer.");
	}
	this.counter++;
    },

    destroy: function(){
	this.counter--;
	if( this.counter<=0 ){
	    this.observerService.removeObserver(this, "http-on-examine-response");
	    this.observerService.removeObserver(this, "http-on-modify-request");
	    this.counter = 0;
	    debugprint("stop kancolle observer.");
	}
    }

};
