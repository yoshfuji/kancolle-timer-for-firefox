// vim: set ts=8 sw=4 sts=4 ff=dos :

/*
 * デッキ/遠征
 *  member/deck		: api_data
 *  member/deck_port	: api_data
 *  member/ship2	: api_data_deck
 */
var KanColleTimerDeckInfo = {
    update: {
	mission: function() {
	    let decks = KanColleDatabase.deck.list();
	    for (let i = 0; i < decks.length; i++){
		let d = KanColleDatabase.deck.get(decks[i]);
		let k = d.api_id;
		if (d.api_mission[0]) {
		    let mission_id = d.api_mission[1]; // 遠征ID
		    let mission = KanColleDatabase.mission.get(mission_id);
		    if (mission) {
			$('deck_mission'+k).value = mission.api_name;
			SetStyleProperty($('deck_mission'+k), 'font-style', '');
			SetStyleProperty($('deck_label'+k), 'color', '');
		    }
		}
	    }
	},

	deck: function(extradata) {
	    let decks = extradata.id ? [ extradata.id ] : KanColleDatabase.deck.list();

	    for (let k of decks) {
		let d = KanColleDatabase.deck.get(k);

		$('deck_label' + k).setAttribute('tooltiptext', d.api_name); // 艦隊名

		if( d.api_mission[0] ){
		    let mission_id = d.api_mission[1]; // 遠征ID
		    let mission = KanColleDatabase.mission.get(mission_id);
		    // 遠征名を表示
		    let mission_name = mission ? mission.api_name : (KanColleData.mission_param[mission_id] || {}).name;
		    if (!mission_name)
			mission_name = 'UNKNOWN_' + mission_id;
		    $('deck_mission'+k).value = mission_name;
		    SetStyleProperty($('deck_mission'+k), 'font-style', '');
		}else{
		    let last_mission = KanColleDatabase.deck.get_last_mission(k);
		    if (last_mission)
			$('deck_mission' + k).value = last_mission.name;
		}
	    }

	    if (extradata) {
		let state = extradata.state || 0;
		let id = extradata.id || 0;
		debugprint(extradata.toSource());
		if (state > 1)
		    AddLog(extradata.message + '\n');
		if (id > 0)
		    this._alarm_set(id, extradata.time);
	    }
	},
	memberBasic: function() {
	    let d = KanColleDatabase.memberBasic.get();
	    let fleets;
	    if (!d)
		return;
	    fleets = document.getElementsByClassName("fleet");
	    for( let i=0; i<4; i++ )
		SetStyleProperty(fleets[i], 'display',
				 (i != 0 && i < d.api_count_deck) ? "" : "none");
	},
    },

    _set_alarm_time: function(id, timeout) {
	let node = $('fleet' + id);
	this._set_alarm_time_node(node, timeout);
    },
    _update_alarm_time: function(id, diff) {
	let node = $('fleet' + id);
	let rnode = $('fleetremain' + id);
	this._update_alarm_time_node(rnode, diff);
	this._update_alarm_style_node(node, diff);
	this._update_alarm_style_node(rnode, diff);
    },

    _set_alarm_format: function() {
	const classname = 'fleet-time';
	let b = KanColleUtils.getBoolPref('display.short');
	let elems = document.getElementsByClassName(classname);
	for(let i = 0; i < elems.length; i++){
	    elems[i].style.display = b ? 'none' : 'block';
	}
    },

    observe: function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm_update();
	    break;
	case 'nsPref:changed':
	    this._set_alarm_format();
	    break;
	default:;
	}
    },

    show: function() {
	let checked = $('deckinfo-toggle').getAttribute('checked') == 'true';
	let nodes = document.getElementsByClassName("fleet");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true' ||
		  $('gentimer-toggle').getAttribute('checked') == 'true' ||
		  $('deckinfo-toggle').getAttribute('checked') == 'true' ||
		  $('ndockinfo-toggle').getAttribute('checked') == 'true' ||
		  $('kdockinfo-toggle').getAttribute('checked') == 'true';
	$('timer').collapsed = !checked;
    },

    restore: function() {
	this.update.memberBasic.call(this);
	this.show();
    },
};
KanColleTimerDeckInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerDeckInfo.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerDeckInfo.unload(); }, false);
