// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("resource://gre/modules/Promise.jsm");
Components.utils.import("chrome://kancolletimer/content/utils.jsm");
Components.utils.import("chrome://kancolletimer/content/e10s-main.jsm");

function $$$(cname) {
    return document.getElementsByClassName(cname);
}

var VideoRecorder = {
    fps: 15,
    record_timer: null,
    record_req: null,
    play_timer: null,
    enable_e10s: false,

    data: [],
    cnt: 0,

    seekbar: null,
    canvas: null,

    setFPS: function( n ){
	console.log( "Record " + n + " fps." );
	this.fps = n;
    },

    _showBlankFrame: function() {
	let canvas = this.canvas;
	let ctx = canvas.getContext( "2d" );
	let [width, height] = [canvas.width, canvas.height];
	ctx.fillStyle = "rgb(0,0,0)";
	ctx.fillRect( 0, 0, width, height );
	ctx.fillStyle = "rgb(255,255,255)";
	ctx.fillRect( 1, 1, width - 2, height - 2 );

	ctx.beginPath();
	ctx.moveTo( 0, 0 );
	ctx.lineTo( width, height );
	ctx.stroke();

	ctx.beginPath();
	ctx.moveTo( 0, height );
	ctx.lineTo( width, 0 );
	ctx.stroke();
    },
    showBlankFrame: function() {
	this._showBlankFrame();
    },

    getFrameURL: function( frame_no ){
	let that = this;
	let isjpeg = KanColleUtils.getBoolPref( "screenshot.jpeg" );
	return new Promise(function(resolve, reject) {
	    let d = that.data[ frame_no ];
	    if (!d) {
		reject(new Error('No such frame.'));
		return;
	    } else if (d.url) {
		resolve(d.url);
		return;
	    } else if (d.canvas) {
		let canvas = d.canvas;
		let url;
		if( isjpeg ){
		    url = canvas.toDataURL( "image/jpeg" );
		}else{
		    url = canvas.toDataURL( "image/png" );
		}
		resolve(url);
		return;
	    } else {
		if (!d.url_resolve)
		    d.url_resolve = [];
		d.url_resolve.push(function(url) {
		    that.transfered++;
		    resolve(url);
		});
		if (d.url_resolve.length == 1) {
		    KanColleTimerE10S.send(that.record_req, {
			cmd: 'get',
			frame_no: frame_no,
			delete: true,
		    });
		}
	    }
	});
    },

    showFrameP: function( frame_no ){
	let canvas = this.canvas;
	let ctx = canvas.getContext( "2d" );
	let that = this;
	return new Promise(function(resolve, reject) {
	    let d = that.data[frame_no];
	    try {
		if (d.canvas) {
		    ctx.drawImage( d.canvas, 0, 0, canvas.width, canvas.height );
		    resolve(frame_no);
		} else {
		    that.getFrameURL(frame_no)
			.then(function(url) {
			    let img = document.createElementNS('http://www.w3.org/1999/xhtml', 'img');
			    img.addEventListener('load', function() {
				ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
				resolve(frame_no);
			    });
			    img.src = url;
			});
		}
	    } catch(e) {
		that._showBlankFrame();
		resolve(frame_no);
	    }
	}).then(function(frame_no) {
	    that.cnt = frame_no;
	    return frame_no;
	});
    },

    play: function() {
	let that = this;
	function loop_actionP() {
	    let state = that.play_state;
	    let frame_no = that.cnt;
	    let played, wait;

	    if (!that.data[frame_no])
		return Promise.resolve(Number.NaN);

	    played = Date.now() - state.start;
	    wait = that.data[frame_no].timestamp - state.record_start - played;

	    if (wait > 0)
		return Promise.resolve(wait);
	    while (that.data[frame_no + 1] &&
		   played >= that.data[frame_no + 1].timestamp - state.record_start) {
		frame_no++;
	    }

	    return that.showFrameP(frame_no)
		    .then(function(frame_no) {
			    that.seekbar.value = frame_no;
			    return that.data[frame_no + 1] ? that.data[frame_no + 1].timestamp - state.record_start - (Date.now() - state.start) : Number.NaN;
		    });
	}
	function delayP(w) {
	    if (isNaN(w))
		return Promise.reject(new Error('finish.'));
	    if (w <= 0)
		return Promise.resolve();
	    return new Promise(function(resolve, reject) {
				setTimeout(function() { resolve(); }, w);
			       });
	}
	function loop(p, f) {
	    return p.then(f)
		    .then(function(w) {
			return loop(delayP(w), f);
		    })
	}

	if (this.data.length < 1)
	    return;

	this.play_state = {
	    start: Date.now(),
	    record_start: this.data[this.cnt].timestamp,
	    timer: null,
	};
	loop(Promise.resolve(), loop_actionP)
	    .catch(function() {
		that.pause();
	    });
    },

    seek: function( frame_no ){
	let that = this;
	let was_playing;

	if (frame_no > this.data.length - 1)
	    frame_no = this.data.length - 1;
	if (frame_no < 0)
	    frame_no = 0;
	if (frame_no == this.cnt)
	    return;

	was_playing = this._pause();
	this.showFrameP(frame_no)
	    .then(function(frame_no) {
		if (was_playing)
		    that.play();
	    });
    },
    _pause: function(){
	let ret = this.play_state != null;
	if (this.play_state && this.play_state.timer) {
	    clearTimeout(this.play_state.timer);
	    this.play_state.timer = null;
	}
	this.play_state = null;
	return ret;
    },
    pause: function() {
	[].slice.call($$$('cmdmenu stop')).forEach(function(e) { e.setAttribute('disabled', 'true'); });
	this._pause();
	[].slice.call($$$('cmdmenu playrecord')).forEach(function(e) { e.setAttribute('disabled', 'false'); });
    },

    start: function(){
	let isjpeg = KanColleUtils.getBoolPref( "screenshot.jpeg" );
	let taking = false;
	let that = this;
	[].slice.call($$$('cmdmenu save')).forEach(function(e) { e.setAttribute('disabled', 'true'); });
	$( 'seekbar' ).disabled = true;
	this.data = [];
	this.cnt = 0;
	this.transfered = 0;
	$( 'seekbar' ).setAttribute('data-recording', 'true');
	$( 'seekbar' ).value = 0;
	$( 'seekbar' ).setAttribute( 'max', 0 );
	this.showBlankFrame();
	if (!this.enable_e10s) {
	    let t = 1000 / this.fps;
	    this.record_timer = setInterval( function(){
		let frame;
		if (taking)
		    return;
		that.data.push({
		    timestamp: Date.now(),
		    canvas: TakeKanColleScreenshot_canvas(),
		})
	    }, t );
	} else {
	    let format = isjpeg ? [ 'image/jpeg' ] : [ 'image/png' ];
	    let seq;
	    let tab = KanColleTimerUtils.window.findTab(KanColleUtils.URL);
	    if (!tab) {
		reject(new Error('Failed to find tab.'));
		return;
	    }
	    if (this.record_req) {
		KanColleTimerE10S.send(this.record_req, {
		    cmd: 'clear',
		});
		KanColleTimerE10S.close(this.record_req);
		this.record_req = null;
	    }
	    this.record_req = KanColleTimerE10S.open(tab.linkedBrowser.messageManager,
						     'multi_screenshot',
						     function(header, msg) {
							if (msg.error) {
							    KanColleTimerUtils.console.log('error: ' + msg.error);
							} else if (!isNaN(msg.data.frame_no)) {
							    let frame = that.data[msg.data.frame_no];
							    if (msg.data.url) {
								frame.url = msg.data.url;
								that.transfered++;
							    }
							    if (frame.url_resolve) {
								frame.url_resolve.forEach(function(e) { e(frame.url); });
								frame.url_resolve = null;
							    }
							} else {
							    that.data.push({
								timestamp: msg.data.timestamp,
								url: msg.data.url,
							    });
							    that.transfered++;
							}
						     });
	    KanColleTimerE10S.send(this.record_req, {
		cmd: 'start',
		format: format,
		framerate: this.fps,
		privacy: KanColleUtils.getBoolPref("screenshot.mask-name"),
	    });
	}
	if (this.play_timer)
	    clearInterval(this.play_timer);
	this.play_timer = setInterval(function() {
	    let n = that.data.length;
	    $( 'message' ).label = "Recording..." + n + " frame(s)";
	    if (!that.enable_e10s && n > 0) {
		that.seekbar.setAttribute( 'max', n - 1 );
		that.seekbar.value = n - 1;
	    }
	}, 1000);

	$( 'message' ).label = "Recording..."
    },

    tweet: function(){
	this.getFrameURL(n)
	    .then(function(url) {
		const IO_SERVICE = Components.classes['@mozilla.org/network/io-service;1']
		    .getService( Components.interfaces.nsIIOService );
		url = IO_SERVICE.newURI( url, null, null );
		OpenTweetDialog( url );
	    });
    },

    stop: function(){
	let n, fps, time;
	if (!this.enable_e10s) {
	    if (!this.record_timer)
		return;
	    clearInterval( this.record_timer );
	    this.record_timer = null;
	} else {
	    if (!this.record_req)
		return;
	    KanColleTimerE10S.send(this.record_req, {
		cmd: 'stop',
	    });
	}
	clearInterval( this.play_timer );
	this.play_timer = null;

	n = this.data.length;
	if ($( 'seekbar' ).hasAttribute('data-recording')) {
	    $( 'seekbar' ).removeAttribute('data-recording');
	    if (n) {
		time = (this.data[n - 1].timestamp - this.data[0].timestamp) / 1000;
		fps = time > 0 ? ((n - 1) / time).toFixed(2) : 'N/A';

		$( 'message' ).label = this.data.length + ' frame(s) captured in ' + time + ' sec(s), ' + fps + ' fps';
		[].slice.call($$$('cmdmenu save')).forEach(function(e) { e.setAttribute('disabled', 'false'); });
	    } else {
		$( 'message' ).label = this.data.length + ' frame(s) captured'
	    }

	    $( 'seekbar' ).setAttribute( 'max', n - 1 );
	    $( 'seekbar' ).disabled = false;
	    $( 'seekbar' ).value = 0;
	    if (this.enable_e10s)
		this.showFrameP(0);
	}
    },

    /*
     *
     */
    play_cmd: function() {
	[].slice.call($$$('cmdmenu playrecord')).forEach(function(e) { e.setAttribute('disabled', 'true'); });
	this.play();
	[].slice.call($$$('cmdmenu stop')).forEach(function(e) { e.setAttribute('disabled', 'false'); });
    },
    stop_cmd: function() {
	[].slice.call($$$('cmdmenu stop')).forEach(function(e) { e.setAttribute('disabled', 'true'); });
	this._pause();
	this.stop();
	[].slice.call($$$('cmdmenu playrecord')).forEach(function(e) { e.setAttribute('disabled', 'false'); });
    },
    record_cmd: function() {
	[].slice.call($$$('cmdmenu playrecord')).forEach(function(e) { e.setAttribute('disabled', 'true'); });
	this.start();
	[].slice.call($$$('cmdmenu stop')).forEach(function(e) { e.setAttribute('disabled', 'false'); });
    },

    /**
     * 動画を、指定のファイル名 + "-X.png" という連番で保存する
     */
    save: function(){
	let isjpeg = KanColleUtils.getBoolPref( "screenshot.jpeg" );

	let defaultdir = KanColleUtils.getUnicharPref( "screenshot.path" );
	let nsIFilePicker = Components.interfaces.nsIFilePicker;
	let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance( nsIFilePicker );

	fp.init( window, "保存先を選んでください", nsIFilePicker.modeSave );
	if( defaultdir ){
	    let file = Components.classes['@mozilla.org/file/local;1']
		.createInstance( Components.interfaces.nsIFile );
	    file.initWithPath( defaultdir );
	    if( file.exists() && file.isDirectory() )
		fp.displayDirectory = file;
	}
	fp.appendFilters( nsIFilePicker.filterImages );
	fp.defaultString = "video" + (isjpeg ? ".jpg" : ".png");
	fp.defaultExtension = isjpeg ? "jpg" : "png";
	let ret = fp.show();
	if( (ret != nsIFilePicker.returnOK && ret != nsIFilePicker.returnReplace) || !fp.file )
	    return null;

	const IO_SERVICE = Components.classes['@mozilla.org/network/io-service;1']
	    .getService( Components.interfaces.nsIIOService );

	let that = this;

	function action(n) {
	    return that.getFrameURL(n)
		    .then(function(_url) {
			url = IO_SERVICE.newURI( _url, null, null );
			let file = OpenFile( fp.file.path + "-" + n + (isjpeg ? ".jpg" : ".png") );
			SaveUrlToFile( url, file );
			return n;
		    });
	}
	function loop(p, f) {
	    return p.then(f)
		    .then(function(n) {
			$( 'saving-progress' ).value = Math.max( parseInt( n * 100 / that.data.length ), 5 );
			$( 'message' ).label = (n + 1) + ' frame(s) saved.';
			if (n + 1 > that.data.length - 1 || that._break)
			    return;
			return loop(Promise.resolve(n + 1), f);
		    });
	}

	console.log( 'saving...' );
	loop(Promise.resolve(0), action)
	    .then(function() {
		console.log( 'done.' );
		$( 'saving-progress' ).value = 0;
	    });
    },

    init: function(){
	console.log( 'init' );

	this.seekbar = $('seekbar');
	this.canvas = $('video-playback');
	this.enable_e10s = KanColleUtils.getBoolPref('screenshot.e10s-videorecorder');

	this.showBlankFrame();
    },
    destroy: function(){
	this._break = true;
	KanColleTimerE10S.send(this.record_req, {
	    cmd: 'clear',
	});
	KanColleTimerE10S.close(this.record_req);
    }
};

window.addEventListener( "load", function( e ){
    VideoRecorder.init();
}, false );

window.addEventListener( "unload", function( e ){
    VideoRecorder.destroy();
}, false );
