// vim: set ts=8 sw=4 sts=4 ff=dos :

var KanColleTimerQuestInfo = {
    _state: null,
    _memo: null,
    _localstate: null,

    update: {
	quest: function() {
	    let questbox = $('quest-list-box');
	    let ids = KanColleDatabase.quest.list();
	    let list = $('quest-list-rows');
	    let listitem;
	    let staletime = KanColleDatabase.ship.timestamp();
	    let mode = parseInt($('quest-information-mode-popup-filter').selectedItem.value, 10);
	    let now = (new Date).getTime();

	    function deadline(t,type){
		const fuzz = 60000;
		let elapsed;	// 期間開始からの経過時間
		let t2;
		let cur;
		let cur_day;
		let cur_date;
		let cur_month;
		let cur_year;
		let start_time;
		let next_time;
		let start_year;
		let next_year;
		let yearly_month;

		if (!t || t < 0 || type == 0)
		    return -1;	// 期限なし、または エラー

		t2 = t + 14400000;	// 5:00JST を 0時UTCとみなすため
		cur = new Date(t2);
		cur_day = (cur.getUTCDay() + 6) % 7;	// 0: Monday
		cur_date = cur.getUTCDate();
		cur_month = cur.getUTCMonth();		// 0: January
		cur_year = cur.getUTCFullYear();

		switch (type) {
		case 0:	// none
		    return 0;
		case 1:	// daily
		    start_time = Date.UTC(cur_year, cur_month, cur_date, 5, 0, 0) - 32400000;
		    next_time = start_time + 86400000;
		    break;
		case 2:	// weekly
		    start_time = Date.UTC(cur_year, cur_month, cur_date, 5, 0, 0) - 32400000;
		    start_time -= cur_day * 86400000;
		    next_time = start_time + 86400000 * 7;
		    break;
		case 3: // monthly
		    start_time = Date.UTC(cur_year, cur_month, 1, 0, 0, 0) - 14400000;
		    // Javascript accepts overflow
		    next_time = Date.UTC(cur_year, cur_month + 1, 1, 0, 0, 0) - 14400000;
		    break;
		case 101: case 102: case 103: case 104:
		case 105: case 106: case 107: case 108:
		case 109: case 110: case 111: case 112:
		    // yearly
		    start_year = next_year = cur_year;
		    yearly_month = type - 101;
		    if (cur_month < yearly_month)
			start_year--;
		    else
			next_year++;
		    debugprint(`yearly: ${ start_year }/${ yearly_month } - ${ next_year }/${ yearly_month }`);
		    start_time = Date.UTC(start_year, yearly_month, 1, 0, 0, 0) - 14400000;
		    next_time = Date.UTC(next_year, yearly_month, 1, 0, 0, 0) - 14400000;
		    break;
		default:
		    return -1;
		}

		elapsed = t - start_time;
		if (elapsed < fuzz)
		    return 0;	// 時計ずれを考慮

		return next_time;
	    }

	    // clear
	    RemoveChildren(list);

	    for (let i = 0; i < ids.length; i++) {
		let no = ids[i];
		let qdata = KanColleData.quest[no];
		let listitem = CreateElement('row');
		let cell;
		let t;
		let tl;
		let q = KanColleDatabase.quest.get(ids[i]);
		let qt = KanColleDatabase.quest.timestamp(ids[i]);
		let type = 0;
		let tid = 'quest-information-deadline-tooltip-' + i;
		let in_progress;
		let was_in_progress;
		let extradata;
		let _extradata;
		let state = '';
		let prog;

		function update_quest_count(ptype, param, d, _d)
		{
		    let ret = null;
		    let v = null;
		    switch (ptype) {
		    case 'map':
		      {
			/* 戦闘結果 */
			//debugprint('param='+param.toSource());
			let stat = KanColleDatabase.battle.stat();
			if (!d.v)
			    d.v = 0;
			v = 0;
			if (stat) {
			    let re = param.regexp ? new RegExp('^' + param.regexp) : null;
			    v = Object.keys(stat.map)
					.filter(function(v) {
					    /* 戦闘セル */
					    //debugprint('1 filter v='+v.toSource());
					    return (!re || re.exec(v) != null);
					})
					.map(function(v) {
					    /* セル毎統計 */
					    //debugprint('2 map v='+v.toSource());
					    return stat.map[v];
					})
					.filter(function(v) {
					    /* 演習/ボス判定 */
					    //debugprint('3 filter v='+v.toSource());
					    return (!param.practice == !v.practice) &&
						   (!param.boss || v.boss);
					})
					.map(function(v) {
					    /* 戦闘結果ランク集計 */
					    //debugprint('4 map v='+v.toSource());
					    return (param.rank || Object.keys(v.rank))
						    .map(function(_v) {
							return v.rank[_v] || 0;
						    })
						    .reduce(function(p,c) {
							return p + c;
						    }, 0);
					})
					.reduce(function(p,c) {
					    return p + c;
					}, 0);
			}
			break;
		      }
		    case 'stype':
		      {
			/* 敵撃沈艦 */
			let stypes = param.mask;
			let stat = KanColleDatabase.battle.stat();
			if (!d.v)
			    d.v = 0;
			v = 0;
			if (stat) {
			    for (let i = 0; i < stypes.length; i++) {
				let stype = stypes[i];
				v += stat.type[stype] || 0;
			    }
			}
			break;
		      }
		    case 'first':
		      {
			/* 出撃 */
			let stat = KanColleDatabase.battle.stat();
			if (!d.v)
			    d.v = 0;
			v = 0;
			if (stat)
			    v = stat.first;
			break;
		      }
		    case 'mission':
		      {
			/* 遠征 */
			let stat = KanColleDatabase.mission.stat();
			if (!d.v)
			    d.v = 0;
			v = 0;
			if (stat) {
			    v = (param.mask || Object.keys(stat))
				.map(function(v) {
				    return stat[v] || {};
				}).map(function(v) {
				    return (param.rank || Object.keys(v))
					    .map(function(_v) {
						    return v[_v] || 0;
					    })
					    .reduce(function(p,c) {
						return p + c;
					    }, 0);
				})
				.reduce(function(p,c) {
					    return p + c;
				}, 0);
			}
		      }
		    }
		    if (v !== null) {
			if (!was_in_progress && in_progress) {
			    // off => on
			    debugprint('off=>on');
			    _d.b = v;
			} else if (was_in_progress) {
			    // on
			    if (isNaN(_d.b)) {
				debugprint('on(reset)');
				_d.b = v;
			    } else {
				debugprint('on');
			    }
			    d.v += v - (_d.b || 0);
			    _d.b = v;
			}
			ret = d.v;
		    }
		    return ret;
		}

		listitem.setAttribute('data-no', no);
		listitem.setAttribute('data-title', q.api_title);

		let memo = q.api_detail.replace(/<[^<>]*>/g, '');
		if (this._memo && this._memo[no] && this._memo[no].length) {
		    memo += "\n\n[メモ]\n" + this._memo[no];
		}
		listitem.setAttribute('tooltiptext', memo);

		// title
		cell = CreateElement('label');
		cell.className = 'quest-title';
		cell.setAttribute('value', q.api_title);
		cell.setAttribute('crop', 'end');
		listitem.appendChild(cell);

		// category
		listitem.setAttribute('data-category', '' + q.api_category);

		// type
		type = q.api_type;
		tl = null;

		// 2016/06/10 API change
		switch (q.api_type) {
		case 1: t = '[日]'; type = 1; break;
		case 2: t = '[週]'; type = 2; break;
		case 3: t = '[月]'; type = 3; break;
		case 4: t = '[単]'; type = 0; break;
		case 5: if (q.api_label_type >= 101 && q.api_label_type <= 112) {
			    t = '[年]';
			    tl = `${ q.api_label_type - 100 }月`;
			    type = q.api_label_type;
			} else {
			    t = '[他]';
			    type = 0;
			}
			break;
		default:
			t = '[' + q.api_type + ']';
		}
		cell = CreateElement('label');
		cell.className = 'quest-deadline';
		cell.setAttribute('value', t);
		if (tl)
		    cell.setAttribute('data-label', tl);
		listitem.appendChild(cell);

		if (qdata && qdata.type) {
		    // 例えば「艦隊精鋭演習」はtype 6 (マンスリー)
		    // だがカウンタを日単位でリセットするため、
		    // 期限はデイリー扱い。
		    type = qdata.type;
		}
		t = deadline(qt, type);

		/* Save state if (and only if) state has beed loaded */
		if (this._state) {
		    if (!this._localstate) {
			this._localstate = {};
		    }
		    if (!this._state[no] || this._state[no].t < now ||
			this._state[no].t < t) {
			this._state[no] = { d: {}, };
			this._localstate[no] = {};
		    }
		    if (!this._localstate)
			this._localstate = {};
		    if (!this._localstate[no])
			this._localstate[no] = {};
		    in_progress = q.api_state == 2 ? 1 : 0;
		    was_in_progress = this._state[no].p;

		    this._state[no].p = in_progress;
		    this._state[no].t = t;

		    extradata = this._state[no].d;
		    _extradata = this._localstate[no];

		    if (qdata && qdata.counter) {
			debugprint(qdata.toSource());
			let qres = [];
			let qp = 0;
			let qc = 0;
			for (let qi = 0; qi < qdata.counter.length; qi++) {
			    let q = qdata.counter[qi];
			    let qr;
			    if (!extradata[qi])
				extradata[qi] = {};
			    if (!_extradata[qi])
				_extradata[qi] = {};
			    qr = update_quest_count(q.type, q.param, extradata[qi], _extradata[qi]);
			    if (qr !== null) {
				qp += Math.min(qr, q.num) / q.num;
				qc++;
				qres.push((q.label ? q.label + ': ' : '') + qr + '/' + q.num);
			    }
			}
			if (qc) {
			    qp /= qc;
			    state = Math.floor(qp * 10000) / 100;
			    if (state >= 100)
				prog = 100;
			    else if (state >= 80)
				prog = 80;
			    else if (state >= 50)
				prog = 50;
			    else if (state >= 0)
				prog = 0;
			    else
				prog = -1;
			    state += '%\n';
			}
			listitem.setAttribute('data-counter', '' + prog);
			state += '\t' + qres.join('\n\t');
		    }

		    debugprint(no + "(" + q.api_title + "): " + this._state[no].toSource());
		}

		if (t > 0) {
		    cell.setAttribute('data-finishtime', '' + t);
		    cell.addEventListener('mouseenter', function(e) {
			let label = this.getAttribute('data-label');
			let remain = Math.floor((this.getAttribute('data-finishtime') - (new Date).getTime()) / 1000);
			if (remain < 0)
			    remain = 0;
			if (label)
			    label += '\n';
			this.setAttribute('tooltiptext', label + GetTimeString(remain, true));
		    });
		}

		// progress
		t = '?';
		prog = -1;
		if (q.api_state == 1 ||
		    q.api_state == 2) {
		    switch (q.api_progress_flag) {
		    case 0:
			    t = '  ';
			    prog = 0;
			    break;
		    case 1: //50%
			    t = '50';
			    prog = 50;
			    break;
		    case 2: //80%
			    t = '80';
			    prog = 80;
			    break;
		    }
		} else if (q.api_state == 3) {
		    //t = '\u2713';	//check mark
		    t = 'OK';
		    prog = 100
		}

		cell = CreateElement('label');
		cell.className = 'quest-progress';
		cell.setAttribute('value', t);
		if (state.length)
		    cell.setAttribute('tooltiptext', state);
		listitem.appendChild(cell);

		// 進行中
		listitem.setAttribute('data-inprogress', q.api_state > 1 ? 'true' : 'false');
		// 0,50,80,100
		listitem.setAttribute('data-progress', '' + prog);

		// 古いときは灰色に。
		listitem.setAttribute('data-stale',
				      (!staletime || qt < staletime) ? 'true' : 'false');

		//debugprint('no: ' + no +
		//	   '[state: ' + q.api_state +
		//	   ', flag:' + q.api_progress_flag +
		//	   '] title: ' + q.api_title +
		//	   '; detail: ' + q.api_detail);

		if (mode == 1) {
		    // 遂行していないかつ進捗なし(-50%)
		    if (q.api_state == 1 &&
			q.api_progress_flag == 0)
			continue;
		} else if (mode == 2) {
		    // 遂行中でも達成済でもない
		    if (q.api_state != 2 && q.api_state != 3)
			continue;
		} else if (mode == 3) {
		    // 遂行中でない
		    if (q.api_state != 2)
			continue;
		} else if (mode == 4) {
		    if ((this._memo[q.api_no] || "").length == 0)
			continue;
		}

		list.appendChild(listitem);
	    }

	    this._save_state();
	},
	ship: 'quest',
	battle: function(stage) {
	    // battle result
	    if (stage == 3)
		this.update.quest.call(this);
	},
	masterMission: function() {
	    this._load_state();
	    this.update.quest.call(this);
	},
    },

    // メモ作成
    createMemo: function(node){
	debugprint(node.nodeName);
	if (!node.triggerNode)
	    return;
	let elem = node.triggerNode;
	let hbox = FindParentElement(elem,"row");
	let no = hbox.getAttribute('data-no');
	let title = hbox.getAttribute('data-title');
	let oldstr = this._memo[no] || "";
	let text = `任務『${ title }』のメモを入力してください。\nツールチップとして表示されるようになります。`;
	let str = InputPrompt(text,`任務#${ no }メモ`, oldstr);
	if( str==null ) return;
	this._memo[no] = str;
	this.update.quest.call(this);
	this._save_state();
    },

    _load_state: function() {
	let data = KanColleUtils.readObject('mission', null);
	this._localstate = null;
	if (!data) {
	    this._state = {};
	    this._memo = {};
	    return;
	}
	if (data.ver == '0.2') {
	    this._state = data.data;
	    //debugprint('mission state loaded: ' + this._state.toSource());
	} else {
	    this._state = {};
	}
	if (data.memo) {
	    this._memo = data.memo;
	} else {
	    this._memo = {};
	}
    },

    _save_state: function() {
	if (this._state) {
	    let data = {
		ver: '0.2',
		data: this._state,
		memo: this._memo,
	    };
	    KanColleUtils.writeObject('mission', data);
	    //debugprint('mission state saved: ' + data.toSource());
	}
    },

    show: function() {
	let checked = $('questinfo-toggle').getAttribute('checked') == 'true';
	$('quest-list-box').collapsed = !checked;
    },

    restore: function() {
	let val = KanColleUtils.getIntPref('quest-info.mode');
	if (!val) {
	    KanColleUtils.setIntPref('quest-info.mode', 0);
	    val = 0;
	}
	$('quest-information-mode-popup-filter').selectedIndex = val;
	this.update.quest.call(this);
	this.show();
    },

    changeMode: function(node) {
	KanColleUtils.setIntPref('quest-info.mode', node.selectedIndex);
	this.update.quest.call(this);
    },
};
KanColleTimerQuestInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerQuestInfo.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerQuestInfo.unload(); }, false);
