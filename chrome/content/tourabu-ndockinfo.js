// vim: set ts=8 sw=4 sts=4 ff=dos :

var TouRabuTimerNdockInfo = {
    update: {
	tourabuRepair: function(extradata) {
	    let swords = KanColleDatabase.tourabuSword.get();
	    let now = KanColleDatabase.tourabuRepair.now();
	    let t = KanColleDatabase.tourabuRepair.timestamp();
	    let list = KanColleDatabase.tourabuRepair.list();
	    for (let i of list) {
		let slot = KanColleDatabase.tourabuRepair.get(i);
		let id = slot ? slot.sword_serial_id : null;
		if (true) {
		    let finish = slot._finished_at;
		    let diff = finish - now;
		    let sword = swords ? swords[id] : null;
		    let sword_name = TouRabuSwordName(id);
		    if (diff > 0) {
			let goal = t + (finish - now);
			$('tourabu-ndock' + i + '-name').value = sword_name;
		    } else {
			$('tourabu-ndock' + i + '-name').value = '';
		    }
		}
	    }
	    if (extradata) {
		let state = extradata.state || 0;
		let id = extradata.id || 0;
		debugprint(extradata.toSource());
		if (state > 1)
		    AddLog(extradata.message + '\n');
		if (id > 0)
		    this._alarm_set(id, extradata.time);
	    }
	},
	tourabuLoginStart: function() {
	    this.restore();
	},
    },

    _set_alarm_time: function(id, timeout) {
	let node = $('tourabu-ndock' + id + '-time');
	this._set_alarm_time_node(node, timeout);
    },
    _update_alarm_time: function(id, diff) {
	let node = $('tourabu-ndock' + id + '-time');
	let rnode = $('tourabu-ndock' + id + '-remain');
	this._update_alarm_time_node(rnode, diff);
	this._update_alarm_style_node(node, diff);
	this._update_alarm_style_node(rnode, diff);
    },

    observe: function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm_update();
	    break;
	default:;
	}
    },

    restore: function() {
	let t = KanColleDatabase.tourabuLoginStart.timestamp();
	let d = KanColleDatabase.tourabuLoginStart.get();
	for (let i = 1; i <= 4; i++) {
	    if (t && i <= d.repair_slot)
		$('tourabu-ndock' + i).collapsed = false;
	    else
		$('tourabu-ndock' + i).collapsed = true;
	}
    },
};
TouRabuTimerNdockInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { TouRabuTimerNdockInfo.load(); }, false);
window.addEventListener('unload', function(e) { TouRabuTimerNdockInfo.unload(); }, false);
