// vim: set ts=8 sw=4 sts=4 ff=dos :


var KanColleTimerMissionBalanceInfo = {
    _id: 'hourly_balance',
    _popupMissionId: null,
    _missions: null,
    _calctime: 0,
    _calcrep: false,

    contextMenuPopupShowing: function() {
	for (let i = 2; i <= 4; i++) {
	    let isdisabled = !KanColleTimerFleetInfo._deckAvailable(i);
	    let item = $('hourly_balance_context_' + i);
	    item.setAttribute('disabled', isdisabled ? "true" : "false");
	}
	this._popupMissionId = document.popupNode.getAttribute('data-missionid');
    },

    contextMenuCommand: function(node) {
	let fleet = parseInt(node.getAttribute('value'), 10);
	let mission_id = parseInt(this._popupMissionId, 10);
	KanColleTimerFleetInfo.setMission(fleet, mission_id);
    },

    _calc: function(id, key) {
	let val = (KanColleData.mission_param[id].balance || {})[key];
	let time = KanColleData.mission_param[id].time;
	let calctime = this._calctime;
	let count;
	if (!this._calctime) {
	    time = 60;
	    count = 1;
	} else {
	    count = Math.floor(this._calctime / time);
	    if (!this._calcrep && count > 1)
		count = 1;
	}
	return val * time * count / 60;
    },

    getDescription: function(missionid) {
	let d = KanColleDatabase.masterMission.get(missionid);
	let param = KanColleData.mission_param[missionid] || {};
	let text = d ? d.api_details.replace(/<[^<>]*>/g, '') : '?';
	if (d.api_difficulty) {
	    text += `\n難易度: ${ d.api_difficulty }`;
	}
	if (d.api_reset_type) {
	    text += `\n遂行制限: ${ d.api_reset_type }`;
	}
	if (param.time) {
	    let min = param.time % 60;
	    let hour = Math.floor((param.time - min) / 60);
	    text += '\n時間: ' + hour + ':' + ('00' + min).slice(-2);
	}
	if (d.api_use_bull || d.api_use_fuel || d.api_damage_type) {
	    let use = [];
	    if (d.api_use_fuel)
		use.push(`燃料 ${ d.api_use_fuel }`);
	    if (d.api_use_bull)
		use.push(`弾薬 ${ d.api_use_bull }`);
	    if (d.api_damage_type)
		use.push(`被弾 ${ d.api_damage_type }`);
	    text += `\n消費: ${ use.join(', ') }`;
	}
	if (d.api_win_mat_level || d.api_win_item1 || d.api_win_item2) {
	    let materials = [ '燃料', '弾薬', '鋼材', 'ボーキサイト' ];
	    let mat = [];
	    text += '\n獲得可能: ';
	    for (let i = 0; i < d.api_win_mat_level.length; i++) {
		if (d.api_win_mat_level[i])
		    mat.push(`${ materials[i] }: ${ d.api_win_mat_level[i] }`);
	    }
	    if (mat.length)
		text += '\n\t' + mat.join(', ');

	    for (let [type,count] of [d.api_win_item1, d.api_win_item2]) {
		if (!type || !count)
		    continue;
		text += `\n\t${ (KanColleDatabase.masterUseitem.get(type) || {}).api_name }: ${ count }`;
	    }
	}
	if (param.lv) {
	    text += '\nLv:';
	    if (param.lv.flagship)
		text += ' ' + param.lv.flagship;
	    if (param.lv.total)
		text += '/' + param.lv.total;
	}
	if (param.stype || param.num) {
	    text += '\n編成:';
	    if (param.num)
		text += ' ' + param.num;
	    if (param.stype) {
		text += '\n' +
			    param.stype.map(function(element) {
						return '\t' +
						    element.mask.map(function(e){
									let name = null;
									if (e >= 0)
									    name = (KanColleDatabase.masterStype.get(e) || {}).api_name;
									else
									    name = (KanColleDatabase.masterShip.get(-e) || {}).api_name;
									return name || `UNKNOWN_${ e }`;
						    }).join('|') +
						': ' + element.num +
						(element.flagship ? '(旗艦)' : '');
			    }).join('\n');
	    }
	}
	if (d.api_sample_fleet) {
	    text += '\n編成例: ' +
		    d.api_sample_fleet.reduce(function(s,c) {
			let ms = KanColleDatabase.masterStype.get(c) || {};
			if (ms.api_name)
			    s.push(ms.api_name);
			return s;
		    }, []).join(',');
	}
	if (param.slotitem) {
	    text += '\n装備:\n' +
		    Object.keys(param.slotitem).map(function(element) {
							let cond = param.slotitem[element];
							let etype = KanColleDatabase.masterSlotitemEquiptype.get(element);
							return '\t' + (etype ? etype.api_name : ('UNKNOWN_' + element)) + ': ' +
							    (cond.ship ? cond.ship : 0) +
							    (cond.num ? '/' + cond.num : '');
						    }).join('\n');
	}
	if (param.karyoku) {
	    text += '\n火力: ' + param.karyoku;
	}
	if (param.taiku) {
	    text += '\n対空: ' + param.taiku;
	}
	if (param.taisen) {
	    text += '\n対潜: ' + param.taisen;
	}
	if (param.sakuteki) {
	    text += '\n索敵: ' + param.sakuteki;
	}
	if (param.remarks) {
	    text += '\n備考:' + param.remarks;
	}
	return text;
    },

    _fillTable: function(key){
	let rows = $('hourly_balance');
	let that = this;

	if (!key)
	    key = 0;
	key--;

	if (!this._missions) {
	    this._missions = Object.keys(KanColleData.mission_param);
	}

	this._missions = (function() {
	    let mission_values = that._missions.map(function(e, i) {
		return { id: e, idx: i, };
	    });
	    mission_values.sort(function(a, b) {
		let va = key >= 0 ? that._calc(a.id, key) : -a.id;
		let vb = key >= 0 ? that._calc(b.id, key) : -b.id;
		return res = isNaN(va) - isNaN(vb) || vb - va || a.idx - b.idx;
	    });
	    return mission_values.map(function(e) {
		return e.id;
	    });
	})();

	for (let i = -1; i < 4; i++)
	    SetStyleProperty($('hourly_balance_label-' + (i + 1)), 'font-weight', i == key ? 'bold' : null);

	this._missions.forEach(function(i) {
	    let l;
	    let row = CreateElement('row');
	    let name = KanColleData.mission_param[i].name;
	    row.setAttribute('data-missionid', '' + i);
	    l = CreateLabel( name );
	    l.setAttribute('data-missionid', '' + i);
	    l.setAttribute('context', 'hourly_balance_context');
	    row.appendChild(l);
	    for( let j=0; j<4; j++ ){
		    let txt = KanColleData.mission_param[i].balance ? that._calc(i,j).toFixed(2) : '?';
		    row.appendChild( CreateLabel(txt) );
	    }
	    row.setAttribute("style","border-bottom: 2px solid gray;");
	    row.setAttribute("tooltiptext", that.getDescription(i));
	    rows.appendChild( row );
	});
    },

    _clearTable: function() {
	let parent = $(this._id);
	let node, next;
	for (node = parent.firstChild; node; node = next) {
	    next = node.nextSibling;
	    if (node.getAttribute('data-missionid'))
		RemoveElement(node);
	}
    },

    change: function() {
	let time = parseInt($('hourly_balance_time').value, 10);
	if (isNaN(time) || time < 0)
	    time = 0;
	this._calctime = time;
	if (!time) {
	    $('hourly_balance_repeat').setAttribute('checked', 'true');
	    $('hourly_balance_repeat').setAttribute('disabled', 'true');
	} else {
	    $('hourly_balance_repeat').setAttribute('disabled', 'false');
	}
	this._calcrep = $('hourly_balance_repeat').getAttribute('checked') == 'true';
	this._clearTable();
	this._fillTable(this._resid);
    },

    changesort: function(node) {
	let resid = parseInt(node.getAttribute('data-resid'), 10);
        this._resid = resid;
	this._clearTable();
	this._fillTable(resid);
    },

    update: {
	masterStype: function() {
	    this._clearTable();
	    this._fillTable(this._resid);
	},
	masterMission: function() {
	    this._clearTable();
	    this._fillTable(this._resid);
	},
	masterSlotitemEquiptype: function() {
	    this._clearTable();
	    this._fillTable(this._resid);
	}
    },

    init: function() {
	KanColleDatabase.init();
	this._update_init();
        this._resid = 0;
    },

    exit: function() {
	this._clearTable();
	this._update_exit();
	KanColleDatabase.exit();
    },
};
KanColleTimerMissionBalanceInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerMissionBalanceInfo.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerMissionBalanceInfo.unload(); }, false);
