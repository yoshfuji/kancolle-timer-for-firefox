// vim: set ts=8 sw=4 sts=4 ff=dos :

/*
 * 建造
 *  member/kdock	: api_data
 */
var KanColleTimerKdockInfo = {
    update: {
	kdock: function(extradata) {
	    let docks = extradata.id ? [ extradata.id ] : KanColleDatabase.kdock.list();

	    // 建造ドック
	    for (let k of docks) {
		let d = KanColleDatabase.kdock.get(k);

		if( d.api_state > 0 ){
		    // 建造艦の推定
		    // 建造艦艇の表示…はあらかじめ分かってしまうと面白みがないのでやらない
		    /*
		    let ship_id = parseInt( d.api_created_ship_id, 10 );
		    let ship_name = FindShipNameByCatId(d.api_created_ship_id);
		     */
		    let timestamp = KanColleDatabase.kdock.timestamp(k);
		    let complete_time = timestamp.complete;
		    let created_time = timestamp.start;
		    debugprint('complete_time='+complete_time+', created_time='+created_time);
		    let ship_name = this._guess_ship(Math.floor(created_time/1000),
						     Math.floor(complete_time/1000));
		    $('kdock-name'+k).setAttribute('tooltiptext', ship_name);
		    SetStyleProperty($('kdock-name'+k), 'color', 'rgba(0,0,0,0)');
		    $('kdock-name'+k).value = FindShipNameByCatId(d.api_created_ship_id);
		}else if (d.api_state == 0) {
		    // 建造していない
		    SetStyleProperty($('kdock-name'+k), 'color', 'rgba(0,0,0,0)');
		    $('kdock-name'+k).value = '-';
		    $('kdock-name'+k).setAttribute('tooltiptext', '');
		}
	    }

	    if (extradata) {
		let state = extradata.state || 0;
		let id = extradata.id || 0;
		debugprint(extradata.toSource());
		if (state > 1)
		    AddLog(extradata.message + '\n');
		if (id)
		    this._alarm_set(id, extradata.time);
	    }
	},

	memberBasic: function() {
	    let d = KanColleDatabase.memberBasic.get();
	    let ndocks;
	    if (!d)
		return;
	    ndocks = document.getElementsByClassName("kdock-box");
	    for ( let i = 0; i < 4; i++ )
		SetStyleProperty(ndocks[i], 'display', i < d.api_count_kdock ? "":"none");
	},

	material: function() {
	    let d = KanColleDatabase.material.get('burner');
	    if (d >= 0)
		$('burner-number').value = d;
	},

    },

    _set_alarm_time: function(id, timeout) {
	let node = $('kdock' + id);
	this._set_alarm_time_node(node, timeout);
    },
    _update_alarm_time: function(id, diff) {
	let node = $('kdock' + id);
	let rnode = $('kdockremain' + id);
	this._update_alarm_time_node(rnode, diff);
	this._update_alarm_style_node(node, diff);
	this._update_alarm_style_node(rnode, diff);
    },

    _set_alarm_format: function() {
	const classname = 'kdock-time';
	let b = KanColleUtils.getBoolPref('display.short');
	let elems = document.getElementsByClassName(classname);
	for(let i = 0; i < elems.length; i++){
	    elems[i].style.display = b ? 'none' : 'block';
	}
    },

    observe: function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm_update();
	    break;
	case 'nsPref:changed':
	    this._set_alarm_format();
	    break;
	default:;
	}
    },

    show: function() {
	let checked = $('kdockinfo-toggle').getAttribute('checked') == 'true';
	let nodes = document.getElementsByClassName("kdock-box");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true' ||
		  $('gentimer-toggle').getAttribute('checked') == 'true' ||
		  $('deckinfo-toggle').getAttribute('checked') == 'true' ||
		  $('ndockinfo-toggle').getAttribute('checked') == 'true' ||
		  $('kdockinfo-toggle').getAttribute('checked') == 'true';
	$('timer').collapsed = !checked;
    },

    restore: function() {
	this.update.memberBasic.call(this);
	this.show();
    },

    /*
     * 建造艦名表示（隠し機能）
     *
     * 建造ドックのNo.欄を素早く何度かダブルクリックすると
     * 艦名を tooltip として表示
     */
    timer: {},

    handleEvent: function(e) {
	let id = e.target.id;
	let now = (new Date).getTime();

	if (!this.timer[id] || this.timer[id] + 1000 < now) {
	    this.timer[id] = now;
	    return;
	}
	this.timer[id] += 1000;

	if (this.timer[id] - now <= 2000)
	    return;

	if (id.match(/^kdock-name(\d+)$/)) {
	    let fleet_id = parseInt(RegExp.$1, 10);
	    let fleet = KanColleDatabase.kdock.get(fleet_id);
	    if( fleet && fleet.api_complete_time ){
		let ship_id = parseInt( fleet.api_created_ship_id, 10 );
		let ship_name = FindShipNameByCatId(ship_id);
		e.target.setAttribute('tooltiptext',ship_name);
	    }
	}
    },

    _guess_ship: function(now, finishedtime) {
	let remain = finishedtime - now;
	for( let k in KanColleData.construction_shipname ){
	    let shipname = KanColleData.construction_shipname[k];
	    k = parseInt( k, 10 );
	    k *= 60;

	    let t1 = k - 30;
	    let t2 = k + 30;

	    if( t1 < remain && remain < t2 ){
		return shipname;
	    }
	}
	return "建造艦種不明";
    },

    init: function() {
	KanColleDatabase.init();
	this._update_init();
	this._prefs_init();
	this._alarm_init();
	for( let i = 0; i < 4; i++ ){
	    let k = 'kdock-name' + (i + 1);
	    $(k).addEventListener('dblclick', this);
	}
    },

    exit: function(){
	for( let i = 0; i < 4; i++ ){
	    let k = 'kdock-name' + (i + 1);
	    $(k).removeEventListener('dblclick', this);
	}
	this._alarm_exit();
	this._prefs_exit();
	this._update_exit();
	KanColleDatabase.exit();
    },
};
KanColleTimerKdockInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerKdockInfo.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerKdockInfo.unload(); }, false);
