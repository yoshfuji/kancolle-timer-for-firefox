// vim: set ts=8 sw=4 sts=4 ff=dos :

var KanColleTimerAirCorpsInfo = {
    _sq_cond_color: function(cond) {
	let color;
	if (cond === undefined) {
	    color = null;
	} else if (cond == 1) {
	    color = '#88ff88';	    //平常
	} else if (cond == 2) {
	    color = '#ffaa44';	    //オレンジ
	} else if (cond == 3) {
	    color = '#ff8888';	    //赤
	} else {
	    color = '#666666';	    //...
	}
	return color;
    },
    _sq_action_color: function(action) {
	let color;
	if (action === undefined) {
	    color = null;
	} else {
	    color = ([
		'rgba(128,128,128,0.875)',    //待機
		'rgba(255,192,192,0.875)',    //出撃
		'rgba(255,255,192,0.875)',    //防空
		'rgba(192,255,255,0.875)',    //退避
		'rgba(192,255,192,0.875)',    //休憩
	    ])[action] || null;
	}
	return color;
    },

    update: {
	baseaircorps: function() {
	    let l = KanColleDatabase.baseaircorps.list();

	    this._show(false);

	    for ( let i = 0; i < l.length; i++ ){
		let base = KanColleDatabase.baseaircorps.get(l[i]);
		let base_container = $(`aircorpsstatus-base-${ base.api_rid }`);
		let basenode = $('aircorpsstatus-' + base.api_rid + '-0');
		let base_name = base.api_name;
		let base_distance = base.api_distance.api_base + base.api_distance.api_bonus;
		let base_action = (['待機','出撃','防空','退避','休息'])[base.api_action_kind] || base.api_action_kind;
		let base_text = base_name + '\nアクション: ' + base_action + '\n航続距離: ' + base_distance;
		basenode.setAttribute('value', `${ base.api_area_id }#${ base.api_rid }`);
		basenode.setAttribute('tooltiptext', base_text);
		SetStyleProperty(base_container, 'background-color', this._sq_action_color(base.api_action_kind));
		for (let j = 0; j < base.api_plane_info.length; j++) {
		    let sq = base.api_plane_info[j];
		    let sq_cond = ([,'平常','疲労','重疲労'])[sq.api_cond] || sq.api_cond;
		    let node = $('aircorpsstatus-' + base.api_rid + '-' + sq.api_squadron_id);
		    let item = KanColleDatabase.slotitem.get(sq.api_slotid) || {};
		    let itemtype = KanColleDatabase.masterSlotitem.get(item.api_slotitem_id) || {};
		    let item_name;
		    let item_desc = '';
		    let item_bgcolor = null;
		    let res_color = 'rgba(0,0,0,0)';

		    switch (sq.api_state | 0) {
		    case 0:
			item_name = '-';
			break;
		    case 2:
			item_name = '*';
			break;
		    default:
			debugprint('Unknown state: ' + sq.api_state);
		    case 1:
			item_name = itemtype.api_name || sq.api_slotid;
			item_desc = item_name;
			if (item.api_level || item.api_alv) {
			    item_desc += '[';
			    if (item.api_level)
				item_desc += '★' + item.api_level;
			    if (item.api_alv)
				item_desc += '+' + item.api_alv;
			    item_desc += ']';
			}
			if (!isNaN(sq.api_count) && !isNaN(sq.api_max_count)) {
			    item_desc += '(' + sq.api_count + '/' + sq.api_max_count + ')';
			    if (sq.api_count < sq.api_max_count)
				res_color = 'red';
			}
			if (!isNaN(sq.api_cond)) {
			    item_desc += '\n状態: ' + sq_cond;
			    item_bgcolor = this._sq_cond_color(sq.api_cond);
			}
			break;
		    }
		    node.value = item_name;
		    node.setAttribute('tooltiptext', item_desc);
		    SetStyleProperty(node, 'outline-color', res_color);
		    SetStyleProperty(node, 'background-color', item_bgcolor);
		}
		this._show(true, base.api_rid);
	    }
	},
	reqMapStart: function() {
	    debugprint('mapstart');
	    this._show(false);
	},
	memberBasic: function() {
	    debugprint('basic');
	    this._show(false);
	},
    },

    _show: function(isenabled, base_id) {
	if (!base_id) {
	    let nodes = document.getElementsByClassName('aircorpsstatus');
	    for (let i = 0; i < nodes.length; i++)
		nodes[i].collapsed = !isenabled;
	} else {
	    $('aircorpsstatus-base-' + base_id).collapsed = !isenabled;
	}
    },

    restore: function() {
	//this.show();
    },

    init: function() {
	KanColleDatabase.init();
	this._update_init();
    },

    exit: function() {
	this._update_exit();
	KanColleDatabase.exit();
    },
};
KanColleTimerAirCorpsInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerAirCorpsInfo.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerAirCorpsInfo.unload(); }, false);
