// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("chrome://kancolletimer/content/utils.jsm");
Components.utils.import("resource://gre/modules/Promise.jsm");
Components.utils.import("chrome://kancolletimer/content/e10s-main.jsm");

var SSTweet = {

    getTempFile:function(){
        let file = Components.classes["@mozilla.org/file/directory_service;1"].getService(Components.interfaces.nsIProperties).get("TmpD", Components.interfaces.nsIFile);
        file.append("kancolletimer-ss-temp.jpg");
	return file;
    },

    presend: function(data) {
	let that = this;
	let p;

	$('send-button').disabled = true;

	if (data) {
	    p = Promise.resolve(data);
	} else {
	    p = new Promise(function(resolve, reject) {
		let tab = KanColleTimerUtils.window.findTab(KanColleUtils.URL);
		if (!tab) {
		    AlertPrompt("艦隊これくしょんのページが見つかりませんでした。","艦これタイマー");
		    reject(new Error('Failed to find tab.'));
		    return;
		}
		KanColleTimerE10S.request(tab.linkedBrowser.messageManager,
					  'screenshot',
					  {
					    format: [ 'image/jpeg' ],
					    privacy: KanColleUtils.getBoolPref("screenshot.mask-name"),
					  },
					  function(header, msg) {
					    if (msg.error)
						reject(new Error(msg.error));
					    else {
						resolve(Components.classes['@mozilla.org/network/io-service;1']
							.getService(Components.interfaces.nsIIOService)
							.newURI(msg.data.url, null, null));
					    }
					  }
		);
	    });
	}

	p.then(function(data) {
	    $('ss-image').src = data.spec;

	    let file = that.getTempFile();
	    debugprint(file.path);

	    debugprint('saving file...');
	    return new Promise(function(resolve, reject) {
		KanColleTimerUtils.file.saveURI(file, data, function(_file, _url) {
		    resolve([_file, _url]);
		});
	    });
	})
	.then(function([file,data]) {
	    debugprint('done.');

	    $('text').focus();

	    if(!Twitter.getScreenName()){
		Twitter.getRequestToken();
	    } else {
		$('screen-name').value = "@" + Twitter.getScreenName();
	    }

	    $('send-button').disabled = false;
	})
	.catch(function(error){
	    debugprint('failed: ' + error);
	});
    },
    send: function(){
	let text = $('text').value;
	let file = this.getTempFile();
	Twitter.updateStatusWithMedia(text, new File(file.path));
	//Twitter.updateStatus(text);
	setTimeout( function(){ $('send-button').disabled = false; }, 1000 );
    },
    init:function(){
	this.presend(window.arguments && window.arguments[0]);
    },
    destroy: function(){
    }

};

window.addEventListener("load", function(e){ SSTweet.init(); }, false);
window.addEventListener("unload", function(e){ SSTweet.destroy(); }, false);
