// vim: set ts=8 sw=4 sts=4 ff=dos :

var KanColleTimerFleetInfo = {
    //_get_shipname: function(deckid,pos) {
    //	let ships = KanColleDatabase.deck.get(deckid).api_ship;
    //	if (pos >= 1 && pos <= 6)
    //	    return FindShipName(ships[pos - 1]);
    //	else
    //	    return '?' + pos;
    //},
    _mission_plan: {},
    _escape: {},
    _slotitem: {},
    _branchpoint_param: 1,

    _ship_hp_color: function(hpratio) {
	let ship_color;
	let ship_damage_color;
	if (hpratio >= 1) {
	    ship_color = null;
	    ship_damage_color = '#ddffdd';
	} else if (hpratio > 0.75) {
	    ship_color = '#0000cc'; //かすり傷
	    ship_damage_color = '#00ff00';
	} else if (hpratio > 0.50) {
	    ship_color = '#ff44cc'; //小破
	    ship_damage_color = '#ffff00';
	} else if (hpratio > 0.25) {
	    ship_color = '#ff4400'; //中破
	    ship_damage_color = '#ffa500';
	} else if (hpratio > 0) {
	    ship_color = '#ff0000'; //大破
	    ship_damage_color = '#ff0000';
	} else {
	    ship_color = '#000000'; //撃沈
	    ship_damage_color = '#6495ed';
	}
	return [ship_color, ship_damage_color];
    },

    _ship_cond_color: function(cond) {
	let color = null;
	if (cond === undefined) {
	    color = null;
	} else if (cond >= 90) {
	    color = '#ffffff';   //キラキラ
	} else if (cond >= 80) {
	    color = '#eeffff';   //キラキラ
	} else if (cond >= 70) {
	    color = '#ddffee';   //キラキラ
	} else if (cond >= 60) {
	    color = '#ccffdd';   //キラキラ
	} else if (cond >= 50) {
	    color = '#bbffcc';   //キラキラ
	} else if (cond >= 40) {
	    color = '#88ff88';   //平常
	} else if (cond >= 30) {
	    color = '#ffdd88';   //間宮
	} else if (cond >= 20) {
	    color = '#ffaa44';   //オレンジ
	} else if (cond >= 0) {
	    color = '#ff8888';   //赤
	} else {
	    color = '#666666';   //...
	}
	return color;
    },

    _show_battleresult_menu: function(enable) {
	debugprint(`_show_battlresult_menu(${ enable })`);
	$('shipstatus-context-battleresult').setAttribute('hidden', !enable);
	$('shipstatus-context-battleresult').addEventListener('command', function() { KanColleTimerFleetInfo._discloseBattleResult(true); }, true);
    },

    __clear_enemy_ships: function(en) {
	debugprint(`__clear_enemy_ships(${ en })`);
	let [dummy,defaultcolor] = this._ship_hp_color(1);
	$('shipstatus-' + en + '-0').value = '-';
	$('shipstatus-' + en + '-0').removeAttribute('data-fleet-available');
	$('shipstatus-' + en + '-0').removeAttribute('data-fleet-frozen');
	for (let i = 1; i <= 6; i++) {
	    let node = $('shipstatus-' + en + '-' + i);
	    node.value = '-';
	    node.removeAttribute('data-fleet-available');
	    SetStyleProperty(node, 'text-shadow', null);
	    SetStyleProperty(node, 'border-left-color', defaultcolor);
	    SetStyleProperty(node, 'text-decoration', null);
	    SetStyleProperty(node, 'background', null);
	    SetStyleProperty(node, '-moz-text-decoration-style', null);
	    SetStyleProperty(node, 'text-decoration-style', null);
	    SetStyleProperty(node, '-moz-text-decoration-color', null);
	    SetStyleProperty(node, 'text-decoration-color', null);
	}
    },
    _clear_enemy_ships: function() {
	debugprint(`_clear_enemy_ships()`);
	this.__clear_enemy_ships('e1');
	this.__clear_enemy_ships('e2');
    },

    __show_enemy_ships: function(en, show) {
	debugprint(`__show_enemy_ships(${en}, ${ show })`);
	if ($(`shipstatus-${ en }-0`).getAttribute('data-fleet-available'))
	    $('shipstatus-fleet-' + en).setAttribute('hidden', !show);
    },
    _show_enemy_ships: function(show) {
	debugprint(`_show_enemy_ships(${ show })`);
	this.__show_enemy_ships('e1', show);
	this.__show_enemy_ships('e2', show);
    },

    _set_escape_ships: function() {
	debugprint(`_set_escape_ships()`);
	let that = this;
	let decks = KanColleDatabase.deck.list();
	decks.forEach(function(deckid) {
	    let res = KanColleDatabase.battle.get(deckid) || {};
	    let escape = res.escape || [];
	    debugprint('escape[' + deckid + ']: ' + escape.toSource());
	    escape.forEach(function(e,i){
		let node = $('shipstatus-' + deckid + '-' + i);
		if (!i)
		    return;
		SetStyleProperty(node, 'border-right-color', e ? 'rgb(64,64,64)' : null);
		if (!that._escape[deckid])
		    that._escape[deckid] = [];
		that._escape[deckid][i] = e ? true : false;
	    });
	});
    },

    ___set_enemy_name: function(en, d) {
	debugprint(`___set_enemy_name(${ en }, ${ d })`);
	let enemy_style = null;
	let enemy_weight = null;
	let enemy_decoration = null;
	let enemy_name;

	if (!d) {
	    debugprint('no enemy information');
	    return;
	}

	if (d.practice || d.ratio !== undefined)
	    enemy_style = 'italic';
	if (d.boss)
	    enemy_weight = 'bold';
	if (d.last)
	    enemy_decoration = 'underline';

	enemy_name = d.name;
	if (d.ratio !== undefined)
	    enemy_name += `[${ (d.ratio * 100).toFixed(0) }%]`;

	$('shipstatus-' + en + '-0').value = enemy_name;
	if (d.fullname)
	    $('shipstatus-' + en + '-0').setAttribute('tooltiptext', d.fullname);
	else
	    $('shipstatus-' + en + '-0').removeAttribute('tooltiptext');

	SetStyleProperty($('shipstatus-' + en + '-0'), 'font-style', enemy_style);
	SetStyleProperty($('shipstatus-' + en + '-0'), 'font-weight', enemy_weight);
	SetStyleProperty($('shipstatus-' + en + '-0'), 'text-decoration', enemy_decoration);
    },

    ___set_enemy_ships: function(en, d, dm) {
	debugprint(`___set_enemy_ships(${ en }, ${ d }, ${ dm })`);
	if (!d || !d.ships) {
	    debugprint('no enemy ship information');
	    return;
	}

	d.ships.forEach(function(e,idx) {
	    if (idx) {
		let node = $('shipstatus-' + en + '-' + idx);
		let eship = dm[e];
		let name = '-';
		let ecolor = null;

		if (eship) {
		    name = eship.name;
		    if (eship.flagship)
			ecolor = 'red';
		    if (eship.elite)
			ecolor = 'orange';
		}

		node.value = name;
		SetStyleProperty(node, 'background', name ? '#cccccc' : null);
		if (e < 0)
		    SetStyleProperty(node, 'border-left-color', null);
		if (ecolor)
		    SetStyleProperty(node, 'text-shadow', '0 0 2px ' + ecolor);
	    }
	});
    },

    __set_enemy_ships: function(en) {
	debugprint(`__set_enemy_ships(${ en })`);
	let d = KanColleDatabase.battle.get(en);
	let dm = {};

	if (!d || !d.ships)
	    return;

	$(`shipstatus-${ en }-0`).setAttribute('data-fleet-available', true);
	$(`shipstatus-${ en }-0`).setAttribute('data-fleet-frozen', true);

	this.___set_enemy_name(en, d);

	d.ships.forEach(function(e,idx) {
	    if (!idx || dm[e])
		return;

	    let ship = KanColleDatabase.masterShip.get(e);
	    if (!ship)
		return;

	    dm[e] = {
		name: ship.api_name,
		flagship: ship.api_yomi == 'flagship',
		elite: ship.api_yomi == 'elite',
	    };
	});
	return this.___set_enemy_ships(en, d, dm);
    },

    _set_enemy_ships: function() {
	debugprint(`_set_enemy_ships()`);
	this.__set_enemy_ships('e1');
	this.__set_enemy_ships('e2');
    },

    __set_guessed_enemy_name: function(en, ranks) {
	debugprint(`_set_guessed_enemy_name(${ en }, ${ JSON.stringify(ranks) })`);

	let frozen = $('shipstatus-' + en + '-0').getAttribute('data-fleet-frozen') == 'true';
	if (frozen)
	    return;

	let _d = $('shipstatus-e1-0').getAttribute('data-guessed-map');
	if (!_d)
	    return;
	let d = JSON.parse(_d);
	if (!d || !d.next)
	    return;

	let db0 = d.next[ranks.cell];
	if (!db0 || !db0.deck_names)
	    return;

	let db1 = db0.deck_names[ranks.deck_name];
	if (!db1 || !db1.decks)
	    return;

	let db2 = db1.decks[ranks.deck];
	if (!db2)
	    return;

	$('shipstatus-' + en + '-0').setAttribute('data-fleet-available', 'true');

	let o = {
	    name: `${ d.area_name }-${ d.next[ranks.cell].cell }${ en == 'e2' ? '#2' : '' }`,
	    fullname: `${ db1.deck_name } [${ (db1.cell_deck_name_ratio *100).toFixed(0) }%]`,
	    ratio: db0.ratio,
	};

	this.___set_enemy_name(en, o);
    },

    _select_guessed_enemy: function(ranks) {
	if (!ranks)
	    ranks = { cell: 0, deck_name: 0, deck: 0 };
	debugprint(`_select_guessed_enemy(${ JSON.stringify(ranks)})`);
	// e1 only for now
	this.__set_guessed_enemy_name('e1', ranks);
    },

    __guess_enemy_ships: function(area_name, path) {
	debugprint(`__guess_enemy_ships(${ area_name }, ${ path })`);
	let d = KanColleDatabase.map.get(area_name, path);
	if (!d)
	    return;
	//
	// #1-2-3 [99%]
	//  +--------------+
	//  |3:敵艦隊1[99%]|>+-----------------------+
	//  |4:敵艦隊2[1%] | |潜水A級,...+...   [50%]|
	//  +--------------+ |潜水B級,...+...   [50%]|
	//                   +-----------------------+
	//
	// [
	//   {
	//     "cell": no,
	//     "count": num,
	//     "ratio": ratio,
	//     "deck_names": [
	//       {
	//         "deck_name": deck_name,
	//         "count": deck_name_count,
	//         "cell_deck_name_ratio": deck_name_ratio,
	//         "decks": [
	//           {
	//             "ships": {
	//               "e1":[...],
	//               "e2":[...],
	//             },
	//             "ship_data": { ... },
	//             "decks_count": decks_count,
	//             "cell_decks_ratio": decks_ratio,
	//           }
	//         ],
	//       }, ...
	//     ],
	//   },...
	// ]
	let next = Object.keys(d.next).sort(function(a,b) {
	    return d.next[b].count - d.next[a].count;
	}).map(function(e) {
	    return { cell: e, count: d.next[e].count, ratio: d.next[e].count / d.next_total };
	});

	//debugprint(`next: ${ JSON.stringify(next) }`);
	//debugprint(`data: ${ JSON.stringify(d.next) }`);
	// next: [{"cell":"1","count":79,"ratio":1}]
	// data: {"1":{"count":79,"battleinfo_total":79,"battleinfo":{"[\"敵偵察艦\",[[1502,\"駆逐ロ級\",\"-\"]],[]]":{"count":25,"first":1596153553196,"last":1608824011948,"data":{"deck_name":"敵偵察艦","ship_data":{"1502":{"api_name":"駆逐ロ級","api_yomi":"-"}},"ships":{"e1":[1502],"e2":[]}}},"[\"敵偵察艦\",[[1503,\"駆逐ハ級\",\"-\"]],[]]":{"count":21,"first":1596154491482,"last":1608875062638,"data":{"deck_name":"敵偵察艦","ship_data":{"1503":{"api_name":"駆逐ハ級","api_yomi":"-"}},"ships":{"e1":[1503],"e2":[]}}},"[\"敵偵察艦\",[[1501,\"駆逐イ級\",\"-\"]],[]]":{"count":33,"first":1596341578058,"last":1608873354932,"data":{"deck_name":"敵偵察艦","ship_data":{"1501":{"api_name":"駆逐イ級","api_yomi":"-"}},"ships":{"e1":[1501],"e2":[]}}}}}}

	//for (let n of next) {
	//    let cell = n.cell;
	//    let db = d.next[cell];
	//    n['decks'] = [];
	//    for (let v of Object.values(db.battleinfo)) {
	//	let vd = JSON.parse(JSON.stringify(v.data));
	//	vd['ratio'] = v.count / db.battleinfo_total;
	//	n.decks.push(vd);
	//    }
	//}

	for (let n of next) {
	    let cell = n.cell;
	    let db = d.next[cell];
	    let deck_names = {};

	    if (!db.battleinfo)
		continue;

	    for (let v of Object.values(db.battleinfo)) {
		let vd = JSON.parse(JSON.stringify(v.data));
		let dn = deck_names[vd.deck_name];
		if (!dn) {
		    deck_names[vd.deck_name] = dn = {
			deck_name: vd.deck_name,
			count: 0,
			first: v.first,
			last: v.last,
			decks: [],
		    }
		}
		dn.count += v.count;
		dn['cell_deck_name_ratio'] = dn.count / db.battleinfo_total;
		if (dn.first < v.first)
		    dn.first = v.first;
		if (dn.last < v.last)
		    dn.last = v.last;
		vd['cell_decks_ratio'] = v.count / db.battleinfo_total;
		delete(vd.deck_name);
		dn.decks.push(vd);
	    }
	    n['deck_names'] = Object.keys(deck_names).sort(function(a,b){
		return deck_names[b].last - deck_names[a].last;
	    }).map(function(v){
		return deck_names[v];
	    });
	}
	debugprint(`next: ${ JSON.stringify(next) }`);
	//[{"cell":"1","count":86,"ratio":1,"deck_names":[{"deck_name":"敵偵察艦","count":86,"first":1596341578058,"last":1608944880184,"decks":[{"ship_data":{"1502":{"api_name":"駆逐ロ級","api_yomi":"-"}},"ships":{"e1":[1502],"e2":[]},"cell_decks_ratio":0.313953488372093},{"ship_data":{"1503":{"api_name":"駆逐ハ級","api_yomi":"-"}},"ships":{"e1":[1503],"e2":[]},"cell_decks_ratio":0.29069767441860467},{"ship_data":{"1501":{"api_name":"駆逐イ級","api_yomi":"-"}},"ships":{"e1":[1501],"e2":[]},"cell_decks_ratio":0.3953488372093023}],"cell_deck_name_ratio":1}]}]
	return next;
    },

    _set_guessed_enemy_ships: function(area_name, path) {
	debugprint(`_set_guessed_enemy_ships(${ area_name }, ${ path })`);
	let next = this.__guess_enemy_ships(area_name, path);
	if (!next)
	    return;
	this._updateMapMenu(next);
	for (let en of ['e1', 'e2']) {
	    $(`shipstatus-${ en }-0`).setAttribute('data-guessed-map', JSON.stringify({
		area_name: area_name,
		next: next
	    }));
	}
    },


    _discloseBattleResult: function(nowarn) {
	debugprint(`_discloseBattleResult(${ nowarn })`);
	let that = this;
	let warnnames_all = [];
	KanColleDatabase.battle.list().forEach(function(deckid) {
	    let battle = KanColleDatabase.battle.get(deckid) || {};
	    let warn = 0;
	    let warnnames = [];
	    if (!battle.result)
		return;
	    Object.keys(battle.result).forEach(function(i) {
		let node = $('shipstatus-' + deckid + '-' + i);
		let res = battle.result[i];
		let ratio = res.cur / res.maxhp;
		let [hpcolor, damagecolor] = that._ship_hp_color(ratio);
		SetStyleProperty(node, 'border-left-color', damagecolor);
		// 大破警告
		//  敵艦,旗艦は除外
		//  出撃中なので入渠中ではない
		if (!nowarn && !isNaN(parseInt(deckid, 10)) && i > 1 &&
		    ratio <= 0.25) {
		    let shipslot = (that._slotitem[deckid] && that._slotitem[deckid][i]) || [];
		    let escape = (that._escape[deckid] && that._escape[deckid][i]) || false;
		    if (!shipslot[23] && !escape) {
			warn++;
			warnnames.push(node.getAttribute('data-name') + ' Lv' + node.getAttribute('data-lv'));
		    }
		}
		// ダメージ時のみ更新
		if (res.damage) {
		    if (hpcolor) {
			SetStyleProperty(node, 'text-decoration', 'line-through');
			SetStyleProperty(node, '-moz-text-decoration-style', 'double');
			SetStyleProperty(node, 'text-decoration-style', 'double');
			SetStyleProperty(node, '-moz-text-decoration-color', hpcolor);
			SetStyleProperty(node, 'text-decoration-color', hpcolor);
		    } else {
			SetStyleProperty(node, 'text-decoration', null);
			SetStyleProperty(node, '-moz-text-decoration-style', null);
			SetStyleProperty(node, 'text-decoration-style', null);
			SetStyleProperty(node, '-moz-text-decoration-color', null);
			SetStyleProperty(node, 'text-decoration-color', null);
		    }
		}
	    });
	    if (!isNaN(parseInt(deckid, 10))) {
		if (warn) {
		    let deckname = KanColleDatabase.deck.get(deckid).api_name;
		    debugprint('#' + deckid + '(' + warn + '): ' + warnnames.join(','));
		    warnnames_all.push(deckname + 'の' + warnnames.join(','));
		}
		SetStyleProperty($('shipstatus-fleet-' + deckid), 'background-color', warn ? 'red' : null);
	    }
	});
    },

    update: {
	deck: function() {
	    let l = KanColleDatabase.deck.list();

	    function timestr(t){
		let d = new Date;
		let h;
		let m;

		d.setTime(t);

		h = d.getHours();
		if (h < 10)
		    h = '0' + h;

		m = d.getMinutes();
		if (m < 10)
		    m = '0' + m;

		return h + ':' + m;
	    }

	    // 艦隊/遠征情報
	    for ( let i = 0; i < l.length; i++ ){
		let fi = KanColleDatabase.deck.get(l[i]);
		let id = parseInt(fi.api_id, 10);
		let fleet_text = fi.api_name;
		let fleet_flagship_lv = 0;
		let fleet_lv = 0;
		let fleet_sally_area = {};
		let fleet_stypes = {};
		let fleet_slotitem_ship = {};
		let fleet_slotitem_num =  {};
		let fleet_ap = 0;
		let fleet_search = 0;
		let fleet_taisen = 0;
		let fleet_taiku = 0;
		let fleet_karyoku = 0;
		let fleet_warn = 0;
		let min_cond = 100;
		let mission_ok;
		let mission_ratio;

		if (fi.api_mission[1])
		    this._mission_plan[id] = fi.api_mission[1];
		[mission_ok, mission_ratio] = this.checkMission(id);

		for ( let j = 0; j < fi.api_ship.length; j++ ){
		    let ship_id = fi.api_ship[j];
		    let ship_name = FindShipName(ship_id);
		    let ship_info = FindShipStatus(ship_id);
		    let ship_cond = FindShipCond(ship_id);
		    let ship = KanColleDatabase.ship.get(ship_id);
		    let ship_escape = (this._escape[id] && this._escape[id][j+1]) || false;
		    let ship_bgcolor;
		    let ship_res_color;
		    let ship_hpratio;
		    let ship_hp_color;
		    let ship_damage_color;
		    let ship_damage_style;
		    let ship_shadow;
		    let ship_text = ship_name + (ship ? ' Lv' + ship.api_lv : '');
		    let shipslot = {};

		    if (ship) {
			let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
			fleet_lv += ship.api_lv;
			if (j == 0)
			    fleet_flagship_lv = ship.api_lv;
			if (shiptype) {
			    if (!fleet_stypes[shiptype.api_stype])
				fleet_stypes[shiptype.api_stype] = 1;
			    else
				fleet_stypes[shiptype.api_stype]++;
			}

			if (ship.api_sally_area !== undefined && !ship_escape)
			    fleet_sally_area[ship.api_sally_area] = (fleet_sally_area[ship.api_sally_area] || 0) + 1;
		    }

		    if (ship_cond === undefined) {
			ship_cond = '-';
			ship_bgcolor = null;
		    } else {
			ship_bgcolor = this._ship_cond_color(ship_cond);
		    }

		    if (ship_cond < 49) {
			let t = KanColleDatabase.ship.timestamp();
			ship_text += ' ' + timestr(t + Math.ceil((49 - ship_cond) / 3) * 180000);

			if (ship_cond < min_cond)
			    min_cond = ship_cond;
		    }

		    if (ship_info === undefined) {
			ship_res_color = 'rgba(0,0,0,0)';
			ship_hp_color = null;
			ship_damage_style = 'solid';
			ship_hpratio = 1;   //XXX
		    } else {
			let in_ndock = KanColleDatabase.ndock.find(ship_id);
			ship_hpratio = ship_info.nowhp / ship_info.maxhp;

			ship_text += '\nHP  : ' + ship_info.nowhp + '/' + ship_info.maxhp + (in_ndock ? ' (入渠中)' : '')
				  +  '\n士気: ' + ship_cond
				  +  '\n燃料: ' + ship_info.bull  + '/' + ship_info.bull_max
				  +  '\n弾薬: ' + ship_info.fuel  + '/' + ship_info.fuel_max
			;

			if (ship_info.bull_max > ship_info.bull ||
			    ship_info.fuel_max > ship_info.fuel) {
			    ship_res_color = 'red';
			} else {
			    ship_res_color = 'rgba(0,0,0,0)';
			}

			if (ship_hpratio >= 1) {
			    ship_shadow = '1px 1px 0 rgba(0,0,0,0)';
			} else {
			    ship_shadow = '1px 1px 0 black';
			}
			[ship_hp_color, ship_damage_color] = this._ship_hp_color(ship_hpratio);
			ship_damage_style = in_ndock ? 'dotted' : 'solid';
		    }

		    if (ship) {
			let ship_taisen = ship.api_taisen ? ship.api_taisen[0] : -1;
			let ship_taiku = ship.api_taiku ? ship.api_taiku[0] : -1;
			let ship_karyoku = ship.api_karyoku ? ship.api_karyoku[0] : -1;
			let ship_search = ShipCalcSearch(ship_id);

			if (ship_taisen > 0) {
			    fleet_taisen += ship_taisen;
			    ship_text += '\n対潜: ' + ship_taisen;
			}
			if (ship_taiku > 0)
			    fleet_taiku += ship_taiku;
			if (ship_karyoku > 0)
			    fleet_karyoku += ship_karyoku;

			if (ship_search >= 0) {
			    if (fleet_search >= 0)
				fleet_search += ship_search;
			} else
			    fleet_search = -1;
		    }

		    if (KanColleUtils.getBoolPref('display.extra-info') && ship) {
			let ship_ap = ShipCalcAirPower(ship_id);
			if (ship_ap >= 0) {
			    //ship_text += '\n制空: ' + ship_ap;
			    if (fleet_ap >= 0)
				fleet_ap += ship_ap;
			} else
			    fleet_ap = -1;
		    }

		    // 装備
		    if (ship) {
			let ship_slot = ship.api_slot.slice();
			let ship_slot_string = [];
			if (ship.api_slot_ex)
			    ship_slot.push(ship.api_slot_ex);
			for (let k = 0; k < ship_slot.length; k++) {
			    let itemid = ship_slot[k];
			    let item;
			    let itemtype;
			    let s = '';

			    if (itemid < 0)
				continue;
			    item = KanColleDatabase.slotitem.get(itemid);
			    itemtype = item ? KanColleDatabase.masterSlotitem.get(item.api_slotitem_id) : null;

			    if (itemtype) {
				s = itemtype.api_name;
				if (item.api_level || item.api_alv) {
				    s += '[';
				    if (item.api_level)
					s += '★' + item.api_level;
				    if (item.api_alv)
					s += '+' + item.api_alv;
				    s += ']';
				}
			    } else {
				s = '-';
			    }
			    ship_slot_string.push(s);

			    if (!itemtype)
				continue;

			    shipslot[itemtype.api_type[2]] = shipslot[itemtype.api_type[2]] ?  shipslot[itemtype.api_type[2]] + 1 : 1;
			}
			if (!this._slotitem[id])
			    this._slotitem[id] = {};
			this._slotitem[id][j+1] = shipslot;
			for (let k in shipslot) {
			    // 装備数
			    fleet_slotitem_num[k] = fleet_slotitem_num[k] ? fleet_slotitem_num[k] + shipslot[k] : shipslot[k];
			    // 所持艦船数
			    fleet_slotitem_ship[k] = fleet_slotitem_ship[k] ? fleet_slotitem_ship[k] + 1 : 1;
			}
			ship_text += '\n装備:' + ship_slot_string.join(', ');
		    }

		    // 大破警告
		    //	旗艦でない、大破、ダメコンなし、待避/入渠していない
		    if (j && ship_hpratio <= 0.25 &&
			shipslot && !shipslot[23] && !ship_escape &&
			!KanColleDatabase.ndock.find(ship_id)) {
			fleet_warn++;
		    }

		    $('shipstatus-' + id + '-' + (j + 1)).value = ship_cond;
		    $('shipstatus-' + id + '-' + (j + 1)).setAttribute('tooltiptext', ship_text);
		    $('shipstatus-' + id + '-' + (j + 1)).setAttribute('data-name', ship_name);
		    $('shipstatus-' + id + '-' + (j + 1)).setAttribute('data-lv', '' + (ship ? ship.api_lv : ''));
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'background-color', ship_bgcolor);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'outline-color', ship_res_color);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'color', ship_hp_color);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'border-left-color', ship_damage_color);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'border-left-style', ship_damage_style);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'text-shadow', ship_shadow);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'text-decoration', null);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), '-moz-text-decoration-style', null);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'text-decoration-style', null);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), '-moz-text-decoration-color', null);
		    SetStyleProperty($('shipstatus-' + id + '-' + (j + 1)), 'text-decoration-color', null);
		}

		if (fleet_flagship_lv > 0) {
		    let stypes;
		    let fleetinfo = [];
		    let timercmd = null;
		    let slotitem2show = [ 12, 13, 23, 24, 30 ];
		    let slotiteminfo = [];

		    let cur = (new Date).getTime();
		    let t = KanColleDatabase.ship.timestamp();
		    let time = t + Math.ceil((49 - min_cond) / 3) * 180000;
		    let str = timestr(time);

		    stypes = Object.keys(fleet_stypes).sort(function(a,b){
			return fleet_stypes[b] - fleet_stypes[a];
		    });

		    if (this._mission_plan[id]) {
			let mission = KanColleDatabase.masterMission.get(this._mission_plan[id]);
			fleet_text += '\n遠征: ' + (mission ? mission.api_name : this._mission_plan[id]) + ' ' +
				      (mission_ok !== undefined ? (mission_ok ? `OK [大成功: ${ (mission_ratio * 100).toFixed(0) }%]` : 'NG') : '?');
		    }

		    fleet_text += '\n編成: ';
		    if (id <= 2) {
			let hensei = [null /*通常*/, '空母機動部隊', '水上打撃部隊', '輸送護衛部隊'];
			let cd = KanColleDatabase.reqHenseiCombined.get();
			if (cd && hensei[cd.api_combined])
			    fleet_text += `${ hensei[cd.api_combined] }; `;
		    }
		    if (Object.keys(fleet_sally_area).length) {
			// 海域0しかないときは表示しない
			let areas = Object.keys(fleet_sally_area).sort(function(a,b) { return a - b; });
			if (areas.length > 1 || areas[0] > 0)
			    fleet_text += '海域' + areas.map(function(e) { return `${ e }(${ fleet_sally_area[e] })`;}).join(',') + '; ';

		    }
		    fleet_text += '旗艦Lv' + fleet_flagship_lv + '/' + fleet_lv;
		    for( let j = 0; j < stypes.length; j++ ){
			let stype = KanColleDatabase.masterStype.get(stypes[j]);
			let stypename = stype ? stype.api_name : null;
			if (!stypename)
			    stypename = 'UNKNOWN_' + stypes[j];
			fleetinfo.push(' ' + stypename + '(' + fleet_stypes[stypes[j]] + ')');
		    }
		    fleet_text += ';' + fleetinfo.join(',');

		    for ( let j = 0; j < slotitem2show.length; j++ ) {
			let k = slotitem2show[j];
			let eqtype = KanColleDatabase.masterSlotitemEquiptype.get(k);

			if (!fleet_slotitem_ship[k])
			    fleet_slotitem_ship[k] = 0;
			if (!fleet_slotitem_num[k])
			    fleet_slotitem_num[k] = 0;

			slotiteminfo.push(eqtype.api_name +
					  '(' + fleet_slotitem_num[k] +
					  '/' + fleet_slotitem_ship[k] +
					  ')');
		    }

		    fleet_text += '\n装備: ' + slotiteminfo.join(', ');

		    if (fleet_search >= 0) {
			let escaped = this._escape[id] || [];
			let fleet_search_4 = FleetCalcSearchV4(fi.api_ship, escaped, this._branchpoint_param);
			let fleet_search_4_rounded = fleet_search_4.toFixed(2);
			if (fleet_search_4 < fleet_search_4_rounded) {
			    fleet_search_4_rounded += '-';
			} else if (fleet_search_4 > fleet_search_4_rounded) {
			    fleet_search_4_rounded += '+';
			}
			fleet_text += `\n索敵: ${ fleet_search_4_rounded } (分岐点係数${ this._branchpoint_param })/${ fleet_search }`;
		    }
		    if (KanColleUtils.getBoolPref('display.extra-info') && fleet_ap >= 0)
			fleet_text += '\n制空: ' + fleet_ap;
		    if (KanColleUtils.getBoolPref('display.extra-info'))
			fleet_text += '\n対潜: ' + fleet_taisen;
		    if (KanColleUtils.getBoolPref('display.extra-info'))
			fleet_text += '\n対空: ' + fleet_taiku;
		    if (KanColleUtils.getBoolPref('display.extra-info'))
			fleet_text += '\n火力: ' + fleet_karyoku;
		}

		$('shipstatus-'+ id +'-0').setAttribute('tooltiptext', fleet_text);

		if (fleet_warn)
		    debugprint('#' + id + ': ' + fleet_warn);
		SetStyleProperty($('shipstatus-fleet-' + id), 'background-color', fleet_warn ? 'red' : null);
	    }
	},
	reqHenseiCombined: 'deck',
	ship: 'deck',
	slotitem: 'deck',

	ndock: function() {
	    let l = KanColleDatabase.ndock.list();
	    for (let i = 0; i < l.length; i++) {
		let d = KanColleDatabase.ndock.get(l[i]);
		if (d.api_state > 0) {
		    let fleet = KanColleDatabase.deck.lookup(d.api_ship_id);
		    if (fleet) {
			SetStyleProperty($('shipstatus-' + fleet.fleet + '-' + (fleet.pos + 1)), 'border-left-style', 'dotted');

			// 大破警告
			//  shipで既に警告済のものを入渠によって除外
			if (fleet.pos) {
			    let warn = 0;
			    try {
				let deck = KanColleDatabase.deck.get(fleet.fleet);
				// 旗艦は除く
				for (let j = 1; j < deck.api_ship.length; j++) {
				    let status, ratio;

				    if (j == fleet.pos ||
					KanColleDatabase.ndock.find(deck.api_ship[j]))
					continue;

				    status = FindShipStatus(deck.api_ship[j]);
				    ratio = status.nowhp / status.maxhp;

				    if (ratio <= 0.25) {
					let shipslot = (this._slotitem[fleet.fleet] && this._slotitem[fleet.fleet][j+1]) || {};
					// 待避中ということはない
					if (!shipslot[23])
					    warn++;
				    }
				}
			    } catch(e) {
			    }
			    if (warn)
				debugprint('#' + fleet.fleet + ': ' + warn);
			    SetStyleProperty($('shipstatus-fleet-' + fleet.fleet), 'background-color', warn ? 'red' : null);
			}
		    }
		}
	    }
	},

	battle: function(data) {
	    let list = KanColleDatabase.battle.list();
	    list.forEach(function(e) {
		let d = KanColleDatabase.battle.get(e) || {};
		debugprint('battle(' + data + '), ' + e + ': ' + d.toSource());
	    });
	    switch(data) {
	    case -3:	// reset
		this._show_enemy_ships(false);
		this._clear_enemy_ships();
		this._set_escape_ships();
		break;
	    case -2:	// enemy information (partial)
		break;
	    case -1:	// enemy information
		this._set_enemy_ships();
		this._show_enemy_ships(true);
		break;
	    case 0:	// before battle
		this._show_battleresult_menu(false);
		this._set_escape_ships();
		this._discloseBattleResult(true);
		break;
	    case 1:	// after battle
		this._show_battleresult_menu(true);
		break;
	    case 2:	// battle finished
		break;
	    case 3:	// final
		this._show_battleresult_menu(false);
		this._discloseBattleResult(KanColleDatabase.battle.get('e1').practice);
		break;
	    default:
	    }
	},

	map: function(extradata) {
	    let [area_name, path, no] = extradata || [null, null, null];
	    debugprint(`(area_name,path,no) = ${area_name}, ${path}, ${no}`);
	    if (!area_name || (no !== null && no !== undefined))
		return;

	    this._set_guessed_enemy_ships(area_name, path);
	    this._select_guessed_enemy(null);
	    this._show_enemy_ships(true);
	},

	masterStype: function() {
	    this._updateMissionMenu();
	},

	masterSlotitemEquiptype: function() {
	    this._updateMissionMenu();
	},
	masterMission: function() {
	    this._updateMissionMenu();
	},
	masterMaparea: function() {
	    this._updateMissionMenu();
	},
    },

    __createMapMenu: function(next) {
	//
	// #1-2-3 [99%]
	//  +--------------+
	//  |3:敵艦隊1[99%]|>+-----------------------+
	//  |4:敵艦隊2[1%] | |潜水A級,...+...   [50%]|
	//  +--------------+ |潜水B級,...+...   [50%]|
	//                   +-----------------------+
	//
	// [
	//   {
	//     "cell": no,
	//     "count": num,
	//     "ratio": ratio,
	//     "deck_names": [
	//       {
	//         "deck_name": deck_name,
	//         "count": deck_name_count,
	//         "cell_deck_name_ratio": deck_name_ratio,
	//         "decks": [
	//           {
	//             "ships": {
	//               "e1":[...],
	//               "e2":[...],
	//             },
	//             "ship_data": { ... },
	//             "decks_count": decks_count,
	//             "cell_decks_ratio": decks_ratio,
	//           }
	//         ],
	//       }, ...
	//     ],
	//   },...
	// ]
	//[{"cell":"1","count":86,"ratio":1,"deck_names":[{"deck_name":"敵偵察艦","count":86,"first":1596341578058,"last":1608944880184,"decks":[{"ship_data":{"1502":{"api_name":"駆逐ロ級","api_yomi":"-"}},"ships":{"e1":[1502],"e2":[]},"cell_decks_ratio":0.313953488372093},{"ship_data":{"1503":{"api_name":"駆逐ハ級","api_yomi":"-"}},"ships":{"e1":[1503],"e2":[]},"cell_decks_ratio":0.29069767441860467},{"ship_data":{"1501":{"api_name":"駆逐イ級","api_yomi":"-"}},"ships":{"e1":[1501],"e2":[]},"cell_decks_ratio":0.3953488372093023}],"cell_deck_name_ratio":1}]}]

	function createItem(d, v, s, nest) {
	    debugprint(`createItem(${ JSON.stringify(d) }, ${ JSON.stringify(v) }, ${ s }, ${ nest })`);
	    let item = document.createElementNS(XUL_NS, 'menuitem');
	    item.setAttribute('label', s || 'unknown');
	    item.setAttribute('disabled', !d);
	    item.setAttribute('value', JSON.stringify(v || null));
	    item.setAttribute('tooltip', JSON.stringify(v || null));
	    debugprint(`createitem: ${ JSON.stringify(v || null) }`);
	    return [[item], v];
	}

	function createMenu(d, v, s, f, nest) {
	    debugprint(`createMenu(${ JSON.stringify(d) }, ${ JSON.stringify(v) }, ${ s }, f, ${ nest })`);

	    if (!d || !f)
		return createItem(null, null, null, nest + 1);

	    //if (d.length == 1)
	    //	return f.submenu(f.sublist(d[0]), f.value(v,d[0],0), (s ? s + ':' : '') + f.title(d[0]));

	    let default_value = null;
	    let items = [];
	    d.forEach(function(e) {
		let title = f.title(e);
		let m0, m1, def;

		if (f.sublist && f.submenu) {
		    let mp0 = document.createElementNS(XUL_NS, 'menupopup');
		    [m1, def] = f.submenu(f.sublist(e), f.value(v,e), title, nest + 1);
		    m1.forEach(function(e) {
			mp0.appendChild(e);
		    });
		    let m0 = document.createElementNS(XUL_NS, 'menu');
		    m0.setAttribute('label', title || 'unknown');
		    m0.appendChild(mp0);
		    items.push(m0);
		} else {
		    if (f.subitem)
			[m1, def] = f.subitem(f.value(v,e), title, nest + 1);
		    else
			[m1, def] = createItem(null, null, null, nest + 1);

		    m1.forEach(function(e) {
			items.push(e);
		    });
		}

		if (!default_value)
		    default_value = def;
	    });

	    return [items, default_value];
	}

	function createDeckItem(v, s, nest) {
	    debugprint(`createDeckItem(${ JSON.stringify(v) }, ${ JSON.stringify(s) }, ${ nest })`);
	    return createItem(v, v, s, nest + 1);
	}

	function createDeckMenu(d, v, s, nest) {
	    debugprint(`createDeckMenu(${ JSON.stringify(d) }, ${ JSON.stringify(v) }, ${ s }, ${ nest })`);
	    return createMenu(d, v, s,
			      {
				title: function(e) {
				    let ships = [];
				    for (let en of ['e1', 'e2']) {
					if (!e.ships[en] || !e.ships[en].length)
					    continue;
					let s = e.ships[en].map(function(shipid) {
					    let ship = e.ship_data[shipid];
					    return `${ ship.api_name }[${ ship.api_yomi }]`;
					}).join(',');
					ships.push(`${ en }: ${ s }`);
				    }
				    return `${ ships.join(' , ') }[${ (e['cell_decks_ratio'] * 100).toFixed(0) }%]`;
				},
				subitem: createDeckItem,
				value: function(v,e) {
				    v['decks'] = e.deck_name;
				    v['cell_decks_ratio'] = e.cell_decks_ratio;
				    return v;
				},
			      }, nest + 1);
	}

	function createDecknameMenu(d, v, s, nest) {
	    debugprint(`createDecknameMenu(${ JSON.stringify(d) }, ${ JSON.stringify(v) }, ${ s }, ${ nest })`);
	    return createMenu(d, v, s,
			      {
				title: function(e) { return `${ e.deck_name }[${ (e.cell_deck_name_ratio * 100).toFixed(0) }%]`; },
			        submenu: createDeckMenu,
			        sublist: function(e) { return e.decks || []; },
			        value: function(v, e) { v['deck_name'] = e.deck_name; v['cell_deck_name_ratio'] = e.cell_deck_name_ratio; return v; },
			      }, nest + 1
	    );
	}

	function createCellMenu(d, v, s, nest) {
	    debugprint(`createCellMenu(${ JSON.stringify(d) }, ${ JSON.stringify(v) }, ${ s }, ${ nest })`);
	    return createMenu(d, v, s,
			      {
				title: function(e) { return `#${ e.cell }[${ (e.ratio * 100).toFixed(0) }%]`; },
				submenu: createDecknameMenu,
				sublist: function(e) { return e.deck_names || []; },
				value: function(v, e) { v['cell'] = e.cell; v['ratio'] = e.ratio; return v; },
			      }, nest + 1
	    );
	}

	if (!next)
	    return;

	return createCellMenu(next, {}, null, 0);
    },

    _updateMapMenu: function(d) {
	debugprint(`_updateMapMenu(${ JSON.stringify(d) })`);
	let m0 = $('shipstatus-context-map');
	let mp0 = $('shipstatus-context-map-menupopup');
	if (!m0 || !mp0)
	    return;

	m0.setAttribute('disabled', 'true');
	RemoveChildren(mp0);
	let [m, def] = this.__createMapMenu(d);
	if (def)
	    m0.setAttribute('collapsed', 'false');
	if (m && m.length) {
	    m.forEach(function(e) {
		mp0.appendChild(e);
	    });
	    m0.setAttribute('disabled', 'false');
	}
    },

    _updateMissionMenu: function() {
	let list;
	let areamenu = $('shipstatus-context-check-maparea');
	let matmenu = $('shipstatus-context-check-material');
	let materials = [ '燃料', '弾薬', '鋼材', 'ボーキサイト' ];
	let popup;

	if (!KanColleDatabase.masterMission.timestamp() ||
	    !KanColleDatabase.masterMaparea.timestamp() ||
	    !KanColleDatabase.masterStype.timestamp() ||
	    !KanColleDatabase.masterSlotitemEquiptype.timestamp()) {
	    $('shipstatus-context-check').setAttribute('disabled', true);
	    return;
	}

	function createMenuItem(missionid) {
	    let d = KanColleDatabase.masterMission.get(missionid);
	    let item = document.createElementNS(XUL_NS, 'menuitem');
	    let param = KanColleData.mission_param[missionid] || {};
	    let tooltip;
	    item.setAttribute('value', '' + d.api_id);
	    item.setAttribute('label', '' + d.api_name);
	    if (param.balance) {
		let desc;
		let t = '?';
		if (d.api_time) {
		    t = d.api_time % 60;
		    if (t < 10)
			t = '0' + t;
		    t = Math.floor(d.api_time / 60) + ':' + t + ' ';
		}
		desc = '[' + param.balance.join(',') + ']';
		item.setAttribute('description', t + desc);
	    }
	    tooltip = KanColleTimerMissionBalanceInfo ? KanColleTimerMissionBalanceInfo.getDescription(missionid) : d.api_details;
	    item.setAttribute('tooltiptext', tooltip);
	    item.addEventListener('command', function() { KanColleTimerFleetInfo._missionMenuCommand(this.value); }, true);
	    return item;
	}

	list = KanColleDatabase.masterMission.list();
	RemoveChildren(areamenu);
	popup = document.createElementNS(XUL_NS, 'menupopup');
	areamenu.appendChild(popup);
	for (let i = 0; i < list.length; i++) {
	    let d = KanColleDatabase.masterMission.get(list[i]);
	    let submenu = $('shipstatus-context-check-maparea-' + d.api_maparea_id);
	    let item = createMenuItem(list[i]);
	    if (!submenu) {
		let area = KanColleDatabase.masterMaparea.get(d.api_maparea_id);
		let e = document.createElementNS(XUL_NS, 'menu');
		e.setAttribute('label', area.api_name);
		submenu = document.createElementNS(XUL_NS, 'menupopup');
		submenu.setAttribute('id', 'shipstatus-context-check-maparea-' + d.api_maparea_id);
		e.appendChild(submenu);
		popup.appendChild(e);
	    }
	    submenu.appendChild(item);
	}

	// sorted by balance
	RemoveChildren(matmenu);
	popup = document.createElementNS(XUL_NS, 'menupopup');
	matmenu.appendChild(popup);
	for (let i = 0; i < materials.length; i++) {
	    let matmenu = $('group-mission-fleet-context-material');
	    let submenu;
	    let e = document.createElementNS(XUL_NS, 'menu');
	    e.setAttribute('label', materials[i]);
	    submenu = document.createElementNS(XUL_NS, 'menupopup');
	    submenu.setAttribute('id', 'group-mission-fleet-context-material-' + i);
	    e.appendChild(submenu);
	    popup.appendChild(e);

	    list = JSON.parse(JSON.stringify(list));
	    list.sort(function(a,b) {
			let ma = (KanColleData.mission_param[a] || {}).balance;
			let mb = (KanColleData.mission_param[b] || {}).balance;
			let ret = !ma - !mb;
			if (!ret && ma && mb)
			    ret = mb[i] - ma[i];
			return ret;
		      });
	    for (let j = 0; j < list.length; j++)
		submenu.appendChild(createMenuItem(list[j]));
	}

	$('shipstatus-context-check').setAttribute('disabled', false);
    },

    _checkMission: function(deckid) {
	let flagship_lv = -1;
	let flagship_stype = -1;
	let flagship_ship = -1;
	let res;
	let param;
	let pass_all = false;
	let ratio;
	let d = KanColleDatabase.deck.get(deckid);
	let mission_id = d.api_mission[1] || this._mission_plan[deckid];

	if (!d || !mission_id ||
	    !KanColleDatabase.ship.timestamp() || !KanColleDatabase.masterShip.timestamp() ||
	    !KanColleDatabase.slotitem.timestamp() || !KanColleDatabase.masterSlotitem.timestamp())
	    return [undefined,undefined];
	res = d.api_ship.map(function(val, idx, array) {
				let ship = val >= 0 ? KanColleDatabase.ship.get(val) : null;
				let res = {};
				if (ship) {
				    let shiptype = KanColleDatabase.masterShip.get(ship.api_ship_id);
				    let ship_slot = ship.api_slot.slice();
				    if (ship.api_slot_ex)
					ship_slot.push(ship.api_slot_ex);
				    res.num = 1;
				    res.lv = ship.api_lv;
				    res.cond = ship.api_cond;
				    res.taisen = ship.api_taisen ? ship.api_taisen[0] : 0;
				    res.taiku = ship.api_taiku ? ship.api_taiku[0] : 0;
				    res.karyoku = ship.api_karyoku ? ship.api_karyoku[0] : 0;
				    res.sakuteki = ship.api_sakuteki ? ship.api_sakuteki[0] : 0;
				    if (idx == 0) {
					flagship_lv = ship.api_lv;
					flagship_stype = shiptype.api_stype;
					flagship_ship = ship.api_ship_id;
				    }
				    res.stype = shiptype.api_stype;
				    res.ship = ship.api_ship_id;
				    res.slotitem = ship_slot.map(function(v, i, a) {
									let item = KanColleDatabase.slotitem.get(v);
									let itemtype = item ? KanColleDatabase.masterSlotitem.get(item.api_slotitem_id) : null;
									return itemtype ? itemtype.api_type[2] : -1;
								     });
				} else {
				    res.num = 0;
				    res.lv = 0;
				    res.cond = 0;
				    res.taisen = 0;
				    res.taiku = 0;
				    res.karyoku = 0;
				    res.sakuteki = 0;
				    res.stype = -1;
				    res.ship = -1;
				    res.slotitem = [];
				}
				return res;
			     }).reduce(function(prev, cur) {
					let items;
					prev.num += cur.num;
					prev.lv += cur.lv;
					if (cur.num > 0)
					    prev.cond.push(cur.cond);
					prev.taisen += cur.taisen;
					prev.taiku += cur.taiku;
					prev.karyoku += cur.karyoku;
					prev.sakuteki += cur.sakuteki;
					if (cur.stype >= 0)
					    prev.stype[cur.stype] = (prev.stype[cur.stype] ? prev.stype[cur.stype] : 0) + 1;
					if (cur.ship >= 0)
					    prev.ship[cur.ship] = (prev.ship[cur.ship] ? prev.ship[cur.ship] : 0) + 1;
					items = cur.slotitem.reduce(function(p,c) {
									if (c >= 0)
									    p[c] = (p[c] ? p[c] : 0) + 1;
									return p;
								    }, {});
					Object.keys(items).forEach(function(v,i,a){
					    if (!prev.slotitem[v])
						prev.slotitem[v] = { ship: 0, num: 0, };
					    prev.slotitem[v].ship++;
					    prev.slotitem[v].num += items[v];
					});
					return prev;
				       }, { num: 0, stype: {}, ship: {}, lv: 0, slotitem: {}, taisen: 0, taiku: 0, karyoku: 0, sakuteki: 0, cond: [] });
	param = KanColleData.mission_param[mission_id];
	if (!param)
	    return [undefined,undefined];

	function check_mission(p) {
	    let pass = true;
	    let ratio;

	    debugprint('check_mission(): ' + JSON.stringify(p));

	    if (p.lv) {
		if (p.lv.flagship)
		    pass &= p.lv.flagship <= flagship_lv;
		if (p.lv.total)
		    pass &= p.lv.total <= res.lv;
	    }
	    if (p.num)
		pass &= p.num <= res.num;
	    if (p.taisen)
		pass &= p.taisen <= res.taisen;
	    if (p.taiku)
		pass &= p.taiku <= res.taiku;
	    if (p.karyoku)
		pass &= p.karyoku <= res.karyoku;
	    if (p.sakuteki)
		pass &= p.sakuteki <= res.sakuteki;
	    if (p.stype)
		pass &= p.stype.every(function(val,idx,array){
					let num = val.mask.map(function(v,i,a) {
								return v >= 0 ? (res.stype[v] ? res.stype[v] : 0) : (res.ship[-v] ? res.ship[-v] : 0);
							       }).reduce(function(p,c) {
								return p + c;
							       });
					return val.num <= num &&
						(!val.flagship || val.mask.some(function(v,i,a) { return v >= 0 ? (v == flagship_stype) : (-v == flagship_ship); }));
				      });
	    if (p.slotitem)
		pass &= Object.keys(p.slotitem).every(function(val,idx,array){
							return res.slotitem[val] &&
								(!p.slotitem[val].ship || (res.slotitem[val].ship && p.slotitem[val].ship <= res.slotitem[val].ship)) &&
								(!p.slotitem[val].num || (res.slotitem[val].num && p.slotitem[val].num <= res.slotitem[val].num));
						      });

	    // 大成功判定
	    if (pass) {
		let num_hi_cond = res.cond.filter(function(e) { return e > 49; }).length;

		ratio = 20 + num_hi_cond * 15;

		if (p.slotitem) {
		    // ドラム缶型
		    //	補正:
		    //	    必要数2 = 0 →  0
		    //	    必要数2 > 0 かつ 必要数2 > 艦隊のドラム缶数 → -15
		    //	    必要数2 > 0 かつ 必要数2 < 艦隊のドラム缶数 → +20
		    ratio += Object.keys(p.slotitem).reduce(function(prev,cur) {
								if (!p.slotitem[cur] || p.slotitem[cur].num2 == 0)
								    return prev;
								if (p.slotitem[cur].num2 <= res.slotitem[cur].num)
								    return prev + 20;
								else
								    return prev - 15;
							    }, 0);
		} else if (p.taisen !== undefined || p.taiku !== undefined || p.sakuteki !== undefined || p.karyoku !== undefined) {
		    // 旗艦Lv型
		    ratio += -5 + Math.floor(Math.sqrt(flagship_lv) + flagship_lv / 10);
		} else {
		    // 通常型
		    if (num_hi_cond < res.num)
			ratio = 0;
		}
		if (ratio < 0)
		    ratio = 0;
		ratio /= 100;
	    }

	    return pass ? ratio : -1;
	}

	ratio = check_mission(param)

	if (param.alt) {
	    for (let i = 0; i < param.alt.length && ratio < 0; i++) {
		let e = param.alt[i];
		let p = {};

		for (let i of [ 'lv', 'num', 'taisen', 'taiku', 'sakuteki', 'karyoku','stype', 'slotitem' ] ) {
		    if (e[i] !== undefined)
			p[i] = e[i];
		    else {
			// p[i] = typeof(param[i]) == 'object') ? JSON.parse(JSON.stringify(param[i])) : param[i];
			p[i] = param[i];
		    }
		}
		ratio = check_mission(p);
	    };
	}

	return [ratio >= 0 ? 1 : 0, ratio]
    },

    _deckAvailable: function(deckid) {
	let basic = KanColleDatabase.memberBasic.get();
	let deck = KanColleDatabase.deck.get(deckid);
	return (basic && deck &&
	        deckid <= basic.api_count_deck &&
		!deck.api_mission[1]);
    },

    setMission: function(deckid, missionid) {
	if (!this._deckAvailable(deckid) || !missionid)
	    return;
	this._mission_plan[deckid] = missionid;
	this.update.deck.call(this);
    },

    checkMission: function(deckid) {
	let [res,ratio] = this._checkMission(deckid);
	SetStyleProperty($('shipstatus-' + deckid + '-0'), 'outline', (res === 0) ? 'solid red 2px' : '');
	return [res,ratio];
    },

    _changeBranchPointParam: function(node, value) {
	let v = parseInt(value, 10);
	if (isNaN(v))
	    return;
	this._branchpoint_param = v;
	this.update.deck.call(this);
    },

    _mapMenuShowing: function(deckid) {
	$('shipstatus-context-map').setAttribute('disabled', 'false');
    },

    _fleetMenuShowing: function(deckid) {
	let min_cond = 101;
	let deck = KanColleDatabase.deck.get(deckid);
	if (deck) {
	    for (let i = 0; i < deck.api_ship.length; i++) {
		if (!this._ship || this._ship - 1 == i) {
		    let cond = FindShipCond(deck.api_ship[i]);
		    if (isNaN(cond))
			continue;
		    if (cond < min_cond)
			min_cond = cond;
		}
	    }
	}
	if (min_cond < 49) {
	    let t = KanColleDatabase.ship.timestamp();
	    let time_offset = KanColleUtils.getIntPref('time-offset', 0);
	    let time = t - ((t - time_offset) % 180000) + Math.ceil((49 - min_cond) / 3) * 180000;
	    let d = new Date;
	    d.setTime(time);
	    $('shipstatus-context-settimer').label = 'タイマー設定(' + d.toLocaleTimeString() + ')';
	    $('shipstatus-context-settimer').setAttribute('disabled', 'false');
	    $('shipstatus-context-settimer').addEventListener('command', function() { KanColleTimer.setGeneralTimerByTime(time); }, true);
	} else {
	    $('shipstatus-context-settimer').label = 'タイマー設定';
	    $('shipstatus-context-settimer').setAttribute('disabled', 'true');
	}

	if (deckid != 1 && !this._ship) {
	    let list = [ 'maparea', 'material' ];
	    let isdisabled = !this._deckAvailable(deckid);
	    let mission;
	    let id;

	    for (let i = 0; i < list.length; i++) {
		let n = $('shipstatus-context-check-' + list[i]);
		n.setAttribute('disabled', isdisabled ? 'true' : 'false');
	    }
	    $('shipstatus-context-check').collapsed = false;
	} else
	    $('shipstatus-context-check').collapsed = true;
    },

    _menuShowing: function(e) {
	if (this._deckid) {
	    let deckid = parseInt(this._deckid, 10);

	    if (!isNaN(deckid)) {
		$('shipstatus-context-settimer').hidden = false;
		$('shipstatus-context-branchpoint-param').collapsed = false;
		$('shipstatus-context-map').collapsed = true;
		return this._fleetMenuShowing(deckid);
	    } else {
		$('shipstatus-context-settimer').hidden = true;
		$('shipstatus-context-branchpoint-param').collapsed = true;
		$('shipstatus-context-check').collapsed = true;
		$('shipstatus-context-map').collapsed = false;
		return this._mapMenuShowing(this._deckid);
	    }
	}
	return e.preventDefault();
    },

    _missionMenuCommand: function(v) {
	let missionid = parseInt(v, 10);
	this.setMission(this._deckid, missionid);
    },

    handleEvent: function(e) {
	let curTarget = e.currentTarget;
	//debugprint("e.type=" + e.type + "; e.target.id=" + e.target.id + "; e.currentTarget.id=" + curTarget.id);
	switch (e.type) {
	case 'contextmenu':
	    this._deckid = e.target.getAttribute('data-deckid');
	    this._ship = parseInt(e.target.getAttribute('data-ship'), 10);
	    break;
	default:;
	}
    },

    show: function() {
	let checked = $('fleetinfo-toggle').getAttribute('checked') == 'true';
	$('fleetinfo').collapsed = !checked;
    },

    restore: function() {
	this.show();
    },

    init: function() {
	debugprint("init");
	KanColleDatabase.init();
	this._update_init();
	$('shipstatus-grid').addEventListener('contextmenu', this, true);
    },

    exit: function() {
	$('shipstatus-grid').removeEventListener('contextmenu', this);
	this._update_exit();
	KanColleDatabase.exit();
    },
};
KanColleTimerFleetInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerFleetInfo.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerFleetInfo.unload(); }, false);
