// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("chrome://kancolletimer/content/e10s-main.jsm");
Components.utils.import("chrome://kancolletimer/content/utils.jsm");
Components.utils.import("resource://gre/modules/Promise.jsm");

var KanColleTimerOverlay = {
    debugprint:function(txt){
	//Application.console.log(txt);
    },

    /**
     * @return スクリーンショットのdataスキーマのnsIURIを返す。艦これのタブがなければnullを返す
     */
    __takeScreenshotP: function(isjpeg) {
	let msg;
	let format = isjpeg ? [ 'image/jpeg' ] : [ 'image/png' ];
	let tab = KanColleTimerUtils.window.findTab(KanColleUtils.URL);
	if (!tab) {
	    AlertPrompt("艦隊これくしょんのページが見つかりませんでした。","艦これタイマー");
	    return Promise.reject(new Error('Failed to find tab.'));
	}
	return new Promise(function(resolve, reject) {
	    KanColleTimerE10S.request(tab.linkedBrowser.messageManager,
				      'screenshot',
				      {
					format: format,
					privacy: KanColleUtils.getBoolPref("screenshot.mask-name"),
				      },
				      function(header, msg) {
					if (msg.error)
					    reject(new Error(msg.error));
					else {
					    resolve(Components.classes['@mozilla.org/network/io-service;1']
						    .getService(Components.interfaces.nsIIOService)
						    .newURI(msg.data.url, null, null));
					}
				      }
	    );
	});
    },

    /**
     * スクリーンショット撮影
     * @param path 保存先のパス(指定なしだとファイル保存ダイアログを出す)
     */
    takeScreenshot: function( path ){
	let that = this;
	let isjpeg = KanColleUtils.getBoolPref('screenshot.jpeg');

	this.__takeScreenshotP(isjpeg)
	    .then(function(url) {
		let file = null;
		if( !path ){
		    var fp = Components.classes['@mozilla.org/filepicker;1']
			.createInstance(Components.interfaces.nsIFilePicker);
		    fp.init(window, "艦これスクリーンショットの保存", fp.modeSave);
		    fp.appendFilters(fp.filterImages);
		    fp.defaultExtension = isjpeg?"jpg":"png";
		    fp.displayDirectory = KanColleUtils.getUnicharPrefFile('screenshot.path');

		    var datestr = that.getNowDateString();
		    fp.defaultString = "screenshot-"+ datestr + (isjpeg?".jpg":".png");
		    if ( fp.show() != fp.returnCancel )
			file = fp.file;
		}else{
		    file = KanColleTimerUtils.file.initWithPath(path);
		    var datestr = that.getNowDateString();
		    var filename = "screenshot-"+ datestr + (isjpeg?".jpg":".png");
		    file.append(filename);
		}
		if (!file)
		    return Promise.reject(new Error('File not selected.'));
		return new Promise(function(resolve, reject) {
		    KanColleTimerUtils.file.saveURI(file, url, function() {
			resolve();
		    });
		});
	    }).catch(function(error) {
		that.debugprint(error);
	    });

	return true;
    },

    takeScreenshotSeriography:function(){
	var path = KanColleUtils.getUnicharPref('screenshot.path');
	this.takeScreenshot(path);
    },

    toggleSidebar: function() {
	KanColleUtils.toggleSidebar(window);
    },

    openTweetDialog: function(){
	var f='chrome,toolbar,modal=no,resizable=no,centerscreen';
	var w = window.openDialog('chrome://kancolletimer/content/sstweet.xul','KanColleTimerTweet',f);
	w.focus();
    },

    openResourceGraph: function(){
	if(KanColleUtils.getBoolPref('tab-open.resourcegraph'))
	    KanColleTimerUtils.window.openTab('chrome://kancolletimer/content/resourcegraph.xul', true)
	else
	    window.open('chrome://kancolletimer/content/resourcegraph.xul','KanColleTimerResourceGraph','chrome,resizable=yes').focus();
    },

    openPowerup: function(){
	window.open('chrome://kancolletimer/content/powerup.xul','KanColleTimerPowerUp','chrome,resizable=yes').focus();
    },

    getNowDateString: function(){
	var d = new Date();
	var month = d.getMonth()+1;
	month = month<10 ? "0"+month : month;
	var date = d.getDate()<10 ? "0"+d.getDate() : d.getDate();
	var hour = d.getHours()<10 ? "0"+d.getHours() : d.getHours();
	var min = d.getMinutes()<10 ? "0"+d.getMinutes() : d.getMinutes();
	var sec = d.getSeconds()<10 ? "0"+d.getSeconds() : d.getSeconds();
	var ms = d.getMilliseconds();
	if( ms<10 ){
	    ms = "000" + ms;
	}else if( ms<100 ){
	    ms = "00" + ms;
	}else if( ms<1000 ){
	    ms = "0" + ms;
	}
	return "" + d.getFullYear() + month + date + hour + min + sec + ms;
    },

    openKanCollePage: function(){
	var url = "http://www.dmm.com/netgame/social/-/gadgets/=/app_id=854854/";
	var tab = KanColleTimerUtils.window.findTab(url);
	if( !tab ){
	    tab = getBrowser().addTab( url );
	}
	getBrowser().selectedTab = tab;
    },

    openSettingsDialog: function(){
	var f='chrome,toolbar,modal=no,resizable=yes,centerscreen';
	var w = window.openDialog('chrome://kancolletimer/content/preferences.xul','KanColleTimerPreference',f);
	w.focus();
    },

    _toolbar_actions: {
	0: {
	    menuid: 'kt-open-sidebar',
	    funcname: 'toggleSidebar',
	},
	1: {
	    menuid: 'kt-take-screenshot',
	    funcname: 'takeScreenshot',
	},
	2: {
	    menuid: 'kt-take-screenshot-seriography',
	    funcname: 'takeScreenshotSeriography',
	},
    },

    onClickToolbar: function(){
	var action = KanColleUtils.getIntPref('default-action.toolbar');
	KanColleTimerOverlay[this._toolbar_actions[action].funcname]();
    },

    setDefaultAction:function(node){
	let val = parseInt(node.getAttribute('value'), 10);
	KanColleUtils.setIntPref('default-action.toolbar', val);
    },

    onDefaultActionMenuShowing: function(){
	var action = KanColleUtils.getIntPref('default-action.toolbar');
	document.getElementById(this._toolbar_actions[action].menuid).setAttribute('checked', 'true');
	return true;
    },

    openShipList: function(){
	let feature="chrome,resizable=yes";
	let w = window.open("chrome://kancolletimer/content/shiplist.xul","KanColleTimerShipList",feature);
	w.focus();
    },

    /**
     * 艦これタイマーを開く
     */
    open:function(){
	let feature="chrome,resizable=yes";

	let win = KanColleUtils.findMainWindow();
	if(win){
	    win.focus();
	}else{
	    let w = window.open("chrome://kancolletimer/content/mainwindow.xul","KanColleTimer",feature);
	    w.focus();
	}
    }
};
