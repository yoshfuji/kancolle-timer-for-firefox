// vim: set ts=8 sw=4 sts=4 ff=unix :

var KanColleDeckExporter = {
    _data: null,

    update: {
	deck: function() {
	    this.show();
	},
	record: 'deck',
	slotitem: 'deck',
	ship: 'deck',
    },

    _check_param: function() {
	let decks = KanColleDatabase.deck.list();
	let checked = { decks: [] };
	for (let i of decks) {
	    let node = $(`deck-${ i }`);
	    if (node.checked)
		checked.decks.push(i);
	}
	debugprint(JSON.stringify(checked));
	return checked;
    },

    _item_obj: function(itemid) {
	// id, rf, mas (string)

	let slotitem = KanColleDatabase.slotitem.get(itemid);
	if (!slotitem)
	    return;

	let d = KanColleDatabase.masterSlotitem.get(slotitem.api_slotitem_id);
	if (!d)
	    return;

	let obj = {};

	obj.id = slotitem.api_slotitem_id;
	//obj._name = d.api_name;
	obj.rf = slotitem.api_level;
	if (slotitem.api_alv)
	    obj.mas = "" + slotitem.api_alv;   //string

	return obj;
    },

    _ship_obj: function(id) {
	let that = this;

	// id, level, luck (num), items
	let ship = KanColleDatabase.ship.get(id);
	if (!ship)
	    return;

	let d = KanColleDatabase.masterShip.get(ship.api_ship_id);
	if (!d)
	    return;

	let obj = {};

	obj.id = "" + ship.api_ship_id;	    //string
	//obj._name = d.api_name;
	obj.lv = ship.api_lv;
	obj.luck = ship.api_lucky[0];
	obj.items = {};
	ship.api_slot.forEach(function(e,i) {
	    if (i >= ship.api_slotnum)
		return;

	    let item = e > 0 ? that._item_obj(e) : {};
	    if (!item)
		return;

	    obj.items[`i${ i + 1 }`] = item;
	});
	if (ship.api_slot_ex) {
	    let item = that._item_obj(ship.api_slot_ex);
	    if (item)
		obj.items['ix'] = item;
	}
	return obj;
    },

    _deck_obj: function(deck_id) {
	let that = this;

	let deck = KanColleDatabase.deck.get(deck_id);
	if (!deck)
	    return;

	let obj = {};
	deck.api_ship.forEach(function(e,i) {
	    let s_obj = that._ship_obj(e);
	    if (!s_obj)
		return;
	    obj[`s${ i + 1 }`] = s_obj;
	});
	return obj;
    },

    _deckbuilder_obj: function() {
	let checked = this._check_param();
	let deck_obj = { version: 4 };

	let lv = (KanColleDatabase.record.get() || {}).api_level;
	if (!isNaN(lv))
	    deck_obj.hqlv = lv;

	for (let deck_id of checked.decks) {
	    let f_obj = this._deck_obj(deck_id);
	    if (!f_obj)
		continue;
	    deck_obj[`f${ deck_id }`] = f_obj;
	}

	debugprint(JSON.stringify(deck_obj));
	return deck_obj;
    },

    to_file: function() {
	let data = this._data;
	if (!data)
	    return;
	SaveObjectToFile(data,`Deck Builder v${ data.version }`);
    },

    to_clipboard: function() {
	let data = this._data;
	if (!data)
	    return;
	CopyToClipboard(JSON.stringify(data));
    },

    open_deckbuilder: function() {
	let data = this._data;
	let baseURI = 'http://kancolle-calc.net/deckbuilder.html?predeck=';
	if (!data)
	    return;
	KanColleTimerUtils.window.openTab(baseURI + encodeURIComponent(JSON.stringify(data)), false);
    },

    show: function() {
	let decks = KanColleDatabase.deck.list();
	for (let i of decks) {
	    let deck = KanColleDatabase.deck.get(i);
	    let node = $(`deck-${ i }`);
	    node.setAttribute('label', deck.api_name);
	    // debugprint(`#${ deck.api_id }: ${ deck.api_name }`);
	}
	this._data = this._deckbuilder_obj();
	$('deck-data').value = JSON.stringify(this._data);
    },

    init: function() {
	debugprint("init");
	KanColleDatabase.init();
	this._update_init();
    },

    exit: function() {
	this._update_exit();
	KanColleDatabase.exit();
    },
};
KanColleDeckExporter.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleDeckExporter.load(); }, false);
window.addEventListener('unload', function(e) { KanColleDeckExporter.unload(); }, false);
