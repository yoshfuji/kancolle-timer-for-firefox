// vim: set ts=8 sw=4 sts=4 ff=dos :

var KanColleTimerMapInfo = {
    update: {
	memberMapinfo: function() {
	    let l = KanColleDatabase.memberMapinfo.list();
	    let rows = $('mapinfo-grid-rows');
	    if (!KanColleDatabase.masterMapinfo.timestamp() || !rows)
		return;
	    RemoveChildren(rows);
	    for (i = 0; i < l.length; i++) {
		let label_id, label_name, label_progress;
		let d = KanColleDatabase.memberMapinfo.get(l[i]);
		let md;
		let row;

		row = CreateElement('row');

		md = KanColleDatabase.masterMapinfo.get(d.api_id);
		label_id = CreateElement('label');
		label_id.className = 'mapinfo-id';
		label_id.setAttribute('value', md.api_maparea_id + '-' + md.api_no);
		label_name = CreateElement('label');
		label_name.className = 'mapinfo-name';
		label_name.setAttribute('value', md.api_name);
		label_name.setAttribute('tooltiptext', md.api_opetext);
		label_progress = CreateElement('label');
		label_progress.className = 'mapinfo-progress';

		if (d.api_eventmap) {
		    if (d.api_eventmap.api_state == 2) {
			label_progress.setAttribute('value', '済');
			label_progress.setAttribute('tooltiptext', '突破済');
		    } else if (d.api_eventmap.api_max_maphp) {
			if (d.api_eventmap.api_selected_rank) {
			    label_progress.setAttribute('value', (d.api_eventmap.api_now_maphp * 100 / d.api_eventmap.api_max_maphp).toFixed(0));
			    label_progress.setAttribute('tooltiptext', d.api_eventmap.api_now_maphp + ' / ' + d.api_eventmap.api_max_maphp);
			} else {
			    label_progress.setAttribute('value', '未');
			    label_progress.setAttribute('tooltiptext', '未選択');
			}
		    }
		    row.setAttribute('data-selected-rank', '' + (d.api_eventmap.api_selected_rank || 0));
		    row.setAttribute('data-cleared', d.api_eventmap.api_state == 2 ? 'true' : 'false');
		    row.setAttribute('data-gauge-type', '' + (d.api_eventmap.api_gauge_type || 0));
		} else if (d.api_required_defeat_count || md.api_required_defeat_count) {
		    let required_defeat_count = d.api_required_defeat_count || md.api_required_defeat_count;
		    label_progress.setAttribute('value', ((required_defeat_count - d.api_defeat_count) * 100 / required_defeat_count).toFixed(0));
		    label_progress.setAttribute('tooltiptext', (required_defeat_count - d.api_defeat_count) + ' / ' + required_defeat_count);
		}

		row.appendChild(label_id);
		row.appendChild(label_name);
		row.appendChild(label_progress);

		if (!d.api_cleared || d.api_eventmap)
		    rows.appendChild(row);
	    }
	},
    },
};
KanColleTimerMapInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerMapInfo.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerMapInfo.unload(); }, false);
