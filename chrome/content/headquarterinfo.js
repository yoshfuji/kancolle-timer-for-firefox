// vim: set ts=8 sw=4 sts=4 ff=dos :

/*
 * 艦娘/装備数
 */
var KanColleTimerHeadQuarterInfo = {
    lonely_ships: [],
    repair_timer: {
	base: Number.NaN,
	_timer: null,
    },
    general_timer: {
	_timer: null,
    },

    update: {
	ship: function() {
	    let remodel_map = {};
	    let ship_bytype = KanColleDatabase.ship.list().map(function(id){
		let ship = KanColleDatabase.ship.get(id);
		let typeid = ship.api_ship_id;
		let name = FindShipNameByCatId(typeid);
		let locked = ship.api_locked;
		if (remodel_map[typeid]) {
		    typeid = remodel_map[typeid];
		} else {
		    let remodel_type = {};	// loop detection
		    let highest_id = -1;
		    while (true) {
			let shiptype = KanColleDatabase.masterShip.get(typeid);
			let aftertypeid = shiptype ? parseInt(shiptype.api_aftershipid, 10) : Number.NaN;

			if (highest_id < typeid)
			    highest_id = typeid;
			remodel_type[typeid] = true;

			if (isNaN(aftertypeid) || !aftertypeid)
			    break;
			if (remodel_type[aftertypeid]) {
			    /* Loop.  Use highest id. */
			    debugprint('Loop detected: ' + name + ' [' + Object.keys(remodel_type).map(function(_id) { return FindShipNameByCatId(_id); }) + '] => ' + FindShipNameByCatId(highest_id));
			    typeid = highest_id;
			    break;
			}
			typeid = aftertypeid;
		    }
		    for (let id in remodel_type)
			remodel_map[id] = typeid;
		}
		return { id: id, name: name, lv: ship.api_lv, type: typeid, locked: locked, };
	    }).reduce(function(p,c) {
		if (!p[c.type])
		    p[c.type] = [];
		p[c.type].push(c);
		return p;
	    }, {});
	    this.lonely_ships = Object.keys(ship_bytype).filter(function(e) {
		ship_bytype[e].sort(function(a,b) {
		    return b.lv - a.lv;
		});
		return !ship_bytype[e][0].locked;
	    }).map(function(e) {
		return ship_bytype[e];
	    });
	    this.update.record.call(this);
	},
	record: function() {
	    let d;
	    let maxships;
	    let maxslotitems;
	    let ships;
	    let slotitems;
	    let shipnumfree = KanColleUtils.getIntPref('display.ship-num-free');
	    let ship_color = null;
	    let slotitem_color = null;
	    let text;

	    function convnan(t) {
		return isNaN(t) ? '-' : t;
	    }

	    function numcolor(cur,mark,max) {
		let col = null;
		if (!isNaN(cur)) {
		    if (!isNaN(max) && cur >= max)
			col = 'red';
		    else if (!isNaN(mark) && cur >= mark)
			col = 'orange';
		    else
			col = 'black';
		}
		return col;
	    }

	    d = KanColleDatabase.record.get();

	    ships = convnan(d.api_ship[0]);
	    maxships = convnan(d.api_ship[1]);
	    ship_color = numcolor(ships, maxships - shipnumfree, maxships);

	    slotitems = convnan(d.api_slotitem[0]);
	    maxslotitems = convnan(d.api_slotitem[1]);
	    slotitem_color = numcolor(slotitems, maxslotitems - shipnumfree * 4, maxslotitems);

	    // 母港100、装備枠500から、母港拡張10ごとに装備枠40増加
	    //   装備枠 = 500 + (母港-100) * 4
	    //
	    // APIでから渡される装備数上限は、それ以上の数では新艦船/
	    // 新装備開発ができなくなる、という実際の制限値であり、
	    // 戦績表示の「最大保有可能装備アイテム数」から3減じた
	    // ものとなっている。
	    //
	    // このため、建造やドロップでの艦船取得によって装備数が
	    // 最大4つ増え、装備数がAPIによる最大装備数を上回ることは
	    // ある。
	    //
	    //if (!isNaN(maxslotitems) && !isNaN(maxships) &&
	    //	maxslotitems < 100 + maxships * 4) {
	    //	maxslotitems = 100 + maxships * 4;
	    //}
	    $('basic-information-shipcount').value = ships;
	    SetStyleProperty($('basic-information-shipcount'), 'color', ship_color);
	    text = ships + ' / ' + maxships;
	    if (this.lonely_ships.length) {
		text += '\n未ロック艦: ' +
			this.lonely_ships.map(function(e) {
			    return e[0].name + ' Lv' + e[0].lv;
			}).join(', ');
		SetStyleProperty($('basic-information-shipcount'), 'outline', 'red solid 2px');
	    } else {
		SetStyleProperty($('basic-information-shipcount'), 'outline', 'rgba(0,0,0,0) solid 2px');
	    }
	    $('basic-information-shipcount').setAttribute('tooltiptext', text);

	    $('basic-information-slotitemcount').value = slotitems;
	    SetStyleProperty($('basic-information-slotitemcount'), 'color', slotitem_color);
	    $('basic-information-slotitemcount').setAttribute('tooltiptext', slotitems + ' / ' + maxslotitems);
	},

	material: function() {
	    let burner = '-';
	    let bucket = '-';
	    let screw = '-';
	    if (KanColleDatabase.material.timestamp()) {
		let d;
		/*
		 * 1: 燃料
		 * 2: 弾薬
		 * 3: 鋼材
		 * 4: ボーキサイト
		 * 5: 高速建造材
		 * 6: 高速修復材
		 * 7: 開発資材
		 * 8: 改修資材
		 */
		burner = KanColleDatabase.material.get('burner');
		bucket = KanColleDatabase.material.get('bucket');
		screw = KanColleDatabase.material.get('screw');
	    }

	    $('basic-information-burnercount').value = burner;
	    $('basic-information-bucketcount').value = bucket;
	    $('basic-information-screwcount').value = screw;
	},

	useitem: function() {
	    let d = KanColleDatabase.useitem.list();
	    if (!d)
		return;

	    let txt = d.map(function(e) {
		let d = KanColleDatabase.useitem.get(e);
		return d ? `${ d.api_name }: ${ d.api_count }` : 'unknown';
	    }).join('\n');

	    $('basic-information-screwcount').setAttribute('tooltiptext', txt);
	},

	port: function() {
	    // 泊地修理タイマー
	    let data = KanColleDatabase.port.get();
	    this.repair_timer.base = data.repair_timer;
	},
    },

    show: function() {
	let nodes, checked;

	checked = $('basicinfo-toggle').getAttribute('checked') == 'true';
	$('basicinfo').collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true';
	nodes = document.getElementsByClassName("porttimer");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('gentimer-toggle').getAttribute('checked') == 'true';
	nodes = document.getElementsByClassName("gentimer");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true' ||
		  $('gentimer-toggle').getAttribute('checked') == 'true' ||
		  $('deckinfo-toggle').getAttribute('checked') == 'true' ||
		  $('ndockinfo-toggle').getAttribute('checked') == 'true' ||
		  $('kdockinfo-toggle').getAttribute('checked') == 'true';
	$('timer').collapsed = !checked;
    },

    restore: function() {
	let general_timer = KanColleUtils.getUnicharPref('timer.general-timer') || null;
	if (!isNaN(general_timer)) {
	    this.start_general_timer(general_timer);
	}
	this.show();
    },

    _update_general_timer: function() {
	let str = "時間になりました。";
	let sound_conf = KanColleUtils.getUnicharPrefFileURI('sound.default');
	if (sound_conf)
	    KanColleTimerUtils.sound.play(sound_conf);
	if( KanColleUtils.getBoolPref('popup.general-timer') ){
	    KanColleTimerUtils.alert.show(KanColleUtils.IconURL,
					  KanColleUtils.Title,
					  str, false, str, null);
	}
	if( KanColleUtils.getBoolPref('ntfy.general-timer') ){
	    let ntfy_prio = 3;
	    let ntfy_tags = ['anchor','alarm_clock'];
	    let ntfy_uri = KanColleUtils.getUnicharPref('ntfy.uri', '');
	    let ntfy_topic = KanColleUtils.getUnicharPref('ntfy.topic', '');
	    let obj = {
	        'topic': ntfy_topic,
		'message': str,
		'title': KanColleUtils.Title,
		'priority': 3,
		'tags': ntfy_tags,
	    };
	    debugprint(`ntfy: url=${ ntfy_uri }, obj=${ JSON.stringify(obj) }`);
	    if (ntfy_uri != '' && ntfy_topic != '') {
	        let xhr = KanColleTimerUtils.net.createXMLHttpRequest();
		KanColleUtils.sendObject(xhr, 'POST', ntfy_uri, obj);
	    }
	}
    },
    start_general_timer: function(time) {
	this.stop_general_timer();
	if (time) {
	    this._start_general_timer(time);
	    this._alarm_set(1, time);
	    KanColleUtils.setUnicharPref('timer.general-timer', '' + time);
	}
    },
    start_general_timer_by_delay: function(delay) {
	let _delay = parseInt(delay, 10);
	if (isNaN(_delay))
	    _delay = 0;
	if (_delay)
	    this.start_general_timer((new Date).getTime() + _delay * 1000);
	else
	    this.stop_general_timer();
    },
    stop_general_timer: function() {
	this._stop_general_timer();
	this._alarm_set(1, Number.NaN);
	KanColleUtils.setUnicharPref('timer.general-timer', '');
    },
    _start_general_timer: function(time) {
	this.general_timer._timer = KanColleTimerUtils.timer.startDelayedEventAt(this, time, { type: 'general_timer' });
    },
    _stop_general_timer: function() {
	KanColleTimerUtils.timer.stop(this.general_timer._timer);
	this.general_timer._timer = null;
    },

    _update_repair_timer: function() {
	if (isNaN(this.repair_timer.base)) {
	    $('port-timer').value = '-';
	} else {
	    let now = (new Date).getTime();
	    let diff = (now - this.repair_timer.base) / 1000;
	    let sec, min, hour;
	    let t;
	    sec = Math.floor(diff) % 60;
	    min = Math.floor(diff / 60) % 60;
	    hour = Math.floor(diff / 3600);
	    t = (hour < 10 ? '0' + hour : hour) + ':' +
		(min < 10 ? '0' + min : min) + ':' +
		(sec < 10 ? '0' + sec : sec);
	    $('port-timer').value = t;
	}
    },

    _start_repair_timer: function() {
	this.repair_timer._timer = KanColleTimerUtils.timer.startRepeatingEvent(this, 1000, { type: 'repair_timer' });
    },
    _stop_repair_timer: function() {
	KanColleTimerUtils.timer.stop(this.repair_timer._timer);
	this.repair_timer._timer = null;
    },

    _set_alarm_time: function(id, timeout) {
	let node = $('general-timer' + id + '');
	this._set_alarm_time_node(node, timeout);
    },
    _update_alarm_time: function(id, diff) {
	let node = $('general-timer' + id + '');
	let rnode = $('general-timer' + id + '-left');
	this._update_alarm_time_node(rnode, diff);
	this._update_alarm_style_node(node, diff);
	this._update_alarm_style_node(rnode, diff);
    },

    _set_alarm_format: function() {
	const classname = 'gentimer-time';
	let b = KanColleUtils.getBoolPref('display.short');
	let elems = document.getElementsByClassName(classname);
	for(let i = 0; i < elems.length; i++){
	    elems[i].style.display = b ? 'none' : 'block';
	}
    },

    observe: function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    if (!data)
		break;
	    switch (data.type) {
	    case 'repair_timer':
		this._update_repair_timer();
		break;
	    case 'general_timer':
		this._update_general_timer(data.extradata);
		break;
	    case 'alarm':
		this._alarm_update();
		break;
	    default:;
	    }
	    break;
	case 'nsPref:changed':
	    this.update.record.call(this);
	    this._set_alarm_format();
	    break;
	default:;
	}
    },

    _utils_callback: function(data) {
	debugprint('file: ' + JSON.stringify(data));
	let writer = data.write_disk ? 2 : (data.write_cache ? 1 : 0);
	let reader = data.read ? 1 : 0;
	let diskio = $('diskio');
	let text = '';
	let color = 'blue';
	if (writer > 0 || reader > 0) {
	    text = '●';
	    if (writer > 1) {
		color = 'red';
	    } else if (writer > 0) {
		color = 'orange';
	    } else if (reader > 0) {
		color = 'black';
	    }
	} else {
	    text = '○';
	    color = 'grey';
	}
	diskio.setAttribute('value', text);
	SetStyleProperty(diskio, 'color', color);
    },

    init: function() {
	KanColleTimerUtils.file.appendCallback(this._utils_callback);
	KanColleDatabase.init();
	this._update_init();
	this._prefs_init();
	this._alarm_init();
	this._start_repair_timer();
    },
    exit: function() {
	this._stop_general_timer()
	this._stop_repair_timer();
	this._alarm_exit();
	this._prefs_exit();
	this._update_exit();
	KanColleDatabase.exit();
	KanColleTimerUtils.file.removeCallback(this._utils_callback);
    },
};
KanColleTimerHeadQuarterInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerHeadQuarterInfo.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerHeadQuarterInfo.unload(); }, false);
