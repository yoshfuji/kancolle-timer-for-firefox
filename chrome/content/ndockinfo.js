// vim: set ts=8 sw=4 sts=4 ff=dos :

/*
 * 入渠ドック
 *  member/ndock	: api_data
 */
var KanColleTimerNdockInfo = {
    update: {
	ndock: function(extradata) {
	    let docks = extradata.id ? [ extradata.id ] : KanColleDatabase.ndock.list();

	    // 入渠ドック
	    for (let k of docks) {
		let d = KanColleDatabase.ndock.get(k);
		if( d.api_state > 0 ){
		    let ship_id = d.api_ship_id;
		    let name = FindShipName( ship_id );
		    $("ndock-name"+k).value = name;
		} else {
		    $("ndock-name" + k).value = "";
		}
	    }

	    if (extradata) {
		let state = extradata.state || 0;
		let id = extradata.id || 0;
		debugprint(extradata.toSource());
		if (state > 1)
		    AddLog(extradata.message + '\n');
		if (id > 0)
		    this._alarm_set(id, extradata.time);
	    }
	},
	memberBasic: function() {
	    let d = KanColleDatabase.memberBasic.get();
	    let ndocks;
	    if (!d)
		return;
	    ndocks  = document.getElementsByClassName("ndock-box");
	    for( let i = 0; i < 4; i++ )
		SetStyleProperty(ndocks[i], 'display', i < d.api_count_ndock ? "":"none");
	},
	material: function() {
	    let d = KanColleDatabase.material.get('bucket');
	    if (d >= 0)
		$('repairkit-number').value = d;
	},
    },

    _set_alarm_time: function(id, timeout) {
	let node = $('ndock' + id);
	this._set_alarm_time_node(node, timeout);
    },
    _update_alarm_time: function(id, diff) {
	let node = $('ndock' + id);
	let rnode = $('ndockremain' + id);
	this._update_alarm_time_node(rnode, diff);
	this._update_alarm_style_node(node, diff);
	this._update_alarm_style_node(rnode, diff);
    },

    _set_alarm_format: function() {
	const classname = 'ndock-time';
	let b = KanColleUtils.getBoolPref('display.short');
	let elems = document.getElementsByClassName(classname);
	for(let i = 0; i < elems.length; i++){
	    elems[i].style.display = b ? 'none' : 'block';
	}
    },

    observe: function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm_update();
	    break;
	case 'nsPref:changed':
	    this._set_alarm_format();
	    break;
	default:;
	}
    },

    // 入渠ドックのメモ作成
    createRepairMemo: function(){
	let elem = $('popup-ndock-memo').triggerNode;
	let hbox = FindParentElement(elem,"row");
	let oldstr = hbox.getAttribute('tooltiptext') || "";
	let text = "入渠ドック"+hbox.firstChild.value+"のメモを入力してください。\nツールチップとして表示されるようになります。";
	let str = InputPrompt(text,"入渠ドックメモ", oldstr);
	if( str==null ) return;
	hbox.setAttribute('tooltiptext',str);

	let ndock_hbox = evaluateXPath(document,"//*[@class='ndock-box']");
	for(let k in ndock_hbox){
	    k = parseInt(k, 10);
	    let elem = ndock_hbox[k];
	    KanColleRemainInfo.ndock_memo[k] = ndock_hbox[k].getAttribute('tooltiptext');
	}
    },

    show: function() {
	let checked = $('ndockinfo-toggle').getAttribute('checked') == 'true';
	let nodes = document.getElementsByClassName("ndock-box");
	for (let i = 0; nodes[i]; i++)
	    nodes[i].collapsed = !checked;

	checked = $('porttimer-toggle').getAttribute('checked') == 'true' ||
		  $('gentimer-toggle').getAttribute('checked') == 'true' ||
		  $('deckinfo-toggle').getAttribute('checked') == 'true' ||
		  $('ndockinfo-toggle').getAttribute('checked') == 'true' ||
		  $('kdockinfo-toggle').getAttribute('checked') == 'true';
	$('timer').collapsed = !checked;
    },

    restore: function() {
	this.update.memberBasic.call(this);
	try{
	    for( let i=0; i < 4; i++ ){
		let k = i + 1;
		if( KanColleRemainInfo.ndock_memo[i] ){
		    $('ndock-box'+k).setAttribute('tooltiptext',
						  KanColleRemainInfo.ndock_memo[i] );
		}
	    }
	} catch(x) {
	}
	this.show();
    },
};
KanColleTimerNdockInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { KanColleTimerNdockInfo.load(); }, false);
window.addEventListener('unload', function(e) { KanColleTimerNdockInfo.unload(); }, false);
