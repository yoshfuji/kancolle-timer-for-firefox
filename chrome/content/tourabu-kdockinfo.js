// vim: set ts=8 sw=4 sts=4 ff=dos :

var TouRabuTimerKdockInfo = {
    update: {
	tourabuForge: function(extradata) {
	    let offset = KanColleDatabase.tourabuForge.timestamp() - KanColleDatabase.tourabuForge.now();
	    let list = KanColleDatabase.tourabuForge.list();
	    for (let i of list) {
		let slot = KanColleDatabase.tourabuForge.get(i);
		if (true) {
		    let finish = slot._finished_at;
		    if (!isNaN(finish)) {
			let goal = finish + offset;
			$('tourabu-kdock' + i + '-name').value = '?';   // slot.sword_id;
		    } else {
			$('tourabu-kdock' + i + '-name').value = '';
		    }
		}
	    }
	    if (extradata) {
		let state = extradata.state || 0;
		let id = extradata.id || 0;
		debugprint(extradata.toSource());
		if (state > 1)
		    AddLog(extradata.message + '\n');
		if (id > 0)
		    this._alarm_set(id, extradata.time);
	    }
	},
	tourabuLoginStart: function() {
	    this.restore();
	},
    },

    _set_alarm_time: function(id, timeout) {
	let node = $('tourabu-kdock' + id + '-time');
	this._set_alarm_time_node(node, timeout);
    },
    _update_alarm_time: function(id, diff) {
	let node = $('tourabu-kdock' + id + '-time');
	let rnode = $('tourabu-kdock' + id + '-remain');
	this._update_alarm_time_node(rnode, diff);
	this._update_alarm_style_node(node, diff);
	this._update_alarm_style_node(rnode, diff);
    },

    observe: function(subject, topic, data) {
	switch (topic) {
	case 'timer-callback':
	    this._alarm_update();
	    break;
	default:;
	}
    },

    restore: function() {
	let t = KanColleDatabase.tourabuLoginStart.timestamp();
	let d = KanColleDatabase.tourabuLoginStart.get();
	for (let i = 1; i <= 4; i++) {
	    if (t && i <= d.forge_slot)
		$('tourabu-kdock' + i).collapsed = false;
	    else
		$('tourabu-kdock' + i).collapsed = true;
	}
    },
};
TouRabuTimerKdockInfo.__proto__ = __KanColleTimerPanel;

window.addEventListener('load', function(e) { TouRabuTimerKdockInfo.load(); }, false);
window.addEventListener('unload', function(e) { TouRabuTimerKdockInfo.unload(); }, false);
