pref("extensions.kancolletimer.test",false);

pref("extensions.kancolletimer.time-offset", 0);
pref("extensions.kancolletimer.write-delay", 3000);
pref("extensions.kancolletimer.record.internalstate", true);

pref("extensions.kancolletimer.display.short",false);
pref("extensions.kancolletimer.display.font-color","");
pref("extensions.kancolletimer.display.font","");
pref("extensions.kancolletimer.display.font-size","");
pref("extensions.kancolletimer.display.ship-num-free",5);

pref("extensions.kancolletimer.default-action.toolbar", 0);

pref("extensions.kancolletimer.record.resource-history",true);

pref("extensions.kancolletimer.screenshot.path","");
pref("extensions.kancolletimer.screenshot.jpeg",false);
pref("extensions.kancolletimer.screenshot.mask-name",false);

pref("extensions.kancolletimer.wallpaper","");
pref("extensions.kancolletimer.wallpaper.alpha",100);

pref("extensions.kancolletimer.sound.api",0);

pref("extensions.kancolletimer.sound.default","");
pref("extensions.kancolletimer.sound.damage-warning","");

pref("extensions.kancolletimer.sound.mission","");
pref("extensions.kancolletimer.sound.ndock","");
pref("extensions.kancolletimer.sound.kdock","");

pref("extensions.kancolletimer.sound.1min.mission","");
pref("extensions.kancolletimer.sound.1min.ndock","");
pref("extensions.kancolletimer.sound.1min.kdock","");

pref("extensions.kancolletimer.popup.general-timer",false);
pref("extensions.kancolletimer.popup.mission",false);
pref("extensions.kancolletimer.popup.mission-1min",false);
pref("extensions.kancolletimer.popup.ndock",false);
pref("extensions.kancolletimer.popup.ndock-1min",false);
pref("extensions.kancolletimer.popup.kdock",false);
pref("extensions.kancolletimer.popup.kdock-1min",false);
pref("extensions.kancolletimer.popup.damage-warning",false);

pref("extensions.kancolletimer.ntfy.general-timer",false);
pref("extensions.kancolletimer.ntfy.mission",false);
pref("extensions.kancolletimer.ntfy.mission-1min",false);
pref("extensions.kancolletimer.ntfy.ndock",false);
pref("extensions.kancolletimer.ntfy.ndock-1min",false);
pref("extensions.kancolletimer.ntfy.kdock",false);
pref("extensions.kancolletimer.ntfy.kdock-1min",false);
pref("extensions.kancolletimer.ntfy.damage-warning",false);
pref("extensions.kancolletimer.ntfy.uri","");
pref("extensions.kancolletimer.ntfy.topic","");

pref("extensions.kancolletimer.twitter.screen_name","");
